/*
 * [The "BSD license"]
 *  Copyright (c) 2014 Terence Parr
 *  Copyright (c) 2014 Sam Harwell
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. The name of the author may not be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 *  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * A Java 8 grammar for ANTLR 4 derived from the Java Language Specification
 * chapter 19.
 *
 * NOTE: This grammar results in a generated antlrparser that is much slower
 *       than the Java 7 grammar in the grammars-v4/java directory. This
 *     one is, however, extremely close to the spec.
 *
 * You can test with
 *
 *  $ antlr4 Java8.g4
 *  $ javac *.java
 *  $ grun Java8 compilationUnit *.java
 *
 * Or,
~/antlr/code/grammars-v4/java8 $ java Test .
/Users/parrt/antlr/code/grammars-v4/java8/./Java8BaseListener.java
/Users/parrt/antlr/code/grammars-v4/java8/./Java8Lexer.java
/Users/parrt/antlr/code/grammars-v4/java8/./Java8Listener.java
/Users/parrt/antlr/code/grammars-v4/java8/./Java8Parser.java
/Users/parrt/antlr/code/grammars-v4/java8/./Test.java
Total lexer+antlrparser time 30844ms.
 */

/*
 * Copyright (c) 2016 Daniel Wyrytowski
 */

/**
 * La gramática de Hydra 2 modifica y extiende la gramática de Java8 propuesta por Terrence Parr y Sam Harwell
 * Las producciones propias de Hydra se especifican arriba, luego del comentario "Hydra2 producations" y cualquier
 * modificación sobre las producciones propias de Java8 deberían ser comentadas con el formato:
 *      // Hydra2 token | Hydra2 production | ...
 */
grammar Hydra;

/*
 * IMPORTANT NOTICES:
 * Most of the productions in this Grammar file come from the java8 grammar as is, written for
 * ANTLR Java parsing. This means there are many overly declarative productions, maybe some
 * that aren't even used. Those I will comment as '// NOT VISITED'
 *
 * I added just a few productions for the Hydra Project, which are mostly enclosed in the
 * "Hydra2 productions" comments, or merged among the original and side commented as well, to not
 * loose track of which is java's and which hydra's.
 *
 * I also modified some of the original productions to add gradual typing for the project, you'll see
 * those productions commented as well.
 *
 * Besides I made some subtle changes to the predefined java grammar to simplify the code in the visitor
 * methods.
 * For example, the following production would force me to make succesive null checks
 * in one method to see wheter the current node contains class or interface children.
 *
 *    exampleProduction: ( ClassType_lfno | Interface_lfno ) (ClassType_lf | Interface_lf )* ;
 *
 * but with the following production separation I get different visitors for each one, which
 * results in shorter and more readable code, without making dangerous changes to the grammar.
 *
 *    exampleProduction:  exampleProduction_lfno   exampleProduction_lf* ;
 *    exampleProduction_lfno : ClassType_lfno | Interface_lfno ;
 *    exampleProduction_lf : ClassType_lf | Interface_lf ;
 *
 * Another real example is the 'dims' production which was originally defined as :
 *
 *    dims: annotation* '[' ']' ( annotation '[' ']' )*
 *
 * and I changed to :
 *
 *    dims: annotatedDim+
 *    annotatedDim: annotation* '[' ']'
 *
 * so I could correctly associate each bracket with it's attached annotations.
 *
 */

/*
 * Hydra2 productions
 */

hydraScript
    :   importDeclaration*
        (typeDeclaration|scriptStatement|functionDeclaration)*
    ;

scriptStatement
    :   globalVariableDeclarationStatement
    |   scriptBlock
    |   scriptBlockStatement
    |   processStatement
    ;

processStatement
    :   'process' processHeader? scriptBlock
    ;

processHeader
    :   '(' inferredFormalParameterList ')'
    |   processName '(' inferredFormalParameterList ')' ':'
    ;

processName
    :   Identifier
    ;

threadStatement
    :   'thread' threadHeader? statement
    ;

threadHeader
    :   '(' inferredFormalParameterList ')'
    |   threadName '(' inferredFormalParameterList ')' ':'
    ;

threadName
    :   Identifier
    ;

globalVariableDeclarationStatement          // DONE
	:	globalVariableDeclaration ';'
	;

globalVariableDeclaration                   // DONE
	:	GLOBAL? variableModifier* unannType? variableDeclaratorList // Hydra2 gradual typing
	;

functionDeclaration                         // DONE
	:	functionModifier? functionHeader functionBody
	;

functionModifier
    :  'synchronized' | 'atomic'
    ;

functionHeader                              // DONE
	:	result? functionDeclarator throws_?
	|	typeParameters  annotation* result? functionDeclarator throws_?
	;

functionDeclarator                          // DONE
	:	functionName '(' formalParameterList? ')' dims?
	;

functionName                                // DONE
    :     Identifier
    ;

functionBody                                // DONE
    :	scriptBlock
    ;

functionInvocationStatement
	:	functionInvocation ';'
	;

functionInvocation                          // DONE
	:	functionName '(' argumentList? ')'
	;

// scriptBlocks differ from regular java blocks in that funcionDeclarations can be made
scriptBlock
	:	'{' (functionDeclaration | scriptBlockStatement)* '}'
	;

scriptBlockStatement
	:	localVariableDeclarationStatement
	|   scriptBlock
	|	classDeclaration
	|	statement
	;

/*
 * Java8 Productions
 */

/*
 * Productions from §3 (Lexical Structure)
 */

literal // .. DONE
	:	IntegerLiteral          # LiteralInteger
	|	FloatingPointLiteral    # LiteralFloatingPoint
	|	BooleanLiteral          # LiteralBoolean
	|	CharacterLiteral        # LiteralCharacter
	|	StringLiteral           # LiteralString
	|	NullLiteral             # LiteralNull
	;

/*
 * Productions from §4 (Types, Values, and Variables)
 */

type                // DONE
	:	primitiveType
	|	referenceType
	;

primitiveType       // DONE
	:	annotation* numericType # primitiveTypeNumeric
	|	annotation* 'boolean'   # primitiveTypeBoolean
	;

numericType         // DONE
	:	integralType
	|	floatingPointType
	;

integralType        // DONE
	:	'byte'      # byteIntegralType
	|	'short'     # shortIntegralType
	|	'int'       # intIntegralType
	|	'long'      # longIntegralType
	|	'char'      # charIntegralType
	;

floatingPointType   // DONE
	:	'float'     # floatFloatingPointType
	|	'double'    # doubleFloatingPointType
	;

referenceType       // DONE
	:	classOrInterfaceType
	|	typeVariable
	|	arrayType
	;

classOrInterfaceType        // DONE
	:	classOrInterfaceType_lfno
		classOrInterfaceType_lf*
	;

classOrInterfaceType_lfno   // DONE
    :   classType_lfno_classOrInterfaceType
    |	interfaceType_lfno_classOrInterfaceType
    ;

classOrInterfaceType_lf     // DONE
    :   classType_lf_classOrInterfaceType
    |	interfaceType_lf_classOrInterfaceType
	;

classType           // DONE
	:	annotation* Identifier typeArguments?                               # classTypeBase
	|	classOrInterfaceType '.' annotation* Identifier typeArguments?      # classTypeWithScope
	;

classType_lf_classOrInterfaceType   // DONE
	:	'.' annotation* Identifier typeArguments?
	;

classType_lfno_classOrInterfaceType // DONE
	:	annotation* Identifier typeArguments?
	;

interfaceType       // DONE
	:	classType
	;

interfaceType_lf_classOrInterfaceType       // DONE
	:	classType_lf_classOrInterfaceType
	;

interfaceType_lfno_classOrInterfaceType     // DONE
	:	classType_lfno_classOrInterfaceType
	;

typeVariable        // DONE
	:	annotation* Identifier
	;

arrayType           // DONE
	:	primitiveType dims          # arrayTypePrimitiveType
	|	classOrInterfaceType dims   # arrayTypeClassOrInterfaceType
	|	typeVariable dims           # arrayTypeTypeVariable
	;

dims                // DONE
	:	annotatedDim+     // separating each bracket pair with its annotations makes it easier to work it as a list
//	:	annotation* '[' ']' (annotation* '[' ']')*
	;

annotatedDim        // DONE
    :   annotation* '[' ']'
    ;

typeParameter           // DONE
	:	typeParameterModifier* Identifier typeBound?
	;

typeParameterModifier   // DONE
	:	annotation
	;

typeBound           // DONE
	:	'extends' typeVariable                              # typeBoundTypeVariable
	|	'extends' classOrInterfaceType additionalBound*     # typeBoundClassOrInterfaceType
	;

additionalBound     // DONE
	:	'&' interfaceType
	;

typeArguments           // DONE
	:	'<' typeArgumentList '>'
	;

typeArgumentList        // DONE
	:	typeArgument (',' typeArgument)*
	;

typeArgument            // DONE
	:	referenceType
	|	wildcard
	;

wildcard                // DONE
	:	annotation* '?' wildcardBounds?
	;

wildcardBounds          // DONE
	:	wildcardBoundsExtends
	|	wildcardBoundsSuper
	;

wildcardBoundsExtends   // DONE
    : 'extends' referenceType
    ;

wildcardBoundsSuper     // DONE
    : 'super' referenceType
    ;

/*
 * Productions from §6 (Names)
 */

packageName
	:	Identifier
	|	packageName '.' Identifier
	;

typeName
	:	Identifier
	|	packageOrTypeName '.' Identifier
	;

packageOrTypeName
	:	Identifier
	|	packageOrTypeName '.' Identifier
	;

expressionName                      // DONE
	:	Identifier
	|	ambiguousName '.' Identifier
	;

methodName
	:	Identifier
	;

ambiguousName                       // DONE
	:	Identifier
	|	ambiguousName '.' Identifier
	;

/*
 * Productions from §7 (Packages)
 */

compilationUnit
	:	packageDeclaration? importDeclaration* typeDeclaration* EOF
	;

packageDeclaration
	:	packageModifier* 'package' Identifier ('.' Identifier)* ';'
	;

packageModifier
	:	annotation
	;

importDeclaration
	:	singleTypeImportDeclaration
	|	typeImportOnDemandDeclaration
	|	singleStaticImportDeclaration
	|	staticImportOnDemandDeclaration
	;

singleTypeImportDeclaration
	:	'import' typeName ';'
	;

typeImportOnDemandDeclaration
	:	'import' packageOrTypeName '.' '*' ';'
	;

singleStaticImportDeclaration
	:	'import' 'static' typeName '.' Identifier ';'
	;

staticImportOnDemandDeclaration
	:	'import' 'static' typeName '.' '*' ';'
	;

typeDeclaration
	:	classDeclaration
	|	interfaceDeclaration
	|	';'
	;

/*
 * Productions from §8 (Classes)
 */

classDeclaration
	:	normalClassDeclaration
	|	enumDeclaration
	;

normalClassDeclaration
	:	classModifier* 'class' Identifier typeParameters? superclass? superinterfaces? classBody
	;

classModifier
	:	annotation
	|	'public'
	|	'protected'
	|	'private'
	|	'abstract'
	|	'static'
	|	'final'
	|	'strictfp'
	;

typeParameters
	:	'<' typeParameterList '>'
	;

typeParameterList
	:	typeParameter (',' typeParameter)*
	;

superclass
	:	'extends' classType
	;

superinterfaces
	:	'implements' interfaceTypeList
	;

interfaceTypeList
	:	interfaceType (',' interfaceType)*
	;

classBody
	:	'{' classBodyDeclaration* '}'
	;

classBodyDeclaration
	:	classMemberDeclaration
	|	instanceInitializer
	|	staticInitializer
	|	constructorDeclaration
	;

classMemberDeclaration
	:	fieldDeclaration
	|	methodDeclaration
	|	classDeclaration
	|	interfaceDeclaration
	|	';'
	;

fieldDeclaration
	:	fieldModifier* unannType? variableDeclaratorList ';' // Hydra2 gradual typing
	;

fieldModifier
	:	annotation
	|	'public'
	|	'protected'
	|	'private'
	|	'static'
	|	'final'
	|	'transient'
	|	'volatile'
	;

variableDeclaratorList          // DONE
	:	variableDeclarator (',' variableDeclarator)*
	;

variableDeclarator              // DONE
	:	variableDeclaratorId ('=' variableInitializer)?
	;

variableDeclaratorId            // DONE
	:	Identifier dims?
	;

variableInitializer             // DONE
	:	expression
	|	arrayInitializer
	;

unannType               // DONE
	:	unannPrimitiveType
	|	unannReferenceType
	;

unannPrimitiveType      // DONE
	:	numericType     # numericUnannPrimitiveType
	|	'boolean'       # booleanUnannPrimitiveType
	;

unannReferenceType      // DONE
	:	unannClassOrInterfaceType
	|	unannTypeVariable
	|	unannArrayType
	;

unannClassOrInterfaceType       // DONE
	:	unannClassOrInterfaceType_lfno
		unannClassOrInterfaceType_lf*
	;

unannClassOrInterfaceType_lfno      // DONE
    :   unannClassType_lfno_unannClassOrInterfaceType
    |	unannInterfaceType_lfno_unannClassOrInterfaceType
	;

unannClassOrInterfaceType_lf       // DONE
    :   unannClassType_lf_unannClassOrInterfaceType
    |   unannInterfaceType_lf_unannClassOrInterfaceType
	;

unannClassType                      // DONE
	:	Identifier typeArguments?                                               # unannClassTypeBase
	|	unannClassOrInterfaceType '.' annotation* Identifier typeArguments?     # unannClassTypeWithScope
	;

unannClassType_lf_unannClassOrInterfaceType     // DONE
	:	'.' annotation* Identifier typeArguments?
	;

unannClassType_lfno_unannClassOrInterfaceType   // DONE
	:	Identifier typeArguments?
	;

unannInterfaceType                              // DONE
	:	unannClassType
	;

unannInterfaceType_lf_unannClassOrInterfaceType     // DONE
	:	unannClassType_lf_unannClassOrInterfaceType
	;

unannInterfaceType_lfno_unannClassOrInterfaceType   // DONE
	:	unannClassType_lfno_unannClassOrInterfaceType
	;

unannTypeVariable       // DONE
	:	Identifier
	;

unannArrayType          // DONE
	:	unannPrimitiveType dims             # unannArrayTypePrimitiveType
	|	unannClassOrInterfaceType dims      # unannArrayTypeClassOrInterfaceType
	|	unannTypeVariable dims              # unannArrayTypeTypeVariable
	;

methodDeclaration
	:	methodModifier* methodHeader methodBody
	;

methodModifier
	:	annotation
	|	'public'
	|	'protected'
	|	'private'
	|	'abstract'
	|	'static'
	|	'final'
	|	'synchronized'
	|	'native'
	|	'strictfp'
	;

methodHeader
	:	result methodDeclarator throws_?
	|	typeParameters annotation* result methodDeclarator throws_?
	;

result                  // DONE
	:	unannType   # resultUnannType
	|	'void'      # resultVoid
	;

methodDeclarator
	:	Identifier '(' formalParameterList? ')' dims?
	;

formalParameterList     // DONE
	:	formalParameters ',' lastFormalParameter
	|	lastFormalParameter
	;

formalParameters        // DONE
	:	formalParameter (',' formalParameter)*      # formalParametersFormalParameter
	|	receiverParameter (',' formalParameter)*    # formalParametersReceiverParameter     // SKIPPED
	;

formalParameter         // DONE
	:	variableModifier* unannType? variableDeclaratorId // Hydra2 gradual typing
	;

variableModifier        // DONE
	:	annotation
	|	'final'
	;


// Because of Hydra's gradual typing, grammar here gets a little abiguous regarding variable arguments' annotations:
//  If no type is specified, all annotations will be parsed as variable modifiers. Only annotations that come after
//  the type will be considered variable arguments' annotations.
lastFormalParameter     // DONE
	:	variableModifier* unannType? annotation* '...' variableDeclaratorId # lastFormalParameterVarArgs // Hydra2 gradual typing
	|	formalParameter                                                     # lastFormalParameterFormalParameter
	;

receiverParameter
	:	annotation* unannType? (Identifier '.')? 'this' // Hydra2 gradual typing
	;

throws_     // DONE
	:	'throws' exceptionTypeList
	;

exceptionTypeList       // DONE
	:	exceptionType (',' exceptionType)*
	;

exceptionType           // DONE
	:	classType
	|	typeVariable
	;

methodBody
	:	block
	|	';'
	;

instanceInitializer
	:	block
	;

staticInitializer
	:	'static' block
	;

constructorDeclaration
	:	constructorModifier* constructorDeclarator throws_? constructorBody
	;

constructorModifier
	:	annotation
	|	'public'
	|	'protected'
	|	'private'
	;

constructorDeclarator
	:	typeParameters? simpleTypeName '(' formalParameterList? ')'
	;

simpleTypeName
	:	Identifier
	;

constructorBody
	:	'{' explicitConstructorInvocation? blockStatements? '}'
	;

explicitConstructorInvocation
	:	typeArguments? 'this' '(' argumentList? ')' ';'
	|	typeArguments? 'super' '(' argumentList? ')' ';'
	|	expressionName '.' typeArguments? 'super' '(' argumentList? ')' ';'
	|	primary '.' typeArguments? 'super' '(' argumentList? ')' ';'
	;

enumDeclaration
	:	classModifier* 'enum' Identifier superinterfaces? enumBody
	;

enumBody
	:	'{' enumConstantList? ','? enumBodyDeclarations? '}'
	;

enumConstantList
	:	enumConstant (',' enumConstant)*
	;

enumConstant
	:	enumConstantModifier* Identifier ('(' argumentList? ')')? classBody?
	;

enumConstantModifier
	:	annotation
	;

enumBodyDeclarations
	:	';' classBodyDeclaration*
	;

/*
 * Productions from §9 (Interfaces)
 */

interfaceDeclaration
	:	normalInterfaceDeclaration
	|	annotationTypeDeclaration
	;

normalInterfaceDeclaration
	:	interfaceModifier* 'interface' Identifier typeParameters? extendsInterfaces? interfaceBody
	;

interfaceModifier
	:	annotation
	|	'public'
	|	'protected'
	|	'private'
	|	'abstract'
	|	'static'
	|	'strictfp'
	;

extendsInterfaces
	:	'extends' interfaceTypeList
	;

interfaceBody
	:	'{' interfaceMemberDeclaration* '}'
	;

interfaceMemberDeclaration
	:	constantDeclaration
	|	interfaceMethodDeclaration
	|	classDeclaration
	|	interfaceDeclaration
	|	';'
	;

constantDeclaration
	:	constantModifier* unannType? variableDeclaratorList ';' // Hydra2 gradual typing
	;

constantModifier
	:	annotation
	|	'public'
	|	'static'
	|	'final'
	;

interfaceMethodDeclaration
	:	interfaceMethodModifier* methodHeader methodBody
	;

interfaceMethodModifier
	:	annotation
	|	'public'
	|	'abstract'
	|	'default'
	|	'static'
	|	'strictfp'
	;

annotationTypeDeclaration
	:	interfaceModifier* '@' 'interface' Identifier annotationTypeBody
	;

annotationTypeBody
	:	'{' annotationTypeMemberDeclaration* '}'
	;

annotationTypeMemberDeclaration
	:	annotationTypeElementDeclaration
	|	constantDeclaration
	|	classDeclaration
	|	interfaceDeclaration
	|	';'
	;

annotationTypeElementDeclaration
	:	annotationTypeElementModifier* unannType Identifier '(' ')' dims? defaultValue? ';'
	;

annotationTypeElementModifier
	:	annotation
	|	'public'
	|	'abstract'
	;

defaultValue
	:	'default' elementValue
	;

annotation
	:	normalAnnotation
	|	markerAnnotation
	|	singleElementAnnotation
	;

normalAnnotation
	:	'@' typeName '(' elementValuePairList? ')'
	;

elementValuePairList
	:	elementValuePair (',' elementValuePair)*
	;

elementValuePair
	:	Identifier '=' elementValue
	;

elementValue
	:	conditionalExpression
	|	elementValueArrayInitializer
	|	annotation
	;

elementValueArrayInitializer
	:	'{' elementValueList? ','? '}'
	;

elementValueList
	:	elementValue (',' elementValue)*
	;

markerAnnotation    // DONE
	:	'@' typeName
	;

singleElementAnnotation
	:	'@' typeName '(' elementValue ')'
	;

/*
 * Productions from §10 (Arrays)
 */

arrayInitializer        // DONE
	:	'{' variableInitializerList? ','? '}'
	;

variableInitializerList // DONE
	:	variableInitializer (',' variableInitializer)*
	;

/*
 * Productions from §14 (Blocks and Statements)
 */

block   // DONE
	:	'{' blockStatements? '}'
	;

blockStatements     // DONE
	:	blockStatement blockStatement*
	;

blockStatement
	:	localVariableDeclarationStatement
	|	classDeclaration
	|	statement
	;

localVariableDeclarationStatement
	:	localVariableDeclaration ';'
	;

localVariableDeclaration
	:	variableModifier* unannType? variableDeclaratorList // Hydra2 gradual typing
	;

statement
	:	statementWithoutTrailingSubstatement
	|	labeledStatement
	|	ifThenStatement
	|	ifThenElseStatement
	|	whileStatement
	|	forStatement
	|   repeatStatement
	;

statementNoShortIf
	:	statementWithoutTrailingSubstatement
	|	labeledStatementNoShortIf
	|	ifThenElseStatementNoShortIf
	|	whileStatementNoShortIf
	|	forStatementNoShortIf
	|   repeatStatementNoShortIf
	;

statementWithoutTrailingSubstatement
	:	block
	|   scriptBlock                     // Hydra production: to support block scope functions
	|	emptyStatement
	|	expressionStatement
	|	assertStatement
	|	switchStatement
	|	doStatement
	|	breakStatement
	|	continueStatement
	|	returnStatement
	|	synchronizedStatement
	|	throwStatement
	|	tryStatement
    |   threadStatement     // Hydra2 production: Should thread statements be allowed inside compilation units?
	;

emptyStatement
	:	';'
	;

labeledStatement
	:	Identifier ':' statement
	;

labeledStatementNoShortIf
	:	Identifier ':' statementNoShortIf
	;

expressionStatement
	:	statementExpression ';'
	;

statementExpression
	:	assignment
	|	preIncrementExpression
	|	preDecrementExpression
	|	postIncrementExpression
	|	postDecrementExpression
	|   functionInvocation      // Hydra2 production : Should functions be allowed inside compilation units?
	|	methodInvocation
	|	classInstanceCreationExpression
	;

ifThenStatement
	:	'if' '(' expression ')' statement
	;

ifThenElseStatement
	:	'if' '(' expression ')' statementNoShortIf 'else' statement
	;

ifThenElseStatementNoShortIf
	:	'if' '(' expression ')' statementNoShortIf 'else' statementNoShortIf
	;

assertStatement
	:	'assert' expression ';'
	|	'assert' expression ':' expression ';'
	;

switchStatement
	:	'switch' '(' expression ')' switchBlock
	;

switchBlock
	:	'{' switchBlockStatementGroup* switchLabel* '}'
	;

switchBlockStatementGroup
	:	switchLabels blockStatements
	;

switchLabels
	:	switchLabel switchLabel*
	;

switchLabel
	:	'case' constantExpression ':'
	|	'case' enumConstantName ':'
	|	'default' ':'
	;

enumConstantName
	:	Identifier
	;

whileStatement
	:	'while' '(' expression ')' statement
	;

whileStatementNoShortIf
	:	'while' '(' expression ')' statementNoShortIf
	;

doStatement
	:	'do' statement 'while' '(' expression ')' ';'
	;

forStatement
	:	basicForStatement
	|	enhancedForStatement
	;

forStatementNoShortIf
	:	basicForStatementNoShortIf
	|	enhancedForStatementNoShortIf
	;

basicForStatement
	:	'for' '(' forInit? ';' expression? ';' forUpdate? ')' statement
	;

basicForStatementNoShortIf
	:	'for' '(' forInit? ';' expression? ';' forUpdate? ')' statementNoShortIf
	;

forInit
	:	statementExpressionList
	|	localVariableDeclaration
	;

forUpdate
	:	statementExpressionList
	;

statementExpressionList
	:	statementExpression (',' statementExpression)*
	;

enhancedForStatement
	:	'for' '(' variableModifier* unannType? variableDeclaratorId ':' expression ')' statement // Hydra2 gradual typing
	;

enhancedForStatementNoShortIf
	:	'for' '(' variableModifier* unannType? variableDeclaratorId ':' expression ')' statementNoShortIf // Hydra2 gradual typing
	;

repeatStatement
    :   'repeat' '(' expression ')' statement
    ;

repeatStatementNoShortIf
    :   'repeat' '(' expression ')' statementNoShortIf
    ;


breakStatement
	:	'break' Identifier? ';'
	;

continueStatement
	:	'continue' Identifier? ';'
	;

returnStatement
	:	'return' expression? ';'
	;

throwStatement
	:	'throw' expression ';'
	;

synchronizedStatement
	:	'synchronized' '(' expression ')' block
	;

tryStatement
	:	'try' block catches             # tryStatementCatches
	|	'try' block catches? finally_   # tryStatementCatchesFinally
	|	tryWithResourcesStatement       # tryStatementWithResources
	;

catches
	:	catchClause catchClause*
	;

catchClause
	:	'catch' '(' catchFormalParameter ')' block
	;

catchFormalParameter
	:	variableModifier* catchType variableDeclaratorId
	;

catchType
	:	unannClassType ('|' classType)*
	;

finally_
	:	'finally' block
	;

tryWithResourcesStatement
	:	'try' resourceSpecification block catches? finally_?
	;

resourceSpecification
	:	'(' resourceList ';'? ')'
	;

resourceList
	:	resource (';' resource)*
	;

resource
	:	variableModifier* unannType? variableDeclaratorId '=' expression // Hydra2 gradual typing
	;

/*
 * Productions from §15 (Expressions)
 */

primary
	:	primary_lfno primaryNoNewArray_lf_primary*
	;

primary_lfno
    :   primaryNoNewArray_lfno_primary
	|	arrayCreationExpression ;

primaryNoNewArray   // NO USAGES
	:	literal
	|	typeName (dim)* '.' 'class'
	|	'void' '.' 'class'
	|	'this'
	|	typeName '.' 'this'
	|	'(' expression ')'
	|	classInstanceCreationExpression
	|	fieldAccess
	|	arrayAccess
	|	methodInvocation
	|	methodReference
	;

primaryNoNewArray_lf_arrayAccess
	:
	;

primaryNoNewArray_lfno_arrayAccess // DONE
	:	literal                             # primaryNoNewArray_lfno_arrayAccessLiteral
	|	typeName (dim)* '.' 'class'         # primaryNoNewArray_lfno_arrayAccessTypeNameClass // DONE
	|	'void' '.' 'class'                  # primaryNoNewArray_lfno_arrayAccessVoidClass // DONE
	|	'this'                              # primaryNoNewArray_lfno_arrayAccessThis
	|	typeName '.' 'this'                 # primaryNoNewArray_lfno_arrayAccessTypeNameThis
	|	'(' expression ')'                  # primaryNoNewArray_lfno_arrayAccessParenExpression
	|	classInstanceCreationExpression     # primaryNoNewArray_lfno_arrayAccessClassInstanceCreation
	|	fieldAccess                         # primaryNoNewArray_lfno_arrayAccessFieldAccess
	|	methodInvocation                    # primaryNoNewArray_lfno_arrayAccessMethodInvocation
	|	methodReference                     # primaryNoNewArray_lfno_arrayAccessMethodReference
	;

dim : '[' ']' ;

primaryNoNewArray_lf_primary
	:	classInstanceCreationExpression_lf_primary // DONE
	|	fieldAccess_lf_primary  // DONE
	|	arrayAccess_lf_primary // DONE
	|	methodInvocation_lf_primary // DONE
	|	methodReference_lf_primary
	;

primaryNoNewArray_lf_primary_lf_arrayAccess_lf_primary
	:
	;

primaryNoNewArray_lf_primary_lfno_arrayAccess_lf_primary
	:	classInstanceCreationExpression_lf_primary  // DONE
	|	fieldAccess_lf_primary                      // DONE
	|	methodInvocation_lf_primary                 // DONE
	|	methodReference_lf_primary
	;

primaryNoNewArray_lfno_primary
	:	literal                                     # PrimaryNoNewArrayLfNoPrimaryLiteral
	|	typeName (dim)* '.' 'class'                 # PrimaryNoNewArrayLfNoPrimaryTypeNameClass // DONE
	|	unannPrimitiveType (dim)* '.' 'class'       # PrimaryNoNewArrayLfNoPrimaryUnnanPrimitiveType // done
	|	'void' '.' 'class'                          # PrimaryNoNewArrayLfNoPrimaryVoidClass // DONE
	|	'this'                                      # PrimaryNoNewArrayLfNoPrimaryThis
	|	typeName '.' 'this'                         # PrimaryNoNewArrayLfNoPrimaryTypeNameThis
	|	'(' expression ')'                          # PrimaryNoNewArrayLfNoPrimaryParenExpression
	|	classInstanceCreationExpression_lfno_primary # PrimaryNoNewArrayLfNoPrimaryClassInstanceCreation
	|	fieldAccess_lfno_primary                    # PrimaryNoNewArrayLfNoPrimaryFieldAccess
	|	arrayAccess_lfno_primary                    # PrimaryNoNewArrayLfNoPrimaryArrayAccess
	|	methodInvocation_lfno_primary               # PrimaryNoNewArrayLfNoPrimaryMethodInvocation
	|	methodReference_lfno_primary                # PrimaryNoNewArrayLfNoPrimaryMethodReference
	;

primaryNoNewArray_lfno_primary_lf_arrayAccess_lfno_primary
	:
	;

primaryNoNewArray_lfno_primary_lfno_arrayAccess_lfno_primary
	:	literal                                     # PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryLiteral
	|	typeName (dim)* '.' 'class'                 # PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryTypeName // DONE
	|	unannPrimitiveType (dim)* '.' 'class'       # PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryUnannPrimitiveType // done
	|	'void' '.' 'class'                          # PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryVoidClass // DONE
	|	'this'                                      # PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryThis
	|	typeName '.' 'this'                         # PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryTypeNameThis
	|	'(' expression ')'                          # PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryParenExpr
	|	classInstanceCreationExpression_lfno_primary # PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryClassInstanceCreation
	|	fieldAccess_lfno_primary                    # PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryFieldAccess
	|	methodInvocation_lfno_primary               # PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryMethodInvocation
	|	methodReference_lfno_primary                # PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryMethodReference
	;

classInstanceCreationExpression
	:	unqualifiedClassInstanceCreationExpression
	|   expressionName classInstanceCreationExpression_lf_primary
	|   primary classInstanceCreationExpression_lf_primary

/*
    // Original
	:	'new' typeArguments? annotation* Identifier ('.' annotation* Identifier)* typeArgumentsOrDiamond? '(' argumentList? ')' classBody?
	|	expressionName '.' 'new' typeArguments? annotation* Identifier typeArgumentsOrDiamond? '(' argumentList? ')' classBody?
	|	primary '.' 'new' typeArguments? annotation* Identifier typeArgumentsOrDiamond? '(' argumentList? ')' classBody?
*/
	;

classInstanceCreationExpression_lf_primary
	:	'.' 'new' typeArguments? annotation* Identifier typeArgumentsOrDiamond? '(' argumentList? ')' classBody?
	;

classInstanceCreationExpression_lfno_primary
	:	unqualifiedClassInstanceCreationExpression                                                                                  # classInstanceCreationExpression_lfno_primary_unqualified
	|	expressionName '.' 'new' typeArguments? annotation* Identifier typeArgumentsOrDiamond? '(' argumentList? ')' classBody?     # classInstanceCreationExpression_lfno_primary_qualified
	;

unqualifiedClassInstanceCreationExpression
    : 'new' typeArguments? unqualifiedClassInstanceCreationExpression_classToInstantiate '(' argumentList? ')' classBody?
    ;

unqualifiedClassInstanceCreationExpression_classToInstantiate
    :   unqualifiedClassInstanceCreationExpression_classToInstantiate_name typeArgumentsOrDiamond?
    ;

unqualifiedClassInstanceCreationExpression_classToInstantiate_name
    :   annotation* Identifier
    |   unqualifiedClassInstanceCreationExpression_classToInstantiate_name '.' annotation* Identifier
    ;

typeArgumentsOrDiamond
	:	typeArguments
	|	'<' '>'
	;

fieldAccess
	:	primary '.' Identifier
	|	'super' '.' Identifier
	|	typeName '.' 'super' '.' Identifier
	;

fieldAccess_lf_primary
	:	'.' Identifier
	;

fieldAccess_lfno_primary
	:	'super' '.' Identifier
	|	typeName '.' 'super' '.' Identifier
	;

arrayAccess // DONE
	:	arrayAccess_nolf arrayAccess_lf*
	;

arrayAccess_nolf // DONE
    :   expressionName '[' expression ']'                       # arrayAccess_nolfExpressionName
    |	primaryNoNewArray_lfno_arrayAccess '[' expression ']'   # arrayAccess_nolfPrimaryNoNewArrayLfnoArrayAccess
    ;

arrayAccess_lf // DONE
    :   primaryNoNewArray_lf_arrayAccess '[' expression ']'
    ;

arrayAccess_lf_primary // DONE
	:	(	primaryNoNewArray_lf_primary_lfno_arrayAccess_lf_primary '[' expression ']'
		)
		(	primaryNoNewArray_lf_primary_lf_arrayAccess_lf_primary '[' expression ']'
		)*
	;

arrayAccess_lfno_primary // DONE
	:	arrayAccess_lfno_primary_lfno arrayAccess_lfno_primary_lf*
	;

arrayAccess_lfno_primary_lfno // DONE
    :   expressionName '[' expression ']'                                                   # ArrayAccessLfnoPrimaryLfnoExpressionName
	|	primaryNoNewArray_lfno_primary_lfno_arrayAccess_lfno_primary '[' expression ']'     # ArrayAccessLfnoPrimaryLfnoPrimary
	;

arrayAccess_lfno_primary_lf // DONE
    :   primaryNoNewArray_lfno_primary_lf_arrayAccess_lfno_primary '[' expression ']'
    ;

methodInvocation
	:	methodName '(' argumentList? ')'                                            # methodInvocationMethodName
	|	typeName '.' typeArguments? Identifier '(' argumentList? ')'                # methodInvocationTypeName
	|	expressionName '.' typeArguments? Identifier '(' argumentList? ')'          # methodInvocationExpressionName
	|	primary '.' typeArguments? Identifier '(' argumentList? ')'                 # methodInvocationPrimary
	|	'super' '.' typeArguments? Identifier '(' argumentList? ')'                 # methodInvocationSuper
	|	typeName '.' 'super' '.' typeArguments? Identifier '(' argumentList? ')'    # methodInvocationTypeNameSuper
	;

methodInvocation_lf_primary
	:	'.' typeArguments? Identifier '(' argumentList? ')'
	;

methodInvocation_lfno_primary
	:	methodName '(' argumentList? ')'                                            # methodInvocationLfNoPrimaryMethodName
	|	typeName '.' typeArguments? Identifier '(' argumentList? ')'                # methodInvocationLfNoPrimaryTypeName
	|	expressionName '.' typeArguments? Identifier '(' argumentList? ')'          # methodInvocationLfNoPrimaryExpressionName
	|	'super' '.' typeArguments? Identifier '(' argumentList? ')'                 # methodInvocationLfNoPrimarySuper
	|	typeName '.' 'super' '.' typeArguments? Identifier '(' argumentList? ')'    # methodInvocationLfNoPrimaryTypeNameSuper
	;

argumentList
	:	expression (',' expression)*
	;

methodReference
	:	expressionName '::' typeArguments? Identifier
	|	referenceType '::' typeArguments? Identifier
	|	primary '::' typeArguments? Identifier
	|	'super' '::' typeArguments? Identifier
	|	typeName '.' 'super' '::' typeArguments? Identifier
	|	classType '::' typeArguments? 'new'
	|	arrayType '::' 'new'
	;

methodReference_lf_primary
	:	'::' typeArguments? Identifier
	;

methodReference_lfno_primary
	:	expressionName '::' typeArguments? Identifier
	|	referenceType '::' typeArguments? Identifier
	|	'super' '::' typeArguments? Identifier
	|	typeName '.' 'super' '::' typeArguments? Identifier
	|	classType '::' typeArguments? 'new'
	|	arrayType '::' 'new'
	;

arrayCreationExpression
	:	'new' primitiveType dimExprs dims?                  # arrayCreationExpressionPrimitiveNoInit
	|	'new' classOrInterfaceType dimExprs dims?           # arrayCreationExpressionClassOrInterfaceTypeNoInit
	|	'new' primitiveType dims arrayInitializer           # arrayCreationExpressionPrimitiveInit
	|	'new' classOrInterfaceType dims arrayInitializer    # arrayCreationExpressionClassOrInterfaceTypeInit
	;

dimExprs
	:	dimExpr dimExpr*
	;

dimExpr
	:	annotation* '[' expression ']'
	;

constantExpression
	:	expression
	;

expression
	:	lambdaExpression
	|	assignmentExpression
	;

lambdaExpression
	:	lambdaParameters '->' lambdaBody
	;

lambdaParameters
	:	Identifier                          # lambdaParametersIdentifier
	|	'(' formalParameterList? ')'        # lambdaParametersFormalParameterList
	|	'(' inferredFormalParameterList ')' # lambdaParametersInferredFormalParameterList
	;

inferredFormalParameterList
	:	Identifier (',' Identifier)*
	;

lambdaBody
	:	expression
	|	block
	;

assignmentExpression
	:	conditionalExpression
	|	assignment
	;

assignment
	:	leftHandSide assignmentOperator expression
	;

leftHandSide
	:	expressionName
	|	fieldAccess
	|	arrayAccess
	;

assignmentOperator
	:	'='
	|	'*='
	|	'/='
	|	'%='
	|	'+='
	|	'-='
	|	'<<='
	|	'>>='
	|	'>>>='
	|	'&='
	|	'^='
	|	'|='
	;

conditionalExpression
	:	conditionalOrExpression                                           # BaseCaseConditionalExpression
	|	conditionalOrExpression '?' expression ':' conditionalExpression  # ConditionalExpressionExpression
	;

conditionalOrExpression
	:	conditionalAndExpression                                # BaseCaseConditionalOrExpression
	|	conditionalOrExpression '||' conditionalAndExpression   # OrConditionalOrExpression
	;

conditionalAndExpression
	:	inclusiveOrExpression                               # BaseCaseConditionalAndExpression
	|	conditionalAndExpression '&&' inclusiveOrExpression # AndConditionalAndExpression
	;

inclusiveOrExpression
	:	exclusiveOrExpression                           # BaseCaseInclusiveOrExpression
	|	inclusiveOrExpression '|' exclusiveOrExpression # OrInclusiveOrExpression
	;

exclusiveOrExpression
	:	andExpression                           # BaseCaseExclusiveOrExpression
	|	exclusiveOrExpression '^' andExpression # XorExclusiveOrExpression
	;

andExpression
	:	equalityExpression                    # BaseCaseAndExpression
	|	andExpression '&' equalityExpression  # AndAndExpression
	;

equalityExpression
	:	relationalExpression                         # BaseCaseEqualityExpression
	|	equalityExpression '==' relationalExpression # EqEqualityExpression
	|	equalityExpression '!=' relationalExpression # NeqEqualityExpression
	;

relationalExpression
	:	shiftExpression                                 # BaseCaseRelationalExpression
	|	relationalExpression '<' shiftExpression        # LtRelationalExpression
	|	relationalExpression '>' shiftExpression        # GtRelationalExpression
	|	relationalExpression '<=' shiftExpression       # LeqRelationalExpression
	|	relationalExpression '>=' shiftExpression       # GeqRelationalExpression
	|	relationalExpression 'instanceof' referenceType # InstanceOfRelationalExpression
	;

shiftExpression
	:	additiveExpression                              # BaseCaseShiftExpression
	|	shiftExpression '<' '<' additiveExpression      # LeftShiftExpression
	|	shiftExpression '>' '>' additiveExpression      # SignedRightShiftExpression
	|	shiftExpression '>' '>' '>' additiveExpression  # UnsignedRightShiftExpression
	;

additiveExpression
	:	multiplicativeExpression                        # BaseCaseAdditiveExpression
	|	additiveExpression '+' multiplicativeExpression # AddAdditiveExpression
	|	additiveExpression '-' multiplicativeExpression # SubAdditiveExpression
	;

multiplicativeExpression
	:	unaryExpression                                 # BaseCaseMultiplicativeExpression
	|	multiplicativeExpression '*' unaryExpression    # MultMultiplicativeExpression
	|	multiplicativeExpression '/' unaryExpression    # DivMultiplicativeExpression
	|	multiplicativeExpression '%' unaryExpression    # ModMultiplicativeExpression
	;

unaryExpression
	:	preIncrementExpression      # PreIncrementUnaryExpression
	|	preDecrementExpression      # PreDecrementUnaryExpression
	|	'+' unaryExpression         # PlusUnaryExpression
	|	'-' unaryExpression         # MinusUnaryExpression
	|	unaryExpressionNotPlusMinus # NotPlusMinusUnaryExpression
	;

preIncrementExpression
	:	'++' unaryExpression
	;

preDecrementExpression
	:	'--' unaryExpression
	;

unaryExpressionNotPlusMinus
	:	postfixExpression           # PostfixUnaryExpressionNotPlusMinus
	|	'~' unaryExpression         # InverseUnaryExpressionNotPlusMinus
	|	'!' unaryExpression         # NotUnaryExpressionNotPlusMinus
	|	castExpression              # CastUnaryExpressionNotPlusMinus
	;

postfixExpression
	:	postfixExpression_nolf
		postfixExpression_lf*
	;

postfixExpression_nolf
    :   primary
    |	expressionName
    ;

postfixExpression_lf
    : 	postIncrementExpression_lf_postfixExpression
	|	postDecrementExpression_lf_postfixExpression
	;

postIncrementExpression
	:	postfixExpression '++'
	;

postIncrementExpression_lf_postfixExpression
	:	'++'
	;

postDecrementExpression
	:	postfixExpression '--'
	;

postDecrementExpression_lf_postfixExpression
	:	'--'
	;

castExpression
	:	'(' primitiveType ')' unaryExpression                                   # castExpressionUnaryExpression
	|	'(' referenceType additionalBound* ')' unaryExpressionNotPlusMinus      # castExpressionUnaryExpressionNotPlusMinus
	|	'(' referenceType additionalBound* ')' lambdaExpression                 # castExpressionLambdaExpression
	;

// LEXER

// §3.9 Keywords

ABSTRACT : 'abstract';
ASSERT : 'assert';
BOOLEAN : 'boolean';
BREAK : 'break';
BYTE : 'byte';
CASE : 'case';
CATCH : 'catch';
CHAR : 'char';
CLASS : 'class';
CONST : 'const';
CONTINUE : 'continue';
DEFAULT : 'default';
DO : 'do';
DOUBLE : 'double';
ELSE : 'else';
ENUM : 'enum';
EXTENDS : 'extends';
FINAL : 'final';
FINALLY : 'finally';
FLOAT : 'float';
FOR : 'for';
IF : 'if';
GLOBAL : 'global';      // Hydra2 token
GOTO : 'goto';
IMPLEMENTS : 'implements';
IMPORT : 'import';
INSTANCEOF : 'instanceof';
INT : 'int';
INTERFACE : 'interface';
LONG : 'long';
NATIVE : 'native';
NEW : 'new';
PACKAGE : 'package';
PRIVATE : 'private';
PROTECTED : 'protected';
PUBLIC : 'public';
RETURN : 'return';
SHORT : 'short';
STATIC : 'static';
STRICTFP : 'strictfp';
SUPER : 'super';
SWITCH : 'switch';
SYNCHRONIZED : 'synchronized';
THIS : 'this';
THREAD : 'thread';      // Hydra2 token
THROW : 'throw';
THROWS : 'throws';
TRANSIENT : 'transient';
TRY : 'try';
VOID : 'void';
VOLATILE : 'volatile';
WHILE : 'while';

// §3.10.1 Integer Literals

IntegerLiteral
	:	DecimalIntegerLiteral
	|	HexIntegerLiteral
	|	OctalIntegerLiteral
	|	BinaryIntegerLiteral
	;

fragment
DecimalIntegerLiteral
	:	DecimalNumeral IntegerTypeSuffix?
	;

fragment
HexIntegerLiteral
	:	HexNumeral IntegerTypeSuffix?
	;

fragment
OctalIntegerLiteral
	:	OctalNumeral IntegerTypeSuffix?
	;

fragment
BinaryIntegerLiteral
	:	BinaryNumeral IntegerTypeSuffix?
	;

fragment
IntegerTypeSuffix
	:	[lL]
	;

fragment
DecimalNumeral
	:	'0'
	|	NonZeroDigit (Digits? | Underscores Digits)
	;

fragment
Digits
	:	Digit (DigitsAndUnderscores? Digit)?
	;

fragment
Digit
	:	'0'
	|	NonZeroDigit
	;

fragment
NonZeroDigit
	:	[1-9]
	;

fragment
DigitsAndUnderscores
	:	DigitOrUnderscore+
	;

fragment
DigitOrUnderscore
	:	Digit
	|	'_'
	;

fragment
Underscores
	:	'_'+
	;

fragment
HexNumeral
	:	'0' [xX] HexDigits
	;

fragment
HexDigits
	:	HexDigit (HexDigitsAndUnderscores? HexDigit)?
	;

fragment
HexDigit
	:	[0-9a-fA-F]
	;

fragment
HexDigitsAndUnderscores
	:	HexDigitOrUnderscore+
	;

fragment
HexDigitOrUnderscore
	:	HexDigit
	|	'_'
	;

fragment
OctalNumeral
	:	'0' Underscores? OctalDigits
	;

fragment
OctalDigits
	:	OctalDigit (OctalDigitsAndUnderscores? OctalDigit)?
	;

fragment
OctalDigit
	:	[0-7]
	;

fragment
OctalDigitsAndUnderscores
	:	OctalDigitOrUnderscore+
	;

fragment
OctalDigitOrUnderscore
	:	OctalDigit
	|	'_'
	;

fragment
BinaryNumeral
	:	'0' [bB] BinaryDigits
	;

fragment
BinaryDigits
	:	BinaryDigit (BinaryDigitsAndUnderscores? BinaryDigit)?
	;

fragment
BinaryDigit
	:	[01]
	;

fragment
BinaryDigitsAndUnderscores
	:	BinaryDigitOrUnderscore+
	;

fragment
BinaryDigitOrUnderscore
	:	BinaryDigit
	|	'_'
	;

// §3.10.2 Floating-Point Literals

FloatingPointLiteral
	:	DecimalFloatingPointLiteral
	|	HexadecimalFloatingPointLiteral
	;

fragment
DecimalFloatingPointLiteral
	:	Digits '.' Digits? ExponentPart? FloatTypeSuffix?
	|	'.' Digits ExponentPart? FloatTypeSuffix?
	|	Digits ExponentPart FloatTypeSuffix?
	|	Digits FloatTypeSuffix
	;

fragment
ExponentPart
	:	ExponentIndicator SignedInteger
	;

fragment
ExponentIndicator
	:	[eE]
	;

fragment
SignedInteger
	:	Sign? Digits
	;

fragment
Sign
	:	[+-]
	;

fragment
FloatTypeSuffix
	:	[fFdD]
	;

fragment
HexadecimalFloatingPointLiteral
	:	HexSignificand BinaryExponent FloatTypeSuffix?
	;

fragment
HexSignificand
	:	HexNumeral '.'?
	|	'0' [xX] HexDigits? '.' HexDigits
	;

fragment
BinaryExponent
	:	BinaryExponentIndicator SignedInteger
	;

fragment
BinaryExponentIndicator
	:	[pP]
	;

// §3.10.3 Boolean Literals

BooleanLiteral
	:	'true'
	|	'false'
	;

// §3.10.4 Character Literals

CharacterLiteral
	:	'\'' SingleCharacter '\''
	|	'\'' EscapeSequence '\''
	;

fragment
SingleCharacter
	:	~['\\]
	;

// §3.10.5 String Literals

StringLiteral
	:	'"' StringCharacters? '"'
	;

fragment
StringCharacters
	:	StringCharacter+
	;

fragment
StringCharacter
	:	~["\\]
	|	EscapeSequence
	;

// §3.10.6 Escape Sequences for Character and String Literals

fragment
EscapeSequence
	:	'\\' [btnfr"'\\]
	|	OctalEscape
    |   UnicodeEscape // This is not in the spec but prevents having to preprocess the input
	;

fragment
OctalEscape
	:	'\\' OctalDigit
	|	'\\' OctalDigit OctalDigit
	|	'\\' ZeroToThree OctalDigit OctalDigit
	;

fragment
ZeroToThree
	:	[0-3]
	;

// This is not in the spec but prevents having to preprocess the input
fragment
UnicodeEscape
    :   '\\' 'u' HexDigit HexDigit HexDigit HexDigit
    ;

// §3.10.7 The Null Literal

NullLiteral
	:	'null'
	;

// §3.11 Separators

LPAREN : '(';
RPAREN : ')';
LBRACE : '{';
RBRACE : '}';
LBRACK : '[';
RBRACK : ']';
SEMI : ';';
COMMA : ',';
DOT : '.';

// §3.12 Operators

ASSIGN : '=';
GT : '>';
LT : '<';
BANG : '!';
TILDE : '~';
QUESTION : '?';
COLON : ':';
EQUAL : '==';
LE : '<=';
GE : '>=';
NOTEQUAL : '!=';
AND : '&&';
OR : '||';
INC : '++';
DEC : '--';
ADD : '+';
SUB : '-';
MUL : '*';
DIV : '/';
BITAND : '&';
BITOR : '|';
CARET : '^';
MOD : '%';
ARROW : '->';
COLONCOLON : '::';

ADD_ASSIGN : '+=';
SUB_ASSIGN : '-=';
MUL_ASSIGN : '*=';
DIV_ASSIGN : '/=';
AND_ASSIGN : '&=';
OR_ASSIGN : '|=';
XOR_ASSIGN : '^=';
MOD_ASSIGN : '%=';
LSHIFT_ASSIGN : '<<=';
RSHIFT_ASSIGN : '>>=';
URSHIFT_ASSIGN : '>>>=';

// §3.8 Identifiers (must appear after all keywords in the grammar)

Identifier
	:	JavaLetter JavaLetterOrDigit*
	;

fragment
JavaLetter
	:	[a-zA-Z$_] // these are the "java letters" below 0x7F
	|	// covers all characters above 0x7F which are not a surrogate
		~[\u0000-\u007F\uD800-\uDBFF]
		{Character.isJavaIdentifierStart(_input.LA(-1))}?
	|	// covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
		[\uD800-\uDBFF] [\uDC00-\uDFFF]
		{Character.isJavaIdentifierStart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))}?
	;

fragment
JavaLetterOrDigit
	:	[a-zA-Z0-9$_] // these are the "java letters or digits" below 0x7F
	|	// covers all characters above 0x7F which are not a surrogate
		~[\u0000-\u007F\uD800-\uDBFF]
		{Character.isJavaIdentifierPart(_input.LA(-1))}?
	|	// covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
		[\uD800-\uDBFF] [\uDC00-\uDFFF]
		{Character.isJavaIdentifierPart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))}?
	;

//
// Additional symbols not defined in the lexical specification
//

AT : '@';
ELLIPSIS : '...';

//
// Whitespace and comments
//

WS  :  [ \t\r\n\u000C]+ -> skip
    ;

COMMENT
    :   '/*' .*? '*/' -> skip
    ;

LINE_COMMENT
    :   '//' ~[\r\n]* -> skip
    ;
