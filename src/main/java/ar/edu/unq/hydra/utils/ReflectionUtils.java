package ar.edu.unq.hydra.utils;

import ar.edu.unq.hydra.interpreter.ClassLoader;
import ar.edu.unq.hydra.interpreter.InvocationResult;
import ar.edu.unq.hydra.utils.exceptions.ReflectionUtilsException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.reflect.MethodUtils;

import java.lang.reflect.*;

import static java.util.Objects.isNull;

/**
 * This class contains a set of useful static methods to simplify some reflective operations on objects and classes.
 * Also it wraps all excplicit Exceptions thrown by java's reflective operations into ReflectionUtilsException which
 * is alings to the project policy of avoiding any kind of explicit Exceptions to be thrown.
 */
public class ReflectionUtils
{
    /**
     * Sets the fieldValue of a field of the given object
     * @param holder
     * @param fieldName
     * @param fieldValue
     */
    public static void setFieldValue(Object holder, String fieldName, Object fieldValue)
    {
        try {
            Field field;
            if (holder instanceof Class)
                field = ((Class) holder).getField(fieldName);
            else
                field = holder.getClass().getField(fieldName);
            field.set(holder, fieldValue);
        } catch (NoSuchFieldException e) {
            String className = (holder instanceof Class) ? holder.toString() : holder.getClass().toString();
            throw new ReflectionUtilsException(className + " doesn't have a field of the name " + fieldName, e);
        } catch (IllegalAccessException e) {
            throw new ReflectionUtilsException(e);
        }
    }

    /**
     * Returns the value of the field of the given object
     * @param holder
     * @param fieldName
     * @return
     */
    public static Object getFieldValue(Object holder, String fieldName)
    {
        try {
            Field field;
            if (holder instanceof Class)
                field = ((Class) holder).getField(fieldName);
            else
                field = holder.getClass().getField(fieldName);
            return field.get(holder);
        } catch (NoSuchFieldException e) {
            String className = (holder instanceof Class) ? holder.toString() : holder.getClass().toString();
            throw new ReflectionUtilsException(className + " doesn't have a field of the name " + fieldName, e);
        } catch (IllegalAccessException e) {
            throw new ReflectionUtilsException(e);
        }
    }

    /**
     * Returns an instance of the given class if a constructor matching the arguments types exists, if no constructor
     * matches an exception is thrown. If the invocation throws any Exceptions it's wrapped in a ReflectiveUtilsException
     * @param clazz
     * @param arguments
     * @param argumentsTypes
     * @return
     */
    public static Object invokeMatchingConstructor(Class clazz, Object[] arguments, Class[] argumentsTypes)
    {
        try {
            return ConstructorUtils.invokeConstructor(clazz, arguments, argumentsTypes);
        } catch (NoSuchMethodException e) {
            throw new ReflectionUtilsException("Cannot find a matching constructor for class " + clazz.toString(), e);
        } catch (IllegalAccessException e) {
            throw new ReflectionUtilsException(e);
        } catch (InvocationTargetException e) {
            throw new ReflectionUtilsException(e);
        } catch (InstantiationException e) {
            throw new ReflectionUtilsException(e);
        }
    }

    /**
     * Denotes if the class of the given name exists, meaning if the given classLoader is able to instance it.
     * @param name
     * @return
     */
    public static boolean classExists(String name)
    {
        try {
            ClassUtils.getClass(getClassLoader(), name);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    private static ClassLoader getClassLoader() {
        return ClassLoader.getInstance();
    }

    /**
     * Denotes if the class is static
     * @param clazz
     * @return
     */
    public static boolean classIsStatic(Class clazz)
    {
        return Modifier.isStatic(clazz.getModifiers());
    }

    /**
     * Returns a Class object of the given name if it's loadable by the classLoader, or an exception is thrown otherwise.
     * @param name
     * @return
     */
    public static Class getClass(String name)
    {
        try {
            return ClassUtils.getClass(getClassLoader(), name);
        } catch (ClassNotFoundException e) {
            throw new ReflectionUtilsException("Class " + name + " not found", e);
        }
    }

    public static Method getMethodForObject(Object scope, String methodName, Class<?>[] argumentsTypes)
    {
        Class clazz = (scope instanceof Class) ? (Class) scope : scope.getClass();
        return MethodUtils.getMatchingAccessibleMethod(clazz, methodName, argumentsTypes);
    }

    public static InvocationResult invokeMethod(Method method, Object scope, Object[] arguments)
    {
        arguments = ReflectionUtils.toVarArgs(method, arguments);
        try {
            Object result = (scope instanceof Class)
                    ? method.invoke(null, arguments)    // invoked as static method
                    : method.invoke(scope, arguments);

            return new InvocationResult(method.getReturnType(),result);  // invoked as instance method
        } catch (IllegalAccessException e) {
            throw new ReflectionUtilsException(e);
        } catch (InvocationTargetException e) {
            throw new ReflectionUtilsException(e);
        }
    }

    /**
     * Denotes if a field or a method of the given name exists in the given class.
     * @param clazz
     * @param name
     * @return
     */
    public static boolean fieldOrMethodExists(Class clazz, String name)
    {
        return fieldExists(clazz, name) || methodExists(clazz, name);
    }

    /**
     * Denotes if a method of the given name exists in the given class.
     * @param clazz
     * @param methodName
     * @return
     */
    public static boolean methodExists(Class clazz, String methodName)
    {
        for (Method m : clazz.getDeclaredMethods())
            if (m.getName().equals(methodName))
                return true ;
        return false ;
    }

    /**
     * Denotes if a field of the given name exists in the given class.
     * @param clazz
     * @param fieldName
     * @return
     */
    public static boolean fieldExists(Class clazz, String fieldName)
    {
        return !isNull(FieldUtils.getField(clazz, fieldName));
    }

    /**
     * Denotes if a static field of the given name exists in the class clazz
     * @param clazz
     * @param fieldName
     * @return
     */
    public static boolean staticFieldExists(Class clazz, String fieldName)
    {
        try {
            Field field = clazz.getField(fieldName);
            return Modifier.isStatic(field.getModifiers());
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    /**
     * Denotes if a static method of the given name exists in the class clazz
     * @param clazz
     * @param methodName
     * @return
     */
    public static boolean staticMethodExists(Class clazz, String methodName)
    {
        for (Method m : clazz.getDeclaredMethods())
            if (m.getName().equals(methodName)
                    && Modifier.isStatic(m.getModifiers()) )
                return true ;
        return false ;
    }

    public static Object[] toVarArgs(Method method, Object[] arguments)
    {
        if (method.isVarArgs())
        {
            Class<?>[] methodParameterTypes = method.getParameterTypes();
            arguments = getVarArgs(arguments, methodParameterTypes);
        }
        return arguments;
    }

    /**
     * This method was copied "as is" from org.apache.commons.lang3.reflect.MethodUtils 3.5 as its not available in the
     * public API
     *
     * <p>Given an arguments array passed to a varargs method, return an array of arguments in the canonical form,
     * i.e. an array with the declared number of parameters, and whose last parameter is an array of the varargs type.
     * </p>
     *
     * @param args the array of arguments passed to the varags method
     * @param methodParameterTypes the declared array of method parameter types
     * @return an array of the variadic arguments passed to the method
     * @since 3.5
     */
    static Object[] getVarArgs(Object[] args, Class<?>[] methodParameterTypes) {
        if (args.length == methodParameterTypes.length
                && args[args.length - 1].getClass().equals(methodParameterTypes[methodParameterTypes.length - 1])) {
            // The args array is already in the canonical form for the method.
            return args;
        }

        // Construct a new array matching the method's declared parameter types.
        Object[] newArgs = new Object[methodParameterTypes.length];

        // Copy the normal (non-varargs) parameters
        System.arraycopy(args, 0, newArgs, 0, methodParameterTypes.length - 1);

        // Construct a new array for the variadic parameters
        Class<?> varArgComponentType = methodParameterTypes[methodParameterTypes.length - 1].getComponentType();
        int varArgLength = args.length - methodParameterTypes.length + 1;

        Object varArgsArray = Array.newInstance(ClassUtils.primitiveToWrapper(varArgComponentType), varArgLength);
        // Copy the variadic arguments into the varargs array.
        System.arraycopy(args, methodParameterTypes.length - 1, varArgsArray, 0, varArgLength);

        if(varArgComponentType.isPrimitive()) {
            // unbox from wrapper type to primitive type
            varArgsArray = ArrayUtils.toPrimitive(varArgsArray);
        }

        // Store the varargs array in the last position of the array to return
        newArgs[methodParameterTypes.length - 1] = varArgsArray;

        // Return the canonical varargs array.
        return newArgs;
    }


}
