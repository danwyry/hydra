package ar.edu.unq.hydra.utils.exceptions;

/**
 * Created by Daniel Wyrytowski on 4/11/17.
 */
public class ReflectionUtilsException extends RuntimeException
{
    static final long serialVersionUID = -7034897190745766939L;

    public ReflectionUtilsException(Throwable var1) {
        super(var1);
    }

    public ReflectionUtilsException(String message, Throwable cause) {
        super(message, cause);
    }
}
