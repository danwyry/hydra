package ar.edu.unq.hydra.interpreter.exceptions;

import ar.edu.unq.hydra.interpreter.interruption_exceptions.HydraFlowInterruption;

/**
 * Created by Daniel Wyrytowski on 2/4/18.
 */
public class HydraInterruptedException extends HydraFlowInterruption {
    public HydraInterruptedException() {
        super("Thread evaluation interrupted abruptly");
    }

    public HydraInterruptedException(Throwable e) {
        super(e);
    }
}
