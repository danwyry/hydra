package ar.edu.unq.hydra.interpreter.values;

import ar.edu.unq.hydra.ast.expr.BinaryExpr;
import ar.edu.unq.hydra.ast.expr.UnaryExpr;
import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidAritmethic;
import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidCastOperation;

/**
 * Created by Daniel Wyrytowski
 */
public class HFloat  extends HNumeric
{
    private float floatVal;

    public HFloat(float val)
    {
        this.floatVal = val;
    }

    @Override
    public short getShortValue() {
        return (short) floatVal;
    }

    @Override
    public char getCharValue()
    {
        throw new HydraInvalidCastOperation(javaTypeName(), "Char");
    }

    @Override
    public int getIntValue() {
        return (int) floatVal;
    }

    @Override
    public long getLongValue() {
        return (long) floatVal;
    }

    @Override
    public float getFloatValue() {
        return floatVal;
    }

    @Override
    public double getDoubleValue() {
        return floatVal;
    }

    @Override
    public byte getByteValue() {
        return 0;
    }

    @Override
    public HFloat plus(HObject pright)
    {
        return new HFloat( this.getFloatValue() + pright.getFloatValue() );
    }

    @Override
    public HFloat times(HObject pright)
    {
        return new HFloat( this.getFloatValue() * pright.getFloatValue() );
    }

    @Override
    public HFloat minus(HObject pright)
    {
        return new HFloat( this.getFloatValue() - pright.getFloatValue() );
    }

    @Override
    public HFloat divide(HObject pright)
    {
        return new HFloat( this.getFloatValue() / pright.getFloatValue() );
    }

    @Override
    public HFloat remainder(HObject pright)
    {
        return new HFloat( this.getFloatValue() % pright.getFloatValue() );
    }

    @Override
    public HNumeric binAnd(HObject pright)
    {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.BINARY_AND.asString(), javaTypeName());
    }

    @Override
    public HNumeric binOr(HObject pright)
    {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.BINARY_OR.asString(), javaTypeName());
    }

    @Override
    public HNumeric lShift(HObject pright)
    {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.LEFT_SHIFT.asString(), javaTypeName());

    }

    @Override
    public HNumeric rSignedShift(HObject pright)
    {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.SIGNED_RIGHT_SHIFT.asString(), javaTypeName());

    }

    @Override
    public HNumeric rUnsignedShift(HObject pright)
    {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.UNSIGNED_RIGHT_SHIFT.asString(), javaTypeName());
    }

    @Override
    public HBoolean eq(HObject pright)
    {
        return new HBoolean(this.getFloatValue() == pright.getFloatValue());

    }

    @Override
    public HBoolean neq(HObject pright)
    {
        return new HBoolean(this.getFloatValue() != pright.getFloatValue());
    }

    @Override
    public HBoolean gt(HObject pright)
    {
        return new HBoolean(this.getFloatValue() > pright.getFloatValue());
    }

    @Override
    public HBoolean lt(HObject pright)
    {
        return new HBoolean(this.getFloatValue() < pright.getFloatValue());
    }

    @Override
    public HBoolean leq(HObject pright)
    {
        return new HBoolean(this.getFloatValue() <= pright.getFloatValue());
    }

    @Override
    public HBoolean geq(HObject pright)
    {
        return new HBoolean(this.getFloatValue() >= pright.getFloatValue());
    }


    @Override
    public HObject inversedHValue()
    {
        throw new HydraInvalidAritmethic(UnaryExpr.Operator.BITWISE_COMPLEMENT.asString(), javaTypeName());
    }

    @Override
    public HObject negativeHValue()
    {
        return new HFloat(-floatVal);
    }

    @Override
    public HObject positiveHValue()
    {
        return new HFloat(+floatVal);
    }

    @Override
    public HFloat preIncrement()
    {
        return new HFloat(++floatVal);
    }

    @Override
    public HFloat posIncrement()
    {
        return new HFloat(floatVal++);
    }

    @Override
    public HFloat preDecrement()
    {
        return new HFloat(--floatVal);
    }

    @Override
    public HFloat posDecrement()
    {
        return new HFloat(floatVal--);
    }

    @Override
    public HObject cast(Class clazz)
    {
        if (clazz == char.class)
            return new HChar((char) floatVal);
        else if (clazz == boolean.class)
            throw new HydraInvalidCastOperation(javaTypeName(), "boolean");
        else if (clazz == byte.class)
            return new HByte((byte)floatVal);
        else if (clazz == short.class)
            return new HShort((short)floatVal);
        else if (clazz == int.class)
            return new HInt((int)floatVal);
        else if (clazz == long.class)
            return new HLong((long)floatVal);
        else if (clazz == float.class)
            return this;
        else if (clazz == double.class)
            return new HDouble((double) floatVal);
        else
            return new HJObject(clazz.cast(floatVal));
    }

    @Override
    public HBoolean isInstanceOf(Class clazz)
    {
        return new HBoolean(clazz.isInstance(floatVal));
    }

    @Override
    public Class getValueClass() {
        return Float.class;
    }

    @Override
    protected String javaTypeName() {
        return "float";
    }
}
