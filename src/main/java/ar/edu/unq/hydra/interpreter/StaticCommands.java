package ar.edu.unq.hydra.interpreter;

/**
 * Created by Daniel Wyrytowski on 7/15/17.
 */
public class StaticCommands
{
    public static void print(Object o)
    {
        System.out.print(o);
    }

    public static void println(Object o)
    {
        System.out.println(o);
    }
}
