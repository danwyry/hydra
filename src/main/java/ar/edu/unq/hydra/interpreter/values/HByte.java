package ar.edu.unq.hydra.interpreter.values;

import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidCastOperation;

/**
 * Created by Daniel Wyrytowski
 */
public class HByte extends HNumeric
{
    private byte byteVal;

    public HByte(byte value)
    {
        byteVal = value;
    }

    @Override
    protected String javaTypeName() {
        return "byte";
    }

    @Override
    public Object getValueAsObject()
    {
        return byteVal;
    }

    @Override
    public HBoolean eq(HObject pright)
    {
        return new HBoolean(byteVal == pright.getByteValue());
    }

    @Override
    public boolean getBooleanValue()
    {
        throw new HydraInvalidCastOperation(javaTypeName(), "Boolean");
    }

    @Override
    public short getShortValue()
    {
        return byteVal;
    }

    @Override
    public char getCharValue() {
        throw new HydraInvalidCastOperation(javaTypeName(), "Char");
    }

    @Override
    public int getIntValue() {
        return byteVal;
    }

    @Override
    public long getLongValue() {
        return byteVal;
    }

    @Override
    public float getFloatValue() {
        return byteVal;
    }

    @Override
    public double getDoubleValue() {
        return byteVal;
    }

    @Override
    public byte getByteValue() {
        return byteVal;
    }

    @Override
    public HObject preIncrement()
    {
        HByte value = new HByte(++byteVal);
        return TypePromoter.unaryPromotion(value);
    }

    @Override
    public HObject posIncrement()
    {
        HByte value = new HByte(byteVal++);
        return TypePromoter.unaryPromotion(value);
    }

    @Override
    public HObject preDecrement()
    {
        HByte value = new HByte(--byteVal);
        return TypePromoter.unaryPromotion(value);
    }

    @Override
    public HObject posDecrement()
    {
        HByte value = new HByte(byteVal--);
        return TypePromoter.unaryPromotion(value);
    }


    @Override
    public HObject cast(Class clazz)
    {
        if (clazz == char.class)
            return new HChar((char) byteVal);
        else if (clazz == boolean.class)
            throw new HydraInvalidCastOperation(javaTypeName(), "boolean");
        else if (clazz == byte.class)
            return this;
        else if (clazz == short.class)
            return new HShort((short)byteVal);
        else if (clazz == int.class)
            return new HInt((int)byteVal);
        else if (clazz == long.class)
            return new HLong((long)byteVal);
        else if (clazz == float.class)
            return new HFloat((float)byteVal);
        else if (clazz == double.class)
            return new HDouble((double) byteVal);
        else
            return new HJObject(clazz.cast(byteVal));
    }

    @Override
    public HBoolean isInstanceOf(Class clazz)
    {
        return new HBoolean(clazz.isInstance(byteVal));
    }

    @Override
    public Class getValueClass() {
        return byte.class;
    }


}
