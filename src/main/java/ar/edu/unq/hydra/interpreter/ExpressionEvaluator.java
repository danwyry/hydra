package ar.edu.unq.hydra.interpreter;

import ar.edu.unq.hydra.ast.ArrayCreationLevel;
import ar.edu.unq.hydra.ast.NodeList;
import ar.edu.unq.hydra.ast.body.FunctionDeclaration;
import ar.edu.unq.hydra.ast.body.Parameter;
import ar.edu.unq.hydra.ast.body.VariableDeclarator;
import ar.edu.unq.hydra.ast.expr.*;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithArguments;
import ar.edu.unq.hydra.ast.stmt.BlockStmt;
import ar.edu.unq.hydra.ast.type.Type;
import ar.edu.unq.hydra.ast.type.UnknownType;
import ar.edu.unq.hydra.ast.visitor.ExpressionVisitor;
import ar.edu.unq.hydra.interpreter.context.Context;
import ar.edu.unq.hydra.interpreter.context.frames.FunctionCallFrame;
import ar.edu.unq.hydra.interpreter.exceptions.*;
import ar.edu.unq.hydra.interpreter.import_manager.ImportManager;
import ar.edu.unq.hydra.interpreter.import_manager.StaticField;
import ar.edu.unq.hydra.interpreter.import_manager.StaticMethod;
import ar.edu.unq.hydra.interpreter.interruption_exceptions.ReturnInterruption;
import ar.edu.unq.hydra.interpreter.values.*;
import ar.edu.unq.hydra.utils.ReflectionUtils;
import ar.edu.unq.hydra.ast.NameUtils;
import ar.edu.unq.hydra.utils.exceptions.ReflectionUtilsException;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;

/**
 * Expression evaluator, as its name suggests evaluates all expressions supported by the language.
 * The class itself is a Visitor class that visits all expression nodes taking a context as argument and returning
 * a HObject representing the value of the expression.
 * The entry point for the class should always be ExpressionEvaluator.eval()
 * The visit methods are public just because java doesn't allow interfaces to define protected methods, but they should
 * never be used as part of the public API.
 *
 * Created by Daniel Wyrytowski
 */
public class ExpressionEvaluator extends ExpressionVisitor<HObject, Context>
{
    private StatementEvaluator statementEvaluator;
    private ImportManager importManager;

    public ExpressionEvaluator(StatementEvaluator statementEvaluator)
    {
        setStatementEvaluator(statementEvaluator);
    }

    public ExpressionEvaluator(StatementEvaluator statementEvaluator, ImportManager importManager)
    {
        setStatementEvaluator(statementEvaluator);
        setImportManager(importManager);
    }

    /**
     * Entry point for expression evaluation. This and only this should be the method to be called when trying to
     * evaluate an expression in a context
     * @param expression
     * @param context
     * @return
     */
    public HObject eval(Expression expression, Context context)
    {
        try {
            return visit(expression, context);
        } catch (ReflectionUtilsException e)
        {
            throw new HydraEvaluationException(e);
        }
    }

    public HObject visit(Expression expression, Context arg)
    {
        return expression.accept(this, arg) ;
    }

    @Override
    public HBoolean visit(BooleanLiteralExpr expr, Context arg)
    {
        return HBoolean.getInstance(expr.getValue());
    }

    @Override
    public HObject visit(NullLiteralExpr n, Context arg)
    {
        return HObject.NULL_OBJECT;
    }

    @Override
    public HObject visit(MethodCallExpr n, Context context)
    {
        String methodName = n.getNameAsString();
        List<HObject> hObjects = mapVisitArguments(n, context);
        List<Object> arguments = mapHObjectsToObjects(hObjects);
        Class[] argumentsTypes = mapHObjectsValueTypesToClasses(hObjects);

        if (n.getScope().isPresent())
        {
            HJObject scope = (HJObject) visit(n.getScope().get(), context);
            return invokeMethod(scope.getValueAsObject(), methodName, arguments.toArray(), argumentsTypes, context);
        } else {
            try {
                return invokeImportedStaticMethod(methodName, arguments.toArray(), argumentsTypes);
            } catch (HydraNoSuchMethodException e) {
                return invokeFunction(methodName, hObjects, argumentsTypes, context);
            }
        }
    }

    @Override
    public HObject visit(NameExpr n, Context context)
    {
        String name = n.getNameAsString();
        if (context.isStoredVariable(name))
        {
            return context.getStoredVariable(n.getNameAsString());
        } else if (importManager.isImportedField(name)) {
            StaticField field = importManager.getImportedField(name);
            return wrapJavaObject(field.get());
        }
        throw new HydraNotSuchVariableException(name);
    }

    @Override
    public HObject visit(ObjectCreationExpr n, Context context)
    {
        List<HObject> hObjects = mapVisitArguments(n, context);
        String className = TypeMapper.getTypeFullyQualifiedClassName(n.getType(), importManager);

        if (ReflectionUtils.classExists(className))
        {
            Class clazz = mapTypeToClass(n.getType());
            Object o = instanceClassWithArguments(clazz, hObjects);
            return wrapJavaObject(o);
        } else {
            throw new HydraClassNotFoundException(className);
        }
    }

    /**
     * Returns an instance object of the given class calling the constructor with the given arguments.
     * If no constructor matches the given arguments, an exception is thrown
     * @param clazz
     * @param hArguments
     * @return
     */
    private Object instanceClassWithArguments(Class clazz, List<HObject> hArguments)
    {
        Object[] arguments = mapHObjectsToObjects(hArguments).toArray();
        Class[] argumentsTypes = mapHObjectsValueTypesToClasses(hArguments);
        return ReflectionUtils.invokeMatchingConstructor(clazz, arguments, argumentsTypes);
    }

    @Override
    public HObject visit(ThisExpr n, Context arg) {
        return null;
    }

    @Override
    public HObject visit(SuperExpr n, Context arg) {
        return null;
    }

    @Override
    public HObject visit(UnaryExpr n, Context arg)
    {
        HObject expr = visit(n.getExpression(), arg);
        return evalUnaryExpression(expr, n.getOperator());
    }

    @Override
    public HObject visit(VariableDeclarationExpr n, Context context)
    {
        HObject value = null;

        for(VariableDeclarator variableDeclarator :  n.getVariables() )
        {
            String name = variableDeclarator.getNameAsString();
            value =  variableDeclarator.getInitializer()
                    .map(expression -> visit(expression, context))
                    .orElseGet(() -> HObject.NULL_OBJECT);

            boolean forceNew = false ;

            Type elementType = variableDeclarator.getType(); //.getElementType();
            if ( ! (elementType instanceof UnknownType) )
            {
                forceNew = true ; // if a type was explicitely set, means it's a new variable in the current scope
                value = castValueIfTypesMisMatch(value, elementType);
            }

            context.storeVariable(name, value, forceNew);
        }

        // Given the ambiguous grammar due to gradual typing some assignments for simply-named variables are parsed
        // as VariableDeclarationExprs, so this expression will return the last evaluated value as a result, even when
        // variable declaration expressions should return void

        return value;
    }

    @Override
    public HObject visit(MarkerAnnotationExpr n, Context arg)
    {
        throw new HydraToBeImplementedException("MarkerAnnotationExpr");
    }

    @Override
    public HObject visit(SingleMemberAnnotationExpr n, Context arg) {
        throw new HydraToBeImplementedException("SingleMemberAnnotationExpr");
    }

    @Override
    public HObject visit(NormalAnnotationExpr n, Context arg) {
        throw new HydraToBeImplementedException("NormalAnnotationExpr");
    }

    @Override
    public HObject visit(LambdaExpr expression, Context context)
    {
        if (expression.getParameters().isEmpty())
        {
            HydraSupplier lambda = createHydraSupplier(expression, context);
            return wrapJavaObject(lambda);
        } else {
            HydraFunction lambda = createHydraFunction(expression, context);
            return wrapJavaObject(lambda);
        }
    }

    /**
     * Maps the LambdaExpr node to an anonymous function expecting one or more parameters
     * @param expression
     * @param context
     * @return
     */
    private HydraFunction createHydraFunction(LambdaExpr expression, Context context)
    {
        return new HydraFunction()
        {
            private FunctionCallFrame functionCallFrame = null ;

//            @Override
            public Object apply(Object... vars)
            {
                functionCallFrame = context.openFunctionCallScope();
                for (int i = 0 ; i < vars.length ; i++ )
                    functionCallFrame.storeVariable(expression.getParameter(i).getNameAsString(), wrapJavaObject(vars[i]));
                return apply((Object)null);
            }

            @Override
            public Object apply(Object o)
            {
                if (isNull(functionCallFrame))
                {
                    functionCallFrame = context.openFunctionCallScope();
                    functionCallFrame.storeVariable(expression.getParameter(0).getNameAsString(), wrapJavaObject(o));
                }

                HObject visit = null;
                try {
                    visit = statementEvaluator.visit(expression.getBody(), context);
                } catch (ReturnInterruption i)
                {
                    // Nothing to be done here, just controlling the flow.
                }

                HObject hObject = functionCallFrame.getReturnValue();
                context.closeScope();
                functionCallFrame = null; // In cases where lambda is applied to a Stream, the same instance is used on each element, so we gotta reset its state
                return hObject instanceof HVoid ? visit.getValueAsObject() : hObject.getValueAsObject();
            }
        };
    }

    /**
     * Maps the LambdaExpr node to a anonymous function expecting no parameters
     * @param expression
     * @param context
     * @return
     */

    private HydraSupplier createHydraSupplier(final LambdaExpr expression, final Context context)
    {
        return () -> {
            FunctionCallFrame functionCallFrame = context.openFunctionCallScope();

            HObject visit = statementEvaluator.visit(expression.getBody(), context);
            HObject hObject = functionCallFrame.getReturnValue();
            context.closeScope();

            return visit.getValueAsObject();
        };
    }

    @Override
    public HObject visit(MethodReferenceExpr n, Context arg)
    {
        throw new HydraToBeImplementedException("MethodReferenceExpr");
    }

    @Override
    public HObject visit(TypeExpr n, Context arg)
    {
        throw new HydraToBeImplementedException("TypeExpr");
    }

    @Override
    public HObject visit(FunctionCallExpr n, Context context)
    {
        List<HObject> arguments = mapVisitArguments(n, context);

        // Used to invoke static method if static method invocation got here because of grammar ambiguity
        Object[] javaArguments = mapHObjectsToObjects(arguments).toArray();
        Class[] argumentsTypes = mapHObjectsValueTypesToClasses(arguments);
        String functionName = n.getName().getIdentifier();

        // due to grammar ambiguity, function call expressions might turn out to be static method invocations, therefore
        // this case is also taken care of here.

        try {
            return invokeFunction(functionName, arguments, argumentsTypes, context);
        } catch (HydraNoSuchMethodException e) {
            return invokeImportedStaticMethod(functionName, javaArguments, argumentsTypes);
        }
    }

    /**
     * Given a list of Hydra values it returns an array of their wrapped java objects
     * @param arguments
     * @return
     */
    private Class[] mapHObjectsValueTypesToClasses(List<HObject> arguments)
    {
        int size = arguments.size();
        Class[] result = new Class[size];
        for(int i = 0; i < size; i++)
            result[i] = arguments.get(i).getValueClass();
        return result;
    }

    @Override
    public HObject visit(AmbiguousNameExpr ambiguousNameExpr, Context context)
    {
        Expression expr = reclassifiyContextuallyAmbiguousName(ambiguousNameExpr, context);
        return visit(expr, context);
    }

    /**
     * Refer to AmbiguousNameClassifier.reClassifyExpression . This is just a shortcut method.
     * @param ambiguousNameExpr
     * @param context
     * @return
     */
    private Expression reclassifiyContextuallyAmbiguousName(AmbiguousNameExpr ambiguousNameExpr, Context context)
    {
        return AmbiguousNameClassifier.reClassifyExpression(ambiguousNameExpr, importManager, context);
    }

    public void setImportManager(ImportManager importManager) {
        this.importManager = importManager;
    }

    /**
     * Invokes a method in the given object (or an static method if scope is a class) by reflection and returns its
     * result wrapped in an Hydra value type
     * @param scope
     * @param methodName
     * @param arguments
     * @param argumentsTypes
     * @param context
     * @return
     */
    private HObject invokeMethod(Object scope, String methodName, Object[] arguments, Class[] argumentsTypes, Context context)
    {
        final Method method = getMethod(scope, methodName, argumentsTypes, context);

        if (method == null)
            throw new HydraNoSuchMethodException(methodName, argumentsTypes);

        InvocationResult invocationResult = ReflectionUtils.invokeMethod(method, scope, arguments);
        return invocationResult.toHObject();
    }

    /**
     * Returns the method declared in the scope class that matches the name and arguments types given.
     * If none matches returns null.
     * @param scope
     * @param methodName
     * @param argumentsTypes
     * @param context
     * @return
     */
    private Method getMethod(Object scope, String methodName, Class[] argumentsTypes, Context context)
    {
        return ReflectionUtils.getMethodForObject(scope, methodName, argumentsTypes);
    }

    /**
     * Returns the resulting value of the invocation of an imported static method with the given arguments, wrapped in
     * a Hydra value object. If the name and arguments don't match an imported static method an exception is thrown.
     * @param methodName
     * @param arguments
     * @param argumentsTypes
     * @return
     */
    private HObject invokeImportedStaticMethod(String methodName, Object[] arguments, Class[] argumentsTypes)
    {
        if (importManager.isImportedMethod(methodName))
        {
            StaticMethod staticMethod = importManager.getImportedMethod(methodName);
            InvocationResult result = staticMethod.tryInvoke(arguments, argumentsTypes);
            return result.toHObject();
        }
        throw new HydraNoSuchMethodException(methodName, argumentsTypes);
    }

    /**
     * Returns the resulting value of the invocation of the function with the given arguments, wrapped in
     * a Hydra value object. If the name and arguments don't match a declared function, an exception is thrown
     * @param functionName
     * @param arguments
     * @param argumentsTypes
     * @param context
     * @return
     */
    private HObject invokeFunction(String functionName, List<HObject> arguments, Class[] argumentsTypes, Context context)
    {
        FunctionDeclaration functionDeclaration = getMatchingFunctionDeclaration(functionName, argumentsTypes, context);

        if (isNull(functionDeclaration))
            throw new HydraNoSuchMethodException(functionName, argumentsTypes);

        if (FunctionDeclarationUtils.functionIsSynchronized(functionDeclaration))
        {
            synchronized (functionDeclaration) {
                return evalFunctionCall(functionDeclaration, arguments, context);
            }
        } else {
            return evalFunctionCall(functionDeclaration, arguments, context);
        }
    }

    /**
     * Performs evaluation of a function declaration in a given context passing it the given arguments.
     * @param functionDeclaration
     * @param arguments
     * @param context
     * @return
     */
    private HObject evalFunctionCall(FunctionDeclaration functionDeclaration, List<HObject> arguments, Context context)
    {
        FunctionCallFrame functionCallFrame = context.openFunctionCallScope();

        NodeList<Parameter> parameters = functionDeclaration.getParameters();
        functionCallFrame.storeParameters(parameters, arguments);

        BlockStmt stmt = functionDeclaration.getBody().get();
        StatementEvaluator statementEvaluator = getStatementEvaluator();

        try {
            statementEvaluator.visit(stmt, context);
        } catch (ReturnInterruption i)
        {
            // Nothing to be done here, just controlling the flow.
        }

        HObject hObject = functionCallFrame.getReturnValue();
        context.closeScope();

        if ( ! (functionDeclaration.getType() instanceof UnknownType))
        {
            Class returnTypeClass = mapTypeToClass(functionDeclaration.getType());
            hObject = castValueIfTypesMisMatch(hObject, returnTypeClass);
        }
        return hObject;
    }

    @Override
    public HChar visit(CharLiteralExpr expr, Context context)
    {
        return new HChar(expr.getCharValue());
    }

    @Override
    public HJObject visit(StringLiteralExpr expr, Context context)
    {
        return wrapJavaObject(expr.getValue());
    }

    @Override
    public HDouble visit(DoubleLiteralExpr expr, Context context)
    {
        return new HDouble(expr.getDoubleValue());
    }

    @Override
    public HInt visit(IntegerLiteralExpr expr, Context context)
    {
        return new HInt(expr.getIntValue());
    }

    @Override
    public HLong visit(LongLiteralExpr expr, Context context)
    {
        return new HLong(expr.getLongValue());
    }

    @Override
    public HObject visit(BinaryExpr binaryExpr, Context context)
    {
        HObject left = visit(binaryExpr.getLeft(), context);
        HObject right = visit(binaryExpr.getRight(), context);

        return  evalBinaryExpression(binaryExpr.getOperator(), left, right);
    }

    @Override
    public HObject visit(CastExpr n, Context context)
    {
        Class clazz = mapTypeToClass(n.getType());
        HObject result = visit(n.getExpression(), context);
        return result.cast(clazz);
    }

    @Override
    public HObject visit(ClassExpr n, Context arg)
    {
        Type type = n.getType();
        Class clazz = mapTypeToClass(type);
        return wrapJavaObject(clazz);
    }

    @Override
    public HObject visit(ConditionalExpr n, Context context)
    {
        HObject condition = visit(n.getCondition(), context);

        return  (condition.getBooleanValue())
            ?   visit(n.getThenExpr(), context)
            :   visit(n.getElseExpr(), context);
    }

    @Override
    public HObject visit(EnclosedExpr n, Context context) {
        return visit(n.getInner().get(), context);
    }

    @Override
    public HObject visit(FieldAccessExpr n, Context context)
    {
        String name = n.getNameAsString() ;
        HObject holderObject = visit(n.getScope().get(), context);
        Object fieldObject = ReflectionUtils.getFieldValue(holderObject.getValueAsObject(), name);

        return wrapJavaObject( fieldObject ) ;
    }

    @Override
    public HObject visit(InstanceOfExpr n, Context context)
    {
        Class clazz = mapTypeToClass(n.getType());
        HObject result = visit(n.getExpression(), context);
        return result.isInstanceOf(clazz);
    }

    @Override
    public HObject visit(ArrayAccessExpr n, Context context)
    {
        HObject index = visit(n.getIndex(), context);
        HObject array = visit(n.getName(), context);
        return wrapJavaObject(Array.get(array.getValueAsObject(), index.getIntValue()));
    }

    @Override
    public HObject visit(ArrayCreationExpr n, Context context)
    {
        Object array ;
        Class clazz = mapTypeToClass(n.getElementType());
        if (!n.getInitializer().isPresent())
        {
            int[] levels = getArrayLevelsDimensions(n, context);
            array = Array.newInstance(clazz, levels);
            return wrapJavaObject(array);
        } else {
            HObject initializer = visit(n.getInitializer().get(), context);
            int[] levels = getArrayLevelsDimensions((Object[]) initializer.getValueAsObject());
            array = Array.newInstance(clazz, levels);
            initializeArray(array, initializer.getValueAsObject());
        }

        return wrapJavaObject(array);
    }

    /**
     * Initializes the target array with the given values in a recursive strategy.
     * This might be a very pooraly-performant operation, but so far I haven't figured out a faster way to do it.
     * @param targetArray
     * @param arrayInitializerValues
     */
    private void initializeArray(Object targetArray, Object arrayInitializerValues)
    {
        int arrayLenght = Array.getLength(arrayInitializerValues);
        if (arrayLenght == 0) return ;
        Object firstElement = Array.get(arrayInitializerValues, 0);

        if (firstElement.getClass().isArray())
            for (int i = 0; i <  arrayLenght; i++)
                initializeArray(Array.get(targetArray, i), Array.get(arrayInitializerValues, i));
        else
            for (int i = 0; i < arrayLenght ; i++)
                Array.set(targetArray, i, Array.get(arrayInitializerValues, i));

    }

    /**
     * Calculates the dimensions for each level of the given array
     * @param array
     * @return
     */
    private int[] getArrayLevelsDimensions(Object[] array)
    {
        Object arrayReference = array;
        List<Integer> dims = new ArrayList();
        while (arrayReference instanceof Object[])
        {
            int len = Array.getLength(arrayReference);
            dims.add(len);
            arrayReference = Array.get(arrayReference, 0);
        }
        return dims.stream().mapToInt(i->i).toArray();
    }

    /**
     * Get the dimensions declared in an array creation expression.
     * @param n
     * @param context
     * @return
     */
    private int[] getArrayLevelsDimensions(ArrayCreationExpr n, Context context)
    {
        int size = n.getLevels().size();
        int[] levels = new int[size] ;
        for (int i = 0 ; i < size ; i++)
        {
            ArrayCreationLevel level = n.getLevels().get(i);
            int dimension = getArrayCreationLevelDimension(level, context);
            levels[i] = dimension;
        }
        return levels;
    }

    /**
     * Given a ArrayCreationLevel node it returns the value of the expression that represents the level dimension, or 0
     * if the expression is empty.
     * @param level
     * @param context
     * @return
     */
    private Integer getArrayCreationLevelDimension(ArrayCreationLevel level, Context context)
    {
        return level.getDimension()
                .map(expression -> visit(expression, context))
                .map(hObject -> hObject.getIntValue())
                .orElse(0);
    }

    @Override
    public HObject visit(ArrayInitializerExpr n, Context context)
    {
        int size = n.getValues().size();

        Object[] initializer = new Object[size];
        for (int i = 0 ; i < size; i++)
            initializer[i] = visit(n.getValues().get(i), context).getValueAsObject();

        return wrapJavaObject(initializer);
    }

    @Override
    public HObject visit(AssignExpr expr, Context context)
    {
        LeftHandSide target = new LeftHandSide(expr.getTarget());
        AssignExpr.Operator operator = expr.getOperator();

        if (operator == AssignExpr.Operator.ASSIGN)
            return evalSimpleAssign(target, expr.getValue(), context);
        else
            return evalCompoundAssign(target, expr.getValue(), operator, context);
    }

    /**
     * Performs a compund assignment where the lhs and rhs are operated and the result is assigned to the target expression
     * @param targetExpr
     * @param rhsExpr
     * @param operator
     * @param context
     * @return
     */
    private HObject evalCompoundAssign(LeftHandSide targetExpr, Expression rhsExpr, AssignExpr.Operator operator, Context context)
    {
        HObject result ;
        if (targetExpr.isAmbiguousName())
            disambiguateLeftHandExpr(targetExpr);

        if ( targetExpr.isName() )
        {
            HObject lhs = visit(targetExpr.getExpressionAsName(), context);
            HObject rhs = visit(rhsExpr, context);
            result = evalBinaryExpression(operator, lhs, rhs);

            String targetName = targetExpr.getExpressionAsName().getNameAsString();
            context.storeVariable(targetName, result);
        } else if ( targetExpr.isFieldAccess() )
        {
            FieldAccessExpr field = targetExpr.getExpressionAsFieldAccess();
            Expression scope = targetExpr.getExpressionAsFieldAccess().getScope().get();
            HObject fieldHolder = visit(scope, context);

            Object lhsObject = ReflectionUtils.getFieldValue(fieldHolder.getValueAsObject(), field.getNameAsString());
            HObject lhs = wrapJavaObject(lhsObject);
            HObject rhs = visit(rhsExpr, context);
            result = evalBinaryExpression(operator, lhs, rhs);

            ReflectionUtils.setFieldValue(fieldHolder.getValueAsObject(), field.getNameAsString(), result.getValueAsObject());
        } else if ( targetExpr.isArrayAccess() )
        {
            ArrayAccessExpr lhsExpr = targetExpr.getExpressionAsArrayAccess();
            HObject subExpr = visit(lhsExpr.getName(), context);
            HObject indexObject = visit(lhsExpr.getIndex(), context);

            HObject lhs = HObject.valueTypeToHObject(subExpr.getClass().getComponentType(), Array.get(subExpr.getValueAsObject(), indexObject.getIntValue()));
            HObject rhs = visit(rhsExpr, context);
            result = evalBinaryExpression(operator, lhs, rhs);

            Array.set(subExpr.getValueAsObject(), indexObject.getIntValue(), result.getValueAsObject());
        } else {
            throw new HydraCompoundAssignmentException(targetExpr.getExpression(), rhsExpr, operator);
        }
        return result;
    }

    /**
     * performs a unary expression based on the given operator
     * @param value
     * @param operator
     * @return
     */
    private HObject evalUnaryExpression(HObject value, UnaryExpr.Operator operator)
    {
        switch (operator)
        {
            case LOGICAL_COMPLEMENT:
                return value.negatedHValue();

            case BITWISE_COMPLEMENT:
                return  value.inversedHValue();
            case MINUS:
                return  value.negativeHValue();
            case PLUS:
                return  value.positiveHValue();

            case PREFIX_DECREMENT:
                return  value.preDecrement();
            case POSTFIX_DECREMENT:
                return  value.posDecrement();
            case PREFIX_INCREMENT:
                return  value.preIncrement();
            case POSTFIX_INCREMENT:
                return  value.posIncrement();
            default :
                return null; // FIXME
        }
    }

    /**
     * Performs a binary expression between to values based on a binary expression operator
     * @param operator
     * @param pleft
     * @param pright
     * @return
     */
    private HObject evalBinaryExpression(BinaryExpr.Operator operator, HObject pleft, HObject pright)
    {
        switch (operator)
        {
            case PLUS:
                return pleft.plus(pright);
            case MULTIPLY:
                return pleft.times(pright);
            case MINUS:
                return pleft.minus(pright);
            case DIVIDE:
                return pleft.divide(pright);
            case REMAINDER:
                return pleft.remainder(pright);

            case BINARY_AND:
                return pleft.binAnd(pright);
            case BINARY_OR:
                return pleft.binOr(pright);
            case LEFT_SHIFT:
                return pleft.lShift(pright);
            case SIGNED_RIGHT_SHIFT:
                return pleft.rSignedShift(pright);
            case UNSIGNED_RIGHT_SHIFT:
                return pleft.rUnsignedShift(pright);

            case EQUALS:
                return pleft.eq(pright);
            case NOT_EQUALS:
                return pleft.neq(pright);
            case GREATER:
                return pleft.gt(pright);
            case LESS:
                return pleft.lt(pright);
            case LESS_EQUALS:
                return pleft.leq(pright);
            case GREATER_EQUALS:
                return pleft.geq(pright);

            case AND:
                return pleft.and(pright) ;
            case OR:
                return pleft.or(pright) ;
            case XOR:
                return pleft.xor(pright) ;
            default:
                return null ;
        }
    }

    /**
     * Performs a binary expression between to values based on a compound assignment operator
     * @param operator
     * @param pleft
     * @param pright
     * @return
     */
    private HObject evalBinaryExpression(AssignExpr.Operator operator, HObject pleft, HObject pright)
    {
        switch (operator)
        {
            case PLUS:
                return pleft.plus(pright);
            case MULTIPLY:
                return pleft.times(pright);
            case MINUS:
                return pleft.minus(pright);
            case DIVIDE:
                return pleft.divide(pright);
            case REMAINDER:
                return pleft.remainder(pright);

            case LEFT_SHIFT:
                return pleft.lShift(pright);
            case SIGNED_RIGHT_SHIFT:
                return pleft.rSignedShift(pright);
            case UNSIGNED_RIGHT_SHIFT:
                return pleft.rUnsignedShift(pright);

            case AND:
                return pleft.and(pright) ;
            case OR:
                return pleft.or(pright) ;
            case XOR:
                return pleft.xor(pright) ;
            default:
                return null ;
        }
    }

    /**
     * Assignes the resulting value of evaluating valueExpr to the target in the given context.
     * @param target
     * @param valueExpr
     * @param context
     * @return
     */
    private HObject evalSimpleAssign(LeftHandSide target, Expression valueExpr, Context context)
    {
        HObject value = visit(valueExpr, context);

        if (target.isAmbiguousName())
            disambiguateLeftHandExpr(target);

        if ( target.isName() )
        {
            String targetName = target.getExpressionAsName().getNameAsString();
            context.storeVariable(targetName, value);
        } else if ( target.isFieldAccess() )
        {
            FieldAccessExpr field = target.getExpressionAsFieldAccess();
            Expression scope = target.getExpressionAsFieldAccess().getScope().get();
            HObject fieldHolder = visit(scope, context);

            ReflectionUtils.setFieldValue(fieldHolder.getValueAsObject(), field.getNameAsString(), value.getValueAsObject());
        } else if ( target.isArrayAccess() )
        {
            ArrayAccessExpr targetExpr = target.getExpressionAsArrayAccess();
            HObject targetObject = visit(targetExpr.getName(), context);
            HObject indexObject = visit(targetExpr.getIndex(), context);
            Array.set(targetObject.getValueAsObject(), indexObject.getIntValue(), value.getValueAsObject());
        }

        return value;
    }

    /**
     * Takes a LeftHandSide whose Expression is an AmbiguousNameExpr and tries to disambiguate it into either a :
     * A NameExpr if the name is just an identifier and it's not the name of a statically imported field
     * A FieldAccessExpr whose scope is a ClassExpr if the name is one of a statically imported field
     * A FieldAccessExpr otherwise.
     * Notice that the disambiguation is not contextual here as there are no checks if a variable exists in either the
     * first or the latter case. This kind of checks will be performed on assignment evaluation time.
     * @param target
     */
    private void disambiguateLeftHandExpr(LeftHandSide target)
    {
        Name name = target.getExpressionAsAmbiguousName().getName();
        if ( ! name.getQualifier().isPresent() )
        {
            if ( importManager.isImportedField(name.getIdentifier()) )
            {
                name = importManager.getImportedFieldName(name.getIdentifier());
                Expression expr = nameToImportedFieldAccessExpr(name);
                target.setExpression(expr);
            } else {
                target.setExpression(new NameExpr(name.getIdentifier()));
            }
        } else {
            target.setExpression(nameToFieldAccessExpr(name));
        }
     }

    /**
     * Returns a FieldAccessExpr out of the given name, pretty straightforward.
     * @param name
     * @return
     */
    private FieldAccessExpr nameToFieldAccessExpr(Name name)
    {
        return NameUtils.nameToFieldAccessExpr(name);
    }

    /**
     * Refer to NameUtils.nameToImportedFieldAccessExpression(name). This is just a shortcut method
     * @param name
     * @return
     */
    private Expression nameToImportedFieldAccessExpr(Name name)
    {
        return NameUtils.nameToImportedFieldAccessExpression(name);
    }

    /**
     * Returns a list of the values resulting of evaluating the arguments of de given node in this context
     * @param n
     * @param context
     * @return
     */
    private List<HObject> mapVisitArguments(NodeWithArguments<? extends Expression> n, Context context)
    {
        List<HObject> result = new ArrayList();
        for (Expression expression : n.getArguments())
            result.add(visit(expression,context));
        return result;
    }

    /**
     * Refer to FunctionDeclarationUtils.getMatchingFunctionDeclaration(). This is just a shortcut method.
     * @param functionName
     * @param argumentsTypes
     * @param context
     * @return
     */
    private FunctionDeclaration getMatchingFunctionDeclaration(String functionName, Class[] argumentsTypes, Context context)
    {
        return FunctionDeclarationUtils.getMatchingFunctionDeclaration(functionName, argumentsTypes, importManager, context);
    }

    /**
     * Same as castValueIfTypesMisMatch(HObject value, Class declaredTypeClass), but it first maps de declaredType node
     * to a Java Class
     * @param value
     * @param declaredType
     * @return
     */
    private HObject castValueIfTypesMisMatch(HObject value, Type declaredType)
    {
        Class declaredTypeClass = mapTypeToClass(declaredType);
        return castValueIfTypesMisMatch(value, declaredTypeClass);
    }

    /**
     * if the type of the value is not assignable to the declaredTypeClass (this includes widening or narrowing of
     * primitive representations) tries to perform a cast operation on the value to the declaredTypeClass.
     * If the value is null then no cast operation is performed.
     * @param value
     * @param declaredTypeClass
     * @return
     */
    private HObject castValueIfTypesMisMatch(HObject value, Class declaredTypeClass)
    {
        Object valueObject = value.getValueAsObject();
        if (declaredTypeClass == void.class)
        {
            if ( isNull(valueObject) )
                return HObject.HVOID;
            else
                throw new ClassCastException("Cannot cast " + valueObject.getClass().getName() + " to void");
        }

        if (isNull(valueObject)) // if value is null there is nothing to cast, null fits every type
            return value;

        if (value.isPrimitive() && ! declaredTypeClass.isPrimitive()
                || ( ! value.isPrimitive() && ! declaredTypeClass.isAssignableFrom(valueObject.getClass()) ))
        {
            value = value.cast(declaredTypeClass);
        }
        return value;
    }

    /**
     * Given a list of HObjects it returns a list containing its wrapped objects
     * @param hObjects
     * @return
     */
    private List<Object> mapHObjectsToObjects(List<HObject> hObjects)
    {
        List<Object> result = new ArrayList();
        for (HObject hObject : hObjects)
            result.add(hObject.getValueAsObject());
        return result;
    }

    /**
     * Maps a type node to a java class (Shortcut to TypeMapper.map())
     * @param type
     * @return
     */
    private Class mapTypeToClass(Type type)
    {
        return getTypeMapper().map(type, importManager);
    }

    public void setStatementEvaluator(StatementEvaluator statementEvaluator)
    {
        this.statementEvaluator = statementEvaluator;
    }

    public TypeMapper getTypeMapper()
    {
        return TypeMapper.getInstance();
    }

    public StatementEvaluator getStatementEvaluator()
    {
        return statementEvaluator;
    }

    /**
     * Takes an Object and wrapps it into a HJObject
     * @param o
     * @return
     */
    private HJObject wrapJavaObject(Object o)
    {
        return new HJObject(o);
    }

}
