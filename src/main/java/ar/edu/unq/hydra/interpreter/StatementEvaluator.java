package ar.edu.unq.hydra.interpreter;

import ar.edu.unq.hydra.ast.HydraScript;
import ar.edu.unq.hydra.ast.ImportDeclaration;
import ar.edu.unq.hydra.ast.NodeList;
import ar.edu.unq.hydra.ast.body.FunctionDeclaration;
import ar.edu.unq.hydra.ast.body.Parameter;
import ar.edu.unq.hydra.ast.body.TypeDeclaration;
import ar.edu.unq.hydra.ast.expr.Expression;
import ar.edu.unq.hydra.ast.modules.*;
import ar.edu.unq.hydra.ast.stmt.*;
import ar.edu.unq.hydra.ast.type.Type;
import ar.edu.unq.hydra.ast.visitor.StatementGenericVisitor;
import ar.edu.unq.hydra.interpreter.context.Context;
import ar.edu.unq.hydra.interpreter.exceptions.HydraEvaluationException;
import ar.edu.unq.hydra.interpreter.exceptions.HydraInterruptedException;
import ar.edu.unq.hydra.interpreter.exceptions.HydraThrowStmtWrappingException;
import ar.edu.unq.hydra.interpreter.exceptions.HydraToBeImplementedException;
import ar.edu.unq.hydra.interpreter.import_manager.ImportManager;
import ar.edu.unq.hydra.interpreter.interruption_exceptions.BreakInterruption;
import ar.edu.unq.hydra.interpreter.interruption_exceptions.ContinueInterruption;
import ar.edu.unq.hydra.interpreter.interruption_exceptions.ReturnInterruption;
import ar.edu.unq.hydra.interpreter.values.HJObject;
import ar.edu.unq.hydra.interpreter.values.HObject;
import org.apache.commons.lang3.ClassUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * Created by Daniel Wyrytowski
 */
public class StatementEvaluator extends StatementGenericVisitor<HObject, Context> implements HydraExceptionMulticaster
{
    private ExpressionEvaluator expressionEvaluator;
    private ImportManager importManager;
    private ThreadGroup threadGroup ;
    private ThreadsMonitor threadsMonitor ;
    private List<HydraExceptionListener> exceptionListeners;
    private AtomicBoolean evalInterrupted;

    public StatementEvaluator()
    {
        exceptionListeners = new ArrayList<>();
        evalInterrupted = new AtomicBoolean(false);
        threadsMonitor = new ThreadsMonitor();
        threadGroup = new ThreadGroup("hydra");
        setExpressionEvaluator(new ExpressionEvaluator(this));
        setImportManager(new ImportManager());
        loadDefaultImports();
    }

    public HObject eval(HydraScript ast)
    {
        evalInterrupted.set(false);
        final Semaphore evalStarted = new Semaphore(0);
        final Thread main = new Thread(threadGroup, () -> {
            threadsMonitor.reset(); ;
            threadsMonitor.add();
            try {
                ast.accept(this, new Context());
            } catch (Throwable e)
            {
                multicastException(e);
            } finally {
                evalStarted.release();
                threadsMonitor.rem();
            }
        });

        main.start();

        try {
            evalStarted.acquire();
        } catch (InterruptedException e) {
            throw new HydraInterruptedException(e);
        }

        while (threadsMonitor.anyRunning())
        {
            synchronized (threadsMonitor) {
                try {
                    threadsMonitor.wait();
                } catch (InterruptedException e) {
                    throw new HydraInterruptedException(e);
                }
            }
        }

        return HObject.HVOID;
    }

    public void interrupt()
    {
        evalInterrupted.set(true);
        threadsMonitor.reset();
    }

    /**
     * Type declaration interpretation is not yet implemented, so this method does nothing.
     * @param types
     * @param context
     */
    private void performTypeDeclarations(List<TypeDeclaration<?>> types, Context context)
    {
        if (isNull(types) || types.isEmpty() ) return ;

        throw new HydraToBeImplementedException("performTypeDeclarations");
    }

    /**
     * Adds to the current context the given function declarations
     * @param functions
     * @param context
     */
    private void performFunctionDeclarations(List<FunctionDeclaration> functions, Context context)
    {
        functions.stream().forEach(functionDeclaration -> context.addFunctionDeclaration(functionDeclaration) );
    }

    @Override
    public HObject visit(HydraScript script, Context context)
    {
        context.openScope();

        performImportDeclarations(script.getImports());
        performFunctionDeclarations(script.getFunctions(), context);
        performTypeDeclarations(script.getTypes(), context);

        for (Statement stmt : script.getStmts())
            visit(stmt, context);

        context.closeScope();

        return HObject.HVOID;
    }

    protected void loadDefaultImports()
    {
        performImportDeclarations(DefaultImports.getImports());
    }

    private void performImportDeclarations(List<ImportDeclaration> imports)
    {
        imports.forEach(importDeclaration -> getImportManager().addImport(importDeclaration));
    }

    private ImportManager getImportManager()
    {
        return importManager ;
    }

    public HObject visit(Statement stmt, Context context)
    {
        if (evalInterrupted.get())
            throw new HydraInterruptedException();
        return stmt.accept(this, context);
    }

    @Override
    public HObject visit(ExplicitConstructorInvocationStmt stmt, Context context) {
        return HObject.HVOID;
    }

    @Override
    public HObject visit(AssertStmt stmt, Context context) {
        return HObject.HVOID;
    }

    @Override
    public HObject visit(BlockStmt stmt, Context context)
    {
        context.openScope();
        for (Statement currStmt : stmt.getStatements())
        {
            visit(currStmt, context);
        }
        context.closeScope();
        return HObject.HVOID;
    }

    @Override
    public HObject visit(LabeledStmt stmt, Context context)
    {
        return HObject.HVOID;
    }

    @Override
    public HObject visit(LocalClassDeclarationStmt n, Context arg)
    {
        return HObject.HVOID;
    }

    @Override
    public HObject visit(EmptyStmt stmt, Context context)
    {
        return HObject.HVOID;
    }

    @Override
    public HObject visit(ExpressionStmt stmt, Context context)
    {
        return evalExpression(stmt.getExpression(), context) ;
    }

    @Override
    public HObject visit(SwitchStmt stmt, Context context)
    {
        final HObject selectorValue = evalExpression(stmt.getSelector(), context);
        boolean found = false ;

        context.openScope();
        try {
            for (SwitchEntryStmt switchEntryStmt : stmt.getEntries())
            {
                found = evalSwitchEntryStatementIfFound(found, selectorValue, switchEntryStmt, context);
            }
        } catch (BreakInterruption i) {
            // nothing to do here
        }
        context.closeScope();

        return HObject.HVOID;
    }

    public boolean evalSwitchEntryStatementIfFound(boolean found, HObject selectorValue, SwitchEntryStmt stmt, Context context)
    {
        Supplier<Boolean> labelMatchesSelectorOrDefault = () -> {
            if (!stmt.getLabel().isPresent())
                return true ;
            HObject pright = evalExpression(stmt.getLabel().get(), context);
            return selectorValue.eq(pright).getBooleanValue();
        };

        if (found || (found = labelMatchesSelectorOrDefault.get()))
            for(Statement currentStmt : stmt.getStatements())
                visit(currentStmt, context);

        return found;
    }

    @Override
    public HObject visit(SwitchEntryStmt stmt, Context context)
    {
        return HObject.HVOID;
    }

    @Override
    public HObject visit(BreakStmt stmt, Context context)
    {
        throw stmt.getLabel().isPresent()
                ? BreakInterruption.getInstance(stmt.getLabel().get().asString())
                : BreakInterruption.getInstance();
    }

    @Override
    public HObject visit(ReturnStmt stmt, Context context)
    {
        Optional<Expression> expressionOptional = stmt.getExpression();
        expressionOptional.map(expression -> evalExpression(expression, context))
            .ifPresent(hObject -> context.getTopFunctionCallScope().setReturnValue(hObject) );

        throw ReturnInterruption.getInstance();
    }

    @Override
    public HObject visit(IfStmt stmt, Context context)
    {
        HObject cond = evalExpression(stmt.getCondition(), context);

        if (cond.getBooleanValue())
        {
            visit(stmt.getThenStmt(), context);
        } else {
            stmt.getElseStmt().ifPresent(statement -> visit(statement, context));
        }

        return HObject.HVOID;
    }

    @Override
    public HObject visit(WhileStmt stmt, Context context)
    {
        context.openScope();
        while (evalExpression(stmt.getCondition(), context).getBooleanValue())
        {
            try {
                visit(stmt.getBody(), context);
            } catch (BreakInterruption breakInterruption) {
                break;
            } catch (ContinueInterruption continueInterruption) {
                continue ;
            }
        }
        context.closeScope();

        return HObject.HVOID;
    }

    @Override
    public HObject visit(ModuleRequiresStmt n, Context arg)
    {
        return HObject.HVOID;
    }

    @Override
    public HObject visit(ModuleExportsStmt n, Context arg)
    {
        return HObject.HVOID;
    }

    @Override
    public HObject visit(ModuleProvidesStmt n, Context arg)
    {
        return HObject.HVOID;
    }

    @Override
    public HObject visit(ModuleUsesStmt n, Context arg)
    {
        return HObject.HVOID;
    }

    @Override
    public HObject visit(ModuleOpensStmt n, Context arg)
    {
        return HObject.HVOID;
    }

    @Override
    public HObject visit(ContinueStmt stmt, Context context)
    {
        throw stmt.getLabel().isPresent()
            ? ContinueInterruption.getInstance(stmt.getLabel().get().asString())
            : ContinueInterruption.getInstance();

    }

    @Override
    public HObject visit(DoStmt stmt, Context context)
    {
        do {
            try {
                visit(stmt.getBody(), context);
            } catch (ContinueInterruption i)
            {
                continue ;
            } catch (BreakInterruption i )
            {
                break ;
            }
        } while (evalExpression(stmt.getCondition(),context).getBooleanValue());

        return HObject.HVOID;
    }

    @Override
    public HObject visit(ForeachStmt stmt, Context context)
    {
        context.openScope();

        Iterable<?> iterable = (Iterable)  evalExpression(stmt.getIterable(),context).getValueAsObject();
        String name = stmt.getVariable().getVariable(0).getNameAsString();
        context.storeVariable(name, HObject.NULL_OBJECT, true);

        for (Object o : iterable)
        {
            HJObject value = new HJObject(o);
            context.storeVariable(name, value);
            try {
                visit(stmt.getBody(), context);
            } catch (ContinueInterruption i)
            {
                continue ;
            } catch (BreakInterruption i)
            {
                break ;
            }
        }
        context.closeScope();

        return HObject.HVOID;
    }

    @Override
    public HObject visit(ForStmt stmt, Context context)
    {
        context.openScope();

        for (Expression expr : stmt.getInitialization() )
            evalExpression(expr, context);

        Expression compareExpression = stmt.getCompare().get();
        while (evalExpression(compareExpression, context).getBooleanValue())
        {
            try {
                visit(stmt.getBody(),context);
            } catch (BreakInterruption breakInterruption) {
                break;
            } catch (ContinueInterruption continueInterruption) {
                continue ;
            }
            for (Expression expr : stmt.getUpdate() )
                evalExpression(expr, context);
        }
        context.closeScope();

        return HObject.HVOID;
    }

    @Override
    public HObject visit(ThrowStmt stmt, Context context)
    {
        throw new HydraThrowStmtWrappingException((Throwable) evalExpression(stmt.getExpression(), context).getValueAsObject());
    }

    @Override
    public HObject visit(SynchronizedStmt stmt, Context context)
    {
        HObject expression = evalExpression(stmt.getExpression(), context);
        synchronized (expression.getValueAsObject())
        {
            return visit(stmt.getBody(), context);
        }
    }

    @Override
    public HObject visit(TryStmt stmt, Context context)
    {
        try {
            visit(stmt.getTryBlock().get(), context);
        } catch (HydraThrowStmtWrappingException exception)
        {
            Throwable cause = exception.getCause();
            CatchClause catchClause = getFittestCatchClause(stmt.getCatchClauses(), cause);

            if (isNull(catchClause))
                throw exception;

            context.openScope();
            String parameterName = catchClause.getParameter().getName().asString();
            context.storeVariable(parameterName, new HJObject(cause), true);

            visit(catchClause.getBody(), context);

            context.closeScope();
        } finally {
            if (stmt.getFinallyBlock().isPresent())
                visit(stmt.getFinallyBlock().get(), context);
        }
        return HObject.HVOID;
    }

    /**
     * Given a list of catch clauses and a throwable, it looks for the best match among the catched types if any,
     * and returns that CatchClause. If none matches it returns null.
     * @param catchClauses
     * @param cause
     * @return
     */
    private CatchClause getFittestCatchClause(NodeList<CatchClause> catchClauses, Throwable cause)
    {
        CatchClause bestMatchClause = null;
        Class bestMatchClass = null;
        for (CatchClause clause : catchClauses)
        {
            Class throwableTypeClass = mapTypeToClass(clause.getParameter().getType());
            if (ClassUtils.isAssignable(cause.getClass(), throwableTypeClass))
            {
                if (isNull(bestMatchClass))
                {
                    bestMatchClass = throwableTypeClass;
                    bestMatchClause = clause;
                } else if (ClassUtils.isAssignable(throwableTypeClass, bestMatchClass)) {
                    bestMatchClass = throwableTypeClass;
                    bestMatchClause = clause;
                }
            }
        }
        return bestMatchClause;
    }

    public HObject visit(ScriptBlockStmt n, Context context)
    {
        context.openScope();

        n.getFunctions().forEach(functionDeclaration -> context.addFunctionDeclaration(functionDeclaration));

        for (Statement statement : n.getStatements())
        {
            statement.accept(this, context);
        }
        context.closeScope();

        return HObject.HVOID;
    }

    @Override
    public HObject visit(ProcessCreationStmt processCreationStmt, Context context)
    {
        final Context processContext = new Context();
        processContext.openScope();
        if (nonNull(processCreationStmt.getParameters()))
        {
            boolean forceNew = true ;
            for(Parameter parameter : processCreationStmt.getParameters())
            {
                final String parameterName = parameter.getNameAsString();
                processContext.storeVariable(parameterName, context.getStoredVariable(parameterName), forceNew);
            }
        }

        Runnable task = () -> {
            try {
                visit(processCreationStmt.getStatement(), processContext);
            } catch (Throwable e) {
                multicastException(e);
            } finally {
                processContext.closeScope();
                threadsMonitor.rem();
            }
        };
        Thread thread = new Thread(threadGroup, task);

        threadsMonitor.add();
        thread.start();

        return HObject.HVOID;
    }

    @Override
    public HObject visit(ThreadCreationStmt threadCreationStmt, Context context)
    {
        final Context threadContext = context.forkContext();
        threadContext.openScope();
        if (nonNull(threadCreationStmt.getParameters()))
        {
            boolean forceNew = true ;
            for(Parameter parameter : threadCreationStmt.getParameters())
            {
                final String parameterName = parameter.getNameAsString();
                threadContext.storeVariable(parameterName, context.getStoredVariable(parameterName), forceNew);
            }
        }

        Runnable task = () -> {
            try {
                visit(threadCreationStmt.getStatement(), threadContext);
            } catch (Throwable e)
            {
                multicastException(e);
            } finally {
                threadContext.closeScope();
                threadsMonitor.rem();
            }
        };
        Thread thread = new Thread(threadGroup, task);

        threadsMonitor.add();
        thread.start();

        return HObject.HVOID;
    }

    @Override
    public HObject visit(RepeatStmt repeatStmt, Context arg)
    {
        HObject repetitions = evalExpression(repeatStmt.getRepetitions(), arg);

        int intValue = repetitions.getIntValue();
        for (int i = 0; i < intValue; i++ )
        {
            visit(repeatStmt.getBody(), arg);
        }

        return HObject.HVOID;
    }

    /**
     * Shortcut for TypeMapper.map()
     * @param type
     * @return
     */
    protected Class mapTypeToClass(Type type)
    {
        return getTypeMapper().map(type, getImportManager());
    }

    protected void setExpressionEvaluator(ExpressionEvaluator expressionEvaluator)
    {
        this.expressionEvaluator = expressionEvaluator;
    }

    protected void setImportManager(ImportManager importManager)
    {
        this.importManager = importManager;
        expressionEvaluator.setImportManager(importManager);
    }

    protected HObject evalExpression(Expression expression, Context context)
    {
        return expressionEvaluator.eval(expression, context);
    }

    protected TypeMapper getTypeMapper()
    {
        return TypeMapper.getInstance();
    }

    public void addExceptionListener(HydraExceptionListener listener)
    {
        exceptionListeners.add(listener);
    }

    @Override
    public List<HydraExceptionListener> getExceptionListeners()
    {
        return exceptionListeners;
    }

    private static class ThreadsMonitor {

        int activeThreads = 0  ;

        synchronized public void add() {
            activeThreads++;
        }

        synchronized public void rem() {
            activeThreads--;
            notify();
        }

        synchronized public boolean anyRunning() {
            return activeThreads > 0 ;
        }

        synchronized public void reset() { activeThreads = 0 ; }
    }
}
