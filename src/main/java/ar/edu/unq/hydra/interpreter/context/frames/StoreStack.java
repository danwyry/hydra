package ar.edu.unq.hydra.interpreter.context.frames;

import ar.edu.unq.hydra.ast.body.FunctionDeclaration;
import ar.edu.unq.hydra.interpreter.exceptions.HydraDuplicateVariableDeclarationException;
import ar.edu.unq.hydra.interpreter.values.HObject;

import java.util.*;

import static java.util.Objects.nonNull;


public class StoreStack
{
    private Frame currentFrame;
    private StoreStack previousStack;

    public StoreStack()
    {
        this(null, null);
    }

    private StoreStack(Frame frame, StoreStack storeStack)
    {
        this.currentFrame = frame;
        this.previousStack = storeStack;
    }

    public StoreStack push(Frame scope)
    {
        return new StoreStack(scope, this);
    }

    public StoreStack pop()
    {
        return previousStack;
    }

    public Frame topScope()
    {
        return currentFrame;
    }

    public FunctionCallFrame getTopFunctionCallScope()
    {
        if (nonNull(currentFrame) && currentFrame instanceof FunctionCallFrame)
            return (FunctionCallFrame) currentFrame;
        else if (nonNull(previousStack))
            return previousStack.getTopFunctionCallScope();
        return null;
    }

    /**
     * Associates value to the nearest ocurrence of name in the store stack.
     * If no entry with under name exists, then the value is stored in the current frame.
     * Precondition: There is an open scope in the stack (aka: currentFrame != null)
     * @param name
     * @param value
     */
    public void storeVariable(String name, HObject value)
    {
        storeVariable(name,value, false);
    }

    /**
     * Associates value to the nearest ocurrence of name in the store stack.
     * If no entry with name 'name' exists or forceNew is true, then the value is stored in the current frame.
     * Precondition: There is an open scope in the stack (aka: currentFrame != null)
     * @param name
     * @param value
     * @param forceNew
     */
    public void storeVariable(String name, HObject value, boolean forceNew)
    {
        Frame targetFrame = currentFrame;

        if (forceNew && targetFrame.isStoredVariable(name))
            throw new HydraDuplicateVariableDeclarationException(name);

        if ( ! forceNew )
            targetFrame = lookupScopeStoring(name).orElse(targetFrame);

        targetFrame.storeVariable(name, value);
    }

    /**
     * Denotes if variable with name 'name' is stored in any of the frames in the stack
     * @param name
     * @return
     */
    public boolean isStoredVariable(String name)
    {
        return (nonNull(currentFrame) && currentFrame.isStoredVariable(name))
                || nonNull(previousStack) && previousStack.isStoredVariable(name);
    }

    /**
     * Looks up for the last scope that stored a variable of name 'name'.
     * If none is found Optional.empty is returned
     * @param name
     * @return
     */
    public Optional<Frame> lookupScopeStoring(String name)
    {
        if (nonNull(currentFrame) && currentFrame.isStoredVariable(name))
            return Optional.of(currentFrame);
        else if (nonNull(previousStack))
            return previousStack.lookupScopeStoring(name);
        return Optional.empty();
    }

    /**
     * @param name
     * @return
     */
    public HObject getStoredVariable(String name)
    {
        if (nonNull(currentFrame) && currentFrame.isStoredVariable(name))
            return currentFrame.getStoredVariable(name);
        else if (nonNull(previousStack))
            return previousStack.getStoredVariable(name);
        return null;
    }

    public List<FunctionDeclaration> getFunctionDeclarations(String functionName)
    {
        List<FunctionDeclaration> functions = new ArrayList<>();
        if (nonNull(currentFrame))
        {
            List<FunctionDeclaration> scopeFunctions = currentFrame.getFunctionDeclarations(functionName);
            if (nonNull(scopeFunctions)) functions.addAll(scopeFunctions);
            if (nonNull(previousStack))  functions.addAll(previousStack.getFunctionDeclarations(functionName));
        }
        return functions;
    }

    public List<FunctionDeclaration> getFunctionDeclarations()
    {
        List<FunctionDeclaration> functions = new ArrayList<>();
        if (nonNull(currentFrame))
        {
            List<FunctionDeclaration> scopeFunctions = currentFrame.getFunctionDeclarations();
            if (nonNull(scopeFunctions)) functions.addAll(scopeFunctions);
            if (nonNull(previousStack))  functions.addAll(previousStack.getFunctionDeclarations());
        }
        return functions;
    }

    /**
     * Precondition: There is an open scope in the stack (aka: currentFrame != null)
     * @param functionDeclaration
     */
    public void declareFunction(FunctionDeclaration functionDeclaration)
    {
        currentFrame.declareFunction(functionDeclaration);
    }
}
