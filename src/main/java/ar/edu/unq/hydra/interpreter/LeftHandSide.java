package ar.edu.unq.hydra.interpreter;

import ar.edu.unq.hydra.ast.expr.*;

/**
 * Created by Daniel Wyrytowski on 3/19/17.
 */
public class LeftHandSide
{
    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    private Expression expression ;

    public LeftHandSide(Expression expression)
    {
        this.expression = expression;
    }

    // as defined in JLS8 §6.5.6 : simpleExprName, instanceVariableName

    public boolean isName()
    {
        return expressionIsType(NameExpr.class);
    }

    public boolean isFieldAccess()
    {
        return expressionIsType(FieldAccessExpr.class) ;
    }

    public boolean isArrayAccess() { return expressionIsType(ArrayAccessExpr.class) ; }

    public Expression getExpression()
    {
        return expression;
    }

    // PRECOND: expression instanceof NameExpr
    public NameExpr getExpressionAsName()
    {
        return (NameExpr) expression;
    }

    // PRECOND: expression instanceof NameExpr
    public FieldAccessExpr getExpressionAsFieldAccess()
    {
        return (FieldAccessExpr) expression;
    }

    // PRECOND: expression instanceof NameExpr
    public ArrayAccessExpr getExpressionAsArrayAccess()
    {
        return (ArrayAccessExpr) expression;
    }

    // PRECOND: expression instanceof AmbiguousNameExpr
    public AmbiguousNameExpr getExpressionAsAmbiguousName()
    {
        return (AmbiguousNameExpr) expression;
    }


    private boolean expressionIsType(Class clazz)
    {
        return expression.getClass() == clazz;
    }

    public boolean isAmbiguousName()
    {
        return expressionIsType(AmbiguousNameExpr.class);
    }
}
