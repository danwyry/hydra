package ar.edu.unq.hydra.interpreter.exceptions;

import ar.edu.unq.hydra.interpreter.context.Context;

/**
 * Created by Daniel Wyrytowski on 11/4/16.
 */
public class HydraEvaluationException extends RuntimeException
{
    public HydraEvaluationException() {
    }

    public HydraEvaluationException(String var1)
    {
        super(var1);
    }

    public HydraEvaluationException(Throwable var1)
    {
        super(var1);
    }

    public HydraEvaluationException(Throwable e, Context context)
    {
        super(e);
    }
}
