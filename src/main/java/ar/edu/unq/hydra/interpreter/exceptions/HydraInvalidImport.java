package ar.edu.unq.hydra.interpreter.exceptions;

/**
 * Created by Daniel Wyrytowski on 4/6/17.
 */
public class HydraInvalidImport extends HydraEvaluationException
{
    public HydraInvalidImport(String s)
    {
        super(s);
    }
}
