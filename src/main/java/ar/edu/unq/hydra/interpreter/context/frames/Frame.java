package ar.edu.unq.hydra.interpreter.context.frames;

import ar.edu.unq.hydra.ast.body.FunctionDeclaration;
import ar.edu.unq.hydra.interpreter.values.HObject;

import java.util.*;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * Created by Daniel Wyrytowski on 5/1/17.
 */
public class Frame
{
    private Map<String, HObject> objectRefs;
    private Map<String, List<FunctionDeclaration>> functions ;

    public Frame()
    {
    }

    /**
     * Denotes
     * @param name
     * @return
     */
    public boolean isStoredVariable(String name)
    {
        return nonNull(objectRefs) && objectRefs.containsKey(name);
    }

    public void storeVariable(String name, HObject value)
    {
        if (isNull(objectRefs))
            objectRefs = new Hashtable<>();
        objectRefs.put(name,value);
    }

    public HObject getStoredVariable(String name)
    {
        return objectRefs.get(name);
    }

    public void declareFunction(FunctionDeclaration function)
    {
        if (isNull(functions))
            functions = new HashMap<>();

        String name = function.getNameAsString();
        List<FunctionDeclaration> declarations;
        if (functions.containsKey(name))
        {
            declarations = functions.get(name);
        } else {
            declarations = new ArrayList<>();
            functions.put(name, declarations);
        }

        declarations.add(function);
    }

    /**
     * Returns a list the functions declared in this scope, or null if there were none
     * @return
     */
    public List<FunctionDeclaration> getFunctionDeclarations()
    {
        return isNull(functions)
                ? null
                : functions.values().stream()
                .reduce( Collections.emptyList(),
                        (l1,l2) -> { l1.addAll(l2); return l1; });
    }

    /**
     * Returns a list the functions declared in this scope with the given name, or null if there were none
     * @param functionName
     * @return
     */
    public List<FunctionDeclaration> getFunctionDeclarations(String functionName)
    {
        return isNull(functions)
                ? null
                : functions.get(functionName);
    }
}
