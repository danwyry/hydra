package ar.edu.unq.hydra.interpreter.context;

import ar.edu.unq.hydra.ast.body.FunctionDeclaration;
import ar.edu.unq.hydra.interpreter.context.frames.Frame;
import ar.edu.unq.hydra.interpreter.context.frames.FunctionCallFrame;
import ar.edu.unq.hydra.interpreter.context.frames.StoreStack;
import ar.edu.unq.hydra.interpreter.exceptions.HydraNotSuchVariableException;
import ar.edu.unq.hydra.interpreter.values.HJObject;
import ar.edu.unq.hydra.interpreter.values.HObject;

import java.util.List;

/**
 * Created by Daniel Wyrytowski
 */
public class Context
{
    protected StoreStack storeStack;

    public Context()
    {
        this(new StoreStack());
    }

    protected Context(StoreStack storeStack)
    {
        this.storeStack = storeStack;
    }

    //-- VARIABLES RELATED METHODS

    /**
     * Returns the value of the closest variable of the given name, stored in the store stack.
     * @param name
     * @return
     */
    public HObject getStoredVariable(String name)
    {
        if (!storeStack.isStoredVariable(name))
            throw new HydraNotSuchVariableException(name);
        return storeStack.getStoredVariable(name);
    }

    //-- FUNCTIONS RELATED METHODS


    public List<FunctionDeclaration> getFunctionDeclarations(String functionName)
    {
        return storeStack.getFunctionDeclarations(functionName);
    }

    public void addFunctionDeclaration(FunctionDeclaration functionDeclaration)
    {
        storeStack.declareFunction(functionDeclaration);
    }

    //-- SCOPE RELATED METHODS

    /**
     * Create a new scope and stacks it over the previous one if any. Scopes create a context in which only allowed
     * entities can reach its members
     */
    public FunctionCallFrame openFunctionCallScope()
    {
        FunctionCallFrame functionCallFrame = new FunctionCallFrame();
        storeStack = storeStack.push(functionCallFrame);
        return functionCallFrame;
    }

    /**
     * Create a new scope and stacks it over the previous one if any. Scopes create a context in which only allowed
     * entities can reach its members
     */
    public Frame openScope()
    {
        Frame scope = new Frame();
        storeStack = storeStack.push(scope);
        return scope;
    }

    /**
     * Destroys the last stacked Scope and pops out of the stack the current scope.
     */
    public void closeScope()
    {
        storeStack = storeStack.pop();
    }

    public FunctionCallFrame getTopFunctionCallScope()
    {
        return storeStack.getTopFunctionCallScope();
    }

    /**
     * Associates value to the nearest ocurrence of name in the store stack. If no entry with under name exists, then the value is stored in the current frame.
     * @param name
     * @param value
     */
    public void storeVariable(String name, HObject value)
    {
        storeVariable(name,value, false);
    }


    public void storeVariable(String name, HObject value, boolean forceNew)
    {
            if (value.isPrimitive() || value instanceof HJObject && ((HJObject) value).isUnboxable())
                value = value.copy();

        storeStack.storeVariable(name, value, forceNew);
    }

    public Context forkContext()
    {
        Context newContext = new Context(storeStack);
        return newContext;
    }

    public boolean isStoredVariable(String name)
    {
        return storeStack.isStoredVariable(name);
    }

    public List<FunctionDeclaration> getFunctionDeclarations()
    {
        return storeStack.getFunctionDeclarations();
    }
}
