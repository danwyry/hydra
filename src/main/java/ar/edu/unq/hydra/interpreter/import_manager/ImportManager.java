package ar.edu.unq.hydra.interpreter.import_manager;

import ar.edu.unq.hydra.ast.ImportDeclaration;
import ar.edu.unq.hydra.ast.expr.Name;
import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidImport;
import ar.edu.unq.hydra.utils.ReflectionUtils;
import ar.edu.unq.hydra.utils.exceptions.ReflectionUtilsException;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Daniel Wyrytowski on 5/21/17.
 */
public class ImportManager
{
    private Map<String, Class> typeImports;
    private Map<String, Class> staticTypeImports;
    private Map<String, StaticField> staticFieldImports;
    private Map<String, StaticMethod> staticMethodImports;
    private List<Name> onDemandStaticImportNames;
    private List<Name> onDemandTypeImportNames;

    private Map<String, Name> typeImportsNames;
    private Map<String, Name> fieldImportsNames;

    public ImportManager()
    {
        setTypeImports(new HashMap<>());
        setStaticTypeImports(new HashMap<>());
        setStaticFieldImports(new HashMap<>());
        setStaticMethodImports(new HashMap<>());
        setOnDemandStaticImportNames(new ArrayList<>());
        setOnDemandTypeImportNames(new ArrayList<>());
        setTypeImportsNames(new HashMap<>());
        setFieldImportsNames(new HashMap<>());
    }

    /**
     * Adds an import declaration to the import manager. Depending on the type of import given, it will perform
     * different operations.
     * For single type declarations it'll resolve the name to a class and cache it in the import manager to be used when
     * referenced from the code.
     * For single static field and method imports it'll wrap the imported object into a StaticField or StaticMethod
     * respectively and cache it to be used when referenced from the code.
     * For on demand declarations it'll register the given name (lang.java in lang.java.*) as an on demand imported
     * name to be used later in type, fields or method resolution.
     *
     * If a single statically imported name cannot be resolved into an object (be it a type, a field or a method) an
     * exception is thrown.
     *
     * @param importDeclaration
     */
    public void addImport(ImportDeclaration importDeclaration)
    {
        if (importDeclaration.isAsterisk())
        {
            addOnDemandImport(importDeclaration);
        } else {
            addSingleImport(importDeclaration);
        }
    }

    private void addSingleImport(ImportDeclaration importDeclaration)
    {
        if (importDeclaration.isStatic())
        {
            addSingleStaticImport(importDeclaration);
        } else
        {
            addSingleTypeImport(importDeclaration);
        }
    }

    private void addSingleTypeImport(ImportDeclaration importDeclaration)
    {
        Name name = importDeclaration.getName();
        String identifier = name.getIdentifier();

        if ( ! isType(name) )
            throw new HydraInvalidImport(name.asString()  + " is not a valid type");

        Class clazz = getTypeClass(name);

        if (isStaticClass(clazz))
            throw new HydraInvalidImport("Trying to statically import a non-static type");

        typeImports.put(identifier, clazz);
        typeImportsNames.put(identifier, importDeclaration.getName());
    }

    private void addSingleStaticImport(ImportDeclaration importDeclaration)
    {
        addSingleStaticTypeImport(importDeclaration);
        addSingleStaticFieldImport(importDeclaration);
        addSingleStaticMethodImport(importDeclaration);
    }

    private void addSingleStaticMethodImport(ImportDeclaration importDeclaration)
    {
        Name name = importDeclaration.getName();
        Name qualifier = name.getQualifier().get();

        if (isType(qualifier))
        {
            Class clazz = getTypeClass(qualifier);
            String identifier = name.getIdentifier();

            if (isStaticMethod(identifier, clazz))
            {
                StaticMethod method = new StaticMethod(identifier, clazz);
                staticMethodImports.put(identifier, method);
            }
        }
    }

    private void addSingleStaticFieldImport(ImportDeclaration importDeclaration)
    {
        Name name = importDeclaration.getName();
        Name qualifier = name.getQualifier().get();

        if (isType(qualifier))
        {
            Class clazz = getTypeClass(qualifier);
            String identifier = name.getIdentifier();

            if (isStaticField(identifier, clazz))
            {
                StaticField field = getStaticField(identifier, clazz);
                addStaticFieldImport(identifier, qualifier, field);
            }
        }
    }

    private void addSingleStaticTypeImport(ImportDeclaration importDeclaration)
    {
        Name name = importDeclaration.getName();

        if ( isType(name) )
        {
            Class clazz = getTypeClass(name);
            if ( !isStaticClass(clazz) )
                throw new HydraInvalidImport("Trying to single import a static type");

            String identifier = name.getIdentifier();
            staticTypeImports.put(identifier, clazz);

            typeImportsNames.put(identifier, importDeclaration.getName());
        }
    }

    private void addOnDemandImport(ImportDeclaration importDeclaration)
    {
        if (importDeclaration.isStatic())
            addOnDemandStaticImport(importDeclaration);
        else
            addOnDemandTypeImport(importDeclaration);
    }

    private void addOnDemandStaticImport(ImportDeclaration importDeclaration)
    {
        onDemandStaticImportNames.add(importDeclaration.getName());
    }

    private void addOnDemandTypeImport(ImportDeclaration importDeclaration)
    {
        onDemandTypeImportNames.add(importDeclaration.getName());
    }

    /**
     * Given and identifier it determines if an object under that name was imported. For example, if any the following
     * imports where added, and the identifiers is 'System', the result would be true:
     *   import java.lang.System;
     *   import java.lang.*;
     * In the case the object was imported on demand, then the object will be also cached, to avoid trying to match the
     * name again against all on demand imported names.
     *
     * @param identifier
     * @return
     */
    public boolean isImport(String identifier)
    {
        return      isImportedType(identifier)
                || isImportedField(identifier)
                || isImportedMethod(identifier);
    }

    public boolean isImportedType(String identifier)
    {
        return      isNonStaticImportedType(identifier)
                ||  isStaticImportedType(identifier)
                ||  isOnDemandImportedType(identifier)
                ||  isOnDemandStaticImportedType(identifier);
    }

    public boolean isImportedMethod(String identifier)
    {
        return staticMethodImports.containsKey(identifier)
                ||  isOnDemandImportedMethod(identifier);
    }

    public boolean isImportedField(String identifier)
    {
        return      staticFieldImports.containsKey(identifier)
                ||  isOnDemandImportedField(identifier);
    }

    private boolean isOnDemandStaticImportedType(String identifier)
    {
        for (Name name : onDemandStaticImportNames)
        {
            Name typeName = new Name(name, identifier);
            if ( isStaticType(typeName) )
            {
                Class clazz = getTypeClass(typeName);
                staticTypeImports.put(identifier, clazz);
                typeImportsNames.put(identifier, typeName);
                return true;
            }
        }
        return false ;
    }

    private boolean isOnDemandImportedField(String identifier)
    {
        for (Name name : onDemandStaticImportNames)
        {
            Class clazz = getTypeClass(name);
            if (isStaticField(identifier, clazz))
            {
                StaticField field = getStaticField(identifier, clazz);
                addStaticFieldImport(identifier, name, field);
                return true;
            }
        }
        return false ;
    }

    private void addStaticFieldImport(String identifier, Name scope, StaticField field)
    {
        staticFieldImports.put(identifier, field);
        fieldImportsNames.put(identifier, new Name(scope, identifier));
    }

    private boolean isOnDemandImportedMethod(String identifier)
    {
        for (Name name : onDemandStaticImportNames)
        {
            Class clazz = getTypeClass(name);
            if (isStaticMethod(identifier, clazz))
            {
                StaticMethod method = new StaticMethod(identifier, clazz);
                staticMethodImports.put(identifier, method);
                return true;
            }
        }
        return false ;
    }

    private boolean isOnDemandImportedType(String identifier)
    {
        for (Name name : onDemandTypeImportNames)
        {
            Name typeName = new Name(name, identifier);
            if ( isNonStaticType(typeName) )
            {
                Class clazz = getTypeClass(typeName);
                staticTypeImports.put(identifier, clazz);
                typeImportsNames.put(identifier, typeName);
                return true;
            }
        }
        return false ;
    }

    private boolean isStaticImportedType(String identifier)
    {
        return staticTypeImports.containsKey(identifier);
    }

    public boolean isNonStaticImportedType(String identifier)
    {
        return typeImports.containsKey(identifier);
    }

    public Class<?> getImportedType(String typeName)
    {
        if ( ! isImportedType(typeName) )
            throw new HydraInvalidImport("Trying to get a type that has not been imported");

        return typeImports.containsKey(typeName)
                ? typeImports.get(typeName)
                : staticTypeImports.get(typeName);
    }

    public StaticField getImportedField(String identifier)
    {
        if ( ! isImportedField(identifier) )
            throw new HydraInvalidImport("Trying to get a field that has not been imported");
        return staticFieldImports.get(identifier);
    }

    public StaticMethod getImportedMethod(String identifier)
    {
        if ( ! isImportedMethod(identifier) )
            throw new HydraInvalidImport("Trying to get a Method that has not been imported");

        return staticMethodImports.get(identifier);
    }

    // -- inside helpers

    private boolean isStaticField(String identifier, Class clazz)
    {
        return ReflectionUtils.staticFieldExists(clazz, identifier);
    }

    private boolean isStaticMethod(String identifier, Class clazz)
    {
        return ReflectionUtils.staticMethodExists(clazz, identifier);
    }

    private StaticField getStaticField(String identifier, Class clazz)
    {
        Field field = FieldUtils.getField(clazz, identifier);
        return new StaticField(field, clazz);
    }

    private Class getTypeClass(Name name)
    {
        String nameAsString = name.asString();
        return ReflectionUtils.getClass(nameAsString);
    }

    private boolean isType(Name name)
    {
        return isNonStaticType(name) || isStaticType(name);
    }

    private boolean isStaticType(Name name)
    {
        try
        {
            Class clazz = getTypeClass(name);
            return Modifier.isStatic(clazz.getModifiers());
        } catch (ReflectionUtilsException e)
        {
            if (e.getCause() instanceof ClassNotFoundException)
                return false ;
            else throw e;
        }
    }

    private boolean isNonStaticType(Name name)
    {
        try
        {
            Class clazz = getTypeClass(name);
            return ! Modifier.isStatic(clazz.getModifiers());
        } catch (ReflectionUtilsException e)
        {
            if (e.getCause() instanceof ClassNotFoundException)
                return false ;
            else throw e;
        }
    }

    private boolean isStaticClass(Class clazz)
    {
        return ReflectionUtils.classIsStatic(clazz);
    }

    public void setTypeImports(Map<String, Class> typeImports)
    {
        this.typeImports = typeImports;
    }

    public void setOnDemandStaticImportNames(List<Name> onDemandStaticImportNames)
    {
        this.onDemandStaticImportNames = onDemandStaticImportNames;
    }

    public void setOnDemandTypeImportNames(List<Name> onDemandTypeImportNames)
    {
        this.onDemandTypeImportNames = onDemandTypeImportNames;
    }

    public void setStaticTypeImports(Map<String, Class> staticTypeImports)
    {
        this.staticTypeImports = staticTypeImports;
    }

    public void setStaticFieldImports(Map<String, StaticField> staticFieldImports)
    {
        this.staticFieldImports = staticFieldImports;
    }

    public void setStaticMethodImports(Map<String, StaticMethod> staticMethodImports)
    {
        this.staticMethodImports = staticMethodImports;
    }

    public void setTypeImportsNames(Map<String, Name> typeImportsNames)
    {
        this.typeImportsNames = typeImportsNames;
    }

    public Name getImportedTypeName(String typeName)
    {
        if (!isImportedType(typeName))
            throw new HydraInvalidImport("Trying to get name of non imported type: " + typeName);

        return typeImportsNames.get(typeName);
    }

    public Name getImportedFieldName(String fieldName)
    {
        if (!isImportedField(fieldName))
            throw new HydraInvalidImport("Trying to get name of non imported field: " + fieldName);

        return fieldImportsNames.get(fieldName);
    }

    public void setFieldImportsNames(HashMap<String,Name> fieldImportsNames)
    {
        this.fieldImportsNames = fieldImportsNames;
    }
}
