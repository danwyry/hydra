package ar.edu.unq.hydra.interpreter.values;

import ar.edu.unq.hydra.ast.expr.UnaryExpr;
import ar.edu.unq.hydra.interpreter.exceptions.HydraEvaluationException;
import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidAritmethic;
import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidCastOperation;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * Created by Daniel Wyrytowski
 */
public class HJObject extends HObject
{

    /**
     * For pre and post increment operators to work on Objects java request the object type to be specific
     * so HJObject has a value field for each of the field types that understand these operators.
     *
     * Invariants:
     *   | If objecVal is NOT an instance of Character, or of a type extending Number, then all fields but 'objectVal' are null
     *   | Otherwise, let 'typeVal' be the field holding the represented value, objectVal should be a reference to 'typeVal', all
     *      other '*Val' fields are null
     *      i.e.:   o = new HJObject( (Integer) 1 ) ==>
     *                            o.intVal == o.objectVal == 1 ,
     *                            o.shortVal == null ,
     *                            o.longVal == null ...
     *
     */
    private Object objectVal ;
    private Integer intVal ;
    private Short shortVal ;
    private Long longVal ;
    private Float floatVal ;
    private Double doubleVal ;
    private Character charVal;
    private Byte byteVal;

    private String typeName ;

    public HJObject(Object value)
    {
        objectVal = value;
        if (isNull(value))
        {
            typeName = "Object";
            return ;
        }

        if (value instanceof Short)
        {
            typeName = "Short";
            shortVal = (Short) value;
        } else if (value instanceof Integer)
        {
            typeName = "Int";
            intVal = (Integer) value;
        } else if (value instanceof Long)
        {
            typeName = "Long";
            longVal = (Long) value;
        } else if (value instanceof Float)
        {
            typeName = "Float";
            floatVal = (Float) value;
        } else if (value instanceof Double)
        {
            typeName = "Double";
            doubleVal = (Double) value;
        } else if (value instanceof Character)
        {
            typeName = "Character";
            charVal = (Character) value;
        } else if (value instanceof Byte)
        {
            typeName = "Byte";
            byteVal = (Byte) value;
        } else
        {
            typeName = value.getClass().toString();
        }
    }

    @Override
    protected String javaTypeName() {
        return typeName;
    }

    @Override
    public boolean isNumeric()
    {
        return nonNull(objectVal) && objectVal instanceof Number;
    }

    @Override
    public boolean getBooleanValue()
    {
        try {
            return (boolean) objectVal;
        } catch (ClassCastException e)
        {
            throw new HydraInvalidCastOperation(javaTypeName(), "boolean");
        }
    }

    @Override
    public short getShortValue()
    {
        try {
            return (short) objectVal;
        } catch (ClassCastException e)
        {
            throw new HydraInvalidCastOperation(javaTypeName(), "short");
        }
    }

    @Override
    public int getIntValue()
    {
        try {
            return (int) objectVal;
        } catch (ClassCastException e)
        {
            throw new HydraInvalidCastOperation(javaTypeName(), "int");
        }
    }

    @Override
    public long getLongValue()
    {
        try {
            return (long) objectVal;
        } catch (ClassCastException e)
        {
            throw new HydraInvalidCastOperation(javaTypeName(), "long");
        }
    }

    @Override
    public float getFloatValue()
    {
        try {
            return (float) objectVal;
        } catch (ClassCastException e)
        {
            throw new HydraInvalidCastOperation(javaTypeName(), "float");
        }
    }

    @Override
    public double getDoubleValue()
    {
        try {
            return (double) objectVal;
        } catch (ClassCastException e)
        {
            throw new HydraInvalidCastOperation(javaTypeName(), "double");
        }
    }

    @Override
    public byte getByteValue()
    {
        try {
            return (byte) objectVal;
        } catch (ClassCastException e)
        {
            throw new HydraInvalidCastOperation(javaTypeName(), "byte");
        }
    }

    @Override
    public boolean isDouble()
    {
        return nonNull(objectVal) && objectVal instanceof Double;
    }

    @Override
    public boolean isFloat()
    {
        return nonNull(objectVal) && objectVal instanceof  Float;
    }

    @Override
    public boolean isLong()
    {
        return nonNull(objectVal) && objectVal instanceof Long;
    }

    @Override
    public boolean isInt()
    {
        return nonNull(objectVal) && objectVal instanceof Integer;
    }

    @Override
    public boolean isShort()
    {
        return nonNull(objectVal) && objectVal instanceof Short;
    }

    @Override
    public boolean isBoolean()
    {
        return nonNull(objectVal) && objectVal instanceof Boolean;
    }

    @Override
    public boolean isPrimitive()
    {
        return false;
    }

    @Override
    public Object getValueAsObject()
    {
        return objectVal ;
    }

    @Override
    public HObject preIncrement()
    {
        if (isNull(objectVal))
            throw new HydraInvalidAritmethic(UnaryExpr.Operator.PREFIX_INCREMENT.asString(), " null values");
        HObject value ;

        if (isByte())
            value = new HInt(++byteVal);
        else if (isChar())
            value = new HInt(++charVal);
        else if (isShort())
            value = new HInt(++shortVal);
        else if (isInt())
            value = new HInt(++intVal);
        else if (isLong())
            value = new HLong(++longVal);
        else if (isFloat())
            value = new HFloat(++floatVal);
        else if (isDouble())
            value = new HDouble(++doubleVal);
        else
            throw new HydraInvalidAritmethic(UnaryExpr.Operator.PREFIX_INCREMENT.asString(), javaTypeName());

        objectVal = value.getValueAsObject();
        return value;
    }

    @Override
    public HObject posIncrement()
    {
        if (isNull(objectVal))
            throw new HydraInvalidAritmethic(UnaryExpr.Operator.POSTFIX_INCREMENT.asString(), " null values");

        HJObject value = (HJObject) this.copy();

        if (isByte())
        {
            byteVal++;
            objectVal = byteVal;
        } else if (isChar()) {
            charVal++;
            objectVal = charVal;
        } else if (isShort()) {
            shortVal++;
            objectVal = shortVal;
        } else if (isInt()) {
            intVal++;
            objectVal = intVal;
        } else if (isLong()) {
            longVal++;
            objectVal = longVal;
        } else if (isFloat()) {
            floatVal++;
            objectVal = floatVal;
        } else if (isDouble()) {
            doubleVal++;
            objectVal = doubleVal;
        } else {
            throw new HydraInvalidAritmethic(UnaryExpr.Operator.POSTFIX_INCREMENT.asString(), javaTypeName());
        }
        return value;
    }

    @Override
    public HObject preDecrement()
    {
        if (isNull(objectVal))
            throw new HydraInvalidAritmethic(UnaryExpr.Operator.PREFIX_DECREMENT.asString(), " null values");

        HObject value ;

        if (isByte())
            value = new HInt(--byteVal);
        else if (isChar())
            value = new HInt(--charVal);
        else if (isShort())
            value = new HInt(--shortVal);
        else if (isInt())
            value = new HInt(--intVal);
        else if (isLong())
            value = new HLong(--longVal);
        else if (isFloat())
            value = new HFloat(--floatVal);
        else if (isDouble())
            value = new HDouble(--doubleVal);
        else
            throw new HydraInvalidAritmethic(UnaryExpr.Operator.PREFIX_DECREMENT.asString(), javaTypeName());

        objectVal = value.getValueAsObject();
        return value;
    }

    @Override
    public HObject posDecrement()
    {
        if (isNull(objectVal))
            throw new HydraInvalidAritmethic(UnaryExpr.Operator.POSTFIX_DECREMENT.asString(), " null values");

        HJObject value = (HJObject) this.copy();
        if (isByte())
        {
            byteVal--;
            objectVal = byteVal;
        } else if (isChar()) {
            charVal--;
            objectVal = charVal;
        } else if (isShort()) {
            shortVal--;
            objectVal = shortVal;
        } else if (isInt()) {
            intVal--;
            objectVal = intVal;
        } else if (isLong()) {
            longVal--;
            objectVal = longVal;
        } else if (isFloat()) {
            floatVal--;
            objectVal = floatVal;
        } else if (isDouble()) {
            doubleVal--;
            objectVal = doubleVal;
        } else {
            throw new HydraInvalidAritmethic(UnaryExpr.Operator.POSTFIX_DECREMENT.asString(), javaTypeName());
        }

        return value;
    }

    @Override
    public HObject unbox()
    {
        if (isBoolean())
            return new HBoolean(getBooleanValue());
        else if (isByte())
            return new HByte(byteVal);
        else if (isChar())
            return new HChar(charVal);
        else if (isShort())
            return new HShort(shortVal);
        else if (isInt())
            return new HInt(intVal);
        else if (isLong())
            return new HLong(longVal);
        else if (isFloat())
            return new HFloat(floatVal);
        else if (isDouble())
            return new HDouble(doubleVal);
        else
            throw new HydraEvaluationException("Not an unboxable type");
    }

    @Override
    public char getCharValue()
    {
        return (char) objectVal;
    }

    @Override
    public boolean isChar()
    {
        return nonNull(objectVal) &&  charVal instanceof  Character;
    }

    @Override
    public boolean isByte()
    {
        return nonNull(objectVal) && byteVal instanceof Byte;
    }

    @Override
    public HObject plus(HObject pright)
    {
        if (objectVal instanceof String)
            return new HJObject(((String)objectVal)+pright.getValueAsObject());
        else
            return super.plus(pright);

    }

    @Override
    public HObject cast(Class clazz)
    {
        if (clazz == char.class)
            return new HChar((char) objectVal);
        else if (clazz == boolean.class)
            return new HBoolean((boolean) objectVal);
        else if (clazz == byte.class)
            return new HByte((byte)objectVal);
        else if (clazz == short.class)
            return new HShort((short)objectVal);
        else if (clazz == int.class)
            return new HInt((int)objectVal);
        else if (clazz == long.class)
            return new HLong((long)objectVal);
        else if (clazz == float.class)
            return new HFloat((float)objectVal);
        else if (clazz == double.class)
            return new HDouble((double) objectVal);
        else
            return new HJObject(clazz.cast(objectVal));
    }

    @Override
    public HBoolean isInstanceOf(Class clazz)
    {
        return new HBoolean(clazz.isInstance(objectVal));
    }

    @Override
    public Class getValueClass()
    {
        return isNull(objectVal) ? Object.class : objectVal.getClass();
    }

    public boolean isUnboxable()
    {
        return isBoolean() || isByte() || isChar() || isShort() || isInt() || isLong() || isFloat() || isDouble();
    }
}
