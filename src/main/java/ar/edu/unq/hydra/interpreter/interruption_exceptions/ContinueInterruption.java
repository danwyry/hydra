package ar.edu.unq.hydra.interpreter.interruption_exceptions;

import static java.util.Objects.isNull;

/**
 * Created by Daniel Wyrytowski on 6/3/17.
 */
public class ContinueInterruption extends HydraLabeledFlowInterruption
{
    private final static ContinueInterruption CONTINUE_INTERRUPTION_NO_LABEL = new ContinueInterruption(null);

    private ContinueInterruption(String label)
    {
        super(label);
    }

    public static ContinueInterruption getInstance()
    {
        return CONTINUE_INTERRUPTION_NO_LABEL;
    }

    public static ContinueInterruption getInstance(String label)
    {
        return isNull(label) ? getInstance() : new ContinueInterruption(label);
    }
}
