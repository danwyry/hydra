package ar.edu.unq.hydra.interpreter.values;

import ar.edu.unq.hydra.interpreter.exceptions.HydraEvaluationException;

/**
 * This class is meant to wrap and operate values regarless whether they are primitive or Objects.
 * Each primitive type should have it's own implementation of HObject providing the necessary behaviour to be operable on.
 * As well, a HJObject Class extends HObject implementing a generic behaviour for object values.
 *
 * Created by Daniel Wyrytowski
 */
abstract public class HObject implements Cloneable
{
    public static final HObject HVOID = new HVoid();
    public static final HObject NULL_OBJECT = new HJObject((Object) null);
    public static final HBoolean FALSE_OBJECT = new HBoolean(false);
    public static final HBoolean TRUE_OBJECT = new HBoolean(true);


    abstract protected String javaTypeName();

    //-- Type checks (Used mostly for type promotion decisions)

    abstract public boolean isNumeric();
    abstract public boolean isBoolean();
    abstract public boolean isPrimitive();
    abstract public boolean isDouble();
    abstract public boolean isFloat();
    abstract public boolean isLong();
    abstract public boolean isInt();
    abstract public boolean isShort();
    abstract public boolean isChar();
    abstract public boolean isByte();

    //-- Getters & Type casts (mostly used for unboxing)

    abstract public Object getValueAsObject();
    abstract public short getShortValue() ;
    abstract public char getCharValue();
    abstract public int getIntValue() ;
    abstract public long getLongValue() ;
    abstract public float getFloatValue() ;
    abstract public double getDoubleValue() ;
    abstract public byte getByteValue();
    abstract public boolean getBooleanValue();


    public abstract HObject unbox();

    //-- Comparations

    public HBoolean eq(HObject pright)
    {
        return new HBoolean(this.getValueAsObject() == pright.getValueAsObject());
    }
    public HObject neq(HObject pright)
    {
        return new HBoolean(this.getValueAsObject() != pright.getValueAsObject());
    }

    public HObject gt(HObject pright)
    {
        TypePromoter.Promotion promoted = TypePromoter.binaryPromotion(this, pright);
        return promoted.left().gt( promoted.right() )  ;
    }

    public HObject lt(HObject pright)
    {
        TypePromoter.Promotion promoted = TypePromoter.binaryPromotion(this, pright);
        return promoted.left().lt( promoted.right() ) ;
    }

    public HObject leq(HObject pright)
    {
        TypePromoter.Promotion promoted = TypePromoter.binaryPromotion(this, pright);
        return promoted.left().leq( promoted.right() ) ;

    }

    public HObject geq(HObject pright)
    {
        TypePromoter.Promotion promoted = TypePromoter.binaryPromotion(this, pright);
        return promoted.left().geq( promoted.right() ) ;
    }

    //-- Unary Operations

    public HObject negatedHValue()
    {
        return this.unbox().negatedHValue();
    }
    public HObject inversedHValue()
    {
        return this.unbox().inversedHValue();
    }
    public HObject negativeHValue()
    {
        return this.unbox().negativeHValue();
    }
    public HObject positiveHValue()
    {
        return this.unbox().positiveHValue();
    }

    abstract public HObject preIncrement();
    abstract public HObject posIncrement();
    abstract public HObject preDecrement();
    abstract public HObject posDecrement();

    //-- Binary Operations

    public HObject plus(HObject pright)
    {
        TypePromoter.Promotion promoted = TypePromoter.binaryPromotion(this, pright);
        return promoted.left().plus(promoted.right()) ;
    }

    public HObject times(HObject pright)
    {
        TypePromoter.Promotion promoted = TypePromoter.binaryPromotion(this, pright);
        return promoted.left().times(promoted.right()) ;
    }

    public HObject minus(HObject pright)
    {
        TypePromoter.Promotion promoted = TypePromoter.binaryPromotion(this, pright);
        return promoted.left().minus(promoted.right()) ;
    }

    public HObject divide(HObject pright)
    {
        TypePromoter.Promotion promoted = TypePromoter.binaryPromotion(this, pright);
        return promoted.left().divide(promoted.right()) ;
    }

    public HObject remainder(HObject pright)
    {
        TypePromoter.Promotion promoted = TypePromoter.binaryPromotion(this, pright);
        return promoted.left().remainder(promoted.right()) ;
    }

    public HObject binAnd(HObject pright)
    {
        TypePromoter.Promotion promoted = TypePromoter.binaryPromotion(this, pright);
        return promoted.left().binAnd(promoted.right()) ;
    }

    public HObject binOr(HObject pright)
    {
        TypePromoter.Promotion promoted = TypePromoter.binaryPromotion(this, pright);
        return promoted.left().binOr(promoted.right()) ;
    }

    public HObject lShift(HObject pright)
    {
        HObject left = TypePromoter.unaryPromotion( this.unbox() );
        HObject right = TypePromoter.unaryPromotion( pright.unbox() );
        return left.lShift(right) ;
    }

    public HObject rSignedShift(HObject pright)
    {
        HObject left = TypePromoter.unaryPromotion( this.unbox() );
        HObject right = TypePromoter.unaryPromotion( pright.unbox() );
        return left.rSignedShift(right) ;
    }

    public HObject rUnsignedShift(HObject pright)
    {
        HObject left = TypePromoter.unaryPromotion( this.unbox() );
        HObject right = TypePromoter.unaryPromotion( pright.unbox() );
        return left.rUnsignedShift(right) ;
    }

    public HObject and(HObject pright)
    {
        return this.unbox().and( pright.unbox() );
    }
    public HObject or(HObject pright)
    {
        return this.unbox().or( pright.unbox() );
    }
    public HObject xor(HObject pright)
    {
        return this.unbox().xor( pright.unbox() );
    }

    public abstract HObject cast(Class clazz);

    public abstract HObject isInstanceOf(Class clazz);


    /**
     * Given a class and an object, wraps the object into an appropiate HObject type
     * @param type
     * @param value
     * @return
     */
    public static HObject valueTypeToHObject(Class type, Object value)
    {
        if (type == void.class)
            return new HVoid();
        else if (type == boolean.class)
            return new HBoolean((boolean)value);
        else if (type == byte.class)
            return new HByte((byte)value);
        else if (type == char.class)
            return new HChar((char)value);
        else if (type == short.class)
            return new HShort((short)value);
        else if (type == int.class)
            return new HInt((int)value);
        else if (type == long.class)
            return new HLong((long)value);
        else if (type == float.class)
            return new HFloat((float)value);
        else if (type == double.class)
            return new HDouble((double)value);
        else
            return new HJObject(value);

    }

    /**
     * Returns a new HObject containing the same value as this instance. In the case of HJObjects the value will be a
     * reference to the same wrapped value as this.
     * @return
     */
    public HObject copy() {
        try {
            return (HObject) this.clone();
        } catch (CloneNotSupportedException e) {
            throw new HydraEvaluationException(e);
        }
    }

    /**
     * Returns the Class of the wrapped value
     * @return
     */
    abstract public Class getValueClass();

}
