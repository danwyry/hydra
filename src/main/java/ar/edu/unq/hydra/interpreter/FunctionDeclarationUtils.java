package ar.edu.unq.hydra.interpreter;

import ar.edu.unq.hydra.ast.Modifier;
import ar.edu.unq.hydra.ast.body.FunctionDeclaration;
import ar.edu.unq.hydra.ast.body.Parameter;
import ar.edu.unq.hydra.interpreter.context.Context;
import ar.edu.unq.hydra.interpreter.import_manager.ImportManager;
import org.apache.commons.lang3.ClassUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Provides a set of methods to handle function matching in a given context.
 */
public class FunctionDeclarationUtils
{
    private FunctionDeclarationUtils() {}
    /**
     * Returns the function declaration whose parameters that better fits the given argument types, or null if none matches.
     * @param functionName
     * @param argumentsTypes
     * @param importManager
     * @param context
     * @return
     */
    public static FunctionDeclaration getMatchingFunctionDeclaration(String functionName, Class[] argumentsTypes, ImportManager importManager, Context context)
    {
        List<FunctionDeclaration> functionDeclarations = context.getFunctionDeclarations(functionName);
        functionDeclarations = getAssignableFunctionDeclarations(functionDeclarations, argumentsTypes, importManager);
        Collections.sort(functionDeclarations, (f1, f2) -> compareFunctionFitness(f1,f2, importManager));
        return functionDeclarations.stream().findFirst().orElse(null);
    }

    /**
     * Determines if the given argument types are assignable to the function declaration parameter types.
     * @param functionDeclaration
     * @param argumentsTypes
     * @return
     */
    public static boolean functionIsAssignable(FunctionDeclaration functionDeclaration, Class[] argumentsTypes, ImportManager importManager)
    {
        if (argumentsTypes.length != functionDeclaration.getParameters().size())
            return false ;

        Class[] functionParameterTypes = mapParametersToClasses(functionDeclaration.getParameters(), importManager);

        for (int i = 0 ; i < argumentsTypes.length ; i++ )
        {
            if (functionParameterTypes[i] == null)
                continue ;

            if (typeIsAssignable(argumentsTypes[i], functionParameterTypes[i]) == false)
                return false;
        }
        return true;
    }

    public static boolean functionIsSynchronized(FunctionDeclaration f)
    {
        return f.getModifiers().contains(Modifier.SYNCHRONIZED);
    }

    /**
     * Given two function declarations and a list of argument types it returns an integer lower than, equal to, or greater
     * than 0, if the given argument types better fit the f1, fit both equally, or better fit f2, respectively.
     * @param f1
     * @param f2
     * @param importManager
     * @return
     */
    private static int compareFunctionFitness(FunctionDeclaration f1, FunctionDeclaration f2, ImportManager importManager)
    {
        Class[] parameterTypesF1 = mapParametersToClasses(f1.getParameters(), importManager);
        Class[] parameterTypesF2 = mapParametersToClasses(f2.getParameters(), importManager);

        int scale = 0 ;
        for (int i = 0 ; i < parameterTypesF1.length; i ++)
        {
            if (parameterTypesF1[i] == null && parameterTypesF2[i] == null) {
                scale++;
            } else if (parameterTypesF1[i] == null) {
                scale++;
            } else if (parameterTypesF2 == null) {
                scale--;
            } else if ( typeIsAssignable(parameterTypesF1[i], parameterTypesF2[i]) ) {
                scale--;
            } else if ( typeIsAssignable(parameterTypesF2[i], parameterTypesF1[i]) ) {
                scale++;
            }
        }
        return scale;
    }

    /**
     * Returns the list of all function declarations that match the given function name and to which the given argument
     * types are assignable to its parameters.
     * @param functionDeclarations
     * @param argumentsTypes
     * @param importManager
     * @return
     */
    private static List<FunctionDeclaration> getAssignableFunctionDeclarations(List<FunctionDeclaration> functionDeclarations, Class[] argumentsTypes, ImportManager importManager)
    {
        return functionDeclarations.stream()
                .filter(functionDeclaration -> functionIsAssignable(functionDeclaration, argumentsTypes, importManager))
                .collect(Collectors.toList());
    }


    private static boolean typeIsAssignable(Class cls, Class toClass)
    {
        return ClassUtils.isAssignable(cls, toClass, true);
    }

    /**
     * Given a list of Parameter nodes it returns a list of their corresponding classes
     * @param parameters
     * @param importManager
     * @return
     */
    private static Class[] mapParametersToClasses(List<Parameter> parameters, ImportManager importManager)
    {
        return parameters.stream()
                .map(parameter -> parameter.getType())
                .map(type -> getTypeMapper().map(type, importManager))
                .collect(Collectors.toList())
                .toArray(new Class[] {});
    }

    public static TypeMapper getTypeMapper()
    {
        return TypeMapper.getInstance();
    }
}
