package ar.edu.unq.hydra.interpreter.interruption_exceptions;

/**
 * Exceptions that extend from this , as its name suggests, are used to control the flow of the program execution, rather
 * than for handling exceptional cases. In practice, its just a hacky way to implement return, break and continue statements
 * keeping the code nice and neat. Truth is, it's not correct, but for the purposes of this project it's very practical.
 *
 * Created by Daniel Wyrytowski on 6/3/17.
 */
abstract public class HydraFlowInterruption extends RuntimeException {
    public HydraFlowInterruption (String message) {
        super(message);
    }

    public HydraFlowInterruption(Throwable e) {
        super(e);
    }

    public HydraFlowInterruption() {

    }
}
