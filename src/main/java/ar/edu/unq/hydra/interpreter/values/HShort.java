package ar.edu.unq.hydra.interpreter.values;

import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidCastOperation;

/**
 * Created by Daniel Wyrytowski
 */
public class HShort extends HNumeric
{
    private short shortVal;

    public HShort(short val)
    {
        this.shortVal = val ;
    }
    @Override
    public short getShortValue() {
        return shortVal;
    }

    @Override
    public char getCharValue()
    {
        throw new HydraInvalidCastOperation(javaTypeName(), "Char");
    }

    @Override
    public int getIntValue() {
        return shortVal;
    }

    @Override
    public long getLongValue() {
        return shortVal;
    }

    @Override
    public float getFloatValue() {
        return shortVal;
    }

    @Override
    public double getDoubleValue() {
        return shortVal;
    }

    @Override
    public byte getByteValue() {
        throw new HydraInvalidCastOperation(javaTypeName(), "Byte");
    }


    @Override
    public HObject preIncrement()
    {
        HShort value = new HShort(++shortVal);
        return TypePromoter.unaryPromotion(value);
    }

    @Override
    public HObject posIncrement()
    {
        HShort value = new HShort(shortVal++);
        return TypePromoter.unaryPromotion(value);
    }

    @Override
    public HObject preDecrement()
    {
        HShort value = new HShort(--shortVal);
        return TypePromoter.unaryPromotion(value);
    }

    @Override
    public HObject posDecrement()
    {
        HShort value = new HShort(shortVal--);
        return TypePromoter.unaryPromotion(value);
    }

    @Override
    public HObject cast(Class clazz)
    {
        if (clazz == char.class)
            return new HChar((char) shortVal);
        else if (clazz == boolean.class)
            throw new HydraInvalidCastOperation(javaTypeName(), "boolean");
        else if (clazz == byte.class)
            return new HByte((byte)shortVal);
        else if (clazz == short.class)
            return this;
        else if (clazz == int.class)
            return new HInt((int)shortVal);
        else if (clazz == long.class)
            return new HLong((long)shortVal);
        else if (clazz == float.class)
            return new HFloat((float)shortVal);
        else if (clazz == double.class)
            return new HDouble((double) shortVal);
        else
            return new HJObject(clazz.cast(shortVal));
    }

    @Override
    public HBoolean isInstanceOf(Class clazz)
    {
        return new HBoolean(clazz.isInstance(shortVal));
    }

    @Override
    public Class getValueClass() {
        return short.class;
    }

    @Override
    protected String javaTypeName() {
        return "short";
    }
}
