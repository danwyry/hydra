package ar.edu.unq.hydra.interpreter.exceptions;

import ar.edu.unq.hydra.ast.expr.AssignExpr;
import ar.edu.unq.hydra.ast.expr.Expression;
import ar.edu.unq.hydra.interpreter.context.Context;

/**
 * Created by Daniel Wyrytowski on 7/27/17.
 */
public class HydraCompoundAssignmentException extends HydraEvaluationException
{
    private final Expression lhsExpr;
    private final Expression rhsExpr;
    private final AssignExpr.Operator operator;

    public HydraCompoundAssignmentException(Expression lhsExpr, Expression rhsExpr, AssignExpr.Operator operator)
    {
        this.lhsExpr = lhsExpr ;
        this.rhsExpr = rhsExpr ;
        this.operator = operator ;
    }
}
