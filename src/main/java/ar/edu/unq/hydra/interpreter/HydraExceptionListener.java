package ar.edu.unq.hydra.interpreter;

import java.util.function.Consumer;

/**
 * Created by Daniel Wyrytowski on 2/4/18.
 */
public interface HydraExceptionListener
{
    void onException(Throwable t);
}
