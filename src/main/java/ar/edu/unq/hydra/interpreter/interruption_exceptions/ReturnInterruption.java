package ar.edu.unq.hydra.interpreter.interruption_exceptions;

/**
 * Created by Daniel Wyrytowski on 6/3/17.
 */
public class ReturnInterruption extends HydraFlowInterruption
{
    private ReturnInterruption() { }
    private static final ReturnInterruption RETURN_INTERRUPTION = new ReturnInterruption();
    public static ReturnInterruption getInstance()
    {
        return RETURN_INTERRUPTION;
    }
}
