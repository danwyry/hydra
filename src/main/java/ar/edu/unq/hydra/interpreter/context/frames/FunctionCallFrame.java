package ar.edu.unq.hydra.interpreter.context.frames;

import ar.edu.unq.hydra.ast.body.Parameter;
import ar.edu.unq.hydra.interpreter.values.HObject;

import java.util.List;

public class FunctionCallFrame extends Frame
{
    private HObject returnValue = HObject.HVOID;

    public HObject getReturnValue()
    {
        return returnValue;
    }

    public void setReturnValue(HObject returnValue)
    {
        this.returnValue = returnValue;
    }

    public void storeParameters(List<Parameter> parameters, List<HObject> arguments)
    {
        for(int i = 0; i < parameters.size() ; i++)
        {
            String name = parameters.get(i).getNameAsString();
            HObject value = arguments.get(i);
            storeVariable(name, value);
        }
    }
}
