package ar.edu.unq.hydra.interpreter;

import ar.edu.unq.hydra.ast.expr.Name;
import ar.edu.unq.hydra.ast.type.*;
import ar.edu.unq.hydra.ast.visitor.TypeVisitor;
import ar.edu.unq.hydra.interpreter.import_manager.ImportManager;
import ar.edu.unq.hydra.utils.ReflectionUtils;

import java.lang.reflect.Array;

/**
 * Performs type mapping related operations between Type AST classes and Java types.
 * Created by Daniel Wyrytowski on 4/25/17.
 */
public class TypeMapper implements TypeVisitor<Class,ImportManager>
{
    private static final TypeMapper INSTANCE = new TypeMapper();

    public static TypeMapper getInstance() { return INSTANCE ; }

    /**
     * Given a Type node and an ImportManager it maps (if possible) the represented type to its corresponding Java class
     * @param type
     * @param importManager
     * @return
     */
    public Class map(Type type, ImportManager importManager)
    {
        return visit(type, importManager);
    }

    public Class visit(Type type, ImportManager importManager)
    {
        return type.accept(this, importManager);
    }

    @Override
    public Class visit(ClassOrInterfaceType type, ImportManager importManager)
    {
        String typeQualifiedName = getTypeFullyQualifiedClassName(type, importManager);
        return ReflectionUtils.getClass(typeQualifiedName);
    }

    @Override
    public Class visit(PrimitiveType type, ImportManager importManager)
    {
        switch (type.getType())
        {
            case BOOLEAN:
                return boolean.class;
            case BYTE:
                return byte.class;
            case CHAR:
                return char.class;
            case SHORT:
                return short.class;
            case INT:
                return int.class;
            case LONG:
                return long.class;
            case FLOAT:
                return float.class;
            case DOUBLE:
                return double.class;
        }
        return null;
    }

    @Override
    public Class visit(ArrayType type, ImportManager importManager)
    {
        Class elementType = visit(type.getElementType(), importManager);
        int [] dims = (int[]) Array.newInstance(int.class, type.getArrayLevel());
        return Array.newInstance(elementType, dims).getClass();
    }

    @Override
    public Class visit(IntersectionType type, ImportManager importManager) {
        return null;
    }

    @Override
    public Class visit(UnionType type, ImportManager importManager) {
        return null;
    }

    @Override
    public Class visit(VoidType type, ImportManager importManager) {
        return void.class;
    }

    @Override
    public Class visit(WildcardType type, ImportManager importManager) {
        return null;
    }

    @Override
    public Class visit(UnknownType type, ImportManager importManager) {
        return null;
    }

    public static String getTypeFullyQualifiedClassName(ClassOrInterfaceType type, ImportManager importManager)
    {
        ClassOrInterfaceType leftest = type.getLeftestScope();
        String scopeTypeIdentifier = leftest.getName().asString();

        if ( importManager.isImportedType(scopeTypeIdentifier) )
        {
            Name importedTypeName = importManager.getImportedTypeName(scopeTypeIdentifier);
            String importedTypeQualifierName = importedTypeName.getQualifier().get().asString();
            return importedTypeQualifierName + "." + type.asString();
        } else {
            return type.asString();
        }
    }
}
