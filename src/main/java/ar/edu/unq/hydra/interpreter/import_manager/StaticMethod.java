package ar.edu.unq.hydra.interpreter.import_manager;

import ar.edu.unq.hydra.interpreter.InvocationResult;
import ar.edu.unq.hydra.interpreter.exceptions.HydraEvaluationException;
import org.apache.commons.lang3.reflect.MethodUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Daniel Wyrytowski on 5/25/17.
 */
public class StaticMethod
{
    private final String name;
    private final Class holder;

    public StaticMethod(String name, Class clazz)
    {
        this.name = name ;
        this.holder = clazz ;
    }

    public InvocationResult tryInvoke(Object[] javaArguments, Class[] argumentsTypes)
    {
        try {
            Method m =  MethodUtils.getMatchingMethod(holder, name, argumentsTypes);
            Object result = m.invoke(holder, javaArguments);
            Class type = m.getReturnType();
            return new InvocationResult(type, result);
        } catch (IllegalAccessException e) {
            throw new HydraEvaluationException(e);
        } catch (InvocationTargetException e) {
            throw new HydraEvaluationException(e);
        }
    }
}
