package ar.edu.unq.hydra.interpreter.import_manager;

import ar.edu.unq.hydra.utils.exceptions.ReflectionUtilsException;

import java.lang.reflect.Field;

/**
 * Created by Daniel Wyrytowski on 5/25/17.
 */
public class StaticField
{
    private final Class holder;
    private final Field field;

    public StaticField(Field field, Class clazz)
    {
        this.field = field ;
        this.holder = clazz;
    }

    public Object get()
    {
        try {
            return field.get(holder);
        } catch (IllegalAccessException e) {
            throw new ReflectionUtilsException(e);
        }
    }

    public void set(Object value)
    {
        try {
            field.set(holder, value);
        } catch (IllegalAccessException e) {
            throw new ReflectionUtilsException(e);
        }
    }
}
