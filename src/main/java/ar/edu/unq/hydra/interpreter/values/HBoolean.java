package ar.edu.unq.hydra.interpreter.values;

import ar.edu.unq.hydra.ast.expr.BinaryExpr;
import ar.edu.unq.hydra.ast.expr.UnaryExpr;
import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidCastOperation;
import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidAritmethic;

/**
 * Created by Daniel Wyrytowski
 */
public class HBoolean extends HPrimitive
{
    private boolean booleanVal;

    public HBoolean(boolean value)
    {
        booleanVal = value ;
    }

    public boolean getBoolean() {
        return booleanVal;
    }

    @Override
    public Object getValueAsObject()
    {
        return booleanVal;
    }

    @Override
    public boolean getBooleanValue()
    {
        return booleanVal;
    }

    @Override
    public HObject negatedHValue()
    {
        return HBoolean.getInstance( ! booleanVal );
    }

    @Override
    protected String javaTypeName() { return "boolean"; }

    @Override
    public short getShortValue()
    {
        throw new HydraInvalidCastOperation(javaTypeName(), "Short");
    }

    @Override
    public char getCharValue() {
        return 0;
    }

    @Override
    public int getIntValue() {
        throw new HydraInvalidCastOperation(javaTypeName(), "Int");
    }

    @Override
    public long getLongValue() {
        throw new HydraInvalidCastOperation(javaTypeName(), "Long");
    }

    @Override
    public float getFloatValue() {
        throw new HydraInvalidCastOperation(javaTypeName(), "Float");
    }

    @Override
    public double getDoubleValue() {
        throw new HydraInvalidCastOperation(javaTypeName(), "Dobule");
    }

    @Override
    public byte getByteValue()
    {
        throw new HydraInvalidCastOperation(javaTypeName(), "Byte");
    }

    @Override
    public HObject inversedHValue()
    {
        throw new HydraInvalidAritmethic(UnaryExpr.Operator.BITWISE_COMPLEMENT.asString(), javaTypeName());
    }

    @Override
    public HObject negativeHValue() {
        throw new HydraInvalidAritmethic(UnaryExpr.Operator.MINUS.asString(), javaTypeName());
    }

    @Override
    public HObject positiveHValue() {
        throw new HydraInvalidAritmethic(UnaryExpr.Operator.PLUS.asString(), javaTypeName());
    }

    @Override
    public HObject preIncrement() {
        throw new HydraInvalidAritmethic(UnaryExpr.Operator.PREFIX_INCREMENT.asString(), javaTypeName());
    }

    @Override
    public HObject posIncrement() {
        throw new HydraInvalidAritmethic(UnaryExpr.Operator.POSTFIX_INCREMENT.asString(), javaTypeName());
    }

    @Override
    public HObject preDecrement() {
        throw new HydraInvalidAritmethic(UnaryExpr.Operator.PREFIX_DECREMENT.asString(), javaTypeName());
    }

    @Override
    public HObject posDecrement() {
        throw new HydraInvalidAritmethic(UnaryExpr.Operator.POSTFIX_DECREMENT.asString(), javaTypeName());
    }

    @Override
    public HNumeric plus(HObject pright) {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.PLUS.asString(), javaTypeName());
    }

    @Override
    public HNumeric times(HObject pright) {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.MULTIPLY.asString(), javaTypeName());
    }

    @Override
    public HNumeric minus(HObject pright) {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.MINUS.asString(), javaTypeName());
    }

    @Override
    public HNumeric divide(HObject pright) {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.DIVIDE.asString(), javaTypeName());
    }

    @Override
    public HNumeric remainder(HObject pright) {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.REMAINDER.asString(), javaTypeName());
    }

    @Override
    public HNumeric binAnd(HObject pright) {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.BINARY_AND.asString(), javaTypeName());
    }

    @Override
    public HNumeric binOr(HObject pright) {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.BINARY_OR.asString(), javaTypeName());
    }

    @Override
    public HNumeric lShift(HObject pright) {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.LEFT_SHIFT.asString(), javaTypeName());
    }

    @Override
    public HNumeric rSignedShift(HObject pright) {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.SIGNED_RIGHT_SHIFT.asString(), javaTypeName());
    }

    @Override
    public HNumeric rUnsignedShift(HObject pright) {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.UNSIGNED_RIGHT_SHIFT.asString(), javaTypeName());
    }

    @Override
    public HObject and(HObject pright)
    {
        return HBoolean.getInstance( getBooleanValue() && pright.getBooleanValue());
    }

    @Override
    public HObject or(HObject pright)
    {
        return HBoolean.getInstance( getBooleanValue() ||  pright.getBooleanValue());
    }

    @Override
    public HObject xor(HObject pright)
    {
        return HBoolean.getInstance( getBooleanValue() ^ pright.getBooleanValue());
    }

    @Override
    public HBoolean eq(HObject pright) {
        return HBoolean.getInstance(getBooleanValue() == pright.getBooleanValue());
    }

    @Override
    public HBoolean neq(HObject pright) {
        return HBoolean.getInstance(getBooleanValue() != pright.getBooleanValue());
    }

    @Override
    public HBoolean gt(HObject pright) {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.GREATER.asString(), javaTypeName());
    }

    @Override
    public HBoolean lt(HObject pright) {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.LESS.asString(), javaTypeName());
    }

    @Override
    public HBoolean leq(HObject pright) {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.LESS_EQUALS.asString(), javaTypeName());
    }

    @Override
    public HBoolean geq(HObject pright) {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.GREATER_EQUALS.asString(), javaTypeName());
    }


    @Override
    public HObject cast(Class clazz)
    {
        if (clazz == char.class)
            throw new HydraInvalidCastOperation(javaTypeName(), "char");
        else if (clazz == boolean.class)
            return this;
        else if (clazz == byte.class)
            throw new HydraInvalidCastOperation(javaTypeName(), "byte");
        else if (clazz == short.class)
            throw new HydraInvalidCastOperation(javaTypeName(), "short");
        else if (clazz == int.class)
            throw new HydraInvalidCastOperation(javaTypeName(), "int");
        else if (clazz == long.class)
            throw new HydraInvalidCastOperation(javaTypeName(), "long");
        else if (clazz == float.class)
            throw new HydraInvalidCastOperation(javaTypeName(), "float");
        else if (clazz == double.class)
            throw new HydraInvalidCastOperation(javaTypeName(), "double");
        else
            return new HJObject(clazz.cast(booleanVal));
    }

    @Override
    public HBoolean isInstanceOf(Class clazz)
    {
        return HBoolean.getInstance(clazz.isInstance(booleanVal));
    }

    @Override
    public Class getValueClass() {
        return boolean.class;
    }

    public static HBoolean getInstance(boolean value)
    {
        return value ? TRUE_OBJECT : FALSE_OBJECT;
    }
}
