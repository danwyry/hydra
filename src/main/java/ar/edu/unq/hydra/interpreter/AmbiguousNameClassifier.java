package ar.edu.unq.hydra.interpreter;

import ar.edu.unq.hydra.ast.expr.*;
import ar.edu.unq.hydra.ast.stmt.RepeatStmt;
import ar.edu.unq.hydra.ast.type.ClassOrInterfaceType;
import ar.edu.unq.hydra.ast.visitor.GenericVisitorAdapter;
import ar.edu.unq.hydra.interpreter.context.Context;
import ar.edu.unq.hydra.interpreter.exceptions.HydraAmbiguousNameClassifierException;
import ar.edu.unq.hydra.interpreter.import_manager.ImportManager;
import ar.edu.unq.hydra.utils.ReflectionUtils;
import ar.edu.unq.hydra.ast.NameUtils;


/**
 * This class is meant to reclassify ambiguous names into a aproximate expression that the expression evaluator can
 * handle, mainly distinguishing type names from expression names. The algorithm to determine the resulting expression
 * is a simplified version of the one described in the JLS § 6.5.2 (Reclassification of Contextually Ambiguous Names)
 * leaving aside some conditions that wouldn't fit well in a language with gradual typing like Hydra.
 * This class is meant to be used as a utility static API so the constructor was privatized and the only method offered
 * to the public is reClassifyExpression(AmbiguousNameExpr name, ImportManager importManager, Context context).
 * Nevertheless, as it extends from the visitor classes it must implement all it's public methods that cannot be static,
 * therefore a singleton private instance is created internally to deal with this.
 *
 * Created by Daniel Wyrytowski on 5/28/17.
 */
public class AmbiguousNameClassifier extends GenericVisitorAdapter<Expression, String>
{
    private final static AmbiguousNameClassifier INSTANCE = new AmbiguousNameClassifier();

    private AmbiguousNameClassifier()
    {
    }

    /**
     * Takes a Name from an ambiguous name expression and returns a contextually reclassified expression that can be
     * evaluated. This method is the entry point for this class.
     * @param name
     * @param context
     * @return
     */

    public static Expression reClassifyExpression(AmbiguousNameExpr name, ImportManager importManager, Context context)
    {
        try {
            return reClassifyExpression(name.getName(), importManager, context);
        } catch (HydraAmbiguousNameClassifierException e)
        {
            throw new HydraAmbiguousNameClassifierException(name);
        }
    }


    public Expression visit(Expression n, String arg) {
        return n.accept(this, arg);
    }

    @Override
    public Expression visit(NameExpr n, String arg )
    {
        return new FieldAccessExpr(n, arg);
    }

    @Override
    public Expression visit(FieldAccessExpr n, String arg)
    {
        return new FieldAccessExpr(n, arg);
    }

    @Override
    public Expression visit(ClassExpr n, String identifier)
    {
        ClassOrInterfaceType type = (ClassOrInterfaceType) n.getType();
        Name canonicalName = type.getCanonicalName();

        if (isClass(canonicalName))
        {
            Class clazz = getClass(canonicalName);

            if (isMethodOrFieldOfClass(identifier, clazz))
            {
                return new FieldAccessExpr(n, identifier);
            } else {
                canonicalName = new Name(canonicalName, identifier);
                if (isClass(canonicalName))
                {
                    type = new ClassOrInterfaceType(type, identifier);
                    return n.setType(type);
                } else {
                    throw new HydraAmbiguousNameClassifierException();
                }
            }
        } else {
            type = new ClassOrInterfaceType(type, identifier);
            return n.setType(type);
        }
    }

    @Override
    public Expression visit(RepeatStmt repeatStmt, String arg) {
        return null;
    }

    private static Expression reClassifyExpression(Name name, ImportManager importManager, Context context)
    {
        String identifier = name.getIdentifier();
        if (name.getQualifier().isPresent())
        {
            Name qualifier = name.getQualifier().get();
            Expression qualifierExpr = reClassifyExpression(qualifier, importManager, context);
            return INSTANCE.visit(qualifierExpr, identifier);
        } else {
            return reClassifiySimpleName(identifier, importManager, context);
        }
    }

    private static Expression reClassifiySimpleName(String identifier, ImportManager importManager, Context context)
    {
        if ( isDeclaredVariable(context, identifier) || importManager.isImportedField(identifier))
        {
            return new NameExpr(identifier);
        } else if ( importManager.isNonStaticImportedType(identifier) )
        {
            Name typeName = importManager.getImportedTypeName(identifier);
            return new ClassExpr(NameUtils.nameToClassOrInterfaceType(typeName) );
        } else {
            return new ClassExpr( new ClassOrInterfaceType(identifier) );
        }
    }

    private boolean isMethodOrFieldOfClass(String identifier, Class clazz)
    {
        return ReflectionUtils.fieldOrMethodExists(clazz, identifier);
    }

    private static boolean isDeclaredVariable(Context context, String identifier)
    {
        return context.isStoredVariable(identifier);
    }

    private boolean isClass(Name name) {
        return ReflectionUtils.classExists(name.asString());
    }

    private Class getClass(Name name)
    {
        return ReflectionUtils.getClass(name.asString());
    }
}
