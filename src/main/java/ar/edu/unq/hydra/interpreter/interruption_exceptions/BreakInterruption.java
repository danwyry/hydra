package ar.edu.unq.hydra.interpreter.interruption_exceptions;

import static java.util.Objects.isNull;

/**
 * Created by Daniel Wyrytowski on 6/3/17.
 */
public class BreakInterruption extends HydraLabeledFlowInterruption
{
    private final static BreakInterruption BREAK_INTERRUPTION_NO_LABEL = new BreakInterruption(null);

    private BreakInterruption(String label)
    {
        super(label);
    }

    public static BreakInterruption getInstance()
    {
        return BREAK_INTERRUPTION_NO_LABEL;
    }

    public static BreakInterruption getInstance(String label)
    {
        return isNull(label) ? getInstance() : new BreakInterruption(label);
    }

}
