package ar.edu.unq.hydra.interpreter.exceptions;

import ar.edu.unq.hydra.ast.expr.AmbiguousNameExpr;
import ar.edu.unq.hydra.ast.expr.ClassExpr;
import ar.edu.unq.hydra.interpreter.context.Context;

/**
 * Created by Daniel Wyrytowski on 7/27/17.
 */
public class HydraAmbiguousNameClassifierException extends HydraEvaluationException
{
    private AmbiguousNameExpr name;

    public HydraAmbiguousNameClassifierException()
    {
    }

    public HydraAmbiguousNameClassifierException(AmbiguousNameExpr name)
    {
        this.name = name;
    }
}
