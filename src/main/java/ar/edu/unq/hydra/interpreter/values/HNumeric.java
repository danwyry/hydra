package ar.edu.unq.hydra.interpreter.values;

import ar.edu.unq.hydra.ast.expr.UnaryExpr;
import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidCastOperation;
import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidAritmethic;

/**
 * Created by Daniel Wyrytowski
 */
abstract public class HNumeric extends HPrimitive
{
    public boolean isNumeric() {
        return true;
    }

    @Override
    public Object getValueAsObject()
    {
        if (isByte())
            return getByteValue();
        if (isChar())
            return getCharValue();
        if (isShort())
            return getShortValue();
        else if (isInt())
            return getIntValue();
        else if (isLong())
            return getLongValue();
        else if (isFloat())
            return getFloatValue();
        else  //if (isDouble())
            return getDoubleValue();
    }


    public boolean getBooleanValue()
    {
        throw new HydraInvalidCastOperation(javaTypeName(), "Boolean");
    }

    public HObject negatedHValue()
    {
        throw new HydraInvalidAritmethic(UnaryExpr.Operator.LOGICAL_COMPLEMENT.asString(), javaTypeName());
    }
}