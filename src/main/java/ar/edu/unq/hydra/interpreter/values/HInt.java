package ar.edu.unq.hydra.interpreter.values;

import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidCastOperation;

/**
 * Created by Daniel Wyrytowski
 */
public class HInt extends HNumeric
{
    private int intVal;

    public HInt(int i)
    {
        intVal = i;
    }

    @Override
    public short getShortValue() {
        return (short) intVal;
    }

    @Override
    public char getCharValue() {
        return (char) intVal;
    }

    @Override
    public int getIntValue() {
        return intVal;
    }

    @Override
    public long getLongValue() {
        return intVal;
    }

    @Override
    public float getFloatValue() {
        return intVal;
    }

    @Override
    public double getDoubleValue() {
        return intVal;
    }

    @Override
    public byte getByteValue() {
        return (byte) intVal;
    }


    @Override
    public HInt plus(HObject pright)
    {
        return new HInt(this.getIntValue() + pright.getIntValue());
    }

    @Override
    public HInt times(HObject pright)
    {
        return new HInt(this.getIntValue() * pright.getIntValue());
    }

    @Override
    public HInt minus(HObject pright)
    {
        return new HInt(this.getIntValue() - pright.getIntValue());
    }

    @Override
    public HInt divide(HObject pright)
    {
        return new HInt(this.getIntValue() / pright.getIntValue());
    }

    @Override
    public HInt remainder(HObject pright)
    {
        return new HInt(this.getIntValue() % pright.getIntValue());
    }

    @Override
    public HInt binAnd(HObject pright)
    {
        return new HInt(this.getIntValue() & pright.getIntValue());
    }

    @Override
    public HInt binOr(HObject pright)
    {
        return new HInt(this.getIntValue() | pright.getIntValue());
    }

    @Override
    public HInt lShift(HObject pright)
    {
        return new HInt(this.getIntValue() << pright.getIntValue());
    }

    @Override
    public HInt rSignedShift(HObject pright)
    {
        return new HInt(this.getIntValue() >> pright.getIntValue());
    }

    @Override
    public HInt rUnsignedShift(HObject pright)
    {
        return new HInt(this.getIntValue() >>> pright.getIntValue());
    }

    @Override
    public HBoolean eq(HObject pright)
    {
        return new HBoolean(this.getIntValue() == pright.getIntValue());
    }

    @Override
    public HBoolean neq(HObject pright)
    {
        return new HBoolean(this.getIntValue() != pright.getIntValue());
    }

    @Override
    public HBoolean gt(HObject pright)
    {
        return new HBoolean(this.getIntValue() > pright.getIntValue());
    }

    @Override
    public HBoolean lt(HObject pright)
    {
        return new HBoolean(this.getIntValue() < pright.getIntValue());
    }

    @Override
    public HBoolean leq(HObject pright)
    {
        return new HBoolean(this.getIntValue() <= pright.getIntValue());
    }

    @Override
    public HBoolean geq(HObject pright)
    {
        return new HBoolean(this.getIntValue() >= pright.getIntValue());
    }


    @Override
    public HObject inversedHValue()
    {
        return new HInt(~intVal);
    }

    @Override
    public HObject negativeHValue()
    {
        return new HInt(-intVal);
    }

    @Override
    public HObject positiveHValue()
    {
        return new HInt(+intVal);
    }

    @Override
    public HInt preIncrement()
    {
        return new HInt(++intVal);
    }

    @Override
    public HInt posIncrement()
    {
        return new HInt(intVal++);
    }

    @Override
    public HInt preDecrement()
    {
        return new HInt(--intVal);
    }

    @Override
    public HInt posDecrement()
    {
        return new HInt(intVal--);
    }


    @Override
    public HObject cast(Class clazz)
    {
        if (clazz == char.class)
            return new HChar((char) intVal);
        else if (clazz == boolean.class)
            throw new HydraInvalidCastOperation(javaTypeName(), "boolean");
        else if (clazz == byte.class)
            return new HByte((byte)intVal);
        else if (clazz == short.class)
            return new HShort((short)intVal);
        else if (clazz == int.class)
            return this;
        else if (clazz == long.class)
            return new HLong((long)intVal);
        else if (clazz == float.class)
            return new HFloat((float)intVal);
        else if (clazz == double.class)
            return new HDouble((double) intVal);
        else
            return new HJObject(clazz.cast(intVal));
    }

    @Override
    public HBoolean isInstanceOf(Class clazz)
    {
        return new HBoolean(clazz.isInstance(intVal));
    }

    @Override
    public Class getValueClass() {
        return int.class;
    }

    @Override
    protected String javaTypeName() {
        return "int";
    }

}

