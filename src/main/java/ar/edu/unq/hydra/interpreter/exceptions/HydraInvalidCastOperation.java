package ar.edu.unq.hydra.interpreter.exceptions;

/**
 * Created by Daniel Wyrytowski on 4/6/17.
 */
public class HydraInvalidCastOperation extends HydraEvaluationException
{
    public HydraInvalidCastOperation(String originalType, String targetType)
    {
        super(originalType + "' cannot be cast to " + targetType);
    }
}
