package ar.edu.unq.hydra.interpreter.values;

import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidCastOperation;

/**
 * Created by Daniel Wyrytowski
 */
public class HChar extends HPrimitive
{
    private char charVal ;

    public HChar(char c)
    {
        charVal = c;
    }

    @Override
    protected String javaTypeName() {
        return "char";
    }

    @Override
    public Object getValueAsObject()
    {
        return charVal;
    }


    @Override
    public short getShortValue()
    {
        throw new HydraInvalidCastOperation(javaTypeName(), "Short");
    }

    @Override
    public char getCharValue() {
        return charVal;
    }

    @Override
    public int getIntValue()
    {
        return charVal ;
    }

    @Override
    public long getLongValue()
    {
        return charVal;
    }

    @Override
    public float getFloatValue()
    {
        return charVal;
    }

    @Override
    public double getDoubleValue()
    {
        return charVal;
    }

    @Override
    public byte getByteValue()
    {
        throw new HydraInvalidCastOperation(javaTypeName(), "Byte");
    }

    @Override
    public boolean getBooleanValue()
    {
        throw new HydraInvalidCastOperation(javaTypeName(), "Boolean");
    }

    @Override
    public HObject preIncrement()
    {
        HChar value = new HChar(++charVal);
        return TypePromoter.unaryPromotion(value);
    }

    @Override
    public HObject posIncrement()
    {
        HChar value = new HChar(charVal++);
        return TypePromoter.unaryPromotion(value);
    }

    @Override
    public HObject preDecrement()
    {
        HChar value = new HChar(--charVal);
        return TypePromoter.unaryPromotion(value);
    }

    @Override
    public HObject posDecrement()
    {
        HChar value = new HChar(charVal--);
        return TypePromoter.unaryPromotion(value);
    }

    @Override
    public HObject cast(Class clazz)
    {
        if (clazz == char.class)
            return this;
        else if (clazz == boolean.class)
            throw new HydraInvalidCastOperation(javaTypeName(), "boolean");
        else if (clazz == byte.class)
            return new HByte((byte)charVal);
        else if (clazz == short.class)
            return new HShort((short)charVal);
        else if (clazz == int.class)
            return new HInt((int)charVal);
        else if (clazz == long.class)
            return new HLong((long)charVal);
        else if (clazz == float.class)
            return new HFloat((float)charVal);
        else if (clazz == double.class)
            return new HDouble((double) charVal);
        else
            return new HJObject(clazz.cast(charVal));
    }

    @Override
    public HBoolean isInstanceOf(Class clazz)
    {
        return new HBoolean(clazz.isInstance(charVal));
    }

    @Override
    public Class getValueClass() {
        return char.class;
    }

}
