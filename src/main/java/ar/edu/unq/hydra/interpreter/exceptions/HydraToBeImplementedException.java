package ar.edu.unq.hydra.interpreter.exceptions;



public class HydraToBeImplementedException extends RuntimeException
{
    public HydraToBeImplementedException(String var1) {
        super(var1);
    }
}
