package ar.edu.unq.hydra.interpreter.exceptions;

import ar.edu.unq.hydra.interpreter.context.Context;

/**
 * Created by Daniel Wyrytowski on 5/3/17.
 */
public class HydraNoSuchMethodException extends HydraEvaluationException
{
    private final String name;
    private final Class[] argumentTypes;

    public HydraNoSuchMethodException(String name, Class[] argumentsTypes)
    {
        this.name = name;
        this.argumentTypes = argumentsTypes ;
    }
}
