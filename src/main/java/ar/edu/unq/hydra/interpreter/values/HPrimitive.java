package ar.edu.unq.hydra.interpreter.values;

/**
 * Created by Daniel Wyrytowski on 10/21/17.
 */
abstract public class HPrimitive extends HObject {
    @Override
    public boolean isPrimitive()
    {
        return true;
    }

    @Override
    public boolean isNumeric() {
        return this instanceof HNumeric;
    }

    @Override
    public boolean isBoolean() {
        return this instanceof HBoolean;
    }

    @Override
    public boolean isLong() {
        return this instanceof HLong;
    }

    @Override
    public boolean isInt() {
        return this instanceof HInt;
    }

    @Override
    public boolean isShort() {
        return this instanceof HShort;
    }

    @Override
    public boolean isDouble() {
        return this instanceof HDouble;
    }

    @Override
    public boolean isFloat() {
        return this instanceof HFloat;
    }

    @Override
    public boolean isChar()
    {
        return this instanceof HChar;
    }

    @Override
    public boolean isByte()
    {
        return this instanceof HByte;
    }

    @Override
    public HObject unbox() {
        return this;
    }

}
