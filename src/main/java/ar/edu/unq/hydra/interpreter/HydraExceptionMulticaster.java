package ar.edu.unq.hydra.interpreter;

import java.util.List;

/**
 * Created by Daniel Wyrytowski on 2/4/18.
 */
public interface HydraExceptionMulticaster
{
    List<HydraExceptionListener> getExceptionListeners();

    default void multicastException(Throwable t)
    {
        for (HydraExceptionListener listener : getExceptionListeners())
            listener.onException(t);
    }
}
