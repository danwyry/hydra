package ar.edu.unq.hydra.interpreter.exceptions;

/**
 * Created by Daniel Wyrytowski on 4/11/17.
 */
public class HydraNotSuchVariableException extends HydraEvaluationException
{
    private final String name;

    public HydraNotSuchVariableException(String name)
    {
        this.name = name ;
    }
}
