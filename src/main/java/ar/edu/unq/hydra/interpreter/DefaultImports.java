package ar.edu.unq.hydra.interpreter;

import ar.edu.unq.hydra.ast.ImportDeclaration;
import ar.edu.unq.hydra.ast.expr.Name;

import java.util.ArrayList;
import java.util.List;

public class DefaultImports
{
    protected static List<ImportDeclaration> importDeclarations = new ArrayList<>();

    static {
        addPackage("java.lang");
        addPackage("java.util");
        addPackage("java.io");
        addPackage("java.util.concurrent");
        addPackage("ar.edu.unq.hydra.didactic");

        addOnDemandStatic("java.lang.Thread");
        addOnDemandStatic("java.lang.Math");
        addOnDemandStatic("ar.edu.unq.hydra.interpreter.StaticCommands");
    }

    /**
     * Adds an on demand import declaration of all classes under the given package name
     * @param name
     */
    private static void addPackage(String name)
    {
        add(name , false, true);
    }

    /**
     * adds an on demand import declaration of static members of the named class or package
     * @param name
     */
    private static  void addOnDemandStatic(String name)
    {
        add(name , true, true);
    }

    private static void add(String qualifiedName, boolean isStatic, boolean isAsterisk)
    {
        importDeclarations.add(new ImportDeclaration(Name.parse(qualifiedName), isStatic, isAsterisk));
    }

    public static List<ImportDeclaration> getImports()
    {
        return importDeclarations;
    }
}
