package ar.edu.unq.hydra.interpreter.exceptions;

/**
 * Created by Daniel Wyrytowski on 4/11/17.
 */
public class HydraDuplicateVariableDeclarationException extends HydraEvaluationException
{
    public HydraDuplicateVariableDeclarationException(String name)
    {
        super(name);
    }
}
