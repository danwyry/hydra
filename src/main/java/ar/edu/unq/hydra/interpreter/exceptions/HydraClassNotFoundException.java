package ar.edu.unq.hydra.interpreter.exceptions;

/**
 * Created by Daniel Wyrytowski on 7/30/17.
 */
public class HydraClassNotFoundException extends HydraEvaluationException
{
    private final String className;

    public HydraClassNotFoundException(String className)
    {
        this.className = className;
    }
}
