package ar.edu.unq.hydra.interpreter;

import ar.edu.unq.hydra.ast.HydraScript;
import ar.edu.unq.hydra.parser.AstBuilderUtils;
import ar.edu.unq.hydra.ui.ide.HydraIDE;

import java.io.PrintStream;
import java.io.Reader;

/**
 * Created by daniel on 10/11/17.
 */
public class Interpreter
{
    private final Reader in;
    private final PrintStream out;
    private final PrintStream err;
    private final PrintStream serr;
    private StatementEvaluator evaluator;

    private final PrintStream sout;

    public Interpreter()
    {
        this(null, System.out, System.err);
    }

    public Interpreter(Reader in, PrintStream out, PrintStream err)
    {
        this.sout = System.out ;
        this.serr = System.err ;
        this.in = in;
        this.out = out;
        this.err = err;
        this.evaluator = new StatementEvaluator();
    }

    public void interpret(String code)
    {
        redirectSystemOut();

        HydraScript ast = AstBuilderUtils.parseHydraScript(code);
        evaluator.eval(ast);

        restoreSystemOut();
    }

    public void interrupt()
    {
        evaluator.interrupt();
        restoreSystemOut();
    }

    private void redirectSystemOut() {
        System.setOut(this.out);
        System.setErr(this.err);
    }

    private void restoreSystemOut()
    {
        System.setOut(sout);
        System.setErr(serr);
    }

    public void addExceptionListener(HydraExceptionListener listener)
    {
        evaluator.addExceptionListener(listener);
    }
}
