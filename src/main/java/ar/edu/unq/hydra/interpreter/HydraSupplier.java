package ar.edu.unq.hydra.interpreter;

import java.util.function.Supplier;

@FunctionalInterface
public interface HydraSupplier extends Supplier<Object>
{
}
