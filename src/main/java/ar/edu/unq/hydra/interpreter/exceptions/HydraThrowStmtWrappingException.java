package ar.edu.unq.hydra.interpreter.exceptions;

/**
 * Created by Daniel Wyrytowski on 7/27/17.
 */
public class HydraThrowStmtWrappingException extends RuntimeException
{

    public HydraThrowStmtWrappingException(Throwable thrownException)
    {
        super(thrownException);
    }
}
