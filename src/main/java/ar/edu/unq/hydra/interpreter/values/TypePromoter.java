package ar.edu.unq.hydra.interpreter.values;

import com.github.javaparser.utils.Pair;

import java.util.function.Function;

public class TypePromoter
{
    public static HObject unaryPromotion(HObject value)
    {
        if (value.isByte() )
            return new HInt(value.getByteValue());
        else if (value.isChar())
            return new HInt(value.getCharValue());
        else if (value.isShort())
            return new HInt(value.getShortValue());
        else
            return value ;
    }

    public static Promotion binaryPromotion(HObject pleft, HObject pright)
    {
        Function<HObject, HNumeric> promoter = getPromoter(pleft, pright);
        HNumeric left = promoter.apply(pleft);
        HNumeric right = promoter.apply(pright);
        return new Promotion(left, right);
    }

    /**
     * Returns a lambda expression that promotes the applied value to the corresponding type according to the promoting
     * algorithm specified in the JLS 8
     * @return HNumeric
     */
    private static Function<HObject, HNumeric> getPromoter(final HObject left, final HObject right)
    {
        if (left.isDouble() || right.isDouble())
        {
            return (HObject value) -> new HDouble(value.getDoubleValue());
        }  else if (left.isFloat() || right.isFloat())
        {
            return (HObject value) -> new HFloat(value.getFloatValue());
        } else if (left.isLong() || right.isLong()) {
            return (HObject value) -> new HLong(value.getLongValue());
        } else {
            return (HObject value) -> new HInt(value.getIntValue());
        }
    }

    public static class Promotion extends Pair<HObject, HObject>
    {
        public Promotion(HObject hObject, HObject hObject2)
        {
            super(hObject, hObject2);
        }
        public HObject left() { return a; }
        public HObject right() { return b ; }
    }
}
