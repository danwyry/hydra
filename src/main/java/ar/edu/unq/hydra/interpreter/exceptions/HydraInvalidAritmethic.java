package ar.edu.unq.hydra.interpreter.exceptions;

/**
 * Created by Daniel Wyrytowski on 4/6/17.
 */
public class HydraInvalidAritmethic extends HydraEvaluationException
{
    public HydraInvalidAritmethic(String operator, String javaTypeName)
    {
        super("Operator '" + operator + "' cannot be applied to " + javaTypeName);
    }
}
