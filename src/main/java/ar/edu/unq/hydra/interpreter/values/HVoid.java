package ar.edu.unq.hydra.interpreter.values;

import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidCastOperation;
import ar.edu.unq.hydra.interpreter.exceptions.HydraEvaluationException;
import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidAritmethic;

/**
 * Created by Daniel Wyrytowski
 */
public class HVoid extends HObject {
    @Override
    protected String javaTypeName() {
        return "void";
    }

    @Override
    public boolean isNumeric() {
        return false;
    }

    @Override
    public boolean isBoolean() {
        return false;
    }

    @Override
    public boolean isPrimitive() {
        return true;
    }

    @Override
    public boolean isDouble() {
        return false;
    }

    @Override
    public boolean isFloat() {
        return false;
    }

    @Override
    public boolean isLong() {
        return false;
    }

    @Override
    public boolean isInt() {
        return false;
    }

    @Override
    public boolean isShort() {
        return false;
    }

    @Override
    public boolean isChar() {
        return false;
    }

    @Override
    public boolean isByte() {
        return false;
    }

    @Override
    public Object getValueAsObject()
    {
        return null;
    }

    @Override
    public short getShortValue() {
        throw new HydraEvaluationException("No value can be retrieved from void type");
    }

    @Override
    public char getCharValue() {
        throw new HydraEvaluationException("No value can be retrieved from void type");
    }

    @Override
    public int getIntValue() {
        throw new HydraEvaluationException("No value can be retrieved from void type");
    }

    @Override
    public long getLongValue() {
        throw new HydraEvaluationException("No value can be retrieved from void type");
    }

    @Override
    public float getFloatValue() {
        throw new HydraEvaluationException("No value can be retrieved from void type");
    }

    @Override
    public double getDoubleValue() {
        throw new HydraEvaluationException("No value can be retrieved from void type");
    }

    @Override
    public byte getByteValue() {
        throw new HydraEvaluationException("No value can be retrieved from void type");
    }

    @Override
    public boolean getBooleanValue() {
        return throwVoidNotAValue();
    }

    private boolean throwVoidNotAValue() {
        throw new HydraEvaluationException("No value can be retrieved from void type");
    }

    @Override
    public HObject unbox() {
        return this;
    }

    @Override
    public HObject preIncrement() {
        throw new HydraInvalidAritmethic("(++)", javaTypeName());

    }

    @Override
    public HObject posIncrement() {
        throw new HydraInvalidAritmethic("(++)", javaTypeName());
    }

    @Override
    public HObject preDecrement() {
        throw new HydraInvalidAritmethic("(--)", javaTypeName());
    }

    @Override
    public HObject posDecrement() {
        throw new HydraInvalidAritmethic("(--)", javaTypeName());
    }

    public HObject cast(Class clazz)
    {
        throw new HydraInvalidCastOperation(javaTypeName(), clazz.getTypeName());
    }

    @Override
    public HBoolean isInstanceOf(Class clazz)
    {
        return new HBoolean(clazz == void.class);
    }

    @Override
    public Class getValueClass() {
        return void.class;
    }

}
