package ar.edu.unq.hydra.interpreter.values;

import ar.edu.unq.hydra.ast.expr.BinaryExpr;
import ar.edu.unq.hydra.ast.expr.UnaryExpr;
import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidAritmethic;
import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidCastOperation;

/**
 * Created by Daniel Wyrytowski
 */
public class HDouble  extends HNumeric
{
    private double doubleVal;

    @Override
    protected String javaTypeName() { return javaTypeName(); };

    public HDouble(double v)
    {
        doubleVal = v;
    }

    @Override
    public short getShortValue() {
        return (short) doubleVal;
    }

    @Override
    public char getCharValue() {
        return 0;
    }

    @Override
    public int getIntValue() {
        return (int) doubleVal;
    }

    @Override
    public long getLongValue() {
        return (long) doubleVal;
    }

    @Override
    public float getFloatValue() {
        return (float) doubleVal;
    }

    @Override
    public double getDoubleValue() {
        return doubleVal;
    }

    @Override
    public byte getByteValue() {
        return 0;
    }

    @Override
    public boolean isDouble()
    {
        return true ;
    }

    @Override
    public HDouble plus(HObject pright)
    {
        return new HDouble(this.getDoubleValue() + pright.getDoubleValue());
    }

    @Override
    public HDouble times(HObject pright)
    {
        return new HDouble(this.getDoubleValue() * pright.getDoubleValue());
    }

    @Override
    public HDouble minus(HObject pright)
    {
        return new HDouble(this.getDoubleValue() - pright.getDoubleValue());
    }

    @Override
    public HDouble divide(HObject pright)
    {
        return new HDouble(this.getDoubleValue() / pright.getDoubleValue());
    }

    @Override
    public HDouble remainder(HObject pright)
    {
        return new HDouble(this.getDoubleValue() % pright.getDoubleValue());
    }

    @Override
    public HNumeric binAnd(HObject pright)
    {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.BINARY_AND.asString(), javaTypeName());
    }

    @Override
    public HNumeric binOr(HObject pright)
    {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.BINARY_OR.asString(), javaTypeName());
    }

    @Override
    public HNumeric lShift(HObject pright)
    {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.LEFT_SHIFT.asString(), javaTypeName());

    }

    @Override
    public HNumeric rSignedShift(HObject pright)
    {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.SIGNED_RIGHT_SHIFT.asString(), javaTypeName());

    }

    @Override
    public HNumeric rUnsignedShift(HObject pright)
    {
        throw new HydraInvalidAritmethic(BinaryExpr.Operator.UNSIGNED_RIGHT_SHIFT.asString(), javaTypeName());
    }

    @Override
    public HBoolean eq(HObject pright)
    {
        return new HBoolean(this.getDoubleValue() == pright.getDoubleValue());

    }

    @Override
    public HBoolean neq(HObject pright)
    {
        return new HBoolean(this.getDoubleValue() != pright.getDoubleValue());
    }

    @Override
    public HBoolean gt(HObject pright)
    {
        return new HBoolean(this.getDoubleValue() > pright.getDoubleValue());
    }

    @Override
    public HBoolean lt(HObject pright)
    {
        return new HBoolean(this.getDoubleValue() < pright.getDoubleValue());
    }

    @Override
    public HBoolean leq(HObject pright)
    {
        return new HBoolean(this.getDoubleValue() <= pright.getDoubleValue());
    }

    @Override
    public HBoolean geq(HObject pright)
    {
        return new HBoolean(this.getDoubleValue() >= pright.getDoubleValue());
    }

    @Override
    public HObject inversedHValue()
    {
        throw new HydraInvalidAritmethic(UnaryExpr.Operator.BITWISE_COMPLEMENT.asString(), javaTypeName());
    }

    @Override
    public HObject negativeHValue()
    {
        return new HDouble(-doubleVal);
    }

    @Override
    public HObject positiveHValue()
    {
        return new HDouble(+doubleVal);
    }

    @Override
    public HDouble preIncrement()
    {
        return new HDouble(++doubleVal);
    }

    @Override
    public HDouble posIncrement()
    {
        return new HDouble(doubleVal++);
    }

    @Override
    public HDouble preDecrement()
    {
        return new HDouble(--doubleVal);
    }

    @Override
    public HDouble posDecrement()
    {
        return new HDouble(doubleVal--);
    }


    @Override
    public HObject cast(Class clazz)
    {
        if (clazz == char.class)
            return new HChar((char) doubleVal);
        else if (clazz == boolean.class)
            throw new HydraInvalidCastOperation(javaTypeName(), "boolean");
        else if (clazz == byte.class)
            return new HByte((byte)doubleVal);
        else if (clazz == short.class)
            return new HShort((short)doubleVal);
        else if (clazz == int.class)
            return new HInt((int)doubleVal);
        else if (clazz == long.class)
            return new HLong((long)doubleVal);
        else if (clazz == float.class)
            return new HFloat((float)doubleVal);
        else if (clazz == double.class)
            return this;
        else
            return new HJObject(clazz.cast(doubleVal));
    }

    @Override
    public HBoolean isInstanceOf(Class clazz)
    {
        return new HBoolean(clazz.isInstance(doubleVal));
    }

    @Override
    public Class getValueClass() {
        return double.class;
    }

}
