package ar.edu.unq.hydra.interpreter;

import java.util.function.Function;

@FunctionalInterface
public interface HydraFunction extends Function<Object, Object>
{
    /**
     * This function will be the entry point when multiple parameters are expected. The function MUST be overriden to
     * be useful, otherwise NoSuchMethodException will be thrown.
     * This is just a hack so this can continue to be a functional interface for the purpose multiple parameters lambda
     * representation
     * @param vars
     * @return
     */
    default Object apply(Object ...vars)
    {
        return null;
    }
}
