package ar.edu.unq.hydra.interpreter;

import ar.edu.unq.hydra.interpreter.values.*;

/**
 * Created by Daniel Wyrytowski on 6/3/17.
 */

public class InvocationResult
{
    private Class type;
    private Object value;

    public InvocationResult(Class type, Object value)
    {
        this.type = type;
        this.value = value;
    }

    public HObject toHObject()
    {
        return HObject.valueTypeToHObject(type, value);
    }
}