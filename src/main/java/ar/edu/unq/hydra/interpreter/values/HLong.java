package ar.edu.unq.hydra.interpreter.values;

import ar.edu.unq.hydra.interpreter.exceptions.HydraInvalidCastOperation;

/**
 * Created by Daniel Wyrytowski
 */
public class HLong extends HNumeric
{
    private long longVal;

    public HLong(long val)
    {
        longVal = val ;
    }

    @Override
    public short getShortValue() {
        return (short) longVal;
    }

    @Override
    public char getCharValue()
    {
        return (char) longVal;
    }

    @Override
    public int getIntValue() {
        return (int) longVal;
    }

    @Override
    public long getLongValue() {
        return longVal;
    }

    @Override
    public float getFloatValue() {
        return longVal;
    }

    @Override
    public double getDoubleValue() {
        return longVal;
    }

    @Override
    public byte getByteValue()
    {
        return (byte) longVal;
    }

    @Override
    public HLong plus(HObject pright)
    {
        return new HLong(this.getLongValue() + pright.getLongValue());
    }

    @Override
    public HLong times(HObject pright)
    {
        return new HLong(this.getLongValue() * pright.getLongValue());
    }

    @Override
    public HLong minus(HObject pright)
    {
        return new HLong(this.getLongValue() - pright.getLongValue());
    }

    @Override
    public HLong divide(HObject pright)
    {
        return new HLong(this.getLongValue() / pright.getLongValue());
    }

    @Override
    public HLong remainder(HObject pright)
    {
        return new HLong(this.getLongValue() % pright.getLongValue());
    }

    @Override
    public HLong binAnd(HObject pright)
    {
        return new HLong(this.getLongValue() & pright.getLongValue());
    }

    @Override
    public HLong binOr(HObject pright)
    {
        return new HLong(this.getLongValue() | pright.getLongValue());
    }

    @Override
    public HLong lShift(HObject pright)
    {
        return new HLong(this.getLongValue() << pright.getLongValue());
    }

    @Override
    public HLong rSignedShift(HObject pright)
    {
        return new HLong(this.getLongValue() >> pright.getLongValue());
    }

    @Override
    public HLong rUnsignedShift(HObject pright)
    {
        return new HLong(this.getLongValue() >>> pright.getLongValue());
    }

    @Override
    public HBoolean eq(HObject pright)
    {
        return new HBoolean(this.getLongValue() == pright.getLongValue());

    }

    @Override
    public HBoolean neq(HObject pright)
    {
        return new HBoolean(this.getLongValue() != pright.getLongValue());
    }

    @Override
    public HBoolean gt(HObject pright)
    {
        return new HBoolean(this.getLongValue() > pright.getLongValue());
    }

    @Override
    public HBoolean lt(HObject pright)
    {
        return new HBoolean(this.getLongValue() < pright.getLongValue());
    }

    @Override
    public HBoolean leq(HObject pright)
    {
        return new HBoolean(this.getLongValue() <= pright.getLongValue());
    }

    @Override
    public HBoolean geq(HObject pright)
    {
        return new HBoolean(this.getLongValue() >= pright.getLongValue());
    }

    @Override
    public HObject inversedHValue()
    {
        return new HLong(~longVal);
    }

    @Override
    public HObject negativeHValue()
    {
        return new HLong(-longVal);
    }

    @Override
    public HObject positiveHValue()
    {
        return new HLong(+longVal);
    }


    @Override
    public HLong preIncrement()
    {
        return new HLong(++longVal);
    }

    @Override
    public HLong posIncrement()
    {
        return new HLong(longVal++);
    }

    @Override
    public HLong preDecrement()
    {
        return new HLong(--longVal);
    }

    @Override
    public HLong posDecrement()
    {
        return new HLong(longVal--);
    }


    @Override
    public HObject cast(Class clazz)
    {
        if (clazz == char.class)
            return new HChar((char) longVal);
        else if (clazz == boolean.class)
            throw new HydraInvalidCastOperation(javaTypeName(), "boolean");
        else if (clazz == byte.class)
            return new HByte((byte)longVal);
        else if (clazz == short.class)
            return new HShort((short)longVal);
        else if (clazz == int.class)
            return new HInt((int)longVal);
        else if (clazz == long.class)
            return this;
        else if (clazz == float.class)
            return new HFloat((float)longVal);
        else if (clazz == double.class)
            return new HDouble((double) longVal);
        else
            return new HJObject(clazz.cast(longVal));
    }

    @Override
    public HBoolean isInstanceOf(Class clazz)
    {
        return new HBoolean(clazz.isInstance(longVal));
    }

    @Override
    public Class getValueClass() {
        return long.class;
    }

    @Override
    protected String javaTypeName() {
        return "long";
    }
}
