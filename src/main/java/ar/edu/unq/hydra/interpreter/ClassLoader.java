package ar.edu.unq.hydra.interpreter;

/**
 * Created by Daniel Wyrytowski on 7/28/17.
 */
public class ClassLoader extends java.lang.ClassLoader
{
    private static ClassLoader INSTANCE = new ClassLoader();
    public static ClassLoader getInstance()
    {
        return INSTANCE;
    }
}
