package ar.edu.unq.hydra.parser.exceptions;

/**
 * Created by Daniel Wyrytowski on 11/6/16.
 */
public class HydraAstBuilderException extends RuntimeException
{
    public HydraAstBuilderException() {
    }

    public HydraAstBuilderException(String var1) {
        super(var1);
    }

    public HydraAstBuilderException(String var1, Throwable var2) {
        super(var1, var2);
    }

    public HydraAstBuilderException(Throwable var1) {
        super(var1);
    }

    protected HydraAstBuilderException(String var1, Throwable var2, boolean var3, boolean var4) {
        super(var1, var2, var3, var4);
    }
}
