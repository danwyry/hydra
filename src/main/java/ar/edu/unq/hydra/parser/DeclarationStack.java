package ar.edu.unq.hydra.parser;

import ar.edu.unq.hydra.ast.NodeList;
import ar.edu.unq.hydra.ast.body.FunctionDeclaration;

import java.util.*;

// -- DECLARATIONS STACK are just used for the means of helping disambiguate and determine meanings of contextually
// ambiguous names


class DeclarationsStack
{
    final private Deque<DeclarationScope> scopes ;
    List<String> singleImports = new ArrayList<>();

    public DeclarationsStack()
    {
        scopes = new ArrayDeque<>();
        openScope();
    }
    public void openScope()
    {
        scopes.addFirst(new DeclarationsStack.DeclarationScope());
    }

    public void addVar(String name)
    {
        currentScope().addVar(name);
    }

    public boolean variableDeclared(String name)
    {
        for (DeclarationScope scope : scopes)
            if (scope.varExists(name))
                return true;
        return false;
    }

    public boolean functionDeclared(String name, int numArguments)
    {
        for (DeclarationScope scope : scopes)
            if (scope.functionExists(name,numArguments))
                return true;
        return false;
    }

    private DeclarationScope currentScope() {
        return scopes.getFirst();
    }

    public void closeScope()
    {
        scopes.removeFirst();
    }

    public void addFunctions(NodeList<FunctionDeclaration> functionDeclarations)
    {
        currentScope().addFunctions(functionDeclarations);
    }

    public void addFunction(FunctionDeclaration functionDeclaration)
    {
        currentScope().addFunction(functionDeclaration);
    }

    public void addSingleImport(String text)
    {
        singleImports.add(text);
    }

    public boolean isImported(String text)
    {
        return singleImports.stream().anyMatch(importName -> importName.equals(text));
    }

    public class DeclarationScope
    {
        final List<String> vars ;
        final List<FunctionDeclaration> functions ;
        public DeclarationScope()
        {
            vars = new ArrayList<>();
            functions = new ArrayList<>();
        }
        public void addVar(String name)
        {
            vars.add(name);
        }
        public void addFunction(FunctionDeclaration function )
        {
            functions.add(function);
        }
        public boolean varExists(String name)
        {
            return vars.stream().anyMatch(s -> s.equals(name));
        }

        public boolean functionExists(String name, int numArguments)
        {
            return functions.stream()
                    .anyMatch(f -> f.getNameAsString().equals(name)
                            && f.getParameters().size() == numArguments);
        }

        public void addFunctions(NodeList<FunctionDeclaration> functionDeclarations)
        {
            functions.addAll(functionDeclarations);
        }
    }
}
