package ar.edu.unq.hydra.parser.exceptions;

/**
 * Created by Daniel Wyrytowski on 11/6/16.
 */
public class HydraAstToBeImplementedException extends RuntimeException
{
    public HydraAstToBeImplementedException() {
    }

    public HydraAstToBeImplementedException(String var1) {
        super(var1);
    }

    public HydraAstToBeImplementedException(String var1, Throwable var2) {
        super(var1, var2);
    }

    public HydraAstToBeImplementedException(Throwable var1) {
        super(var1);
    }

    protected HydraAstToBeImplementedException(String var1, Throwable var2, boolean var3, boolean var4) {
        super(var1, var2, var3, var4);
    }
}
