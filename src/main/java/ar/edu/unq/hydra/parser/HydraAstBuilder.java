package ar.edu.unq.hydra.parser;

import ar.edu.unq.hydra.ast.*;
import ar.edu.unq.hydra.ast.body.*;
import ar.edu.unq.hydra.ast.expr.*;
import ar.edu.unq.hydra.ast.stmt.*;
import ar.edu.unq.hydra.ast.type.*;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.parser.exceptions.HydraAstBuilderException;
import ar.edu.unq.hydra.parser.exceptions.HydraAstToBeImplementedException;
import ar.edu.unq.hydra.ast.NameUtils;
import ar.edu.unq.hydra.ast.Range;
import com.github.javaparser.utils.Pair;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.*;
import java.util.stream.Collectors;

import static com.github.javaparser.utils.Utils.coalesce;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/*
 * IMPORTANT:
 * Due to Hydra's gradual typing, the some code will result ambiguous in some cases, and the parser will
 * enter through a typeName, packageName or PackageOrTypeName when it should be a fieldAccessExpression, and
 * viceversa. That's why visitors for all of this contexts will result in a Name node, which should be converted
 * to the correct type depending in the context, or, if that's not possible (i.e. a type might resolve in runtime)
 * an AmbiguousNameExpr node should be constructed.
 * The same disclaimer goes for invocations to functions and imported static methods. but in this case
 */

/**
 *
 */
public class HydraAstBuilder extends HydraBaseVisitor<Visitable>
{

    private static <X extends Node> NodeList<X> emptyList() {
        return new NodeList<X>();
    }

    private <T extends Node> NodeList<T> add(NodeList<T> list, T obj) {
        if (list == null) {
            list = new NodeList<T>();
        }
        list.add(obj);
        return list;
    }

    private <T extends Node> NodeList<T> addWhenNotNull(NodeList<T> list, T obj) {
        if(obj == null) {
            return list;
        }
        return add(list, obj);
    }


    private <T extends Node> NodeList<T> add(int pos, NodeList<T> list, T obj) {
        if (list == null) {
            list = new NodeList<T>();
        }
        list.add(pos, obj);
        return list;
    }

    private <T extends Node> NodeList<T> addAll(NodeList<T> list, List<T> obj) {
        if (list == null) {
            list = new NodeList<T>();
        }
        list.addAll(obj);
        return list;
    }

    private <T extends Node> NodeList<T> addAllWhenNotNull(NodeList<T> list, List<T> obj)
    {
        if(obj == null) {
            return list;
        }
        return addAll(list, obj);
    }


    /**
     * Explicitely cast the result of a visit call, or return null if the context is null
     * @param ctx
     * @param <RT> Return type
     * @param <CTX> Context
     * @return
     */
    private <RT, CTX extends ParserRuleContext> RT visitAndCastOrNull(CTX ctx)
    {
        return isNull(ctx) ? null : (RT) visit(ctx);
    }

    /**
     * Explicitely cast the result of a visit call, or return UnknownType instance if ctx is null
     * @param ctx
     * @param <RT>
     * @param <CTX>
     * @return
     */
    private <RT extends Type, CTX extends ParserRuleContext> Type visitAndCastOrUnknownType(CTX ctx)
    {
        return isNull(ctx) ? unknownType() : (RT) visit(ctx);
    }

    /**
     * given a ParserRuleContext it forces the visit result to a NodeList, if the context is null
     * @param ctx
     * @return
     */
    private <T extends Node> NodeList<T> visitAndEnsureNodeList(ParserRuleContext ctx)
    {
        if ( isNull(ctx) ) return NodeList.nodeList();

        return coalesce(    () -> visitAndCastOrNull(ctx)
                        ,   () -> new NodeList<T>() );
    }

    private DeclarationsStack declarationsStack = new DeclarationsStack();

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override public HydraScript visitHydraScript(HydraParser.HydraScriptContext ctx)
    {
        declarationsStack.openScope();

        NodeList<ImportDeclaration> imports = mapImports(ctx.importDeclaration());
        NodeList<TypeDeclaration<?>> typeDeclarations = mapTypeDeclarations(ctx.typeDeclaration());
        NodeList<FunctionDeclaration> functionDeclarations = mapFunctionDeclarations(ctx.functionDeclaration());
        NodeList<Statement> statements = mapScriptStatements(ctx.scriptStatement());

        declarationsStack.closeScope();

        HydraScript node = new HydraScript(statements, imports, typeDeclarations, functionDeclarations);

        return  node;
    }


    /**
     * Given a List of ScriptStatementContext it returns a list of Statements that results from visiting each ScriptStatementContext
     * @param scriptStatementContexts
     * @return
     */
    private NodeList<Statement> mapScriptStatements(List<HydraParser.ScriptStatementContext> scriptStatementContexts)
    {
         List<Statement> list = scriptStatementContexts.stream()
                .map(stmtCtx -> (Statement) visit(stmtCtx))
                .collect(Collectors.toList());

        return NodeList.nodeList(list);
    }

    /**
     * Given a List of ImportDeclaractionContext it returns a list of ImportDeclarations that results from visiting each ImportDeclarationContext
     * @param importDeclarationContexts
     * @return
     */
    private NodeList<ImportDeclaration> mapImports(List<HydraParser.ImportDeclarationContext> importDeclarationContexts)
    {
        List<ImportDeclaration> list = importDeclarationContexts.stream()
                .map(importDeclarationCxt -> (ImportDeclaration) visit(importDeclarationCxt))
                .collect(Collectors.toList());

        return NodeList.nodeList(list);
    }


    /**
     * Given a List of TypeDeclarationContetxt it returns a list of TypeDeclaration that results from visiting each TypeDeclarationContext
     * @param typeDeclarationContexts
     * @return
     */
    private NodeList<TypeDeclaration<?>> mapTypeDeclarations(List<HydraParser.TypeDeclarationContext> typeDeclarationContexts)
    {
        List<TypeDeclaration<?>> list = typeDeclarationContexts.stream()
                .map(typeDeclarationCxt -> (TypeDeclaration<?>) visit(typeDeclarationCxt))
                .collect(Collectors.toList());

        return NodeList.nodeList(list);
    }

    @Override
    public Node visitSingleTypeImportDeclaration(HydraParser.SingleTypeImportDeclarationContext ctx)
    {
        Name name = (Name) visit(ctx.typeName());
        return new ImportDeclaration(name, false, false);
    }

    @Override
    public Node visitTypeImportOnDemandDeclaration(HydraParser.TypeImportOnDemandDeclarationContext ctx)
    {
        Name name = (Name) visit(ctx.packageOrTypeName());
        return new ImportDeclaration(name, false, true);

    }

    @Override
    public Node visitSingleStaticImportDeclaration(HydraParser.SingleStaticImportDeclarationContext ctx)
    {
        Name name = (Name) visit(ctx.typeName());
        String identifier = ctx.Identifier().getText();
        name = new Name(name, identifier);
        return new ImportDeclaration(name, true, false);
    }

    @Override
    public Node visitStaticImportOnDemandDeclaration(HydraParser.StaticImportOnDemandDeclarationContext ctx)
    {
        Name name = (Name) visit(ctx.typeName());
        return new ImportDeclaration(name, true, true);
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override public ExpressionStmt visitGlobalVariableDeclarationStatement(HydraParser.GlobalVariableDeclarationStatementContext ctx)
    {
        VariableDeclarationExpr expr = (VariableDeclarationExpr) visit(ctx.globalVariableDeclaration());

        return new ExpressionStmt(expr);
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override public VariableDeclarationExpr visitGlobalVariableDeclaration(HydraParser.GlobalVariableDeclarationContext ctx)
    {
        Boolean global = true ;

        EnumSet<Modifier> modifiers = mapVariableModifiers(ctx.variableModifier()) ;
        NodeList<AnnotationExpr> annotations =  mapVariableAnnotations(ctx.variableModifier());
        Type partialType = visitAndCastOrUnknownType(ctx.unannType());
        NodeList<VariableDeclarator> variables = mapVariableDeclarators(partialType, ctx.variableDeclaratorList().variableDeclarator());

        addVariableDeclaratorsToDeclarationStack(variables);

        return new VariableDeclarationExpr(modifiers,annotations,variables).setGlobal(global); // modifiers, annotations, variables).setGlobal(global);
    }

    private void addVariableDeclaratorsToDeclarationStack(NodeList<VariableDeclarator> variables)
    {
        variables.forEach(variableDeclarator -> addVariableDeclaratorToDeclarationStack(variableDeclarator));
    }

    private void addVariableDeclaratorToDeclarationStack(VariableDeclarator variableDeclarator)
    {
        declarationsStack.addVar(variableDeclarator.getNameAsString());
    }


    private NodeList<VariableDeclarator> mapVariableDeclarators(Type partialType, List<HydraParser.VariableDeclaratorContext> variableDeclaratorContexts)
    {
        List<VariableDeclarator> list = variableDeclaratorContexts
                .stream()
                .map(variableDeclaratorContext -> mapVariableDeclarator(partialType, variableDeclaratorContext))
                .collect(Collectors.toList());

        return NodeList.nodeList(list);
    }

    /**
     * GIven a type and a VariableDeclaratorContext, returns a VariableDeclarator node and completes its array
     * type if dims are defined to the right of the variable identifier.
     * i.e. :     var1 [] @SomeAnnotation[]
     *
     * @param type
     * @param variableDeclaratorContext
     * @return
     */
    private VariableDeclarator mapVariableDeclarator(Type type, HydraParser.VariableDeclaratorContext variableDeclaratorContext)
    {
        VariableDeclarator variableDeclarator = (VariableDeclarator) visit(variableDeclaratorContext);

        if ( nonNull(variableDeclaratorContext.variableDeclaratorId().dims()) )
        {
            List<ArrayType.ArrayBracketPair> arrayBracketPairs = mapAnnotatedDimsToArrayBracketPair(variableDeclaratorContext.variableDeclaratorId().dims().annotatedDim());
            type = juggleArrayType(type, arrayBracketPairs);
        }
        variableDeclarator.setType(type);

        return variableDeclarator ;
    }

    @Override
    public VariableDeclarator visitVariableDeclarator(HydraParser.VariableDeclaratorContext ctx)
    {
        SimpleName name = (SimpleName) visit(ctx.variableDeclaratorId());
        Expression init = visitAndCastOrNull(ctx.variableInitializer());

        // As the VariableDeclaratorContext doesn't contain the variable type I set Unknown type by default
        UnknownType type = unknownType();

        return new VariableDeclarator(type,name,init);
    }

    @Override
    public SimpleName visitVariableDeclaratorId(HydraParser.VariableDeclaratorIdContext ctx)
    {
        String name = ctx.Identifier().getText();
        return new SimpleName(name);
    }

    /**
     * Given a VariableModifierContext it returns a NodeList of AnnotationExpressions
     * @param variableModifierContexts
     * @return
     */
    private NodeList<AnnotationExpr> mapVariableAnnotations(List<HydraParser.VariableModifierContext> variableModifierContexts)
    {
        List<AnnotationExpr> list = variableModifierContexts.stream()
                .filter(variableModifierContext -> nonNull(variableModifierContext.annotation()))
                .map(variableModifierCtx -> (AnnotationExpr) visit(variableModifierCtx)).collect(Collectors.toList());
        return NodeList.nodeList(list);
    }

    /**
     * Given a VariableModifierContext it returns a EnumSet of Modifiers matching the ones that came in the context
     * @param variableModifierContexts
     * @return
     */
    private EnumSet<Modifier> mapVariableModifiers(List<HydraParser.VariableModifierContext> variableModifierContexts)
    {
        List<Modifier> modifiersList = variableModifierContexts.stream()
                .filter(variableModifierContext -> isNull(variableModifierContext.annotation()))
                .map(variableModifierCtx -> mapVariableModifier(variableModifierCtx)).collect(Collectors.toList());

        return (modifiersList.isEmpty()) ? EnumSet.noneOf(Modifier.class) : EnumSet.copyOf(modifiersList);
    }

    private Modifier mapVariableModifier(HydraParser.VariableModifierContext variableModifierCtx)
    {
        return Modifier.fromString(variableModifierCtx.getText());
    }

    @Override
    public Visitable visitArrayInitializer(HydraParser.ArrayInitializerContext ctx)
    {
        NodeList<Expression> values = visitAndEnsureNodeList(ctx.variableInitializerList());
        return new ArrayInitializerExpr(values);
    }

    @Override
    public NodeList<Expression> visitVariableInitializerList(HydraParser.VariableInitializerListContext ctx)
    {
        List<Expression> initializers = ctx.variableInitializer().stream()
                .map(variableInitializerContext -> (Expression) visit(variableInitializerContext))
                .collect(Collectors.toList());

        return NodeList.nodeList(initializers);
    }

    @Override
    public Type visitUnannClassOrInterfaceType(HydraParser.UnannClassOrInterfaceTypeContext ctx)
    {
        ClassOrInterfaceType node = (ClassOrInterfaceType) visit(ctx.unannClassOrInterfaceType_lfno());
        ClassOrInterfaceType curr = null, scope = node ;
        curr = scope ;

        for (HydraParser.UnannClassOrInterfaceType_lfContext lf_ctx : ctx.unannClassOrInterfaceType_lf())
        {
            curr = (ClassOrInterfaceType) visit(lf_ctx);
            curr.setScope(scope);
            scope = curr ;
        }
        return curr;
    }

    @Override
    public Node visitUnannClassType_lf_unannClassOrInterfaceType(HydraParser.UnannClassType_lf_unannClassOrInterfaceTypeContext ctx)
    {
        return createClassOrInterfaceType(ctx.Identifier(), ctx.annotation(), ctx.typeArguments());
    }

    @Override
    public ClassOrInterfaceType visitUnannClassOrInterfaceType_lfno(HydraParser.UnannClassOrInterfaceType_lfnoContext ctx)
    {
        ParserRuleContext context = coalesce(
                () -> ctx.unannClassType_lfno_unannClassOrInterfaceType(),
                () -> ctx.unannInterfaceType_lfno_unannClassOrInterfaceType()
        );
        return (ClassOrInterfaceType) visit(context);
    }

    @Override
    public ClassOrInterfaceType visitUnannClassType_lfno_unannClassOrInterfaceType(HydraParser.UnannClassType_lfno_unannClassOrInterfaceTypeContext ctx)
    {
        return createClassOrInterfaceType(ctx.Identifier(), null, ctx.typeArguments());
    }

    @Override
    public ClassOrInterfaceType visitUnannTypeVariable(HydraParser.UnannTypeVariableContext ctx)
    {
        return new ClassOrInterfaceType( ctx.Identifier().getText() );
    }

    @Override
    public Visitable visitUnannArrayTypePrimitiveType(HydraParser.UnannArrayTypePrimitiveTypeContext ctx)
    {
        Type partialType = (Type) visit(ctx.unannPrimitiveType());
        List<ArrayType.ArrayBracketPair> arrayBracketPairs = mapAnnotatedDimsToArrayBracketPair(ctx.dims().annotatedDim());

        return ArrayType.wrapInArrayTypes(partialType, arrayBracketPairs);
    }

    @Override
    public Visitable visitUnannArrayTypeClassOrInterfaceType(HydraParser.UnannArrayTypeClassOrInterfaceTypeContext ctx)
    {
        Type partialType = (Type) visit(ctx.unannClassOrInterfaceType());
        List<ArrayType.ArrayBracketPair> arrayBracketPairs = mapAnnotatedDimsToArrayBracketPair(ctx.dims().annotatedDim());

        return ArrayType.wrapInArrayTypes(partialType, arrayBracketPairs);
    }

    @Override
    public Visitable visitUnannArrayTypeTypeVariable(HydraParser.UnannArrayTypeTypeVariableContext ctx)
    {
        Type partialType = (Type) visit(ctx.unannTypeVariable());
        List<ArrayType.ArrayBracketPair> arrayBracketPairs = mapAnnotatedDimsToArrayBracketPair(ctx.dims().annotatedDim());

        return ArrayType.wrapInArrayTypes(partialType, arrayBracketPairs);
    }

    private List<ArrayType.ArrayBracketPair> mapAnnotatedDimsToArrayBracketPair(List<HydraParser.AnnotatedDimContext> annotatedDimContexts)
    {
        List<ArrayType.ArrayBracketPair> list = annotatedDimContexts.stream()
                .map(annotatedDimContext -> {
                    NodeList<AnnotationExpr> annotations = mapDimAnnotations(annotatedDimContext.annotation());
                    return new ArrayType.ArrayBracketPair(null, annotations);
                })
                .collect(Collectors.toList());
        return list;
    }

    private NodeList<AnnotationExpr> mapDimAnnotations(List<HydraParser.AnnotationContext> annotations)
    {
        List<AnnotationExpr> list = annotations.stream()
                .map(annotationContext -> (AnnotationExpr) visit(annotationContext))
                .collect(Collectors.toList());
        return NodeList.nodeList(list);
    }

    @Override
    public Type visitUnannType(HydraParser.UnannTypeContext ctx)
    {
        return isNull(ctx) ? unknownType() : (Type) super.visitUnannType(ctx);
    }

    @Override
    public PrimitiveType visitBooleanUnannPrimitiveType(HydraParser.BooleanUnannPrimitiveTypeContext ctx)
    {
        return PrimitiveType.booleanType();
    }

    @Override
    public PrimitiveType visitByteIntegralType(HydraParser.ByteIntegralTypeContext ctx)
    {
        return PrimitiveType.byteType();
    }

    @Override
    public PrimitiveType visitShortIntegralType(HydraParser.ShortIntegralTypeContext ctx)
    {
        return PrimitiveType.shortType();
    }

    @Override
    public PrimitiveType visitIntIntegralType(HydraParser.IntIntegralTypeContext ctx)
    {
        return PrimitiveType.intType();
    }

    @Override
    public PrimitiveType visitLongIntegralType(HydraParser.LongIntegralTypeContext ctx)
    {
        return PrimitiveType.longType();
    }

    @Override
    public PrimitiveType visitCharIntegralType(HydraParser.CharIntegralTypeContext ctx)
    {
        return PrimitiveType.charType();
    }

    @Override
    public PrimitiveType visitFloatFloatingPointType(HydraParser.FloatFloatingPointTypeContext ctx)
    {
        return PrimitiveType.floatType();
    }

    @Override
    public PrimitiveType visitDoubleFloatingPointType(HydraParser.DoubleFloatingPointTypeContext ctx)
    {
        return PrimitiveType.doubleType();
    }

    @Override
    public Visitable visitUnannClassTypeBase(HydraParser.UnannClassTypeBaseContext ctx)
    {
        return createClassOrInterfaceType(ctx.Identifier(), null, ctx.typeArguments());
    }

    @Override
    public Visitable visitUnannClassTypeWithScope(HydraParser.UnannClassTypeWithScopeContext ctx)
    {
        return createClassOrInterfaceType(ctx.unannClassOrInterfaceType(), ctx.Identifier(), ctx.annotation(), ctx.typeArguments());
    }

    @Override
    public Node visitClassOrInterfaceType(HydraParser.ClassOrInterfaceTypeContext ctx)
    {
        ClassOrInterfaceType node = (ClassOrInterfaceType) visit(ctx.classOrInterfaceType_lfno());

        ClassOrInterfaceType curr = null, scope = node ;
        curr = scope ;

        for (HydraParser.ClassOrInterfaceType_lfContext lf_ctx : ctx.classOrInterfaceType_lf())
        {
            curr = (ClassOrInterfaceType) visit(lf_ctx);
            curr.setScope(scope);
            scope = curr ;
        }

        return curr;
    }

    @Override
    public ClassOrInterfaceType visitClassOrInterfaceType_lfno(HydraParser.ClassOrInterfaceType_lfnoContext ctx)
    {
        ParserRuleContext context = coalesce(
              () -> ctx.classType_lfno_classOrInterfaceType()
            , () -> ctx.interfaceType_lfno_classOrInterfaceType() );

        return visitAndCastOrNull(context);
    }

    @Override
    public Node visitClassOrInterfaceType_lf(HydraParser.ClassOrInterfaceType_lfContext ctx)
    {
        ParserRuleContext context = coalesce(
              () -> ctx.classType_lf_classOrInterfaceType()
            , () -> ctx.interfaceType_lf_classOrInterfaceType() );

        return (ClassOrInterfaceType) visit(context);
    }

    @Override
    public NormalAnnotationExpr visitNormalAnnotation(HydraParser.NormalAnnotationContext ctx)
    {
        throw new HydraAstToBeImplementedException("visitNormalAnnotation");
//        return new NormalAnnotationExpr();  // TODO
    }

    @Override
    public MarkerAnnotationExpr visitMarkerAnnotation(HydraParser.MarkerAnnotationContext ctx)
    {
        Name name = (Name) visit(ctx.typeName());
        return new MarkerAnnotationExpr(name);
    }

    @Override
    public SingleMemberAnnotationExpr visitSingleElementAnnotation(HydraParser.SingleElementAnnotationContext ctx)
    {
        throw new HydraAstToBeImplementedException("visitSingleElementAnnotation");
//        return new SingleMemberAnnotationExpr(); // TODO
    }

    @Override
    public Name visitPackageName(HydraParser.PackageNameContext ctx)
    {
        return Name.parse( ctx.getText() ) ;
    }

    /**
     * Due to Hydra's gradual typing, the grammar definition will result ambiguous in some cases, and the parser will
     * enter through a typeName, packageName or PackageOrTypeName when it should be a fieldAccessExpression, and
     * viceversa. That's why visitors for all of this productions will result in a Name node, which should be converted
     * to the correct type depending in the context, or, if that's not possible because a type might resolve in runtime,
     * an AmbiguousNameExpr node should be constructed.
     * @param ctx
     * @return
     */
    @Override
    public Name visitTypeName(HydraParser.TypeNameContext ctx)
    {
        return Name.parse(ctx.getText());
    }


    @Override
    public Name visitPackageOrTypeName(HydraParser.PackageOrTypeNameContext ctx)
    {
        return Name.parse( ctx.getText() ) ;
    }

    @Override
    public BlockStmt visitBlock(HydraParser.BlockContext ctx)
    {
        declarationsStack.openScope();
        NodeList<Statement> statements = visitAndEnsureNodeList(ctx.blockStatements());
        declarationsStack.closeScope();
        return new BlockStmt(statements);
    }

    @Override
    public NodeList<Statement> visitBlockStatements(HydraParser.BlockStatementsContext ctx)
    {
        List<Statement> list = ctx.blockStatement().stream()
                .map(blockStatementContext -> (Statement) visit(blockStatementContext))
                .collect(Collectors.toList());

        return NodeList.nodeList(list);
    }

    @Override
    public ExpressionStmt visitLocalVariableDeclarationStatement(HydraParser.LocalVariableDeclarationStatementContext ctx)
    {
        VariableDeclarationExpr expr = (VariableDeclarationExpr) visit(ctx.localVariableDeclaration());

        return new ExpressionStmt(expr);
    }

    @Override
    public VariableDeclarationExpr visitLocalVariableDeclaration(HydraParser.LocalVariableDeclarationContext ctx)
    {
        Type partialType = visitAndCastOrUnknownType(ctx.unannType());
        EnumSet<Modifier> modifiers = mapVariableModifiers(ctx.variableModifier()) ;
        NodeList<AnnotationExpr> annotations =  mapVariableAnnotations(ctx.variableModifier());
        NodeList<VariableDeclarator> variables = mapVariableDeclarators(partialType, ctx.variableDeclaratorList().variableDeclarator());

        addVariableDeclaratorsToDeclarationStack(variables);

        return new VariableDeclarationExpr(modifiers, annotations, variables);
    }

    @Override
    public EmptyStmt visitEmptyStatement(HydraParser.EmptyStatementContext ctx)
    {
        return new EmptyStmt();
    }

    @Override
    public IfStmt visitIfThenStatement(HydraParser.IfThenStatementContext ctx)
    {
        Expression condition = (Expression) visit(ctx.expression());
        Statement thenStatement = (Statement) visit(ctx.statement());

        return new IfStmt(condition, thenStatement, null);
    }

    @Override
    public IfStmt visitIfThenElseStatement(HydraParser.IfThenElseStatementContext ctx)
    {
        Expression condition = (Expression) visit(ctx.expression());
        Statement thenStatement = (Statement) visit(ctx.statementNoShortIf());
        Statement elseStatement = (Statement) visit(ctx.statement());

        return new IfStmt(condition, thenStatement, elseStatement);
    }

    @Override
    public IfStmt visitIfThenElseStatementNoShortIf(HydraParser.IfThenElseStatementNoShortIfContext ctx)
    {
        Expression condition = (Expression) visit(ctx.expression());
        Statement thenStatement = (Statement) visit(ctx.statementNoShortIf(0));
        Statement elseStatement = (Statement) visit(ctx.statementNoShortIf(1));

        return new IfStmt(condition, thenStatement, elseStatement);
    }

    @Override
    public WhileStmt visitWhileStatement(HydraParser.WhileStatementContext ctx)
    {
        declarationsStack.openScope();
        Expression expr = (Expression) visit(ctx.expression());
        Statement body = (Statement) visit(ctx.statement());
        declarationsStack.closeScope();
        return new WhileStmt(expr,body);
    }

    @Override
    public ForStmt visitBasicForStatement(HydraParser.BasicForStatementContext ctx)
    {
        declarationsStack.openScope();

        NodeList<Expression> init = (NodeList<Expression>) visit(ctx.forInit());
        Expression compare = (Expression) visit(ctx.expression());
        NodeList<Expression> update = mapStatementExpressionList(ctx.forUpdate().statementExpressionList().statementExpression());
        Statement body = (Statement) visit(ctx.statement());

        declarationsStack.closeScope();

        return new ForStmt(init, compare, update, body);
    }

    @Override
    public NodeList<Expression> visitForInit(HydraParser.ForInitContext forInitContext)
    {
        return ( nonNull(forInitContext.localVariableDeclaration()))
            ? add(null, (Expression) visit(forInitContext.localVariableDeclaration()))
            : mapStatementExpressionList( forInitContext.statementExpressionList().statementExpression() );
    }

    private NodeList<Expression> mapStatementExpressionList(List<HydraParser.StatementExpressionContext> statementExpressionContexts)
    {
        List<Expression> list = statementExpressionContexts
                .stream()
                .map(statementExpressionContext -> (Expression) visit(statementExpressionContext))
                .collect(Collectors.toList());
        return NodeList.nodeList(list);
    }

    @Override
    public ForeachStmt visitEnhancedForStatement(HydraParser.EnhancedForStatementContext ctx) // foreach
    {
        declarationsStack.openScope();

        String name = ctx.variableDeclaratorId().Identifier().getText();
        Type type = visitAndCastOrUnknownType(ctx.unannType());
        VariableDeclarator variableDeclarator = new VariableDeclarator(type, name);
        EnumSet<Modifier> modifiers = mapVariableModifiers(ctx.variableModifier());

        VariableDeclarationExpr var = new VariableDeclarationExpr(variableDeclarator);
        var.setModifiers(modifiers);

        addVariableDeclaratorToDeclarationStack(variableDeclarator);

        Expression iterable = (Expression) visit(ctx.expression());
        Statement body = (Statement) visit(ctx.statement());

        declarationsStack.closeScope();

        return new ForeachStmt(var, iterable, body);
    }

    @Override
    public Node visitRepeatStatement(HydraParser.RepeatStatementContext ctx)
    {
        Expression iterations = (Expression) visit(ctx.expression());
        Statement body = (Statement) visit(ctx.statement());
        return new RepeatStmt(iterations, body);
    }

    @Override
    public Node visitExpressionStatement(HydraParser.ExpressionStatementContext ctx)
    {
        Expression expr = (Expression) visit(ctx.statementExpression());
        return new ExpressionStmt(expr);
    }

    //------------------
    //----- EXPRESSIONS

    @Override
    public LiteralExpr visitLiteralInteger(HydraParser.LiteralIntegerContext ctx)
    {
        LiteralExpr expression ;
        // TODO: Falta contemplar el caso de los octales y binaryIntegerLiteral
        String value = ctx.IntegerLiteral().getText();

        int lastIdx = ctx.IntegerLiteral().getText().length() -1 ;
        char lastChar = ctx.IntegerLiteral().getText().charAt(lastIdx);

        if (lastChar != 'l' && lastChar != 'L' )
        {
            expression = new IntegerLiteralExpr(value);
        } else {
            value = value.substring(0,lastIdx);
            expression = new LongLiteralExpr(value);
        }
        return expression;
    }

    @Override
    public Expression visitLiteralCharacter(HydraParser.LiteralCharacterContext ctx)
    {
//        FIXME: No contempla caso de EscapeSequence
        String value = ctx.CharacterLiteral().getText();
        value = value.substring(1,value.length()-1); // strip beggining and ending quotes

        return new CharLiteralExpr(value);
    }

    @Override
    public Node visitLiteralString(HydraParser.LiteralStringContext ctx)
    {
        String value = ctx.StringLiteral().getText();
        value = value.substring(1,value.length()-1); // strip beggining and ending quotes

        return new StringLiteralExpr(value);
    }

    @Override
    public Expression visitLiteralBoolean(HydraParser.LiteralBooleanContext ctx)
    {
        return new BooleanLiteralExpr( Boolean.valueOf(ctx.getText()) );
    }

    @Override
    public Expression visitLiteralFloatingPoint(HydraParser.LiteralFloatingPointContext ctx)
    {
        int lastIdx = ctx.FloatingPointLiteral().getText().length() -1 ;
        String value = ctx.FloatingPointLiteral().getText().substring(0,lastIdx);
        return new DoubleLiteralExpr(value);
    }

    @Override
    public Node visitLiteralNull(HydraParser.LiteralNullContext ctx)
    {
        return new NullLiteralExpr();
    }



    @Override
    public AssignExpr visitAssignment(HydraParser.AssignmentContext ctx)
    {
        Expression nameExpr = (Expression) visit(ctx.leftHandSide());
        Expression valueExpr = (Expression) visit(ctx.expression());

        AssignExpr.Operator operator = mapOperator(ctx.assignmentOperator());

        return new AssignExpr(nameExpr, valueExpr, operator);
    }

    @Override
    public Expression visitExpressionName(HydraParser.ExpressionNameContext ctx)
    {
//        String qualifiedName =  ctx.Identifier().getText();
//        if (!isNull(ctx.ambiguousName()) ) qualifiedName = ctx.ambiguousName().getText() + "." + qualifiedName;
//
//        Name parse = Name.parse(qualifiedName);
//        return  parseExpressionName(parse);

        if ( isNull(ctx.ambiguousName()) )
        {
            if (declarationsStack.variableDeclared(ctx.Identifier().getText()) || declarationsStack.isImported(ctx.Identifier().getText()))
                return new NameExpr(ctx.Identifier().getText());
        } else if ( nonNull(ctx.ambiguousName()) )
        {
            Name name = Name.parse(ctx.getText());
            Name leftest = name.getLeftest();
            if (declarationsStack.variableDeclared(leftest.asString()) || declarationsStack.isImported(leftest.asString()))
                return nameToFieldAccessExpr(name);
        }

        return new AmbiguousNameExpr(ctx.getText());
    }


    private AssignExpr.Operator mapOperator(HydraParser.AssignmentOperatorContext assignmentOperatorContext)
    {
        switch (assignmentOperatorContext.getText())
        {
            case "=":
                return AssignExpr.Operator.ASSIGN;
            case "*=":
                return AssignExpr.Operator.MULTIPLY;
            case "/=":
                return AssignExpr.Operator.DIVIDE;
            case "%=":
                return AssignExpr.Operator.REMAINDER;
            case "+=":
                return AssignExpr.Operator.PLUS;
            case "-=":
                return AssignExpr.Operator.MINUS;
            case "<<=":
                return AssignExpr.Operator.LEFT_SHIFT;
            case ">>=":
                return AssignExpr.Operator.SIGNED_RIGHT_SHIFT;
            case ">>>=":
                return AssignExpr.Operator.UNSIGNED_RIGHT_SHIFT;
            case "&=":
                return AssignExpr.Operator.AND;
            case "^=":
                return AssignExpr.Operator.XOR;
            case "|=":
                return AssignExpr.Operator.OR;
            default:
                return null; // FIXME
        }
    }

    @Override
    public Node visitPostfixExpression(HydraParser.PostfixExpressionContext ctx)
    {
        Expression expression = (Expression) visit(ctx.postfixExpression_nolf());

        for (HydraParser.PostfixExpression_lfContext postfixExpression_lfContext :  ctx.postfixExpression_lf())
        {
            UnaryExpr.Operator op = mapOperator(postfixExpression_lfContext);
            expression = new UnaryExpr(expression, op);
        }

        return expression;
    }

    private UnaryExpr.Operator mapOperator(HydraParser.PostfixExpression_lfContext ctx)
    {
        if ( nonNull(ctx.postDecrementExpression_lf_postfixExpression()) )
            return UnaryExpr.Operator.POSTFIX_DECREMENT;
        else if (nonNull(ctx.postIncrementExpression_lf_postfixExpression()))
            return UnaryExpr.Operator.POSTFIX_INCREMENT;
        else
            return null ; // FIXME
    }

    @Override
    public Node visitPreIncrementExpression(HydraParser.PreIncrementExpressionContext ctx)
    {
        Expression expression = (Expression) visit(ctx.unaryExpression());
        UnaryExpr unaryExpr = new UnaryExpr(expression, UnaryExpr.Operator.PREFIX_INCREMENT);
        return unaryExpr;
    }

    @Override
    public Node visitPreDecrementExpression(HydraParser.PreDecrementExpressionContext ctx)
    {
        Expression expression = (Expression) visit(ctx.unaryExpression());
        UnaryExpr unaryExpr = new UnaryExpr(expression, UnaryExpr.Operator.PREFIX_DECREMENT);
        return unaryExpr;
    }

    @Override
    public Node visitPostIncrementExpression(HydraParser.PostIncrementExpressionContext ctx)
    {
        Expression expression = (Expression) visit(ctx.postfixExpression());
        UnaryExpr unaryExpr = new UnaryExpr(expression, UnaryExpr.Operator.POSTFIX_INCREMENT);
        return unaryExpr;
    }

    @Override
    public Node visitPostDecrementExpression(HydraParser.PostDecrementExpressionContext ctx)
    {
        Expression expression = (Expression) visit(ctx.postfixExpression());
        UnaryExpr unaryExpr = new UnaryExpr(expression, UnaryExpr.Operator.POSTFIX_DECREMENT);
        return unaryExpr;
    }

    @Override
    public Node visitPlusUnaryExpression(HydraParser.PlusUnaryExpressionContext ctx)
    {
        Expression expression = (Expression) visit(ctx.unaryExpression());
        UnaryExpr unaryExpr = new UnaryExpr(expression, UnaryExpr.Operator.PLUS);
        return unaryExpr;
    }

    @Override
    public Node visitMinusUnaryExpression(HydraParser.MinusUnaryExpressionContext ctx)
    {
        Expression expression = (Expression) visit(ctx.unaryExpression());
        UnaryExpr unaryExpr = new UnaryExpr(expression, UnaryExpr.Operator.MINUS);
        return unaryExpr;
    }

    @Override
    public Node visitInverseUnaryExpressionNotPlusMinus(HydraParser.InverseUnaryExpressionNotPlusMinusContext ctx)
    {
        Expression expression = (Expression) visit(ctx.unaryExpression());
        UnaryExpr unaryExpr = new UnaryExpr(expression, UnaryExpr.Operator.BITWISE_COMPLEMENT);
        return unaryExpr;
    }

    @Override
    public Node visitNotUnaryExpressionNotPlusMinus(HydraParser.NotUnaryExpressionNotPlusMinusContext ctx)
    {
        Expression expression = (Expression) visit(ctx.unaryExpression());
        UnaryExpr unaryExpr = new UnaryExpr(expression, UnaryExpr.Operator.LOGICAL_COMPLEMENT);
        return unaryExpr;
    }

    @Override
    public Expression visitPrimaryNoNewArrayLfNoPrimaryParenExpression(HydraParser.PrimaryNoNewArrayLfNoPrimaryParenExpressionContext ctx)
    {
        return (Expression) visit( ctx.expression() );
    }

//    @Override
//    public Node visitCastExpression(HydraParser.CastExpressionContext ctx) {
//        return super.visitCastExpression(ctx); // TODO
//    }

    @Override
    public Expression visitPrimaryNoNewArrayLfNoPrimaryLiteral(HydraParser.PrimaryNoNewArrayLfNoPrimaryLiteralContext ctx)
    {
        return (Expression) visit(ctx.literal());
    }

    // ------------------
    // Binary expressions


    @Override
    public BinaryExpr visitMultMultiplicativeExpression(HydraParser.MultMultiplicativeExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.multiplicativeExpression());
        Expression right = (Expression) visit(ctx.unaryExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.MULTIPLY);
        return expr;
    }

    @Override
    public Node visitDivMultiplicativeExpression(HydraParser.DivMultiplicativeExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.multiplicativeExpression());
        Expression right = (Expression) visit(ctx.unaryExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.DIVIDE);
        return expr;
    }

    @Override
    public Node visitModMultiplicativeExpression(HydraParser.ModMultiplicativeExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.multiplicativeExpression());
        Expression right = (Expression) visit(ctx.unaryExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.REMAINDER);
        return expr;
    }

    @Override
    public Node visitAddAdditiveExpression(HydraParser.AddAdditiveExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.additiveExpression());
        Expression right = (Expression) visit(ctx.multiplicativeExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.PLUS);
        return expr;
    }

    @Override
    public Node visitSubAdditiveExpression(HydraParser.SubAdditiveExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.additiveExpression());
        Expression right = (Expression) visit(ctx.multiplicativeExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.MINUS);
        return expr;
    }

    @Override
    public Node visitLeftShiftExpression(HydraParser.LeftShiftExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.shiftExpression());
        Expression right = (Expression) visit(ctx.additiveExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.LEFT_SHIFT);
        return expr;
    }

    @Override
    public BinaryExpr visitSignedRightShiftExpression(HydraParser.SignedRightShiftExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.shiftExpression());
        Expression right = (Expression) visit(ctx.additiveExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.SIGNED_RIGHT_SHIFT);
        return expr;
    }

    @Override
    public Node visitUnsignedRightShiftExpression(HydraParser.UnsignedRightShiftExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.shiftExpression());
        Expression right = (Expression) visit(ctx.additiveExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.UNSIGNED_RIGHT_SHIFT);
        return expr;

    }

    @Override
    public Node visitLtRelationalExpression(HydraParser.LtRelationalExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.relationalExpression());
        Expression right = (Expression) visit(ctx.shiftExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.LESS);
        return expr;
    }

    @Override
    public Node visitLeqRelationalExpression(HydraParser.LeqRelationalExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.relationalExpression());
        Expression right = (Expression) visit(ctx.shiftExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.LESS_EQUALS);
        return expr;
    }

    @Override
    public Node visitGtRelationalExpression(HydraParser.GtRelationalExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.relationalExpression());
        Expression right = (Expression) visit(ctx.shiftExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.GREATER);
        return expr;
    }

    @Override
    public Node visitGeqRelationalExpression(HydraParser.GeqRelationalExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.relationalExpression());
        Expression right = (Expression) visit(ctx.shiftExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.GREATER_EQUALS);
        return expr;
    }

    @Override
    public InstanceOfExpr visitInstanceOfRelationalExpression(HydraParser.InstanceOfRelationalExpressionContext ctx)
    {
        Expression relExpr = (Expression) visit(ctx.relationalExpression());
        ReferenceType type = (ReferenceType) visit(ctx.referenceType());
        InstanceOfExpr expr = new InstanceOfExpr(relExpr,type);
        return expr;
    }

    @Override
    public Node visitEqEqualityExpression(HydraParser.EqEqualityExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.equalityExpression());
        Expression right = (Expression) visit(ctx.relationalExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.EQUALS);
        return expr;
    }

    @Override
    public Node visitNeqEqualityExpression(HydraParser.NeqEqualityExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.equalityExpression());
        Expression right = (Expression) visit(ctx.relationalExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.NOT_EQUALS);
        return expr;
    }

    //-- CONDITIONAL EXPRESSIONs

    @Override
    public Node visitXorExclusiveOrExpression(HydraParser.XorExclusiveOrExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.exclusiveOrExpression());
        Expression right = (Expression) visit(ctx.andExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.XOR);
        return expr;
    }

    @Override
    public Node visitOrInclusiveOrExpression(HydraParser.OrInclusiveOrExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.inclusiveOrExpression());
        Expression right = (Expression) visit(ctx.exclusiveOrExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.BINARY_OR);
        return expr;
    }

    @Override
    public Node visitAndAndExpression(HydraParser.AndAndExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.andExpression());
        Expression right = (Expression) visit(ctx.equalityExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.BINARY_AND);
        return expr;
    }

    //-- CONDITIONAL EXPRESSIONs

    @Override
    public Node visitAndConditionalAndExpression(HydraParser.AndConditionalAndExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.conditionalAndExpression());
        Expression right = (Expression) visit(ctx.inclusiveOrExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.AND);
        return expr;
    }

    @Override
    public Node visitOrConditionalOrExpression(HydraParser.OrConditionalOrExpressionContext ctx)
    {
        Expression left = (Expression) visit(ctx.conditionalOrExpression());
        Expression right = (Expression) visit(ctx.conditionalAndExpression());
        BinaryExpr expr = new BinaryExpr(left, right, BinaryExpr.Operator.OR);
        return expr;
    }

    @Override
    public Node visitConditionalExpressionExpression(HydraParser.ConditionalExpressionExpressionContext ctx)
    {
        Expression condition = (Expression) visit(ctx.conditionalOrExpression());
        Expression thenExpr = (Expression) visit(ctx.expression());
        Expression elseExpr = (Expression) visit(ctx.conditionalExpression());

        return new ConditionalExpr(condition, thenExpr,elseExpr);
    }


    @Override
    public Expression visitFunctionInvocation(HydraParser.FunctionInvocationContext ctx)
    {
        SimpleName name = visitAndCastOrNull(ctx.functionName());
        NodeList<Expression> arguments = visitAndEnsureNodeList(ctx.argumentList());
        if (declarationsStack.functionDeclared(name.asString(), arguments.size()))
            return new FunctionCallExpr(name, arguments);
        else
            return new MethodCallExpr(null, name, arguments);
    }

    @Override
    public NodeList<Expression> visitArgumentList(HydraParser.ArgumentListContext ctx)
    {
        List<Expression> list = ctx.expression().stream()
                .map(expressionContext -> (Expression) visit(expressionContext))
                .collect(Collectors.toList());

        return NodeList.nodeList(list);
    }

    @Override
    public SimpleName visitFunctionName(HydraParser.FunctionNameContext ctx)
    {
        return new SimpleName(ctx.getText());
    }

    @Override
    public Node visitFunctionDeclaration(HydraParser.FunctionDeclarationContext ctx)
    {
        FunctionDeclaration node = new FunctionDeclaration();

        EnumSet<Modifier> modifiers;
        if (nonNull(ctx.functionModifier()))
        {
            modifiers = EnumSet.of(Modifier.SYNCHRONIZED);
            node.setModifiers(modifiers);
        }

        SimpleName name = (SimpleName) visit(ctx.functionHeader().functionDeclarator().functionName());
        node.setName(name);

        NodeList<Parameter> parameters = visitAndEnsureNodeList(ctx.functionHeader().functionDeclarator().formalParameterList());
        node.setParameters(parameters);

        Type type = mapFunctionType(ctx.functionHeader());
        node.setType(type);

        NodeList<TypeParameter> typeParameters = visitAndEnsureNodeList(ctx.functionHeader().typeParameters());
        node.setTypeParameters(typeParameters);

        NodeList <AnnotationExpr> annotations = mapAnnotations(ctx.functionHeader().annotation());
        node.setAnnotations(annotations);

        NodeList<ReferenceType<?>> thrownExceptions = visitAndEnsureNodeList(ctx.functionHeader().throws_());
        node.setThrownExceptions(thrownExceptions);

        BlockStmt body = (BlockStmt) visit(ctx.functionBody());
        node.setBody(body);

        return node ;
    }

    @Override
    public Visitable visitReturnStatement(HydraParser.ReturnStatementContext ctx)
    {
        Expression expression = visitAndCastOrNull(ctx.expression());
        return new ReturnStmt(expression);
    }

    @Override
    public NodeList<TypeParameter> visitTypeParameters(HydraParser.TypeParametersContext ctx)
    {
        return visitAndEnsureNodeList(ctx.typeParameterList());
    }

    @Override
    public Visitable visitTypeParameterList(HydraParser.TypeParameterListContext ctx)
    {
        List<TypeParameter> typeParameters = ctx.typeParameter()
                .stream()
                .map(typeParameterContext -> (TypeParameter) visit(typeParameterContext))
                .collect(Collectors.toList());
        return NodeList.nodeList(typeParameters);
    }

    private Type mapFunctionType(HydraParser.FunctionHeaderContext ctx)
    {
        Type type = visitAndCastOrUnknownType(ctx.result());
        if ( nonNull(ctx.functionDeclarator().dims()) )
        {
            List<ArrayType.ArrayBracketPair> arrayBracketPairs = mapAnnotatedDimsToArrayBracketPair(ctx.functionDeclarator().dims().annotatedDim());
            type = juggleArrayType(type, arrayBracketPairs);
        }
        return type;
    }



    @Override
    public Visitable visitScriptBlock(HydraParser.ScriptBlockContext ctx)
    {
        declarationsStack.openScope();

        ScriptBlockStmt body = new ScriptBlockStmt();

        if ( nonNull(ctx.scriptBlockStatement()) )
        {
            NodeList<Statement> statements = mapScriptBlockStatements(ctx.scriptBlockStatement());
            body.setStatements(statements);
        }

        NodeList<FunctionDeclaration> functionDeclarations = mapFunctionDeclarations(ctx.functionDeclaration());
        body.setFunctions(functionDeclarations);

        declarationsStack.closeScope();

        return body ;
    }

    private NodeList<FunctionDeclaration> mapFunctionDeclarations(List<HydraParser.FunctionDeclarationContext> functionDeclarationContexts)
    {
        List<FunctionDeclaration> list = functionDeclarationContexts.stream()
                .map(functionDeclarationContext -> (FunctionDeclaration) visit(functionDeclarationContext))
                .collect(Collectors.toList());
        return NodeList.nodeList(list);
    }

    private NodeList<Statement> mapScriptBlockStatements(List<HydraParser.ScriptBlockStatementContext> scriptBlockStatementsContext)
    {
        List<Statement> list = scriptBlockStatementsContext.stream()
                .map(scriptBlockStatementContext -> (Statement) visit(scriptBlockStatementContext) )
                .collect(Collectors.toList());

        return NodeList.nodeList(list);
    }

    @Override
    public NodeList<ClassOrInterfaceType> visitThrows_(HydraParser.Throws_Context ctx)
    {
        return visitAndEnsureNodeList(ctx.exceptionTypeList());
    }

    @Override
    public Visitable visitExceptionTypeList(HydraParser.ExceptionTypeListContext ctx)
    {
        List<ClassOrInterfaceType> list = ctx.exceptionType().stream()
                .map(exceptionTypeContext -> (ClassOrInterfaceType) visit(exceptionTypeContext) )
                .collect(Collectors.toList());

        return NodeList.nodeList(list);
    }

    @Override
    public ClassOrInterfaceType visitExceptionType(HydraParser.ExceptionTypeContext ctx)
    {
        ParserRuleContext context = coalesce( () -> ctx.classType(), () -> ctx.typeVariable() );
        return (ClassOrInterfaceType) visit(context);
    }

    @Override
    public NodeList<Parameter> visitFormalParameterList(HydraParser.FormalParameterListContext ctx)
    {
        NodeList<Parameter> parameters = visitAndEnsureNodeList(ctx.formalParameters());

        Parameter lastFormalParameter = (Parameter) visit(ctx.lastFormalParameter());
        parameters.add(lastFormalParameter);

        return parameters ;
    }

    @Override
    public NodeList<Parameter> visitFormalParametersFormalParameter(HydraParser.FormalParametersFormalParameterContext ctx)
    {
        List<Parameter> list = ctx.formalParameter().stream()
                .map(formalParameterContext -> (Parameter) visit(formalParameterContext))
                .collect(Collectors.toList());

        return NodeList.nodeList(list);
    }

    @Override
    public Visitable visitFormalParametersReceiverParameter(HydraParser.FormalParametersReceiverParameterContext ctx)
    {
        throw new HydraAstBuilderException("visitFormalParametersReceiverParameter parsing pending");
    }

    @Override
    public Parameter visitFormalParameter(HydraParser.FormalParameterContext ctx)
    {
        return createFormalParameter(ctx.unannType(), ctx.variableModifier(), null, ctx.variableDeclaratorId(), false);
    }

    @Override
    public Visitable visitLastFormalParameterVarArgs(HydraParser.LastFormalParameterVarArgsContext ctx)
    {
        return createFormalParameter(ctx.unannType(), ctx.variableModifier(), ctx.annotation(), ctx.variableDeclaratorId(), true);
    }

    public Parameter createFormalParameter(HydraParser.UnannTypeContext typeContext,
                                           List<HydraParser.VariableModifierContext> variableModifierContexts,
                                           List<HydraParser.AnnotationContext> varArgsAnnotationContests,
                                           HydraParser.VariableDeclaratorIdContext variableDeclaratorIdContext,
                                           boolean isVarArgs
                                           )
    {
        Type type = visitAndCastOrUnknownType(typeContext);
        EnumSet<Modifier> modifiers = mapVariableModifiers(variableModifierContexts);
        NodeList<AnnotationExpr> annotations = mapVariableAnnotations(variableModifierContexts);
        NodeList<AnnotationExpr> varArgsAnnotations = mapAnnotations(varArgsAnnotationContests);
        SimpleName name = new SimpleName(variableDeclaratorIdContext.Identifier().getText());

        if ( nonNull(variableDeclaratorIdContext.dims()) )
        {
            List<ArrayType.ArrayBracketPair> arrayBracketPairs = mapAnnotatedDimsToArrayBracketPair(variableDeclaratorIdContext.dims().annotatedDim());
            type = juggleArrayType(type, arrayBracketPairs);
        }

        return new Parameter(modifiers, annotations, type, isVarArgs, varArgsAnnotations, name);
    }

    @Override
    public Visitable visitResultUnannType(HydraParser.ResultUnannTypeContext ctx)
    {
        return visitAndCastOrUnknownType(ctx.unannType());
    }

    @Override
    public Visitable visitResultVoid(HydraParser.ResultVoidContext ctx)
    {
        return new VoidType();
    }

    @Override
    public Visitable visitArrayCreationExpressionPrimitiveNoInit(HydraParser.ArrayCreationExpressionPrimitiveNoInitContext ctx)
    {
        return createArrayCreationExpression(ctx.primitiveType(), ctx.dimExprs(), ctx.dims(), null);
    }

    @Override
    public Visitable visitArrayCreationExpressionClassOrInterfaceTypeNoInit(HydraParser.ArrayCreationExpressionClassOrInterfaceTypeNoInitContext ctx)
    {
        return createArrayCreationExpression(ctx.classOrInterfaceType(), ctx.dimExprs(), ctx.dims(), null);
    }

    @Override
    public Visitable visitArrayCreationExpressionPrimitiveInit(HydraParser.ArrayCreationExpressionPrimitiveInitContext ctx)
    {
        return createArrayCreationExpression(ctx.primitiveType(), null, ctx.dims(), ctx.arrayInitializer());
    }

    @Override
    public Visitable visitArrayCreationExpressionClassOrInterfaceTypeInit(HydraParser.ArrayCreationExpressionClassOrInterfaceTypeInitContext ctx)
    {
        return createArrayCreationExpression(ctx.classOrInterfaceType(), null, ctx.dims(), ctx.arrayInitializer());
    }

    private Visitable createArrayCreationExpression(ParserRuleContext typeContext, HydraParser.DimExprsContext dimExprsContext, HydraParser.DimsContext dimsContext, HydraParser.ArrayInitializerContext initializerContext)
    {
        Type type = (Type) visit(typeContext);

        NodeList<ArrayCreationLevel> levels = null;

        if (nonNull(dimExprsContext))
        {
            levels = addAll(levels, visitAndEnsureNodeList(dimExprsContext) );
        }

        if (nonNull(dimsContext))
        {
            levels = addAll(levels, visitAndCastOrNull(dimsContext));
        }
        ArrayInitializerExpr initializer = visitAndCastOrNull(initializerContext);
        return new ArrayCreationExpr(type, levels, initializer);
    }

    @Override
    public NodeList<ArrayCreationLevel> visitDims(HydraParser.DimsContext ctx)
    {
        return mapAnnotatedDimsToArrayCreationLevel(ctx.annotatedDim());
    }

    private NodeList<ArrayCreationLevel> mapAnnotatedDimsToArrayCreationLevel(List<HydraParser.AnnotatedDimContext> annotatedDimContexts)
    {
        if (annotatedDimContexts.isEmpty())
            return null;

        List<ArrayCreationLevel> list = annotatedDimContexts.stream()
                .map(annotatedDimContext -> mapAnnotatedDimToArrayCreationLevel(annotatedDimContext))
                .collect(Collectors.toList());

        return NodeList.nodeList(list);
    }

    private ArrayCreationLevel mapAnnotatedDimToArrayCreationLevel(HydraParser.AnnotatedDimContext annotatedDimContext)
    {
        Expression dimension = null;
        NodeList<AnnotationExpr> annotations = mapDimAnnotations(annotatedDimContext.annotation());
        return new ArrayCreationLevel(dimension, annotations);
    }

    @Override
    public NodeList<ArrayCreationLevel> visitDimExprs(HydraParser.DimExprsContext ctx)
    {
        List<ArrayCreationLevel> list = ctx.dimExpr().stream()
                .map(dimExprContext -> (ArrayCreationLevel) visit(dimExprContext))
                .collect(Collectors.toList());
        return NodeList.nodeList(list);
    }

    @Override
    public ArrayCreationLevel visitDimExpr(HydraParser.DimExprContext ctx)
    {
        Expression dimension = visitAndCastOrNull(ctx.expression());
        NodeList<AnnotationExpr> annotations = mapDimAnnotations(ctx.annotation());
        return new ArrayCreationLevel(dimension, annotations);
    }

    private Type juggleArrayType(Type partialType, List<ArrayType.ArrayBracketPair> additionalBrackets)
    {
        Pair<Type, List<ArrayType.ArrayBracketPair>> partialParts = ArrayType.unwrapArrayTypes(partialType);
        Type elementType = partialParts.a;
        List<ArrayType.ArrayBracketPair> leftMostBrackets = partialParts.b;
        return ArrayType.wrapInArrayTypes(elementType, leftMostBrackets, additionalBrackets);
    }

    private ArrayCreationExpr juggleArrayCreation(Range range, List<Range> levelRanges, Type type, NodeList<Expression> dimensions, List<NodeList<AnnotationExpr>> arrayAnnotations, ArrayInitializerExpr arrayInitializerExpr)
    {
        NodeList<ArrayCreationLevel> levels = new NodeList<ArrayCreationLevel>();

        for(int i = 0; i < arrayAnnotations.size(); i++){
            levels.add(new ArrayCreationLevel(levelRanges.get(i), dimensions.get(i), arrayAnnotations.get(i)));
        }
        return new ArrayCreationExpr(range, type, levels, arrayInitializerExpr);
    }

    @Override
    public Visitable visitArrayAccess(HydraParser.ArrayAccessContext ctx)
    {
        Expression expression = (Expression) visit(ctx.arrayAccess_nolf());

        for (HydraParser.ArrayAccess_lfContext lfctx : ctx.arrayAccess_lf())
        {
            Expression indexExpr = (Expression) visit(lfctx.expression());
            expression = new ArrayAccessExpr(expression, indexExpr);
        }

        return expression;
    }

    @Override
    public Visitable visitArrayAccess_nolfExpressionName(HydraParser.ArrayAccess_nolfExpressionNameContext ctx)
    {
        Expression expression = new AmbiguousNameExpr( Name.parse(ctx.expressionName().getText()) );
        Expression indexExpr = (Expression) visit(ctx.expression());
        return new ArrayAccessExpr(expression, indexExpr);
    }

    @Override
    public Visitable visitArrayAccess_nolfPrimaryNoNewArrayLfnoArrayAccess(HydraParser.ArrayAccess_nolfPrimaryNoNewArrayLfnoArrayAccessContext ctx)
    {
        Expression expression = (Expression) visit(ctx.primaryNoNewArray_lfno_arrayAccess());
        Expression indexExpr = (Expression) visit(ctx.expression());
        return new ArrayAccessExpr(expression, indexExpr);
    }

    @Override
    public Expression visitPrimaryNoNewArray_lfno_arrayAccessLiteral(HydraParser.PrimaryNoNewArray_lfno_arrayAccessLiteralContext ctx)
    {
        return (Expression) visit(ctx.literal());
    }

    @Override
    public Expression visitPrimaryNoNewArray_lfno_arrayAccessTypeNameClass(HydraParser.PrimaryNoNewArray_lfno_arrayAccessTypeNameClassContext ctx)
    {
        return createClassExprWithTypeName(ctx.typeName(), ctx.dim());
    }

    private List<ArrayType.ArrayBracketPair> createArrayBracketPairList(int size)
    {
        List<ArrayType.ArrayBracketPair> list = new ArrayList<>();
        for (int i = 0 ; i < size ; i++)
            list.add(new ArrayType.ArrayBracketPair(null, emptyList()));
        return list;
    }

    @Override
    public Visitable visitPrimaryNoNewArrayLfNoPrimaryTypeNameClass(HydraParser.PrimaryNoNewArrayLfNoPrimaryTypeNameClassContext ctx)
    {
        return createClassExprWithTypeName(ctx.typeName(), ctx.dim());
    }

    @Override
    public Visitable visitPrimaryNoNewArrayLfNoPrimaryUnnanPrimitiveType(HydraParser.PrimaryNoNewArrayLfNoPrimaryUnnanPrimitiveTypeContext ctx)
    {
        return createClassExprWithUnannPrimitiveType(ctx.unannPrimitiveType(), ctx.dim());
    }

    @Override
    public Visitable visitPrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryTypeName(HydraParser.PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryTypeNameContext ctx)
    {
        return createClassExprWithTypeName(ctx.typeName(), ctx.dim());
    }

    private ClassExpr createClassExprWithTypeName(HydraParser.TypeNameContext typeNameCtx, List<HydraParser.DimContext> dimListCtx)
    {
        Type type = nameToClassOrInterfaceType((Name) visit(typeNameCtx));

        List<ArrayType.ArrayBracketPair> arrayBracketPairs = null;
        if (!dimListCtx.isEmpty())
            arrayBracketPairs = createArrayBracketPairList(dimListCtx.size());

        type = juggleArrayType(type, arrayBracketPairs);
        return new ClassExpr(type);
    }

    @Override
    public Expression visitPrimaryNoNewArray_lfno_arrayAccessVoidClass(HydraParser.PrimaryNoNewArray_lfno_arrayAccessVoidClassContext ctx)
    {
        return new ClassExpr(voidType());
    }

    @Override
    public Visitable visitPrimaryNoNewArrayLfNoPrimaryVoidClass(HydraParser.PrimaryNoNewArrayLfNoPrimaryVoidClassContext ctx)
    {
        return new ClassExpr(voidType());
    }

    @Override
    public Expression visitPrimaryNoNewArray_lfno_arrayAccessThis(HydraParser.PrimaryNoNewArray_lfno_arrayAccessThisContext ctx)
    {
        throw new HydraAstToBeImplementedException("visitPrimaryNoNewArray_lfno_arrayAccessThis");
    }

    @Override
    public Expression visitPrimaryNoNewArray_lfno_arrayAccessTypeNameThis(HydraParser.PrimaryNoNewArray_lfno_arrayAccessTypeNameThisContext ctx)
    {
        throw new HydraAstToBeImplementedException("visitPrimaryNoNewArray_lfno_arrayAccessTypeNameThis");
    }

    @Override
    public Expression visitPrimaryNoNewArray_lfno_arrayAccessParenExpression(HydraParser.PrimaryNoNewArray_lfno_arrayAccessParenExpressionContext ctx)
    {
        return new EnclosedExpr((Expression) visit(ctx.expression()));
    }

    @Override
    public Expression visitPrimaryNoNewArray_lfno_arrayAccessClassInstanceCreation(HydraParser.PrimaryNoNewArray_lfno_arrayAccessClassInstanceCreationContext ctx)
    {
        return (Expression) visit(ctx.classInstanceCreationExpression());
    }

    @Override
    public Expression visitPrimaryNoNewArray_lfno_arrayAccessFieldAccess(HydraParser.PrimaryNoNewArray_lfno_arrayAccessFieldAccessContext ctx)
    {
        return (Expression) visit(ctx.fieldAccess());
    }

    @Override
    public Expression visitPrimaryNoNewArray_lfno_arrayAccessMethodInvocation(HydraParser.PrimaryNoNewArray_lfno_arrayAccessMethodInvocationContext ctx)
    {
        return (Expression) visit(ctx.methodInvocation());
    }

    @Override
    public Expression visitPrimaryNoNewArray_lfno_arrayAccessMethodReference(HydraParser.PrimaryNoNewArray_lfno_arrayAccessMethodReferenceContext ctx)
    {
        return (Expression) visit(ctx.methodReference());
    }

    @Override
    public PrimitiveType visitPrimitiveTypeNumeric(HydraParser.PrimitiveTypeNumericContext ctx)
    {
        PrimitiveType type = (PrimitiveType) visit(ctx.numericType());

        NodeList<AnnotationExpr> annotations = mapAnnotations(ctx.annotation());
        type.setAnnotations(annotations);

        return type ;
    }

    @Override
    public Expression visitPrimary(HydraParser.PrimaryContext ctx)
    {
        Expression scope = (Expression) visit(ctx.primary_lfno());
        Expression expression = scope;

        for (HydraParser.PrimaryNoNewArray_lf_primaryContext lfctx : ctx.primaryNoNewArray_lf_primary())
        {
             expression = (Expression) visit(lfctx);
             setScope(scope, expression);
             scope = expression;
        }
        return expression;
    }

    @Override
    public Visitable visitClassInstanceCreationExpression_lf_primary(HydraParser.ClassInstanceCreationExpression_lf_primaryContext ctx)
    {
        return createObjectCreationExpression(null, ctx.typeArguments(),ctx.annotation(),ctx.Identifier().getText(),ctx.typeArgumentsOrDiamond(),ctx.argumentList(),ctx.classBody());
    }

    private void setScope(Expression scope, Expression expression)
    {
        if (expression instanceof MethodCallExpr)
        {
            ((MethodCallExpr) expression).setScope(scope);
        } else if (expression instanceof ArrayAccessExpr) {
            ((ArrayAccessExpr) expression).setName(scope);
        } else if (expression instanceof FieldAccessExpr) {
            ((FieldAccessExpr) expression).setScope(scope);
        } else if (expression instanceof ObjectCreationExpr) {
            ((ObjectCreationExpr) expression).setScope(scope);
        } else if (expression instanceof MethodReferenceExpr) {
            ((MethodReferenceExpr) expression).setScope(scope);
        }
    }

    @Override
    public Visitable visitFieldAccess_lf_primary(HydraParser.FieldAccess_lf_primaryContext ctx)
    {
        return new FieldAccessExpr(null, ctx.Identifier().getText());
    }

    @Override
    public Visitable visitArrayAccess_lf_primary(HydraParser.ArrayAccess_lf_primaryContext ctx)
    {
        Expression scopeExpr = (Expression) visit(ctx.primaryNoNewArray_lf_primary_lfno_arrayAccess_lf_primary());

        for (HydraParser.ExpressionContext expressionContext : ctx.expression() )
        {
            Expression indexExpression = (Expression) visit(expressionContext);
            Expression arrayAccessExpr = new ArrayAccessExpr(scopeExpr, indexExpression);
            scopeExpr = arrayAccessExpr ;
        }

        return scopeExpr;
    }

    @Override
    public Visitable visitArrayAccess_lf(HydraParser.ArrayAccess_lfContext ctx)
    {
        Expression expr  = (Expression) visit(ctx.primaryNoNewArray_lf_arrayAccess());
        Expression indexExpr = (Expression) visit(ctx.expression());
        return new ArrayAccessExpr(expr, indexExpr);
    }

    @Override
    public Visitable visitArrayAccess_lfno_primary(HydraParser.ArrayAccess_lfno_primaryContext ctx)
    {
        ArrayAccessExpr expr = (ArrayAccessExpr) visit(ctx.arrayAccess_lfno_primary_lfno());

        for (int i = 0 ; i < ctx.arrayAccess_lfno_primary_lf().size() ; i++)
        {
            Expression indexExpr = (Expression) visit(ctx.arrayAccess_lfno_primary_lf(i).expression());
            expr = new ArrayAccessExpr(expr, indexExpr);
        }
        return expr;
    }


    @Override
    public Visitable visitArrayAccessLfnoPrimaryLfnoExpressionName(HydraParser.ArrayAccessLfnoPrimaryLfnoExpressionNameContext ctx)
    {
        Expression expr = (Expression) visit(ctx.expressionName());
        Expression indexExpr = (Expression) visit(ctx.expression());

        return new ArrayAccessExpr(expr, indexExpr);
    }

    @Override
    public Visitable visitArrayAccessLfnoPrimaryLfnoPrimary(HydraParser.ArrayAccessLfnoPrimaryLfnoPrimaryContext ctx)
    {
        Expression expr = (Expression) visit(ctx.primaryNoNewArray_lfno_primary_lfno_arrayAccess_lfno_primary());
        Expression indexExpr = (Expression) visit(ctx.expression());

        return new ArrayAccessExpr(expr, indexExpr);
    }

    @Override
    public Visitable visitPrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryLiteral(HydraParser.PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryLiteralContext ctx)
    {
        throw new HydraAstToBeImplementedException("visitPrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryLiteral");
    }

    @Override
    public Visitable visitPrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryUnannPrimitiveType(HydraParser.PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryUnannPrimitiveTypeContext ctx)
    {
        return createClassExprWithUnannPrimitiveType(ctx.unannPrimitiveType(), ctx.dim());
    }

    private Visitable createClassExprWithUnannPrimitiveType(HydraParser.UnannPrimitiveTypeContext unannPrimitiveTypeCtx, List<HydraParser.DimContext> dimListCtx)
    {
        Type type = (Type) visit(unannPrimitiveTypeCtx);

        List<ArrayType.ArrayBracketPair> arrayBracketPairs = null;
        if (!dimListCtx.isEmpty())
            arrayBracketPairs = createArrayBracketPairList(dimListCtx.size());

        type = juggleArrayType(type, arrayBracketPairs);
        return new ClassExpr(type);
    }

    @Override
    public Visitable visitPrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryVoidClass(HydraParser.PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryVoidClassContext ctx)
    {
        return new ClassExpr(voidType());
    }

    private Type voidType()
    {
        return new VoidType();
    }

    @Override
    public Visitable visitPrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryThis(HydraParser.PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryThisContext ctx)
    {
        throw new HydraAstToBeImplementedException("visitPrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryThis");
    }

    @Override
    public Visitable visitPrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryTypeNameThis(HydraParser.PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryTypeNameThisContext ctx)
    {
        throw new HydraAstToBeImplementedException("visitPrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryTypeNameThis");
    }

    @Override
    public Visitable visitPrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryParenExpr(HydraParser.PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryParenExprContext ctx)
    {
        Expression expr = (Expression) visit(ctx.expression());
        return new EnclosedExpr(expr);
    }

    @Override
    public Visitable visitPrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryClassInstanceCreation(HydraParser.PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryClassInstanceCreationContext ctx)
    {
        return visit(ctx.classInstanceCreationExpression_lfno_primary());
    }

    @Override
    public Visitable visitPrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryFieldAccess(HydraParser.PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryFieldAccessContext ctx)
    {
        return visit(ctx.fieldAccess_lfno_primary());
    }

    @Override
    public Visitable visitPrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryMethodInvocation(HydraParser.PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryMethodInvocationContext ctx)
    {
        return visit(ctx.methodInvocation_lfno_primary());
    }

    @Override
    public Visitable visitPrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryMethodReference(HydraParser.PrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryMethodReferenceContext ctx)
    {
        return super.visitPrimaryNoNewArrayLfnoPrimaryLfnoArrayAccessLfnoPrimaryMethodReference(ctx);
    }

    @Override
    public Visitable visitFieldAccess_lfno_primary(HydraParser.FieldAccess_lfno_primaryContext ctx)
    {
        Expression classExpr = mapTypeNameToClassExpr(ctx.typeName());
        Expression scope = new SuperExpr(classExpr);
        return new FieldAccessExpr(scope, ctx.Identifier().getText());
    }

    private Expression mapTypeNameToClassExpr(HydraParser.TypeNameContext typeNameContext)
    {
        if (isNull(typeNameContext)) return null;

        ClassOrInterfaceType type = new ClassOrInterfaceType(typeNameContext.Identifier().getText());
        ClassOrInterfaceType currentType = type;
        HydraParser.PackageOrTypeNameContext currentCtx  = typeNameContext.packageOrTypeName() ;

        while (nonNull(currentCtx))
        {
            ClassOrInterfaceType scopeType = new ClassOrInterfaceType(currentCtx.Identifier().getText());
            currentType.setScope(scopeType) ;
            currentType = scopeType;
        }
        return new ClassExpr(type);
    }



    @Override
    public Visitable visitMethodInvocation_lf_primary(HydraParser.MethodInvocation_lf_primaryContext ctx)
    {
        return createMethodOrFunctionCallExpr(null, ctx.Identifier().getText(), ctx.argumentList(), ctx.typeArguments());
    }

    private Expression createMethodOrFunctionCallExpr(ParserRuleContext ctx, String identifier, HydraParser.ArgumentListContext argumentListContext, HydraParser.TypeArgumentsContext typeArgumentsContext)
    {
        Expression scope = visitAndCastOrNull(ctx);
        SimpleName name = new SimpleName(identifier);
        NodeList<Expression> arguments = visitAndEnsureNodeList(argumentListContext);
        NodeList<Type> typearguments = visitAndCastOrNull(typeArgumentsContext);

        if (isNull(scope) && declarationsStack.functionDeclared(identifier, arguments.size()))
            return new FunctionCallExpr(name, arguments);
        else
            return new MethodCallExpr(scope,typearguments,name,arguments);
    }

    @Override
    public Node visitPrimitiveTypeBoolean(HydraParser.PrimitiveTypeBooleanContext ctx)
    {
        PrimitiveType type = PrimitiveType.booleanType();
        NodeList<AnnotationExpr> annotations = mapAnnotations(ctx.annotation());
        type.setAnnotations(annotations);

        return type ;
    }

    @Override
    public Node visitClassType_lfno_classOrInterfaceType(HydraParser.ClassType_lfno_classOrInterfaceTypeContext ctx)
    {
        return createClassOrInterfaceType(ctx.Identifier(), ctx.annotation(), ctx.typeArguments());
    }


    @Override
    public ClassOrInterfaceType visitClassTypeBase(HydraParser.ClassTypeBaseContext ctx)
    {
        return createClassOrInterfaceType(ctx.Identifier(), ctx.annotation(), ctx.typeArguments());
    }

    @Override
    public ClassOrInterfaceType visitClassTypeWithScope(HydraParser.ClassTypeWithScopeContext ctx)
    {
        // TODO: revisar si debo retornar classType o (ctx.classOrInterfaceType())
        return createClassOrInterfaceType(ctx.classOrInterfaceType(), ctx.Identifier(), ctx.annotation(), ctx.typeArguments());
    }

    @Override
    public ClassOrInterfaceType visitClassType_lf_classOrInterfaceType(HydraParser.ClassType_lf_classOrInterfaceTypeContext ctx)
    {
        return createClassOrInterfaceType(ctx.Identifier(), ctx.annotation(), ctx.typeArguments());
    }

    private ClassOrInterfaceType createClassOrInterfaceType(TerminalNode identifier, List<HydraParser.AnnotationContext> annotationContexts, HydraParser.TypeArgumentsContext typeArgumentsContext)
    {
        return createClassOrInterfaceType(null, identifier, annotationContexts, typeArgumentsContext);
    }

    private ClassOrInterfaceType createClassOrInterfaceType(ParserRuleContext scopeContext, TerminalNode identifier, List<HydraParser.AnnotationContext> annotationContexts, HydraParser.TypeArgumentsContext typeArgumentsContext)
    {
        ClassOrInterfaceType type = new ClassOrInterfaceType(identifier.getText());

        if ( nonNull(scopeContext))
        {
            ClassOrInterfaceType scope = visitAndCastOrNull(scopeContext);
            type.setScope(scope);
        }

        if ( nonNull(annotationContexts) )
        {
            NodeList<AnnotationExpr> annotations = mapAnnotations(annotationContexts);
            type.setAnnotations(annotations);
        }

        if ( nonNull(typeArgumentsContext))
        {
            NodeList<Type> typeArguments = visitAndCastOrNull(typeArgumentsContext);
            type.setTypeArguments(typeArguments);
        }

        return type;
    }

    @Override
    public NodeList<Type> visitTypeArguments(HydraParser.TypeArgumentsContext ctx)
    {
        return mapTypeArguments(ctx.typeArgumentList().typeArgument());
    }

    private NodeList<Type> mapTypeArguments(List<HydraParser.TypeArgumentContext> typeArgumentContexts)
    {
        List<Type> list = typeArgumentContexts.stream()
                .map(typeArgumentContext -> (Type) visit(typeArgumentContext))
                .collect(Collectors.toList());

        return NodeList.nodeList(list);
    }

    private NodeList<AnnotationExpr> mapAnnotations(List<HydraParser.AnnotationContext> annotations)
    {
        if (isNull(annotations)) return NodeList.nodeList();

        List<AnnotationExpr> list = annotations.stream()
                .map( annotationContext -> (AnnotationExpr) visit(annotationContext))
                .collect(Collectors.toList());
        return NodeList.nodeList(list);
    }

    @Override
    public Visitable visitTypeVariable(HydraParser.TypeVariableContext ctx)
    {
        return createClassOrInterfaceType(ctx.Identifier(), ctx.annotation(), null);
    }

    @Override
    public Type visitArrayTypePrimitiveType(HydraParser.ArrayTypePrimitiveTypeContext ctx)
    {
        Type partialType = (Type) visit(ctx.primitiveType());
        List<ArrayType.ArrayBracketPair> arrayBracketPairs = mapAnnotatedDimsToArrayBracketPair(ctx.dims().annotatedDim());

        return ArrayType.wrapInArrayTypes(partialType, arrayBracketPairs);
    }

    @Override
    public Visitable visitArrayTypeClassOrInterfaceType(HydraParser.ArrayTypeClassOrInterfaceTypeContext ctx)
    {
        Type partialType = (Type) visit(ctx.classOrInterfaceType());
        List<ArrayType.ArrayBracketPair> arrayBracketPairs = mapAnnotatedDimsToArrayBracketPair(ctx.dims().annotatedDim());

        return ArrayType.wrapInArrayTypes(partialType, arrayBracketPairs);
    }

    @Override
    public Visitable visitArrayTypeTypeVariable(HydraParser.ArrayTypeTypeVariableContext ctx)
    {
        Type partialType = (Type) visit(ctx.typeVariable());
        List<ArrayType.ArrayBracketPair> arrayBracketPairs = mapAnnotatedDimsToArrayBracketPair(ctx.dims().annotatedDim());

        return ArrayType.wrapInArrayTypes(partialType, arrayBracketPairs);
    }

    @Override
    public Visitable visitTypeParameter(HydraParser.TypeParameterContext ctx)
    {
        SimpleName name = new SimpleName(ctx.Identifier().getText());

        mapTypeParameterModifiers(ctx.typeParameterModifier());
        NodeList<ClassOrInterfaceType> typeBound = visitAndEnsureNodeList(ctx.typeBound());
        NodeList<AnnotationExpr> annotations = mapTypeParameterModifiers(ctx.typeParameterModifier());

        TypeParameter node = new TypeParameter(name,typeBound,annotations);

        return node;
    }

    private NodeList<AnnotationExpr> mapTypeParameterModifiers(List<HydraParser.TypeParameterModifierContext> typeParameterModifierContexts)
    {
        List<AnnotationExpr> list = typeParameterModifierContexts
                .stream()
                .map( typeParameterModifierContext -> (AnnotationExpr) visit(typeParameterModifierContext.annotation()) )
                .collect(Collectors.toList());

        return NodeList.nodeList(list);
    }

    @Override
    public NodeList<ClassOrInterfaceType> visitTypeBoundTypeVariable(HydraParser.TypeBoundTypeVariableContext ctx)
    {
        NodeList<ClassOrInterfaceType> list = new NodeList<>() ;
        list.add( (ClassOrInterfaceType) visit(ctx.typeVariable()) );
        return list;
    }

    @Override
    public NodeList<ClassOrInterfaceType> visitTypeBoundClassOrInterfaceType(HydraParser.TypeBoundClassOrInterfaceTypeContext ctx)
    {
        NodeList<ClassOrInterfaceType> list = new NodeList<>() ;

        ClassOrInterfaceType node = (ClassOrInterfaceType) visit(ctx.classOrInterfaceType());
        list.add(node);

        for (HydraParser.AdditionalBoundContext additionalBoundContext : ctx.additionalBound())
        {
            node = (ClassOrInterfaceType) visit(additionalBoundContext.interfaceType());
            list.add(node);
        }

        return list;
    }

    @Override
    public ClassOrInterfaceType visitAdditionalBound(HydraParser.AdditionalBoundContext ctx)
    {
        return (ClassOrInterfaceType) visit(ctx.interfaceType());
    }

    @Override
    public Visitable visitWildcard(HydraParser.WildcardContext ctx)
    {
        WildcardType wildcardType = new WildcardType();

        if ( nonNull(ctx.wildcardBounds()) && nonNull(ctx.wildcardBounds().wildcardBoundsExtends()) )
        {
            ReferenceType extendsType = visitAndCastOrNull(ctx.wildcardBounds().wildcardBoundsExtends().referenceType());
            wildcardType.setExtendedType(extendsType);
        }

        if ( nonNull(ctx.wildcardBounds()) && nonNull(ctx.wildcardBounds().wildcardBoundsSuper()) )
        {
            ReferenceType superType = visitAndCastOrNull(ctx.wildcardBounds().wildcardBoundsSuper().referenceType());
            wildcardType.setSuperType(superType);
        }

        NodeList<AnnotationExpr> annotations  = mapAnnotations(ctx.annotation());
        wildcardType.setAnnotations(annotations);

        return wildcardType;
    }

    @Override
    public Visitable visitPrimaryNoNewArrayLfNoPrimaryClassInstanceCreation(HydraParser.PrimaryNoNewArrayLfNoPrimaryClassInstanceCreationContext ctx)
    {
        return visit(ctx.classInstanceCreationExpression_lfno_primary());
    }

    @Override
    public Visitable visitClassInstanceCreationExpression_lfno_primary_unqualified(HydraParser.ClassInstanceCreationExpression_lfno_primary_unqualifiedContext ctx)
    {
        return visit(ctx.unqualifiedClassInstanceCreationExpression());
    }

    @Override
    public Visitable visitClassInstanceCreationExpression_lfno_primary_qualified(HydraParser.ClassInstanceCreationExpression_lfno_primary_qualifiedContext ctx)
    {
        return createObjectCreationExpression(ctx.expressionName(), ctx.typeArguments(), ctx.annotation(), ctx.Identifier().getText(), ctx.typeArgumentsOrDiamond(), ctx.argumentList(), ctx.classBody());
    }

    private Visitable createObjectCreationExpression(ParserRuleContext scopeContext,
                                                     HydraParser.TypeArgumentsContext typeArgumentsContext,
                                                     List<HydraParser.AnnotationContext> annotationContexts,
                                                     String typeName,
                                                     HydraParser.TypeArgumentsOrDiamondContext typeArgumentsOrDiamondContext,
                                                     HydraParser.ArgumentListContext argumentListContext,
                                                     HydraParser.ClassBodyContext classBodyContext)
    {
        Expression scopeExpr = visitAndCastOrNull(scopeContext);
        NodeList<Type> typeArguments = visitAndCastOrNull(typeArgumentsContext);
        NodeList<AnnotationExpr> annotations = mapAnnotations(annotationContexts);
        NodeList<Type> typeArgumentsOrDiamond = visitAndCastOrNull(typeArgumentsOrDiamondContext);


        ClassOrInterfaceType type = nameToClassOrInterfaceType( Name.parse(typeName) );
        type.setTypeArguments(typeArgumentsOrDiamond);
        type.setAnnotations(annotations);

        NodeList<Expression> arguments = visitAndEnsureNodeList(argumentListContext);

        NodeList<BodyDeclaration<?>> classBodyList = new NodeList<>();
        if ( nonNull(classBodyContext) )
        {
            ClassOrInterfaceDeclaration classBody = visitAndCastOrNull(classBodyContext);
            classBodyList.add(classBody);
        }

        return new ObjectCreationExpr(scopeExpr, type, typeArguments, arguments, classBodyList);
    }

    private ClassOrInterfaceType nameToClassOrInterfaceType(Name name)
    {
        return NameUtils.nameToClassOrInterfaceType(name);
    }

    @Override
    public Visitable visitUnqualifiedClassInstanceCreationExpression(HydraParser.UnqualifiedClassInstanceCreationExpressionContext ctx)
    {
        Expression scope = null;
        ClassOrInterfaceType type = (ClassOrInterfaceType) visit(ctx.unqualifiedClassInstanceCreationExpression_classToInstantiate());
        NodeList<Type> typeArguments = visitAndEnsureNodeList(ctx.typeArguments());
        NodeList<Expression> arguments = visitAndEnsureNodeList(ctx.argumentList());
        NodeList<BodyDeclaration<?>> body = visitAndCastOrNull(ctx.classBody());

        return new ObjectCreationExpr(scope,type,typeArguments,arguments,body);
    }

    @Override
    public Visitable visitUnqualifiedClassInstanceCreationExpression_classToInstantiate(HydraParser.UnqualifiedClassInstanceCreationExpression_classToInstantiateContext ctx)
    {
        ClassOrInterfaceType clazz = (ClassOrInterfaceType) visit(ctx.unqualifiedClassInstanceCreationExpression_classToInstantiate_name());
        NodeList<Type> list = visitAndCastOrNull(ctx.typeArgumentsOrDiamond());
        clazz.setTypeArguments(list);
        return clazz ;
    }

    @Override
    public Visitable visitUnqualifiedClassInstanceCreationExpression_classToInstantiate_name(HydraParser.UnqualifiedClassInstanceCreationExpression_classToInstantiate_nameContext ctx)
    {
        return createClassOrInterfaceType(ctx.unqualifiedClassInstanceCreationExpression_classToInstantiate_name(), ctx.Identifier(), ctx.annotation(), null);
    }

    @Override
    public Visitable visitTypeArgumentsOrDiamond(HydraParser.TypeArgumentsOrDiamondContext ctx)
    {
        return visitAndEnsureNodeList(ctx.typeArguments());
    }

    @Override
    public Expression visitMethodInvocationTypeName(HydraParser.MethodInvocationTypeNameContext ctx)
    {
        Expression expression = createMethodOrFunctionCallExpr(null, ctx.Identifier().getText(), ctx.argumentList(), ctx.typeArguments());

        if (expression instanceof MethodCallExpr)
            ((MethodCallExpr) expression).setScope( new AmbiguousNameExpr(ctx.typeName().getText()) );
        return expression;
    }

    /**
     * Takes a qualified name and returns a FieldAccessExpr
     * @param parse
     * @return
     */
    private Expression nameToFieldAccessExpr(Name parse)
    {
        return NameUtils.nameToFieldAccessExpr(parse);
    }

    private Type nameToType(Name name)
    {
        return NameUtils.nameToType(name);
    }


    @Override
    public Visitable visitMethodInvocationLfNoPrimaryMethodName(HydraParser.MethodInvocationLfNoPrimaryMethodNameContext ctx)
    {
        return createMethodOrFunctionCallExpr(null, ctx.methodName().Identifier().getText(), ctx.argumentList(), null );

    }

    @Override
    public Visitable visitMethodInvocationLfNoPrimaryTypeName(HydraParser.MethodInvocationLfNoPrimaryTypeNameContext ctx)
    {
        Expression expression = createMethodOrFunctionCallExpr(null, ctx.Identifier().getText(), ctx.argumentList(), ctx.typeArguments());

        if (expression instanceof MethodCallExpr)
            ((MethodCallExpr) expression).setScope(new AmbiguousNameExpr(ctx.typeName().getText()));
        return expression;
    }

    @Override
    public Visitable visitContinueStatement(HydraParser.ContinueStatementContext ctx)
    {
        SimpleName label = isNull(ctx.Identifier()) ? null : new SimpleName(ctx.Identifier().getText());
        return new ContinueStmt(label);
    }

    @Override
    public Visitable visitBreakStatement(HydraParser.BreakStatementContext ctx)
    {
        SimpleName label = isNull(ctx.Identifier()) ? null : new SimpleName(ctx.Identifier().getText());
        return new BreakStmt(label);
    }

    @Override
    public CastExpr visitCastExpressionUnaryExpression(HydraParser.CastExpressionUnaryExpressionContext ctx)
    {
        Type type = visitAndCastOrNull(ctx.primitiveType());
        Expression expression = visitAndCastOrNull(ctx.unaryExpression());
        return new CastExpr(type, expression);
    }

    @Override
    public CastExpr visitCastExpressionUnaryExpressionNotPlusMinus(HydraParser.CastExpressionUnaryExpressionNotPlusMinusContext ctx)
    {
        Type type = createIntersectionType(ctx.referenceType(), ctx.additionalBound());
        Expression expression = visitAndCastOrNull(ctx.unaryExpressionNotPlusMinus());
        return new CastExpr(type, expression);
    }

    @Override
    public CastExpr visitCastExpressionLambdaExpression(HydraParser.CastExpressionLambdaExpressionContext ctx)
    {
        Type type = createIntersectionType(ctx.referenceType(), ctx.additionalBound());
        Expression expression = visitAndCastOrNull(ctx.lambdaExpression());
        return new CastExpr(type, expression);
    }

    @Override
    public CastExpr visitCastUnaryExpressionNotPlusMinus(HydraParser.CastUnaryExpressionNotPlusMinusContext ctx) {
        return visitAndCastOrNull(ctx.castExpression());
    }

    private Type createIntersectionType(HydraParser.ReferenceTypeContext referenceTypeContext, List<HydraParser.AdditionalBoundContext> additionalBoundContexts)
    {
        Type type = visitAndCastOrNull(referenceTypeContext);
        if ( ! additionalBoundContexts.isEmpty() )
        {
            NodeList<ReferenceType<?>> typeList = new NodeList<>();
            typeList.add((ClassOrInterfaceType) type);
            typeList.addAll( mapAdditionalBoundList(additionalBoundContexts) );
            type = new IntersectionType(typeList);
        }
        return type;
    }

    private List<ReferenceType<?>> mapAdditionalBoundList(List<HydraParser.AdditionalBoundContext> additionalBoundContexts)
    {
        return additionalBoundContexts.stream()
                .map(ctx -> (ReferenceType<?>) visit(ctx.interfaceType()))
                .collect(Collectors.toList());
    }

    @Override
    public Visitable visitThreadStatement(HydraParser.ThreadStatementContext ctx)
    {
        SimpleName name = null ;
        HydraParser.ThreadHeaderContext threadHeaderCtx = ctx.threadHeader();

        NodeList<Parameter> parameters = null;
        if (nonNull(threadHeaderCtx))
        {
            if  (nonNull(threadHeaderCtx.threadName()))
                name = new SimpleName(threadHeaderCtx.threadName().Identifier().getText());

            if (nonNull(threadHeaderCtx.inferredFormalParameterList()))
                parameters = (NodeList<Parameter>) visit(threadHeaderCtx.inferredFormalParameterList());
        }

        Statement stmt = (Statement) visit(ctx.statement());
        return new ThreadCreationStmt(name, stmt, parameters);
    }

    @Override
    public Visitable visitTryStatementCatches(HydraParser.TryStatementCatchesContext ctx)
    {
        return createTryStatement(ctx.block(), ctx.catches(), null);
    }

    @Override
    public Visitable visitTryStatementCatchesFinally(HydraParser.TryStatementCatchesFinallyContext ctx)
    {
        return createTryStatement(ctx.block(), ctx.catches(), ctx.finally_());
    }

    @Override
    public Visitable visitTryStatementWithResources(HydraParser.TryStatementWithResourcesContext ctx)
    {
        return visit(ctx.tryWithResourcesStatement());
    }

    @Override
    public Visitable visitTryWithResourcesStatement(HydraParser.TryWithResourcesStatementContext ctx)
    {
        return createTryStatement(ctx.block(), ctx.catches(), ctx.finally_(), ctx.resourceSpecification());
    }

    public TryStmt createTryStatement(HydraParser.BlockContext tryContext, HydraParser.CatchesContext catchesContext,
                                      HydraParser.Finally_Context finallyContext)
    {
        return createTryStatement(tryContext, catchesContext, finallyContext, null);
    }

    public TryStmt createTryStatement(HydraParser.BlockContext tryContext, HydraParser.CatchesContext catchesContext,
                                      HydraParser.Finally_Context finallyContext, HydraParser.ResourceSpecificationContext resourcesContext)
    {
        declarationsStack.openScope();

        NodeList<VariableDeclarationExpr> resources = visitAndEnsureNodeList(resourcesContext);
        BlockStmt tryBlock = (BlockStmt) visit(tryContext);
        NodeList<CatchClause> catchClauses = visitAndEnsureNodeList(catchesContext);
        BlockStmt finallyBlock = visitAndCastOrNull(finallyContext);

        declarationsStack.closeScope();

        return new TryStmt(tryBlock, catchClauses, finallyBlock).setResources(resources);
    }

    @Override
    public NodeList<VariableDeclarationExpr> visitResourceSpecification(HydraParser.ResourceSpecificationContext ctx)
    {
        return (NodeList<VariableDeclarationExpr>) visit(ctx.resourceList());
    }

    @Override
    public NodeList<VariableDeclarationExpr> visitResourceList(HydraParser.ResourceListContext ctx)
    {
        List<VariableDeclarationExpr> list = ctx.resource().stream()
                .map(resourceContext -> (VariableDeclarationExpr) visit(resourceContext))
                .collect(Collectors.toList());

        list.forEach(variableDeclarationExpr -> addVariableDeclaratorsToDeclarationStack(variableDeclarationExpr.getVariables()));

        return NodeList.nodeList(list);
    }

    @Override
    public VariableDeclarationExpr visitResource(HydraParser.ResourceContext ctx)
    {
        EnumSet<Modifier> variableModifiers = mapVariableModifiers(ctx.variableModifier());
        Type unannType = visitAndCastOrUnknownType(ctx.unannType());
        Expression init = visitAndCastOrNull(ctx.expression());
        SimpleName name = new SimpleName(ctx.variableDeclaratorId().Identifier().getText());

        VariableDeclarator declarator = new VariableDeclarator(unannType, name, init);

        return new VariableDeclarationExpr(declarator).setModifiers(variableModifiers);
    }

    @Override
    public BlockStmt visitFinally_(HydraParser.Finally_Context ctx)
    {
        return (BlockStmt) visit(ctx.block());
    }

    @Override
    public NodeList<CatchClause> visitCatches(HydraParser.CatchesContext ctx)
    {
        List<CatchClause> list = ctx.catchClause().stream()
                .map(catchClauseContext -> (CatchClause) visit(catchClauseContext))
                .collect(Collectors.toList());
        return NodeList.nodeList(list);
    }

    @Override
    public Visitable visitCatchClause(HydraParser.CatchClauseContext ctx)
    {

        EnumSet<Modifier> modifiers = mapVariableModifiers(ctx.catchFormalParameter().variableModifier());
        NodeList<AnnotationExpr> annotations = emptyList();
        ClassOrInterfaceType type = (ClassOrInterfaceType) visit(ctx.catchFormalParameter().catchType());
        SimpleName name = new SimpleName(ctx.catchFormalParameter().variableDeclaratorId().Identifier().getText());
        BlockStmt body = (BlockStmt) visit(ctx.block());

        return new CatchClause(modifiers, annotations, type, name,body);
    }

    @Override
    public Visitable visitCatchType(HydraParser.CatchTypeContext ctx)
    {
        ClassOrInterfaceType unannClassType = (ClassOrInterfaceType) visit(ctx.unannClassType());

        if (ctx.classType().isEmpty()) return unannClassType;

        NodeList<ReferenceType<?>> elements = new NodeList<>();
        elements.add(unannClassType);

        ctx.classType().
                forEach(classTypeContext -> {
                    ReferenceType type = (ReferenceType) visit(classTypeContext);
                    elements.add(type);
                });

        return new UnionType(elements);
    }

    @Override
    public ThrowStmt visitThrowStatement(HydraParser.ThrowStatementContext ctx)
    {
        Expression expression = (Expression) visit(ctx.expression());
        return new ThrowStmt(expression);
    }

    @Override
    public Visitable visitLambdaExpression(HydraParser.LambdaExpressionContext ctx)
    {
        String lambdaParametersFirstToken = ctx.lambdaParameters().getChild(0).getText();
        boolean isEnclosingParameters = lambdaParametersFirstToken.length() > 0
                && lambdaParametersFirstToken.equals("(") ;
        Visitable body = visit(ctx.lambdaBody());
        if (body instanceof Expression)
            body = new ExpressionStmt((Expression)body);

        NodeList<Parameter> parameters = (NodeList<Parameter>) visit(ctx.lambdaParameters());
        return new LambdaExpr(parameters, (Statement) body, isEnclosingParameters);
    }

    @Override
    public NodeList<Parameter> visitLambdaParametersIdentifier(HydraParser.LambdaParametersIdentifierContext ctx)
    {
        NodeList<Parameter> parameters = new NodeList<>();
        Parameter parameter = unknownTypeParameter(ctx.getText());
        parameters.add(parameter);
        return parameters;
    }

    @Override
    public NodeList<Parameter> visitLambdaParametersFormalParameterList(HydraParser.LambdaParametersFormalParameterListContext ctx)
    {
        return isNull(ctx.formalParameterList())
                ? emptyList()
                : (NodeList<Parameter>) visit(ctx.formalParameterList());
    }

    @Override
    public NodeList<Parameter> visitLambdaParametersInferredFormalParameterList(HydraParser.LambdaParametersInferredFormalParameterListContext ctx)
    {
        return isNull(ctx.inferredFormalParameterList())
                ? emptyList()
                : (NodeList<Parameter>) visit(ctx.inferredFormalParameterList());
    }

    @Override
    public NodeList<Parameter> visitInferredFormalParameterList(HydraParser.InferredFormalParameterListContext ctx)
    {
        List<Parameter> list = ctx.Identifier().stream()
                .map(identifier -> unknownTypeParameter(identifier.getText()))
                .collect(Collectors.toList());
        return NodeList.nodeList(list);
    }

    private Parameter unknownTypeParameter(String text) {
        return new Parameter(unknownType(), text);
    }

    private UnknownType unknownType()
    {
        return new UnknownType();
    }

    @Override
    public ProcessCreationStmt visitProcessStatement(HydraParser.ProcessStatementContext ctx)
    {
        SimpleName name = null ;
        HydraParser.ProcessHeaderContext processHeaderCtx = ctx.processHeader();

        NodeList<Parameter> parameters = null;
        if (nonNull(processHeaderCtx))
        {
            if  (nonNull(processHeaderCtx.processName()))
                name = new SimpleName(processHeaderCtx.processName().Identifier().getText());

            if (nonNull(processHeaderCtx.inferredFormalParameterList()))
                parameters = (NodeList<Parameter>) visit(processHeaderCtx.inferredFormalParameterList());
        }

        Statement stmt = (Statement) visit(ctx.scriptBlock());
        return new ProcessCreationStmt(name, stmt, parameters);
    }

    @Override
    public SwitchStmt visitSwitchStatement(HydraParser.SwitchStatementContext ctx)
    {
        Expression selector = (Expression) visit(ctx.expression());
        NodeList<SwitchEntryStmt> entries = (NodeList<SwitchEntryStmt>) visit(ctx.switchBlock());
        return new SwitchStmt(selector, entries);
    }

    @Override
    public NodeList<SwitchEntryStmt> visitSwitchBlock(HydraParser.SwitchBlockContext ctx)
    {
        NodeList<SwitchEntryStmt> list = emptyList();

        for (HydraParser.SwitchBlockStatementGroupContext switchBlockStatementGroupCtx: ctx.switchBlockStatementGroup())
        {
            addSwitchLabelsForSwitchBlockStatementGroup(list, switchBlockStatementGroupCtx);
            SwitchEntryStmt lastEntryOfGroup = list.get(list.size()-1);
            NodeList<Statement> statements = (NodeList<Statement>) visit(switchBlockStatementGroupCtx.blockStatements());
            lastEntryOfGroup.setStatements(statements);
        }

        for (HydraParser.SwitchLabelContext switchLabelCtx : ctx.switchLabel())
            list.add(createSwitchEntryStmtForLabel(switchLabelCtx));

        return list;
    }

    private void addSwitchLabelsForSwitchBlockStatementGroup(NodeList<SwitchEntryStmt> list, HydraParser.SwitchBlockStatementGroupContext sbsgCtx)
    {
        for (HydraParser.SwitchLabelContext switchLabelContext : sbsgCtx.switchLabels().switchLabel())
        {
            SwitchEntryStmt switchEntryStmt = createSwitchEntryStmtForLabel(switchLabelContext);
            list.add(switchEntryStmt);
        }
    }

    private SwitchEntryStmt createSwitchEntryStmtForLabel(HydraParser.SwitchLabelContext switchLabelContext)
    {
        Expression labelExpr = visitAndCastOrNull(switchLabelContext);

        if (nonNull(switchLabelContext.constantExpression()))
            labelExpr = (Expression) visit(switchLabelContext.constantExpression());
        else if (nonNull(switchLabelContext.constantExpression()))
            labelExpr = (Expression) visit(switchLabelContext.enumConstantName());

        SwitchEntryStmt switchEntryStmt = new SwitchEntryStmt();

        if (nonNull(labelExpr))
            switchEntryStmt.setLabel(labelExpr);

        return switchEntryStmt;
    }

    @Override
    public Visitable visitSwitchLabel(HydraParser.SwitchLabelContext ctx)
    {
        Expression labelExpr = null ;

        if (nonNull(ctx.constantExpression()))
            labelExpr = (Expression) visit(ctx.constantExpression());
        else if (nonNull(ctx.constantExpression()))
            labelExpr = (Expression) visit(ctx.enumConstantName());

        return labelExpr;
    }

    @Override
    public Visitable visitSynchronizedStatement(HydraParser.SynchronizedStatementContext ctx)
    {
        Expression expr  = (Expression) visit(ctx.expression());
        BlockStmt stmt = (BlockStmt) visit(ctx.block());
        return new SynchronizedStmt(expr, stmt);
    }
}

