package ar.edu.unq.hydra.parser;

import ar.edu.unq.hydra.ast.HydraScript;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.parser.exceptions.HydraAstBuilderException;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.function.Function;

public class AstBuilderUtils
{
    public static HydraScript parseHydraScript(String sourceCode)
    {
        return parseToAstNode(sourceCode, parser -> parser.hydraScript());
    }

    /**
     * Builds a AST node from a valid portion of Hydra source code.
     * I.e. : AstBuilderUtils.parseToAstNode("01", parser -> parser.expression()
     *
     * @param sourceCodeBit portion of Hydra source code
     * @param parserVisitFunction
     * @param <T>
     * @return
     */
    public static <T extends Visitable> T parseToAstNode(String sourceCodeBit, Function<HydraParser,ParseTree> parserVisitFunction)
    {
        try {
            HydraParser parser = parserForString(sourceCodeBit);
            ParseTree pt = parserVisitFunction.apply(parser);
            HydraAstBuilder astGenerator = new HydraAstBuilder();
            return (T) pt.accept(astGenerator);
        } catch (HydraAstBuilderException e) {
            throw e;
        } catch (Throwable t)
        {
            throw new HydraAstBuilderException(t);
        }
    }

    protected static HydraParser parserForString(String str)
    {
        ANTLRInputStream input = new ANTLRInputStream(str);
        HydraLexer lexer = new HydraLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        HydraParser parser = new HydraParser(tokens);
        return parser ;
    }
}
