package ar.edu.unq.hydra.ast.nodeTypes.modifiers;

import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithModifiers;

import static ar.edu.unq.hydra.ast.Modifier.ABSTRACT;

/**
 * A node that can be abstract.
 */
public interface NodeWithAbstractModifier<N extends Node> extends NodeWithModifiers<N> {
    default boolean isAbstract() {
        return getModifiers().contains(ABSTRACT);
    }

    @SuppressWarnings("unchecked")
    default N setAbstract(boolean set) {
        return setModifier(ABSTRACT, set);
    }
}
