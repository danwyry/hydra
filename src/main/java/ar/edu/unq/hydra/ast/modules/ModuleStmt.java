package ar.edu.unq.hydra.ast.modules;

import ar.edu.unq.hydra.ast.Range;
import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.visitor.CloneVisitor;

public abstract class ModuleStmt extends Node {

    public ModuleStmt(Range range) {
        super(range);
    }

    @Override
    public boolean remove(Node node) {
        if (node == null)
            return false;
        return super.remove(node);
    }

    @Override
    public ModuleStmt clone() {
        return (ModuleStmt) accept(new CloneVisitor(), null);
    }

}
