package ar.edu.unq.hydra.ast.nodeTypes.modifiers;

import ar.edu.unq.hydra.ast.Node;

/**
 * A node that can be public, protected, and/or private.
 */
public interface NodeWithAccessModifiers<N extends Node> extends NodeWithPublicModifier<N>, NodeWithPrivateModifier<N>, NodeWithProtectedModifier<N> {
}
