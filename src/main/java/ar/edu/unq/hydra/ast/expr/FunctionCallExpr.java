/*
 * Copyright (C) 2007-2010 Júlio Vilmar Gesser.
 * Copyright (C) 2011, 2013-2016 The JavaParser Team.
 *
 * This file is part of JavaParser.
 *
 * JavaParser can be used either under the terms of
 * a) the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * b) the terms of the Apache License
 *
 * You should have received a copy of both licenses in LICENCE.LGPL and
 * LICENCE.APACHE. Please refer to those files for details.
 *
 * JavaParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */
package ar.edu.unq.hydra.ast.expr;

import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.NodeList;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithArguments;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithSimpleName;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithTypeArguments;
import ar.edu.unq.hydra.ast.observer.ObservableProperty;
import ar.edu.unq.hydra.ast.type.Type;
import ar.edu.unq.hydra.ast.visitor.CloneVisitor;
import ar.edu.unq.hydra.ast.visitor.GenericVisitor;
import ar.edu.unq.hydra.ast.visitor.VoidVisitor;
import ar.edu.unq.hydra.ast.Range;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.github.javaparser.utils.Utils.assertNotNull;

/**
 * A method call on an object. <br/><code>circle.circumference()</code> <br/>In <code>a.&lt;String&gt;bb(15);</code> a
 * is the scope, String is a type argument, bb is the name and 15 is an argument.
 *
 * @author Julio Vilmar Gesser
 */
public final class FunctionCallExpr extends Expression implements NodeWithTypeArguments<FunctionCallExpr>, NodeWithArguments<FunctionCallExpr>, NodeWithSimpleName<FunctionCallExpr>
{
    private NodeList<Type> typeArguments;

    private SimpleName name;

    private NodeList<Expression> arguments;

    public FunctionCallExpr() {
        this(null, new NodeList<>(), new SimpleName(), new NodeList<>());
    }

    public FunctionCallExpr(final String name) {
        this(null, new NodeList<>(), new SimpleName(name), new NodeList<>());
    }

    public FunctionCallExpr(final SimpleName name, final NodeList<Expression> arguments) {
        this(null, new NodeList<>(), name, arguments);
    }


    public FunctionCallExpr(final NodeList<Type> typeArguments, final SimpleName name, final NodeList<Expression> arguments) {
        this(null, typeArguments, name, arguments);
    }

    public FunctionCallExpr(final Range range, final NodeList<Type> typeArguments, final SimpleName name, final NodeList<Expression> arguments) {
        super(range);
        setTypeArguments(typeArguments);
        setName(name);
        setArguments(arguments);
    }

    @Override
    public <R, A> R accept(final GenericVisitor<R, A> v, final A arg) {
        return v.visit(this, arg);
    }

    @Override
    public <A> void accept(final VoidVisitor<A> v, final A arg) {
        v.visit(this, arg);
    }

    public NodeList<Expression> getArguments() {
        return arguments;
    }

    @Override
    public SimpleName getName() {
        return name;
    }

    public FunctionCallExpr setArguments(final NodeList<Expression> arguments) {
        assertNotNull(arguments);
        if (arguments == this.arguments) {
            return (FunctionCallExpr) this;
        }
        notifyPropertyChange(ObservableProperty.ARGUMENTS, this.arguments, arguments);
        if (this.arguments != null)
            this.arguments.setParentNode(null);
        this.arguments = arguments;
        setAsParentNodeOf(arguments);
        return this;
    }

    @Override
    public FunctionCallExpr setName(final SimpleName name) {
        assertNotNull(name);
        if (name == this.name) {
            return (FunctionCallExpr) this;
        }
        notifyPropertyChange(ObservableProperty.NAME, this.name, name);
        if (this.name != null)
            this.name.setParentNode(null);
        this.name = name;
        setAsParentNodeOf(name);
        return this;
    }



    @Override
    public Optional<NodeList<Type>> getTypeArguments() {
        return Optional.ofNullable(typeArguments);
    }

    /**
     * Sets the typeArguments
     *
     * @param typeArguments the typeArguments, can be null
     * @return this, the MethodCallExpr
     */
    @Override
    public FunctionCallExpr setTypeArguments(final NodeList<Type> typeArguments) {
        if (typeArguments == this.typeArguments) {
            return (FunctionCallExpr) this;
        }
        notifyPropertyChange(ObservableProperty.TYPE_ARGUMENTS, this.typeArguments, typeArguments);
        if (this.typeArguments != null)
            this.typeArguments.setParentNode(null);
        this.typeArguments = typeArguments;
        setAsParentNodeOf(typeArguments);
        return this;
    }

    @Override
    public List<NodeList<?>> getNodeLists() {
        return Arrays.asList(getArguments(), getTypeArguments().orElse(null));
    }

    @Override
    public boolean remove(Node node) {
        if (node == null)
            return false;
        for (int i = 0; i < arguments.size(); i++) {
            if (arguments.get(i) == node) {
                arguments.remove(i);
                return true;
            }
        }
        if (typeArguments != null) {
            for (int i = 0; i < typeArguments.size(); i++) {
                if (typeArguments.get(i) == node) {
                    typeArguments.remove(i);
                    return true;
                }
            }
        }
        return super.remove(node);
    }

    @Override
    public FunctionCallExpr clone() {
        return (FunctionCallExpr) accept(new CloneVisitor(), null);
    }

}
