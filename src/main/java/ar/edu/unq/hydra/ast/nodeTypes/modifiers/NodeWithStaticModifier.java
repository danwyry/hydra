package ar.edu.unq.hydra.ast.nodeTypes.modifiers;

import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithModifiers;

import static ar.edu.unq.hydra.ast.Modifier.STATIC;

/**
 * A node that can be static.
 */
public interface NodeWithStaticModifier<N extends Node> extends NodeWithModifiers<N> {

    default boolean isStatic() {
        return getModifiers().contains(STATIC);
    }

    @SuppressWarnings("unchecked")
    default N setStatic(boolean set) {
        return setModifier(STATIC, set);
    }

}
