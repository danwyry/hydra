package ar.edu.unq.hydra.ast.visitor;

import ar.edu.unq.hydra.ast.*;
import ar.edu.unq.hydra.ast.body.*;
import ar.edu.unq.hydra.ast.comments.BlockComment;
import ar.edu.unq.hydra.ast.comments.JavadocComment;
import ar.edu.unq.hydra.ast.comments.LineComment;
import ar.edu.unq.hydra.ast.expr.*;
import ar.edu.unq.hydra.ast.modules.*;
import ar.edu.unq.hydra.ast.stmt.*;
import ar.edu.unq.hydra.ast.type.*;

/**
 * Created by Daniel Wyrytowski on 1/14/17.
 */
abstract public class ExpressionVisitor<R, A> implements GenericVisitor<R,A> {
    @Override
    public R visit(CompilationUnit n, A arg) {
        return null;
    }

    @Override
    public R visit(PackageDeclaration n, A arg) {
        return null;
    }

    @Override
    public R visit(TypeParameter n, A arg) {
        return null;
    }

    @Override
    public R visit(LineComment n, A arg) {
        return null;
    }

    @Override
    public R visit(BlockComment n, A arg) {
        return null;
    }

    @Override
    public R visit(ClassOrInterfaceDeclaration n, A arg) {
        return null;
    }

    @Override
    public R visit(EnumDeclaration n, A arg) {
        return null;
    }

    @Override
    public R visit(EnumConstantDeclaration n, A arg) {
        return null;
    }

    @Override
    public R visit(AnnotationDeclaration n, A arg) {
        return null;
    }

    @Override
    public R visit(AnnotationMemberDeclaration n, A arg) {
        return null;
    }

    @Override
    public R visit(FieldDeclaration n, A arg) {
        return null;
    }

    @Override
    public R visit(VariableDeclarator n, A arg) {
        return null;
    }

    @Override
    public R visit(ConstructorDeclaration n, A arg) {
        return null;
    }

    @Override
    public R visit(MethodDeclaration n, A arg) {
        return null;
    }

    @Override
    public R visit(Parameter n, A arg) {
        return null;
    }

    @Override
    public R visit(EmptyMemberDeclaration n, A arg) {
        return null;
    }

    @Override
    public R visit(InitializerDeclaration n, A arg) {
        return null;
    }

    @Override
    public R visit(JavadocComment n, A arg) {
        return null;
    }

    @Override
    public R visit(ClassOrInterfaceType n, A arg) {
        return null;
    }

    @Override
    public R visit(PrimitiveType n, A arg) {
        return null;
    }

    @Override
    public R visit(ArrayType n, A arg) {
        return null;
    }

    @Override
    public R visit(ArrayCreationLevel n, A arg) {
        return null;
    }

    @Override
    public R visit(IntersectionType n, A arg) {
        return null;
    }

    @Override
    public R visit(UnionType n, A arg) {
        return null;
    }

    @Override
    public R visit(VoidType n, A arg) {
        return null;
    }

    @Override
    public R visit(WildcardType n, A arg) {
        return null;
    }

    @Override
    public R visit(UnknownType n, A arg) {
        return null;
    }

    abstract public R visit(ArrayAccessExpr n, A arg);

    abstract public R visit(ArrayCreationExpr n, A arg);

    abstract public R visit(ArrayInitializerExpr n, A arg);

    abstract public R visit(AssignExpr n, A arg) ;

    abstract public R visit(BinaryExpr n, A arg) ;

    abstract public R visit(CastExpr n, A arg);

    abstract public R visit(ClassExpr n, A arg);

    abstract public R visit(ConditionalExpr n, A arg);

    abstract public R visit(EnclosedExpr n, A arg);

    abstract public R visit(FieldAccessExpr n, A arg);

    abstract public R visit(InstanceOfExpr n, A arg);

    abstract public R visit(StringLiteralExpr n, A arg);

    abstract public R visit(IntegerLiteralExpr n, A arg);

    abstract public R visit(LongLiteralExpr n, A arg);

    abstract public R visit(CharLiteralExpr n, A arg);

    abstract public R visit(DoubleLiteralExpr n, A arg);

    abstract public R visit(BooleanLiteralExpr n, A arg);

    abstract public R visit(NullLiteralExpr n, A arg);

    abstract public R visit(MethodCallExpr n, A arg);

    abstract public R visit(NameExpr n, A arg);

    abstract public R visit(ObjectCreationExpr n, A arg);

    abstract public R visit(ThisExpr n, A arg);

    abstract public R visit(SuperExpr n, A arg);

    abstract public R visit(UnaryExpr n, A arg) ;

    abstract public R visit(VariableDeclarationExpr n, A arg);

    abstract public R visit(MarkerAnnotationExpr n, A arg);

    abstract public R visit(SingleMemberAnnotationExpr n, A arg);

    abstract public R visit(NormalAnnotationExpr n, A arg);

    @Override
    public R visit(MemberValuePair n, A arg) {
        return null;
    }

    @Override
    public R visit(ExplicitConstructorInvocationStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(LocalClassDeclarationStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(AssertStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(BlockStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(LabeledStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(EmptyStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(ExpressionStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(SwitchStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(SwitchEntryStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(BreakStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(ReturnStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(IfStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(WhileStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(ContinueStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(DoStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(ForeachStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(ForStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(ThrowStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(SynchronizedStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(TryStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(CatchClause n, A arg) {
        return null;
    }

    abstract public R visit(LambdaExpr n, A arg);

    abstract public R visit(MethodReferenceExpr n, A arg);

    abstract public R visit(TypeExpr n, A arg);

    @Override
    public R visit(NodeList n, A arg) {
        return null;
    }

    @Override
    public R visit(Name n, A arg) {
        return null;
    }

    @Override
    public R visit(SimpleName n, A arg) {
        return null;
    }

    @Override
    public R visit(ImportDeclaration n, A arg) {
        return null;
    }

    @Override
    public R visit(ModuleDeclaration n, A arg) {
        return null;
    }

    @Override
    public R visit(ModuleRequiresStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(ModuleExportsStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(ModuleProvidesStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(ModuleUsesStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(ModuleOpensStmt n, A arg) {
        return null;
    }

    @Override
    public R visit(FunctionDeclaration n, A arg) {
        return null;
    }

    @Override
    public R visit(HydraScript n, A arg) {
        return null;
    }

    @Override
    public R visit(ScriptBlockStmt n, A arg)
    {
        return null;
    }

    @Override
    abstract public R visit(FunctionCallExpr functionCallExpr, A arg) ;

    @Override
    public R visit(ThreadCreationStmt threadCreationStmt, A arg)
    {
        return null;
    }

    @Override
    public R visit(RepeatStmt repeatStmt, A arg)
    {
        return null;
    }

    @Override
    abstract public R visit(AmbiguousNameExpr ambiguousNameExpr, A arg) ;

    @Override
    public R visit(ProcessCreationStmt processCreationStmt, A arg)
    {
        return null;
    }
}
