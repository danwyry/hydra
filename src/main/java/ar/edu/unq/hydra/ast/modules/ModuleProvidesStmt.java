package ar.edu.unq.hydra.ast.modules;

import ar.edu.unq.hydra.ast.Range;
import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.NodeList;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithType;
import ar.edu.unq.hydra.ast.observer.ObservableProperty;
import ar.edu.unq.hydra.ast.type.ClassOrInterfaceType;
import ar.edu.unq.hydra.ast.type.Type;
import ar.edu.unq.hydra.ast.visitor.CloneVisitor;
import ar.edu.unq.hydra.ast.visitor.GenericVisitor;
import ar.edu.unq.hydra.ast.visitor.VoidVisitor;

import java.util.Arrays;
import java.util.List;

import static com.github.javaparser.utils.Utils.assertNotNull;

public class ModuleProvidesStmt extends ModuleStmt implements NodeWithType<ModuleProvidesStmt, Type> {

    private Type type;

    private NodeList<Type> withTypes;

    public ModuleProvidesStmt() {
        this(null, new ClassOrInterfaceType(), new NodeList<>());
    }


    public ModuleProvidesStmt(Type type, NodeList<Type> withTypes) {
        this(null, type, withTypes);
    }

    public ModuleProvidesStmt(Range range, Type type, NodeList<Type> withTypes) {
        super(range);
        setType(type);
        setWithTypes(withTypes);
    }

    @Override
    public <R, A> R accept(GenericVisitor<R, A> v, A arg) {
        return v.visit(this, arg);
    }

    @Override
    public <A> void accept(VoidVisitor<A> v, A arg) {
        v.visit(this, arg);
    }

    @Override
    public boolean remove(Node node) {
        if (node == null)
            return false;
        for (int i = 0; i < withTypes.size(); i++) {
            if (withTypes.get(i) == node) {
                withTypes.remove(i);
                return true;
            }
        }
        return super.remove(node);
    }

    public Type getType() {
        return type;
    }

    public ModuleProvidesStmt setType(final Type type) {
        assertNotNull(type);
        if (type == this.type) {
            return (ModuleProvidesStmt) this;
        }
        notifyPropertyChange(ObservableProperty.TYPE, this.type, type);
        if (this.type != null)
            this.type.setParentNode(null);
        this.type = type;
        setAsParentNodeOf(type);
        return this;
    }

    public NodeList<Type> getWithTypes() {
        return withTypes;
    }

    public ModuleProvidesStmt setWithTypes(final NodeList<Type> withTypes) {
        assertNotNull(withTypes);
        if (withTypes == this.withTypes) {
            return (ModuleProvidesStmt) this;
        }
        notifyPropertyChange(ObservableProperty.WITH_TYPES, this.withTypes, withTypes);
        if (this.withTypes != null)
            this.withTypes.setParentNode(null);
        this.withTypes = withTypes;
        setAsParentNodeOf(withTypes);
        return this;
    }

    @Override
    public List<NodeList<?>> getNodeLists() {
        return Arrays.asList(getWithTypes());
    }

    @Override
    public ModuleProvidesStmt clone() {
        return (ModuleProvidesStmt) accept(new CloneVisitor(), null);
    }

}
