package ar.edu.unq.hydra.ast;

import ar.edu.unq.hydra.ast.expr.*;
import ar.edu.unq.hydra.ast.type.ClassOrInterfaceType;
import ar.edu.unq.hydra.ast.type.PrimitiveType;
import ar.edu.unq.hydra.ast.type.Type;

/**
 * Created by Daniel Wyrytowski on 6/11/17.
 */
public class NameUtils
{
    public static ClassOrInterfaceType nameToClassOrInterfaceType(Name typeName)
    {
        Name currName = typeName;
        ClassOrInterfaceType retType = new ClassOrInterfaceType(currName.getIdentifier());
        ClassOrInterfaceType currType = retType;

        while ( currName.getQualifier().isPresent() )
        {
            currName = currName.getQualifier().get();
            ClassOrInterfaceType scopeType = new ClassOrInterfaceType(currName.getIdentifier());
            currType.setScope(scopeType);
            currType = scopeType;
        }

        return retType;
    }

    public static FieldAccessExpr nameToFieldAccessExpr(Name name)
    {
        FieldAccessExpr fieldAccessExpr = new FieldAccessExpr(null, name.getIdentifier());
        Expression currentExpr = fieldAccessExpr;
        do
        {
            name = name.getQualifier().get();
            if (name.getQualifier().isPresent())
            {
                Expression scopeExpr = new FieldAccessExpr(null, name.getIdentifier());
                ((FieldAccessExpr) currentExpr).setScope(scopeExpr);
                currentExpr = scopeExpr;
            } else {
                Expression scopeExpr = new NameExpr(name.getIdentifier());
                ((FieldAccessExpr) currentExpr).setScope(scopeExpr);
            }
        } while (name.getQualifier().isPresent());

        return fieldAccessExpr;

    }


    public static Type nameToType(Name name)
    {
        switch (name.asString())
        {
            case "boolean":
                return PrimitiveType.booleanType();
            case "byte":
                return PrimitiveType.byteType();
            case "char":
                return PrimitiveType.charType();
            case "short":
                return PrimitiveType.shortType();
            case "int":
                return PrimitiveType.intType();
            case "long":
                return PrimitiveType.longType();
            case "float":
                return PrimitiveType.floatType();
            case "double":
                return PrimitiveType.doubleType();
        }

        return nameToClassOrInterfaceType(name);
    }

    /**
     * Returns a FieldAccessExpr in which the scope is the ClassExpr where the statically imported field lives.
     * For example, given the name: A.B.staticField it will return:
     *  FieldAccessExpr(ClassExpr(ClassOrInterfaceType(ClassOrInterfaceType(B),A)), SimpleName(staticField))
     * @param name
     * @return
     */
    public static Expression nameToImportedFieldAccessExpression(Name name)
    {
        Name typeName = name.getQualifier().get();
        String fieldName = name.getIdentifier();

        ClassOrInterfaceType type = NameUtils.nameToClassOrInterfaceType(typeName);
        return new FieldAccessExpr(new ClassExpr(type), fieldName);

    }
}
