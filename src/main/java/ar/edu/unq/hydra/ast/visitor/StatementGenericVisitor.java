package ar.edu.unq.hydra.ast.visitor;

import ar.edu.unq.hydra.ast.*;
import ar.edu.unq.hydra.ast.body.*;
import ar.edu.unq.hydra.ast.comments.BlockComment;
import ar.edu.unq.hydra.ast.comments.JavadocComment;
import ar.edu.unq.hydra.ast.comments.LineComment;
import ar.edu.unq.hydra.ast.expr.*;
import ar.edu.unq.hydra.ast.modules.*;
import ar.edu.unq.hydra.ast.stmt.*;
import ar.edu.unq.hydra.ast.type.*;

/**
 * Created by Daniel Wyrytowski on 2/6/17.
 */
abstract public class StatementGenericVisitor<R,A> implements GenericVisitor<R,A>
{

    @Override
    public R visit(NodeList n, A arg) { return null ; }

    @Override
    public R visit(AnnotationDeclaration n, A arg) { return null ; }

    @Override
    public R visit(AnnotationMemberDeclaration n, A arg) { return null ; }

    @Override
    public R visit(ArrayAccessExpr n, A arg) { return null ; }

    @Override
    public R visit(ArrayCreationExpr n, A arg) { return null ; }

    @Override
    public R visit(ArrayCreationLevel n, A arg) { return null ; }

    @Override
    public R visit(ArrayInitializerExpr n, A arg) { return null ; }

    @Override
    public R visit(ArrayType n, A arg) { return null ; }

    @Override
    abstract public R visit(AssertStmt n, A arg);

    @Override
    public R visit(AssignExpr n, A arg) { return null ; }

    @Override
    public R visit(BinaryExpr n, A arg) { return null ; }

    @Override
    public R visit(BlockComment n, A arg) { return null ; }

    @Override
    abstract public R visit(BlockStmt n, A arg);

    @Override
    public R visit(BooleanLiteralExpr n, A arg) { return null ; }

    @Override
    abstract public R visit(BreakStmt n, A arg);

    @Override
    public R visit(CastExpr n, A arg) { return null ; }

    @Override
    public R visit(CatchClause n, A arg) { return null ; }

    @Override
    public R visit(CharLiteralExpr n, A arg) { return null ; }

    @Override
    public R visit(ClassExpr n, A arg) { return null ; }

    @Override
    public R visit(ClassOrInterfaceDeclaration n, A arg) { return null ; }

    @Override
    public R visit(ClassOrInterfaceType n, A arg) { return null ; }

    @Override
    public R visit(CompilationUnit n, A arg) { return null ; }

    @Override
    public R visit(ConditionalExpr n, A arg) { return null ; }

    @Override
    public R visit(ConstructorDeclaration n, A arg) { return null ; }

    @Override
    abstract public R visit(ContinueStmt n, A arg);

    @Override
    abstract public R visit(DoStmt n, A arg);

    @Override
    public R visit(DoubleLiteralExpr n, A arg) { return null ; }

    @Override
    public R visit(EmptyMemberDeclaration n, A arg) { return null ; }

    @Override
    abstract public R visit(EmptyStmt n, A arg);

    @Override
    public R visit(EnclosedExpr n, A arg) { return null ; }

    @Override
    public R visit(EnumConstantDeclaration n, A arg) { return null ; }

    @Override
    public R visit(EnumDeclaration n, A arg) { return null ; }

    @Override
    abstract public R visit(ExplicitConstructorInvocationStmt n, A arg);

    @Override
    abstract public R visit(ExpressionStmt n, A arg);

    @Override
    public R visit(FieldAccessExpr n, A arg) { return null ; }

    @Override
    public R visit(FieldDeclaration n, A arg) { return null ; }

    @Override
    abstract public R visit(ForStmt n, A arg);

    @Override
    abstract public R visit(ForeachStmt n, A arg);

    @Override
    abstract public R visit(IfStmt n, A arg);

    @Override
    public R visit(ImportDeclaration n, A arg) { return null ; }

    @Override
    public R visit(InitializerDeclaration n, A arg) { return null ; }

    @Override
    public R visit(InstanceOfExpr n, A arg) { return null ; }

    @Override
    public R visit(IntegerLiteralExpr n, A arg) { return null ; }

    @Override
    public R visit(IntersectionType n, A arg) { return null ; }

    @Override
    public R visit(JavadocComment n, A arg) { return null ; }

    abstract public R visit(LabeledStmt n, A arg);

    @Override
    public R visit(LambdaExpr n, A arg) { return null ; }

    @Override
    public R visit(LineComment n, A arg) { return null ; }

    @Override
    abstract public R visit(LocalClassDeclarationStmt n, A arg);

    @Override
    public R visit(LongLiteralExpr n, A arg) { return null ; }

    @Override
    public R visit(MarkerAnnotationExpr n, A arg) { return null ; }

    @Override
    public R visit(MemberValuePair n, A arg) { return null ; }

    @Override
    public R visit(MethodCallExpr n, A arg) { return null ; }

    @Override
    public R visit(MethodDeclaration n, A arg) { return null ; }

    @Override
    public R visit(MethodReferenceExpr n, A arg) { return null ; }

    @Override
    public R visit(NameExpr n, A arg) { return null ; }

    @Override
    public R visit(Name n, A arg) { return null ; }

    @Override
    public R visit(NormalAnnotationExpr n, A arg) { return null ; }

    @Override
    public R visit(NullLiteralExpr n, A arg) { return null ; }

    @Override
    public R visit(ObjectCreationExpr n, A arg) { return null ; }

    @Override
    public R visit(PackageDeclaration n, A arg) { return null ; }

    @Override
    public R visit(Parameter n, A arg) { return null ; }

    @Override
    public R visit(PrimitiveType n, A arg) { return null ; }

    abstract public R visit(ReturnStmt n, A arg);

    @Override
    public R visit(SimpleName n, A arg) { return null ; }

    @Override
    public R visit(SingleMemberAnnotationExpr n, A arg) { return null ; }

    @Override
    public R visit(StringLiteralExpr n, A arg) { return null ; }

    @Override
    public R visit(SuperExpr n, A arg) { return null ; }

    @Override
    abstract public R visit(SwitchEntryStmt n, A arg);

    @Override
    abstract public R visit(SwitchStmt n, A arg);

    @Override
    abstract public R visit(SynchronizedStmt n, A arg);

    @Override
    public R visit(ThisExpr n, A arg) { return null ; }

    abstract public R visit(ThrowStmt n, A arg);

    abstract public R visit(TryStmt n, A arg);

    @Override
    public R visit(TypeExpr n, A arg) { return null ; }

    @Override
    public R visit(TypeParameter n, A arg) { return null ; }

    @Override
    public R visit(UnaryExpr n, A arg) { return null ; }

    @Override
    public R visit(UnionType n, A arg) { return null ; }

    @Override
    public R visit(UnknownType n, A arg) { return null ; }

    @Override
    public R visit(VariableDeclarationExpr n, A arg) { return null ; }

    @Override
    public R visit(VariableDeclarator n, A arg) { return null ; }

    @Override
    public R visit(VoidType n, A arg) { return null ; }

    @Override
    abstract public R visit(WhileStmt n, A arg);

    @Override
    public R visit(WildcardType n, A arg) { return null ; }

    @Override
    public R visit(ModuleDeclaration n, A arg) { return null ; }

    @Override
    abstract public R visit(ModuleRequiresStmt n, A arg);

    @Override
    abstract public R visit(ModuleExportsStmt n, A arg);

    @Override
    abstract public R visit(ModuleProvidesStmt n, A arg);

    @Override
    abstract public R visit(ModuleUsesStmt n, A arg);

    @Override
    abstract public R visit(ModuleOpensStmt n, A arg);

    @Override
    public R visit(FunctionDeclaration n, A arg)
    { return null ; }

    @Override
    abstract public R visit(HydraScript n, A arg) ;

    @Override
    public R visit(FunctionCallExpr n, A arg)
    { return null ; }

    @Override
    abstract public R visit(ScriptBlockStmt n, A arg) ;

    @Override
    abstract public R visit(ThreadCreationStmt threadCreationStmt, A arg) ;

    @Override
    public R visit(AmbiguousNameExpr ambiguousNameExpr, A arg)
    { return null ; }

    @Override
    abstract public R visit(RepeatStmt repeatStmt, A arg) ;
}
