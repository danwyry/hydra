/*
 * Copyright (C) 2007-2010 Júlio Vilmar Gesser.
 * Copyright (C) 2011, 2013-2016 The JavaParser Team.
 *
 * This file is part of JavaParser.
 *
 * JavaParser can be used either under the terms of
 * a) the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * b) the terms of the Apache License
 *
 * You should have received a copy of both licenses in LICENCE.LGPL and
 * LICENCE.APACHE. Please refer to those files for details.
 *
 * JavaParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */
package ar.edu.unq.hydra.ast.stmt;

import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.expr.Expression;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithBody;
import ar.edu.unq.hydra.ast.observer.ObservableProperty;
import ar.edu.unq.hydra.ast.visitor.CloneVisitor;
import ar.edu.unq.hydra.ast.visitor.GenericVisitor;
import ar.edu.unq.hydra.ast.visitor.VoidVisitor;
import ar.edu.unq.hydra.ast.Range;

import static com.github.javaparser.utils.Utils.assertNotNull;

/**
 * A for-each statement.
 * <br/><code>for(Object o: objects) { ... }</code>
 *
 * @author Julio Vilmar Gesser
 */
public final class RepeatStmt extends Statement implements NodeWithBody<RepeatStmt> {

    private Expression repetitions;

    private Statement body;

    public RepeatStmt(final Expression iterations, final Statement body) {
        this(null, iterations, body);
    }

    public RepeatStmt(Range range, final Expression iterations, final Statement body) {
        super(range);
        setRepetitions(iterations);
        setBody(body);
    }

    @Override
    public <R, A> R accept(final GenericVisitor<R, A> v, final A arg) {
        return v.visit(this, arg);
    }

    @Override
    public <A> void accept(final VoidVisitor<A> v, final A arg) {
        v.visit(this, arg);
    }

    @Override
    public Statement getBody() {
        return body;
    }

    public Expression getRepetitions() {
        return repetitions;
    }

    @Override
    public RepeatStmt setBody(final Statement body) {
        assertNotNull(body);
        if (body == this.body) {
            return (RepeatStmt) this;
        }
        notifyPropertyChange(ObservableProperty.BODY, this.body, body);
        if (this.body != null)
            this.body.setParentNode(null);
        this.body = body;
        setAsParentNodeOf(body);
        return this;
    }

    public RepeatStmt setRepetitions(final Expression repetitions) {
        assertNotNull(repetitions);
        if (repetitions == this.repetitions) {
            return (RepeatStmt) this;
        }
        notifyPropertyChange(ObservableProperty.ITERABLE, this.repetitions, repetitions);
        if (this.repetitions != null)
            this.repetitions.setParentNode(null);
        this.repetitions = repetitions;
        setAsParentNodeOf(repetitions);
        return this;
    }


    @Override
    public boolean remove(Node node) {
        if (node == null)
            return false;
        return super.remove(node);
    }

    @Override
    public RepeatStmt clone() {
        return (RepeatStmt) accept(new CloneVisitor(), null);
    }

}
