package ar.edu.unq.hydra.ast;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum Modifier {
    PUBLIC,
    PROTECTED,
    PRIVATE,
    ABSTRACT,
    STATIC,
    FINAL,
    TRANSIENT,
    VOLATILE,
    SYNCHRONIZED,
    NATIVE,
    STRICTFP,
    TRANSITIVE,
    DEFAULT;

    final String codeRepresentation;

    Modifier() {
        this.codeRepresentation = name().toLowerCase();
    }

    /**
     * @return the keyword represented by this modifier.
     */
    public String asString() {
        return codeRepresentation;
    }

    public EnumSet<Modifier> toEnumSet() {
        return EnumSet.of(this);
    }

    public static AccessSpecifier getAccessSpecifier(EnumSet<Modifier> modifiers) {
        if (modifiers.contains(Modifier.PUBLIC)) {
            return AccessSpecifier.PUBLIC;
        } else if (modifiers.contains(Modifier.PROTECTED)) {
            return AccessSpecifier.PROTECTED;
        } else if (modifiers.contains(Modifier.PRIVATE)) {
            return AccessSpecifier.PRIVATE;
        } else {
            return AccessSpecifier.DEFAULT;
        }
    }

    /* The stringToEnum and fromString methods were copied from "Effective Java", by Joshua Bloch */
    private static final Map<String, Modifier> stringToEnum
            = new HashMap<String, Modifier>();
    static {
        for (Modifier mod : values())
            stringToEnum.put(mod.codeRepresentation, mod);
    }

    public static Modifier fromString(String symbol) { return stringToEnum.get(symbol); }


}
