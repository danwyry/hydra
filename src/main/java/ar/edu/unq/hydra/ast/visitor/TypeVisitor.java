package ar.edu.unq.hydra.ast.visitor;

import ar.edu.unq.hydra.ast.*;
import ar.edu.unq.hydra.ast.body.*;
import ar.edu.unq.hydra.ast.comments.BlockComment;
import ar.edu.unq.hydra.ast.comments.JavadocComment;
import ar.edu.unq.hydra.ast.comments.LineComment;
import ar.edu.unq.hydra.ast.expr.*;
import ar.edu.unq.hydra.ast.modules.*;
import ar.edu.unq.hydra.ast.stmt.*;
import ar.edu.unq.hydra.ast.type.*;

public interface TypeVisitor<R,A> extends GenericVisitor<R,A> {
    @Override
    default R visit(CompilationUnit n, A arg) {
        return null;
    }

    @Override
    default R visit(PackageDeclaration n, A arg) {
        return null;
    }

    @Override
    default R visit(TypeParameter n, A arg) {
        return null;
    }

    @Override
    default R visit(LineComment n, A arg) {
        return null;
    }

    @Override
    default R visit(BlockComment n, A arg) {
        return null;
    }

    @Override
    default R visit(ClassOrInterfaceDeclaration n, A arg) {
        return null;
    }

    @Override
    default R visit(EnumDeclaration n, A arg) {
        return null;
    }

    @Override
    default R visit(EnumConstantDeclaration n, A arg) {
        return null;
    }

    @Override
    default R visit(AnnotationDeclaration n, A arg) {
        return null;
    }

    @Override
    default R visit(AnnotationMemberDeclaration n, A arg) {
        return null;
    }

    @Override
    default R visit(FieldDeclaration n, A arg) {
        return null;
    }

    @Override
    default R visit(VariableDeclarator n, A arg) {
        return null;
    }

    @Override
    default R visit(ConstructorDeclaration n, A arg) {
        return null;
    }

    @Override
    default R visit(MethodDeclaration n, A arg) {
        return null;
    }

    @Override
    default R visit(Parameter n, A arg) {
        return null;
    }

    @Override
    default R visit(EmptyMemberDeclaration n, A arg) {
        return null;
    }

    @Override
    default R visit(InitializerDeclaration n, A arg) {
        return null;
    }

    @Override
    default R visit(JavadocComment n, A arg) {
        return null;
    }

    R visit(ClassOrInterfaceType n, A arg) ;


    R visit(PrimitiveType n, A arg) ;

    R visit(ArrayType n, A arg) ;

    @Override
    default R visit(ArrayCreationLevel n, A arg) {
        return null;
    }

    R visit(IntersectionType n, A arg) ;

    R visit(UnionType n, A arg) ;

    R visit(VoidType n, A arg) ;

    R visit(WildcardType n, A arg) ;

    R visit(UnknownType n, A arg) ;

    @Override
    default R visit(ArrayAccessExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(ArrayCreationExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(ArrayInitializerExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(AssignExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(BinaryExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(CastExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(ClassExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(ConditionalExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(EnclosedExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(FieldAccessExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(InstanceOfExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(StringLiteralExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(IntegerLiteralExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(LongLiteralExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(CharLiteralExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(DoubleLiteralExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(BooleanLiteralExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(NullLiteralExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(MethodCallExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(NameExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(ObjectCreationExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(ThisExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(SuperExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(UnaryExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(VariableDeclarationExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(MarkerAnnotationExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(SingleMemberAnnotationExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(NormalAnnotationExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(MemberValuePair n, A arg) {
        return null;
    }

    @Override
    default R visit(ExplicitConstructorInvocationStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(LocalClassDeclarationStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(AssertStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(BlockStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(LabeledStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(EmptyStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(ExpressionStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(SwitchStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(SwitchEntryStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(BreakStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(ReturnStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(IfStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(WhileStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(ContinueStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(DoStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(ForeachStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(ForStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(ThrowStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(SynchronizedStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(TryStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(CatchClause n, A arg) {
        return null;
    }

    @Override
    default R visit(LambdaExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(MethodReferenceExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(TypeExpr n, A arg) {
        return null;
    }

    @Override
    default R visit(NodeList n, A arg) {
        return null;
    }

    @Override
    default R visit(Name n, A arg) {
        return null;
    }

    @Override
    default R visit(SimpleName n, A arg) {
        return null;
    }

    @Override
    default R visit(ImportDeclaration n, A arg) {
        return null;
    }

    @Override
    default R visit(ModuleDeclaration n, A arg) {
        return null;
    }

    @Override
    default R visit(ModuleRequiresStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(ModuleExportsStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(ModuleProvidesStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(ModuleUsesStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(ModuleOpensStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(FunctionDeclaration n, A arg) {
        return null;
    }

    @Override
    default R visit(HydraScript n, A arg) {
        return null;
    }

    @Override
    default R visit(ScriptBlockStmt n, A arg) {
        return null;
    }

    @Override
    default R visit(FunctionCallExpr functionCallExpr, A arg) {
        return null;
    }

    @Override
    default R visit(ThreadCreationStmt threadCreationStmt, A arg)
    {
        return null;
    }

    @Override
    default R visit(AmbiguousNameExpr ambiguousNameExpr, A arg) {return null ; }

    @Override
    default R visit(RepeatStmt repeatStmt, A arg) {return null; }

    @Override
    default R visit(ProcessCreationStmt processCreationStmt, A arg)
    {
        return null;
    }
}
