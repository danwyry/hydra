package ar.edu.unq.hydra.ast.modules;

import ar.edu.unq.hydra.ast.Range;
import ar.edu.unq.hydra.ast.Modifier;
import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.expr.Name;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithName;
import ar.edu.unq.hydra.ast.nodeTypes.modifiers.NodeWithStaticModifier;
import ar.edu.unq.hydra.ast.observer.ObservableProperty;
import ar.edu.unq.hydra.ast.visitor.CloneVisitor;
import ar.edu.unq.hydra.ast.visitor.GenericVisitor;
import ar.edu.unq.hydra.ast.visitor.VoidVisitor;

import java.util.EnumSet;

import static ar.edu.unq.hydra.ast.Modifier.TRANSITIVE;
import static com.github.javaparser.utils.Utils.assertNotNull;

/**
 * A require statement in module-info.java. <code>require a.b.C;</code>
 */
public class ModuleRequiresStmt extends ModuleStmt implements NodeWithStaticModifier<ModuleRequiresStmt>, NodeWithName<ModuleRequiresStmt> {

    private EnumSet<Modifier> modifiers;

    private Name name;

    public ModuleRequiresStmt() {
        this(null, EnumSet.noneOf(Modifier.class), new Name());
    }


    public ModuleRequiresStmt(EnumSet<Modifier> modifiers, Name name) {
        this(null, modifiers, name);
    }

    public ModuleRequiresStmt(Range range, EnumSet<Modifier> modifiers, Name name) {
        super(range);
        setModifiers(modifiers);
        setName(name);
    }

    @Override
    public <R, A> R accept(GenericVisitor<R, A> v, A arg) {
        return v.visit(this, arg);
    }

    @Override
    public <A> void accept(VoidVisitor<A> v, A arg) {
        v.visit(this, arg);
    }

    @Override
    public EnumSet<Modifier> getModifiers() {
        return modifiers;
    }

    @Override
    public ModuleRequiresStmt setModifiers(final EnumSet<Modifier> modifiers) {
        assertNotNull(modifiers);
        if (modifiers == this.modifiers) {
            return (ModuleRequiresStmt) this;
        }
        notifyPropertyChange(ObservableProperty.MODIFIERS, this.modifiers, modifiers);
        this.modifiers = modifiers;
        return this;
    }

    @Override
    public Name getName() {
        return name;
    }

    @Override
    public ModuleRequiresStmt setName(final Name name) {
        assertNotNull(name);
        if (name == this.name) {
            return (ModuleRequiresStmt) this;
        }
        notifyPropertyChange(ObservableProperty.NAME, this.name, name);
        if (this.name != null)
            this.name.setParentNode(null);
        this.name = name;
        setAsParentNodeOf(name);
        return this;
    }

    public boolean isTransitive() {
        return getModifiers().contains(TRANSITIVE);
    }

    public ModuleRequiresStmt setTransitive(boolean set) {
        return setModifier(TRANSITIVE, set);
    }

    @Override
    public boolean remove(Node node) {
        if (node == null)
            return false;
        return super.remove(node);
    }

    @Override
    public ModuleRequiresStmt clone() {
        return (ModuleRequiresStmt) accept(new CloneVisitor(), null);
    }

}
