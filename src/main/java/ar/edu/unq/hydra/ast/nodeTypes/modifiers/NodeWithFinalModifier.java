package ar.edu.unq.hydra.ast.nodeTypes.modifiers;

import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithModifiers;

import static ar.edu.unq.hydra.ast.Modifier.FINAL;

/**
 * A node that can be final.
 */
public interface NodeWithFinalModifier<N extends Node> extends NodeWithModifiers<N> {
    default boolean isFinal() {
        return getModifiers().contains(FINAL);
    }

    @SuppressWarnings("unchecked")
    default N setFinal(boolean set) {
        return setModifier(FINAL, set);
    }
}
