package ar.edu.unq.hydra.ast.nodeTypes;

import ar.edu.unq.hydra.ast.Modifier;
import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.body.FunctionDeclaration;
import ar.edu.unq.hydra.ast.type.VoidType;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

/**
 * A node having functions.
 *
 * The main reason for this interface is to permit users to manipulate homogeneously all nodes with a getFunctions
 * method.
 *
 */
public interface NodeWithFunctions<T> {
    List<FunctionDeclaration> getFunctions();

    T setFunctions(List<FunctionDeclaration> functions);

    /**
     * Adds a functions with void return by default to this
     *
     * @param functionName the function name
     * @param modifiers the modifiers like {@link Modifier#PUBLIC}
     * @return the {@link FunctionDeclaration} created
     */
    default FunctionDeclaration addFunction(String functionName, Modifier... modifiers) {
        FunctionDeclaration functionDeclaration = new FunctionDeclaration();
        functionDeclaration.setName(functionName);
        functionDeclaration.setType(new VoidType());
        functionDeclaration.setModifiers(Arrays.stream(modifiers)
                .collect(toCollection(() -> EnumSet.noneOf(Modifier.class))));
        getFunctions().add(functionDeclaration);
        functionDeclaration.setParentNode((Node) this);
        return functionDeclaration;
    }

    /**
     * Try to find a {@link FunctionDeclaration} by its name
     *
     * @param name the name of the function
     * @return the functions found (multiple in case of polymorphism)
     */
    default List<FunctionDeclaration> getFunctionsByName(String name) {
        return getFunctions().stream()
                .filter(m -> m instanceof FunctionDeclaration && ((FunctionDeclaration) m).getName().equals(name))
                .map(m -> (FunctionDeclaration) m).collect(toList());
    }

    /**
     * Find all functions in the members of this node.
     *
     * @return the functions found. This list is immutable.
     */
//    default List<FunctionDeclaration> getFunctions() {
//        return unmodifiableList(getFunctions().stream()
//                .filter(m -> m instanceof FunctionDeclaration)
//                .map(m -> (FunctionDeclaration) m)
//                .collect(toList()));
//    }

    /**
     * Try to find a {@link FunctionDeclaration} by its parameters types
     *
     * @param paramTypes the types of parameters like "Map&lt;Integer,String&gt;","int" to match<br>
     *            void foo(Map&lt;Integer,String&gt; myMap,int number)
     * @return the functions found (multiple in case of polymorphism)
     */
    default List<FunctionDeclaration> getFunctionsByParameterTypes(String... paramTypes) {
        return getFunctions().stream()
                .filter(m -> m instanceof FunctionDeclaration
                        && ((FunctionDeclaration) m).getParameters().stream().map(p -> p.getType().toString())
                                .collect(toSet()).equals(Stream.of(paramTypes).collect(toSet())))
                .map(m -> (FunctionDeclaration) m).collect(toList());
    }

    /**
     * Try to find a {@link FunctionDeclaration} by its parameters types
     *
     * @param paramTypes the types of parameters like "Map&lt;Integer,String&gt;","int" to match<br>
     *            void foo(Map&lt;Integer,String&gt; myMap,int number)
     * @return the functions found (multiple in case of polymorphism)
     */
    default List<FunctionDeclaration> getFunctionsByParameterTypes(Class<?>... paramTypes) {
        return getFunctions().stream()
                .filter(m -> m instanceof FunctionDeclaration
                        && ((FunctionDeclaration) m).getParameters().stream().map(p -> p.getType().toString())
                                .collect(toSet())
                                .equals(Stream.of(paramTypes).map(Class::getSimpleName).collect(toSet())))
                .map(m -> (FunctionDeclaration) m).collect(toList());
    }

}
