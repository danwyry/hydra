/*
 * Copyright (C) 2007-2010 Júlio Vilmar Gesser.
 * Copyright (C) 2011, 2013-2016 The JavaParser Team.
 *
 * This file is part of JavaParser.
 * 
 * JavaParser can be used either under the terms of
 * a) the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * b) the terms of the Apache License 
 *
 * You should have received a copy of both licenses in LICENCE.LGPL and
 * LICENCE.APACHE. Please refer to those files for details.
 *
 * JavaParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

package ar.edu.unq.hydra.ast.stmt;


import ar.edu.unq.hydra.ast.NodeList;
import ar.edu.unq.hydra.ast.body.FunctionDeclaration;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithFunctions;
import ar.edu.unq.hydra.ast.visitor.GenericVisitor;
import ar.edu.unq.hydra.ast.visitor.VoidVisitor;

import java.util.List;

/**
 * @author Julio Vilmar Gesser
 */
public final class ScriptBlockStmt extends BlockStmt implements  NodeWithFunctions<ScriptBlockStmt> {

    NodeList<FunctionDeclaration> functions ;

    public ScriptBlockStmt()
    {
        super();
        setFunctions(new NodeList<FunctionDeclaration>());
    }


    @Override
    public <R, A> R accept(final GenericVisitor<R, A> v, final A arg) {
        return v.visit(this, arg);
    }

    @Override
    public <A> void accept(final VoidVisitor<A> v, final A arg) {
        v.visit(this, arg);
    }

    @Override
    public NodeList<FunctionDeclaration> getFunctions()
    {
        return functions;
    }

    @Override
    public ScriptBlockStmt setFunctions(List<FunctionDeclaration> functions)
    {
        return setFunctions( NodeList.nodeList(functions) );
    }

    public ScriptBlockStmt setFunctions(NodeList<FunctionDeclaration> functions)
    {
        this.functions = functions;
        return this;
    }





}
