/*
 * Copyright (C) 2007-2010 Júlio Vilmar Gesser.
 * Copyright (C) 2011, 2013-2016 The JavaParser Team.
 *
 * This file is part of JavaParser.
 *
 * JavaParser can be used either under the terms of
 * a) the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * b) the terms of the Apache License
 *
 * You should have received a copy of both licenses in LICENCE.LGPL and
 * LICENCE.APACHE. Please refer to those files for details.
 *
 * JavaParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */
package ar.edu.unq.hydra.ast.body;

import ar.edu.unq.hydra.ast.Range;
import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.NodeList;
import ar.edu.unq.hydra.ast.expr.AnnotationExpr;
import ar.edu.unq.hydra.ast.expr.Expression;
import ar.edu.unq.hydra.ast.expr.SimpleName;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithArguments;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithJavadoc;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithSimpleName;
import ar.edu.unq.hydra.ast.observer.ObservableProperty;
import ar.edu.unq.hydra.ast.visitor.CloneVisitor;
import ar.edu.unq.hydra.ast.visitor.GenericVisitor;
import ar.edu.unq.hydra.ast.visitor.VoidVisitor;

import java.util.Arrays;
import java.util.List;

import static com.github.javaparser.utils.Utils.assertNotNull;

/**
 * One of the values an enum can take. A(1) and B(2) in this example: <code>enum X { A(1), B(2) }</code>
 *
 * @author Julio Vilmar Gesser
 */
public final class EnumConstantDeclaration extends BodyDeclaration<EnumConstantDeclaration> implements NodeWithJavadoc<EnumConstantDeclaration>, NodeWithSimpleName<EnumConstantDeclaration>, NodeWithArguments<EnumConstantDeclaration> {

    private SimpleName name;

    private NodeList<Expression> arguments;

    private NodeList<BodyDeclaration<?>> classBody;

    public EnumConstantDeclaration() {
        this(null, new NodeList<>(), new SimpleName(), new NodeList<>(), new NodeList<>());
    }

    public EnumConstantDeclaration(String name) {
        this(null, new NodeList<>(), new SimpleName(name), new NodeList<>(), new NodeList<>());
    }


    public EnumConstantDeclaration(NodeList<AnnotationExpr> annotations, SimpleName name, NodeList<Expression> arguments, NodeList<BodyDeclaration<?>> classBody) {
        this(null, annotations, name, arguments, classBody);
    }

    public EnumConstantDeclaration(Range range, NodeList<AnnotationExpr> annotations, SimpleName name, NodeList<Expression> arguments, NodeList<BodyDeclaration<?>> classBody) {
        super(range, annotations);
        setName(name);
        setArguments(arguments);
        setClassBody(classBody);
    }

    @Override
    public <R, A> R accept(GenericVisitor<R, A> v, A arg) {
        return v.visit(this, arg);
    }

    @Override
    public <A> void accept(VoidVisitor<A> v, A arg) {
        v.visit(this, arg);
    }

    public NodeList<Expression> getArguments() {
        return arguments;
    }

    public NodeList<BodyDeclaration<?>> getClassBody() {
        return classBody;
    }

    @Override
    public SimpleName getName() {
        return name;
    }

    public EnumConstantDeclaration setArguments(final NodeList<Expression> arguments) {
        assertNotNull(arguments);
        if (arguments == this.arguments) {
            return (EnumConstantDeclaration) this;
        }
        notifyPropertyChange(ObservableProperty.ARGUMENTS, this.arguments, arguments);
        if (this.arguments != null)
            this.arguments.setParentNode(null);
        this.arguments = arguments;
        setAsParentNodeOf(arguments);
        return this;
    }

    public EnumConstantDeclaration setClassBody(final NodeList<BodyDeclaration<?>> classBody) {
        assertNotNull(classBody);
        if (classBody == this.classBody) {
            return (EnumConstantDeclaration) this;
        }
        notifyPropertyChange(ObservableProperty.CLASS_BODY, this.classBody, classBody);
        if (this.classBody != null)
            this.classBody.setParentNode(null);
        this.classBody = classBody;
        setAsParentNodeOf(classBody);
        return this;
    }

    @Override
    public EnumConstantDeclaration setName(final SimpleName name) {
        assertNotNull(name);
        if (name == this.name) {
            return (EnumConstantDeclaration) this;
        }
        notifyPropertyChange(ObservableProperty.NAME, this.name, name);
        if (this.name != null)
            this.name.setParentNode(null);
        this.name = name;
        setAsParentNodeOf(name);
        return this;
    }

    @Override
    public List<NodeList<?>> getNodeLists() {
        return Arrays.asList(getArguments(), getClassBody(), getAnnotations());
    }

    @Override
    public boolean remove(Node node) {
        if (node == null)
            return false;
        for (int i = 0; i < arguments.size(); i++) {
            if (arguments.get(i) == node) {
                arguments.remove(i);
                return true;
            }
        }
        for (int i = 0; i < classBody.size(); i++) {
            if (classBody.get(i) == node) {
                classBody.remove(i);
                return true;
            }
        }
        return super.remove(node);
    }

    @Override
    public EnumConstantDeclaration clone() {
        return (EnumConstantDeclaration) accept(new CloneVisitor(), null);
    }

}
