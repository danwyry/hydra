/*
 * Copyright (C) 2007-2010 Júlio Vilmar Gesser.
 * Copyright (C) 2011, 2013-2016 The JavaParser Team.
 *
 * This file is part of JavaParser.
 *
 * JavaParser can be used either under the terms of
 * a) the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * b) the terms of the Apache License
 *
 * You should have received a copy of both licenses in LICENCE.LGPL and
 * LICENCE.APACHE. Please refer to those files for details.
 *
 * JavaParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */
package ar.edu.unq.hydra.ast.expr;

import ar.edu.unq.hydra.ast.Range;
import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.NodeList;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithAnnotations;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithIdentifier;
import ar.edu.unq.hydra.ast.observer.ObservableProperty;
import ar.edu.unq.hydra.ast.visitor.CloneVisitor;
import ar.edu.unq.hydra.ast.visitor.GenericVisitor;
import ar.edu.unq.hydra.ast.visitor.VoidVisitor;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.github.javaparser.utils.Utils.assertNonEmpty;
import static com.github.javaparser.utils.Utils.assertNotNull;

/**
 * A name that may consist of multiple identifiers.
 * In other words: it.may.contain.dots.
 * <p>
 * The rightmost identifier is "identifier",
 * The one to the left of it is "qualifier.identifier", etc.
 * <p>
 * You can construct one from a String with the name(...) method.
 *
 * @author Julio Vilmar Gesser
 * @see SimpleName
 */
public class Name extends Node implements NodeWithIdentifier<Name>, NodeWithAnnotations<Name> {

    private String identifier;

    private Name qualifier;

    private NodeList<AnnotationExpr> annotations;

    public Name() {
        this(null, null, "empty", new NodeList<>());
    }

    public Name(final String identifier) {
        this(null, null, identifier, new NodeList<>());
    }

    public Name(Name qualifier, final String identifier) {
        this(null, qualifier, identifier, new NodeList<>());
    }


    public Name(Name qualifier, final String identifier, NodeList<AnnotationExpr> annotations) {
        this(null, qualifier, identifier, annotations);
    }

    public Name(Range range, Name qualifier, final String identifier, NodeList<AnnotationExpr> annotations) {
        super(range);
        setIdentifier(identifier);
        setQualifier(qualifier);
        setAnnotations(annotations);
    }

    @Override
    public <R, A> R accept(final GenericVisitor<R, A> v, final A arg) {
        return v.visit(this, arg);
    }

    @Override
    public <A> void accept(final VoidVisitor<A> v, final A arg) {
        v.visit(this, arg);
    }

    @Override
    public final String getIdentifier() {
        return identifier;
    }

    @Override
    public Name setIdentifier(final String identifier) {
        assertNonEmpty(identifier);
        if (identifier == this.identifier) {
            return (Name) this;
        }
        notifyPropertyChange(ObservableProperty.IDENTIFIER, this.identifier, identifier);
        this.identifier = identifier;
        return this;
    }

    /**
     * Creates a new {@link Name} from a qualified name.<br>
     * The qualified name can contains "." (dot) characters.
     *
     * @param qualifiedName qualified name
     * @return instanceof {@link Name}
     */
    @Deprecated
    public static Name parse(String qualifiedName)
    {
        assertNonEmpty(qualifiedName);
        String[] split = qualifiedName.split("\\.");
        Name ret = new Name(split[0]);
        for (int i = 1; i < split.length; i++)
        {
            ret = new Name(ret, split[i]);
        }
        return ret;
    }

    /**
     * @return the complete qualified name. Only the identifiers and the dots, so no comments or whitespace.
     */
    public String asString() {
        if (qualifier != null) {
            return qualifier.asString() + "." + identifier;
        }
        return identifier;
    }

    public Optional<Name> getQualifier() {
        return Optional.ofNullable(qualifier);
    }

    public Name setQualifier(final Name qualifier) {
        if (qualifier == this.qualifier) {
            return (Name) this;
        }
        notifyPropertyChange(ObservableProperty.QUALIFIER, this.qualifier, qualifier);
        if (this.qualifier != null)
            this.qualifier.setParentNode(null);
        this.qualifier = qualifier;
        setAsParentNodeOf(qualifier);
        return this;
    }

    @Override
    public boolean remove(Node node) {
        if (node == null)
            return false;
        for (int i = 0; i < annotations.size(); i++) {
            if (annotations.get(i) == node) {
                annotations.remove(i);
                return true;
            }
        }
        if (qualifier != null) {
            if (node == qualifier) {
                removeQualifier();
                return true;
            }
        }
        return super.remove(node);
    }

    public Name removeQualifier() {
        return setQualifier((Name) null);
    }

    @Override
    public NodeList<AnnotationExpr> getAnnotations() {
        return annotations;
    }

    @Override
    public Name setAnnotations(final NodeList<AnnotationExpr> annotations) {
        assertNotNull(annotations);
        if (annotations == this.annotations) {
            return (Name) this;
        }
        notifyPropertyChange(ObservableProperty.ANNOTATIONS, this.annotations, annotations);
        if (this.annotations != null)
            this.annotations.setParentNode(null);
        this.annotations = annotations;
        setAsParentNodeOf(annotations);
        return this;
    }

    @Override
    public List<NodeList<?>> getNodeLists() {
        return Arrays.asList(getAnnotations());
    }

    @Override
    public Name clone() {
        return (Name) accept(new CloneVisitor(), null);
    }

    public Name getLeftest()
    {
        Name current = this;

        while (current.getQualifier().isPresent())
            current = current.getQualifier().get();

        return current;
    }
}
