/*
 * Copyright (C) 2007-2010 Júlio Vilmar Gesser.
 * Copyright (C) 2011, 2013-2016 The JavaParser Team.
 *
 * This file is part of JavaParser.
 *
 * JavaParser can be used either under the terms of
 * a) the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * b) the terms of the Apache License
 *
 * You should have received a copy of both licenses in LICENCE.LGPL and
 * LICENCE.APACHE. Please refer to those files for details.
 *
 * JavaParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */
package ar.edu.unq.hydra.ast.stmt;

import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.NodeList;
import ar.edu.unq.hydra.ast.body.Parameter;
import ar.edu.unq.hydra.ast.expr.SimpleName;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithOptionalLabel;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithParameters;
import ar.edu.unq.hydra.ast.observer.ObservableProperty;
import ar.edu.unq.hydra.ast.visitor.CloneVisitor;
import ar.edu.unq.hydra.ast.visitor.GenericVisitor;
import ar.edu.unq.hydra.ast.visitor.VoidVisitor;
import ar.edu.unq.hydra.ast.Range;

import java.util.Optional;

import static com.github.javaparser.utils.Utils.assertNotNull;

/**
 * A statement that is labeled, like <code>label123: println("continuing");</code>
 *
 * @author Julio Vilmar Gesser
 */
public final class ProcessCreationStmt extends Statement implements NodeWithOptionalLabel, NodeWithParameters<ProcessCreationStmt>
{
    private SimpleName name;

    private Statement statement;
    private NodeList<Parameter> parameters;

    public ProcessCreationStmt() {
        this(null, new SimpleName(), new EmptyStmt());
    }

    public ProcessCreationStmt(final String name, final Statement statement) {
        this(null, new SimpleName(name), statement);
    }

    public ProcessCreationStmt(final SimpleName name, final Statement statement) {
        this(null, name, statement);
    }

    public ProcessCreationStmt(Range range, final SimpleName name, final Statement statement)
    {
        this(range, name, statement, null);
    }

    public ProcessCreationStmt(final SimpleName name, final Statement stmt, final NodeList<Parameter> parameters)
    {
        this(null, name, stmt, parameters);
    }

    public ProcessCreationStmt(Range range, final SimpleName name, final Statement statement, final NodeList<Parameter> parameters)
    {
        super(range);
        setName(name);
        setStatement(statement);
        setParameters(parameters);
    }

    @Override
    public <R, A> R accept(final GenericVisitor<R, A> v, final A arg) {
        return v.visit(this, arg);
    }

    @Override
    public <A> void accept(final VoidVisitor<A> v, final A arg) {
        v.visit(this, arg);
    }

    public Statement getStatement() {
        return statement;
    }

    public ProcessCreationStmt setStatement(final Statement statement) {
        assertNotNull(statement);
        if (statement == this.statement) {
            return (ProcessCreationStmt) this;
        }
        notifyPropertyChange(ObservableProperty.STATEMENT, this.statement, statement);
        if (this.statement != null)
            this.statement.setParentNode(null);
        this.statement = statement;
        setAsParentNodeOf(statement);
        return this;
    }

    public final SimpleName getName() {
        return name;
    }

    public ProcessCreationStmt setName(final SimpleName name)
    {
//        assertNotNull(name);
        if (name == this.name) {
            return (ProcessCreationStmt) this;
        }
        notifyPropertyChange(ObservableProperty.LABEL, this.name, name);
        if (this.name != null)
            this.name.setParentNode(null);
        this.name = name;
        setAsParentNodeOf(name);
        return this;
    }

    @Override
    public boolean remove(Node node) {
        if (node == null)
            return false;
        return super.remove(node);
    }

    @Override
    public ProcessCreationStmt clone() {
        return (ProcessCreationStmt) accept(new CloneVisitor(), null);
    }

    @Override
    public Optional<SimpleName> getLabel() {
        return Optional.ofNullable(getName());
    }

    @Override
    public ProcessCreationStmt setLabel(SimpleName label)
    {
        return this.setName(label);
    }

    @Override
    public NodeList<Parameter> getParameters()
    {
        return parameters;
    }

    @Override
    public ProcessCreationStmt setParameters(NodeList<Parameter> parameters)
    {
        this.parameters = parameters;
        return this;
    }
}
