package ar.edu.unq.hydra.ast.nodeTypes.modifiers;

import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithModifiers;

import static ar.edu.unq.hydra.ast.Modifier.PROTECTED;

/**
 * A node that can be protected.
 */
public interface NodeWithProtectedModifier<N extends Node> extends NodeWithModifiers<N> {
    default boolean isProtected() {
        return getModifiers().contains(PROTECTED);
    }

    @SuppressWarnings("unchecked")
    default N setProtected(boolean set) {
        return setModifier(PROTECTED, set);
    }
}
