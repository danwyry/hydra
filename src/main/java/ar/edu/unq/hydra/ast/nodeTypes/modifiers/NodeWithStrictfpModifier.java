package ar.edu.unq.hydra.ast.nodeTypes.modifiers;

import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithModifiers;

import static ar.edu.unq.hydra.ast.Modifier.STRICTFP;

/**
 * A node that can be strictfp.
 */
public interface NodeWithStrictfpModifier<N extends Node> extends NodeWithModifiers<N> {
    default boolean isStrictfp() {
        return getModifiers().contains(STRICTFP);
    }

    @SuppressWarnings("unchecked")
    default N setStrictfp(boolean set) {
        return setModifier(STRICTFP, set);
    }
}
