package ar.edu.unq.hydra.ast.nodeTypes.modifiers;

import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.nodeTypes.NodeWithModifiers;

import static ar.edu.unq.hydra.ast.Modifier.PUBLIC;

/**
 * A node that can be public.
 */
public interface NodeWithPublicModifier<N extends Node> extends NodeWithModifiers<N> {
    default boolean isPublic() {
        return getModifiers().contains(PUBLIC);
    }

    @SuppressWarnings("unchecked")
    default N setPublic(boolean set) {
        return setModifier(PUBLIC, set);
    }

}
