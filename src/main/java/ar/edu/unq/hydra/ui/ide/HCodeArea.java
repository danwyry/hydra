package ar.edu.unq.hydra.ui.ide;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;

/**
 * Created by daniel on 06/11/17.
 */
public class HCodeArea extends RSyntaxTextArea
{
    public HCodeArea()
    {
        super();
        setColumns(60);
        setRows(20);
        setAutoIndentEnabled(true);
        setAntiAliasingEnabled(true);
        setAnimateBracketMatching(true);
        setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
        setCodeFoldingEnabled(true);
    }
}
