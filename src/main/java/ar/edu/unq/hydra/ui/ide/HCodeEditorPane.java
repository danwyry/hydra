package ar.edu.unq.hydra.ui.ide;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rtextarea.RTextScrollPane;

/**
 * Created by daniel on 06/11/17.
 */
public class HCodeEditorPane extends RTextScrollPane
{
    public HCodeEditorPane()
    {
        super(buildCodeArea());
    }

    public RSyntaxTextArea getTextArea()
    {
        return (RSyntaxTextArea) super.getTextArea();
    }

    private static HCodeArea buildCodeArea()
    {
        return new HCodeArea();
    }
}
