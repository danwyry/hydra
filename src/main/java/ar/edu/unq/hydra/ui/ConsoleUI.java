package ar.edu.unq.hydra.ui;

import ar.edu.unq.hydra.interpreter.Interpreter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static java.lang.System.exit;

/**
 * Created by Daniel Wyrytowski on 5/17/17.
 */
public class ConsoleUI
{
    public static void main(String path)
    {
//        try {
//            Runtime.getRuntime().addShutdownHook(new Thread(()->System.out.println("shutting down")));
//        } catch (Throwable t) {
//        }

        try {
            String code = getSource(path);
            interpret(code);

        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private static void interpret(String code)
    {
        Interpreter interpreter = new Interpreter();
        interpreter.interpret(code);
    }

    private static String getSource(String path) throws IOException
    {
        FileReader reader = new FileReader(path);
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringBuilder builder = new StringBuilder();
        String line;
        while (null != (line = bufferedReader.readLine()))
            builder.append(line + "\n");
        reader.close();
        return builder.toString();
    }
}
