package ar.edu.unq.hydra.ui.ide;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.nonNull;

/**
 * Created by daniel on 06/11/17.
 */
class HMenuBar extends JMenuBar
{
    Map<String, AbstractButton> items = new HashMap<>();

    public AbstractButton addItem(String path)
    {
        return addItem(path,null,null);
    }

    public AbstractButton getItem(String path)
    {
        return items.get(path);
    }

    public AbstractButton addItem(String path, ActionListener actionListener, ItemListener itemListener)
    {
        String[] parts = path.split("\\.");
        AbstractButton item = null;

        if (parts.length == 1)
        {
            item = addButton(path, actionListener, itemListener);
        } else if (parts.length > 1) {
            item = createItemInPath(parts, actionListener, itemListener);
        }

        items.put(path, item);
        return item;
    }

    private JMenuItem createItemInPath(String[] parts, ActionListener actionListener, ItemListener itemListener)
    {
        JMenu menu = createMenuPath(parts);

        String name = parts[parts.length - 1];
        JMenuItem menuItem = new JMenuItem(name);
        menuItem.setName(name);

        if (nonNull(actionListener))
            menuItem.addActionListener(actionListener);

        if (nonNull(itemListener))
            menuItem.addItemListener(itemListener);

        menu.add(menuItem);

        return menuItem;
    }

    private JMenu createMenuPath(String[] parts)
    {
        String name = parts[0] ;
        JMenu last ;
        if (existsMenu(name)) {
            last = getMenu(name);
        } else {
            last = newMenu(name);
            add(last);
        }

        for (int i = 1; i < parts.length-2; i++)
        {
            name = parts[i] ;
            if (existsSubMenu(last, name)) {
               last = getSubMenu(last, name);
            }else {
                JMenu item = newMenu(name);
                last.add(item);
                last = item ;
            }
        }
        return last;
    }

    private JMenu newMenu(String name) {
        JMenu last;
        last = new JMenu(name);
        last.setName(name);
        return last;
    }

    private boolean existsSubMenu(JMenu menu, String name)
    {
        return nonNull(getSubMenu(menu,name));
    }

    private JMenu getSubMenu(JMenu menu, String name)
    {
        for (Component c : menu.getComponents())
            if (c.getName().equals(name))
                return (JMenu)c;
        return null;
    }

    private JMenu getMenu(String name)
    {
        for(Component c : getComponents())
            if (c.getName().equals(name))
                return (JMenu)c ;
        return null;
    }

    private boolean existsMenu(String name)
    {
        return nonNull(getMenu(name));
    }

    private JButton addButton(String name, ActionListener actionListener, ItemListener itemListener)
    {
        JButton button = new JButton(name);
        if (nonNull(actionListener))
            button.addActionListener(actionListener);
        if (nonNull(itemListener))
            button.addItemListener(itemListener);
        add(button);
        return button;
    }
}
