package ar.edu.unq.hydra.ui;


import ar.edu.unq.hydra.ui.ide.HydraIDE;
import org.apache.commons.cli.*;

import java.io.IOException;

import static java.lang.System.exit;

public class HLaucher
{
    public static void main(String[] args) throws ParseException
    {

        if (args.length != 1) {
            usage();
            exit(-1);
        }

        Options options = new Options();
        options.addOption("gui", false, "graphical user interface");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);

        if (cmd.hasOption("gui"))
        {
            HydraIDE.main();
        } else {
            String path = args[0];
            ConsoleUI.main(path);
        }

//        try {
//            Runtime.getRuntime().addShutdownHook(new Thread(()->System.out.println("shutting down")));
//        } catch (Throwable t) {
//        }

    }

    private static void usage()
    {
        System.out.println("Usage of hydra: hydra intputFile.hyd or hydra -gui");
    }
}
