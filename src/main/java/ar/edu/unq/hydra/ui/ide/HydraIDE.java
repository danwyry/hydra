package ar.edu.unq.hydra.ui.ide;

import javax.swing.*;

import ar.edu.unq.hydra.interpreter.HydraExceptionListener;
import ar.edu.unq.hydra.interpreter.Interpreter;
import ar.edu.unq.hydra.parser.exceptions.HydraAstBuilderException;
import org.fife.ui.rsyntaxtextarea.*;

import java.util.concurrent.Semaphore;

/**
 * Created by Daniel Wyrytowski on 10/31/17.
 */
public class HydraIDE extends JFrame
        implements HydraExceptionListener
{
    private HMenuBar menubar;
    private RSyntaxTextArea codeArea;
    private JConsole console;
    private JTabbedPane toolsWindowPane;

    public static void main()
    {
        // Start all Swing applications on the EDT.
        SwingUtilities.invokeLater(() -> {
            HydraIDE ide = new HydraIDE();
            ide.setVisible(true);
            ide.run();
        });
    }

    public HydraIDE()
    {
        initGUIComponents();
    }

    private void initGUIComponents()
    {
        initMenuBar();
        initContentPane();

        setTitle("Hydra IDE");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
    }

    private void initMenuBar()
    {
        menubar = new HMenuBar();
//        menubar.addItem("File.Open");
//        menubar.addItem("File.Save");
        menubar.addItem("Run");
        menubar.addItem("Stop").setVisible(false);

        setJMenuBar(menubar);
    }

    private void initContentPane()
    {
        HCodeEditorPane codeEditorPane = new HCodeEditorPane();
        codeArea = codeEditorPane.getTextArea();
        toolsWindowPane = buildToolsWindowPane();

        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, codeEditorPane, toolsWindowPane);
        splitPane.setOneTouchExpandable(true);
        setContentPane(splitPane);
    }

    private JTabbedPane buildToolsWindowPane()
    {
        JTabbedPane jTabbedPane = new JTabbedPane();
        JScrollPane consolePane = buildConsolePane();
        jTabbedPane.addTab("Output", consolePane);

        return jTabbedPane;

    }

    private JScrollPane buildConsolePane()
    {
        console = new JConsole();
        return new JScrollPane(console);
    }

    private void run()
    {
        AbstractButton runButton = menubar.getItem("Run");
        AbstractButton stopButton = menubar.getItem("Stop");

        final Semaphore running = new Semaphore(1);
        final Semaphore working = new Semaphore(0);
        final Interpreter interpreter = new Interpreter(console.getIn(), console.getOut(), console.getErr());
        interpreter.addExceptionListener(this);

        final Thread dispatcher = new Thread()
        {
            private volatile Thread worker;
            @Override
            public void run()
            {
                while (true)
                {
                    running.acquireUninterruptibly();
                    if (worker != null) {
                        interpreter.interrupt();
                    }
                    working.acquireUninterruptibly();
                    worker = new Thread(() -> {
                        toolsWindowPane.setSelectedIndex( toolsWindowPane.indexOfTab("Output") );
                        try {
                            interpreter.interpret(codeArea.getText());
                        } catch (HydraAstBuilderException e)
                        {
                            console.error("Parsing error: " + e.getMessage());
                        } finally {
                            running.release();
                            runButton.setVisible(true);
                            stopButton.setVisible(false);
                        }
                    });
                    worker.start();
                }
            }
        };
        dispatcher.start();

        runButton.addActionListener(e -> {
            console.clear();
            working.release();
            stopButton.setVisible(true);
            runButton.setVisible(false);
        });

        stopButton.addActionListener(e -> {
            interpreter.interrupt();
            running.release();
            runButton.setVisible(true);
            stopButton.setVisible(false);
        });
    }

    @Override
    public void onException(Throwable t)
    {
        console.error(t.getMessage() + "\n");
    }
}