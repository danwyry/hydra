package ar.edu.unq.hydra.parser.expr;

import ar.edu.unq.hydra.ast.expr.AmbiguousNameExpr;
import ar.edu.unq.hydra.ast.expr.FieldAccessExpr;
import ar.edu.unq.hydra.ast.expr.NameExpr;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static org.junit.Assert.assertEquals;


/**
 * Beacuse of the way nameExpressions are defined in the grammar :
 *   expressionName:	Identifier | ambiguousName '.' Identifier
 */

public class NameExpressionTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedExpressionName_Should_BeANameExpr()
    {
        String declaration = "a";
        Visitable node =  astNodeFor(declaration, parser -> parser.expressionName());
        assertObjectInstanceOf(node, AmbiguousNameExpr.class);
    }

    @Test
    public void testParsedExpressionName_Should_HaveACorrectName()
    {
        String declaration = "a";
        AmbiguousNameExpr node =  astNodeFor(declaration, parser -> parser.expressionName());
        assertEquals(declaration, node.getNameAsString());
    }

    @Test
    public void testParsedExpressionNameWithAmbiguousName_Should_BeAFieldAccessExpr()
    {
        String declaration = "a.b;";
        Visitable node =  astNodeFor(declaration, parser -> parser.expressionName());
        assertObjectInstanceOf(node, AmbiguousNameExpr.class);
    }

    @Test
    public void testParsedExpressionNameWithAmbiguousName_Should_HaveACorrectName()
    {
        String declaration = "a.b;";
        AmbiguousNameExpr node =  astNodeFor(declaration, parser -> parser.expressionName());
        assertEquals(node.getName().getIdentifier(), "b");
    }
//
//    @Test
//    public void testParsedExpressionNameWithAmbiguousName_Scope_Should_BeANameExpr()
//    {
//        String declaration = "a.b;";
//        AmbiguousNameExpr node =  astNodeFor(declaration, parser -> parser.expressionName());
//        assertObjectInstanceOf(node.getScope().get(),NameExpr.class);
//    }
//
//    @Test
//    public void testParsedExpressionNameWithAmbiguousName_Scope_Should_HaveACorrectName()
//    {
//        String declaration = "a.b;";
//        FieldAccessExpr node =  astNodeFor(declaration, parser -> parser.expressionName());
//
//        NameExpr ambiguousName = (NameExpr) node.getScope().get();
//        assertEquals(ambiguousName.getNameAsString(), "a");
//    }
//
//    @Test
//    public void testParsedExpressionNameWithAmbiguousName_Should_HaveCorrectFieldAccessExprCount()
//    {
//        String declaration = "a.b.c.d.e;";
//        FieldAccessExpr node =  astNodeFor(declaration, parser -> parser.expressionName());
//
//        Integer expected  = 4;
//        assertEquals(expected, getFieldAccessExprScopeLevel(node));
//    }
//
//
//    private Integer getFieldAccessExprScopeLevel(FieldAccessExpr node)
//    {
//        Integer scopes = 1 ;
//        while (node.getScope().get() instanceof FieldAccessExpr )
//        {
//            scopes++;
//            node = (FieldAccessExpr) node.getScope().get();
//        }
//        return scopes;
//    }

}
