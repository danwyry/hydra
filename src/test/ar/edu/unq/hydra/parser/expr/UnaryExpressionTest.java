package ar.edu.unq.hydra.parser.expr;

import ar.edu.unq.hydra.ast.expr.AmbiguousNameExpr;
import ar.edu.unq.hydra.ast.expr.Expression;
import ar.edu.unq.hydra.ast.expr.NameExpr;
import ar.edu.unq.hydra.ast.expr.UnaryExpr;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectsClassExtends;
import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static org.junit.Assert.assertEquals;


public class UnaryExpressionTest extends HydraAstBuilderTest
{
    // Test RESULTING NODE TYPES
    @Test
    public void testPreIncrementExpressionResult_Should_BeOfTypeUnaryExpr() throws Exception
    {
        Expression node = astNodeFor("++i", parser -> parser.preIncrementExpression());

        assertObjectInstanceOf(node, UnaryExpr.class);
    }

    @Test
    public void testPreDecrementExpressionResult_Should_BeOfTypeUnaryExpr() throws Exception
    {
        Expression node = astNodeFor("--i", parser -> parser.preDecrementExpression());

        assertObjectInstanceOf(node, UnaryExpr.class);
    }

    @Test
    public void testPostIncrementExpressionResult_Should_BeOfTypeUnaryExpr() throws Exception
    {
        Expression node = astNodeFor("i++", parser -> parser.postIncrementExpression());

        assertObjectInstanceOf(node, UnaryExpr.class);
    }

    @Test
    public void testPostDecrementExpressionResult_Should_BeOfTypeUnaryExpr() throws Exception
    {
        Expression node = astNodeFor("i--", parser -> parser.postDecrementExpression());

        assertObjectInstanceOf(node, UnaryExpr.class);
    }

    @Test
    public void testPostfixExpressionResult_Should_BeOfTypeUnaryExpr() throws Exception
    {
        Expression node = astNodeFor("i++", parser -> parser.postfixExpression());

        assertObjectInstanceOf(node, UnaryExpr.class);
    }

    @Test
    public void testPositiveResult_Should_BeOfTypeUnaryExpr() throws Exception
    {
        Expression node = astNodeFor("+i", parser -> parser.unaryExpression());

        assertObjectInstanceOf(node, UnaryExpr.class);
    }

    @Test
    public void testNegativeResult_Should_BeOfTypeUnaryExpr() throws Exception
    {
        Expression node = astNodeFor("-i", parser -> parser.unaryExpression());

        assertObjectInstanceOf(node, UnaryExpr.class);
    }

    @Test
    public void testInverseResult_Should_BeOfTypeUnaryExpr() throws Exception
    {
        Expression node = astNodeFor("~i", parser -> parser.unaryExpressionNotPlusMinus());

        assertObjectInstanceOf(node, UnaryExpr.class);
    }

    @Test
    public void testNotResult_Should_BeOfTypeUnaryExpr() throws Exception
    {
        Expression node = astNodeFor("!i", parser -> parser.unaryExpressionNotPlusMinus());

        assertObjectInstanceOf(node, UnaryExpr.class);
    }

    // Test RESULTING NODE CONTENTS
    // NOTA: en el string del código testeado en las siguientes expresiones se agrega un punto y coma al final de la
    // expresión para evitar un bug conocido del antlr que se da solo al parsear por una regla específica que espera un
    // EOF al final de la expresión o el siguiente token. El punto y coma funciona como siguiente token y permite el
    // correcto parseo de la expresión. Es un hack feo pero sin el era dificil hacer un testing unitario regla a regla.
    // Debido a este hack, puede salir un warning del con la siguiente forma al ejecutar los tests:
    //          line 1:3 extraneous input ';' expecting {<EOF>, '++', '--'}
    //          line 1:3 missing '++' at '<EOF>'


    @Test
    public void testPreIncrementExpression_Should_HoldCorrectValues() throws Exception
    {
        UnaryExpr exprNode = astNodeFor("++i;", parser -> parser.preIncrementExpression());

        assertObjectsClassExtends(exprNode.getExpression(), Expression.class);
        assertEquals(exprNode.getOperator(), UnaryExpr.Operator.PREFIX_INCREMENT);
    }

    @Test
    public void testPostIncrementExpression_Should_HoldCorrectValues() throws Exception
    {
        UnaryExpr node = astNodeFor("i++;", parser -> parser.postIncrementExpression());

        assertObjectsClassExtends(node.getExpression(), AmbiguousNameExpr.class);
        assertEquals(node.getOperator(), UnaryExpr.Operator.POSTFIX_INCREMENT);
    }

    @Test
    public void testPreDecrementExpression_Should_HoldCorrectValues() throws Exception
    {
        UnaryExpr exprNode = astNodeFor("--i;", parser -> parser.preDecrementExpression());

        assertObjectsClassExtends(exprNode.getExpression(), AmbiguousNameExpr.class);
        assertEquals(exprNode.getOperator(), UnaryExpr.Operator.PREFIX_DECREMENT);
    }

    @Test
    public void testPostDecrementExpression_Should_HoldCorrectValues() throws Exception
    {
        UnaryExpr exprNode = astNodeFor("i--;", parser -> parser.postDecrementExpression());

        assertObjectsClassExtends(exprNode.getExpression(), AmbiguousNameExpr.class);
        assertEquals(exprNode.getOperator(), UnaryExpr.Operator.POSTFIX_DECREMENT);
    }

    @Test
    public void testPostfixExpression_Should_HoldCorrectValues() throws Exception
    {
        UnaryExpr exprNode = astNodeFor("i--;", parser -> parser.postfixExpression());

        assertObjectsClassExtends(exprNode.getExpression(), AmbiguousNameExpr.class);
        assertEquals(exprNode.getOperator(), UnaryExpr.Operator.POSTFIX_DECREMENT);
    }

    @Test
    public void testPositiveExpression_Should_HoldCorrectValues() throws Exception
    {
        UnaryExpr exprNode = astNodeFor("+i", parser -> parser.unaryExpression());

        assertObjectsClassExtends(exprNode.getExpression(), AmbiguousNameExpr.class);
        assertEquals(exprNode.getOperator(), UnaryExpr.Operator.PLUS);
    }

    @Test
    public void testNegativeExpression_Should_HoldCorrectValues() throws Exception
    {
        UnaryExpr exprNode = astNodeFor("-i", parser -> parser.unaryExpression());

        assertObjectsClassExtends(exprNode.getExpression(), AmbiguousNameExpr.class);
        assertEquals(exprNode.getOperator(), UnaryExpr.Operator.MINUS);
    }

    @Test
    public void testInverseExpression_Should_HoldCorrectValues() throws Exception
    {
        UnaryExpr exprNode = astNodeFor("~i", parser -> parser.unaryExpressionNotPlusMinus());

        assertObjectsClassExtends(exprNode.getExpression(), AmbiguousNameExpr.class);
        assertEquals(exprNode.getOperator(), UnaryExpr.Operator.BITWISE_COMPLEMENT);
    }

    @Test
    public void testNotExpression_Should_HoldCorrectValues() throws Exception
    {
        UnaryExpr exprNode = astNodeFor("!i", parser -> parser.unaryExpressionNotPlusMinus());

        assertObjectInstanceOf(exprNode.getExpression(), AmbiguousNameExpr.class);
        assertEquals(exprNode.getOperator(), UnaryExpr.Operator.LOGICAL_COMPLEMENT);
    }


}
