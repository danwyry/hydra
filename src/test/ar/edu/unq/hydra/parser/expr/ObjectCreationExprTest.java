package ar.edu.unq.hydra.parser.expr;

import ar.edu.unq.hydra.ast.expr.ObjectCreationExpr;
import ar.edu.unq.hydra.ast.type.ClassOrInterfaceType;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static org.junit.Assert.assertEquals;


/**
 * Beacuse of the way nameExpressions are defined in the grammar :
 *   expressionName:	Identifier | ambiguousName '.' Identifier
 */

public class ObjectCreationExprTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedClassInstanceCreationExpression_lfno_primary_Should_BeAnObjectCreationExpr()
    {
        String declaration = "new Tested()";
        Visitable node =  astNodeFor(declaration, parser -> parser.classInstanceCreationExpression_lfno_primary());
        assertObjectInstanceOf(node, ObjectCreationExpr.class);
    }

    @Test
    public void testParsedUnqualifiedClassInstanceCreationExpression_Should_BeAnObjectCreationExpr()
    {
        String declaration = "new Tested()";
        Visitable node =  astNodeFor(declaration, parser -> parser.classInstanceCreationExpression_lfno_primary());
        assertObjectInstanceOf(node, ObjectCreationExpr.class);
    }

    @Test
    public void testParsedUnqualifiedClassInstanceCreationExpression_Type_Should_BeAClassOrInterfaceType()
    {
        String declaration = "new Tested()";
        ObjectCreationExpr node =  astNodeFor(declaration, parser -> parser.classInstanceCreationExpression_lfno_primary());
        assertObjectInstanceOf(node.getType(), ClassOrInterfaceType.class);
    }

    @Test
    public void testParsedUnqualifiedClassInstanceCreationExpression_Type_Should_HaveCorrectName()
    {
        String declaration = "new Tested()";
        ObjectCreationExpr node =  astNodeFor(declaration, parser -> parser.classInstanceCreationExpression_lfno_primary());
        assertEquals("Tested", node.getType().getNameAsString());
    }

    @Test
    public void testParsedUnqualifiedClassInstanceCreationExpression_Type_Should_HaveWithScopeAsItsScope()
    {
        String declaration = "new WithScope.Tested()";
        ObjectCreationExpr node =  astNodeFor(declaration, parser -> parser.classInstanceCreationExpression_lfno_primary());

        assertEquals("WithScope.Tested", node.getType().asString());
    }

    @Test
    public void testParsedUnqualifiedClassInstanceCreationExpression_Type_Should_HaveDiamond()
    {
        String declaration = "new WithScope.Tested<>()";
        ObjectCreationExpr node =  astNodeFor(declaration, parser -> parser.classInstanceCreationExpression_lfno_primary());

        assertEquals("WithScope.Tested<>", node.getType().asString());
    }

    @Test
    public void testParsedUnqualifiedClassInstanceCreationExpression_Type_Should_HaveCorrectNumberOfTypeArguments()
    {
        String declaration = "new WithScope.Tested<T,R,S>()";
        ObjectCreationExpr node =  astNodeFor(declaration, parser -> parser.classInstanceCreationExpression_lfno_primary());

        assertEquals(3, node.getType().getTypeArguments().get().size());
    }

    @Test
    public void testParsedUnqualifiedClassInstanceCreationExpression_Should_HaveCorrectNumberOfArguments()
    {
        String declaration = "new WithScope.Tested<T,R,S>(1,2,3)";
        ObjectCreationExpr node =  astNodeFor(declaration, parser -> parser.classInstanceCreationExpression_lfno_primary());

        assertEquals(3, node.getArguments().size());
    }

}
