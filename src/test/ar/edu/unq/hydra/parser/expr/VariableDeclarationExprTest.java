package ar.edu.unq.hydra.parser.expr;

import ar.edu.unq.hydra.ast.expr.IntegerLiteralExpr;
import ar.edu.unq.hydra.ast.expr.VariableDeclarationExpr;
import ar.edu.unq.hydra.ast.type.ArrayType;
import ar.edu.unq.hydra.ast.type.PrimitiveType;
import ar.edu.unq.hydra.ast.type.UnknownType;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class VariableDeclarationExprTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedLocalVariableDeclaration_Should_BeAVariableDeclarationExpr() throws Exception
    {
        String declaration = "variable";
        Visitable node = astNodeFor(declaration, parser -> parser.localVariableDeclaration());
        assertObjectInstanceOf(node, VariableDeclarationExpr.class);
    }

    @Test
    public void testParsedLocalVariableDeclaration_Should_HaveCorrectNumberOfVariables() throws Exception
    {
        String declaration = "var1, var2";
        VariableDeclarationExpr node = astNodeFor(declaration, parser -> parser.localVariableDeclaration());

        assertEquals(2, node.getVariables().size());
    }

    @Test
    public void testParsedLocalVariableDeclaration_Should_HaveVariablesWithCorrectNames() throws Exception
    {
        String declaration = "var1, var2";
        VariableDeclarationExpr node = astNodeFor(declaration, parser -> parser.localVariableDeclaration());

        assertEquals("var1", node.getVariables().get(0).getNameAsString());
        assertEquals("var2", node.getVariables().get(1).getNameAsString());
    }

    @Test
    public void testParsedLocalVariableDeclaration_Should_HaveVariablesWithUnknownType() throws Exception
    {
        String declaration = "var1, var2";
        VariableDeclarationExpr node = astNodeFor(declaration, parser -> parser.localVariableDeclaration());

        assertObjectInstanceOf(node.getVariables().get(0).getType(), UnknownType.class);
        assertObjectInstanceOf(node.getVariables().get(1).getType(), UnknownType.class);
    }

    @Test
    public void testParsedLocalVariableDeclaration_Should_HaveVariablesWithPrimitiveIntType() throws Exception
    {
        String declaration = "int var1, var2";
        VariableDeclarationExpr node = astNodeFor(declaration, parser -> parser.localVariableDeclaration());

        assertObjectInstanceOf(node.getVariables().get(0).getType(), PrimitiveType.class);
        assertObjectInstanceOf(node.getVariables().get(1).getType(), PrimitiveType.class);
    }

    @Test
    public void testParsedLocalVariableDeclaration_Should_HaveVariablesWithArrayType() throws Exception
    {
        String declaration = "int[] var1, var2";
        VariableDeclarationExpr node = astNodeFor(declaration, parser -> parser.localVariableDeclaration());

        assertObjectInstanceOf(node.getVariables().get(0).getType(), ArrayType.class);
        assertObjectInstanceOf(node.getVariables().get(1).getType(), ArrayType.class);
    }

    @Test
    public void testParsedLocalVariableDeclaration_Should_HaveVariablesWithArrayTypeWithDifferenteLevels() throws Exception
    {
        String declaration = "int[] var1[], var2[][]";
        VariableDeclarationExpr node = astNodeFor(declaration, parser -> parser.localVariableDeclaration());

        ArrayType typeVar1 = (ArrayType) node.getVariables().get(0).getType();
        ArrayType typeVar2 = (ArrayType) node.getVariables().get(1).getType();

        assertEquals(2, typeVar1.getArrayLevel());
        assertEquals(3, typeVar2.getArrayLevel());
    }

    @Test
    public void testParsedLocalVariableDeclaration_Should_HaveInitializerExpression() throws Exception
    {
        String declaration = "int var1 = 1";
        VariableDeclarationExpr node = astNodeFor(declaration, parser -> parser.localVariableDeclaration());

        assertTrue(node.getVariables().get(0).getInitializer().isPresent());
    }

    @Test
    public void testParsedLocalVariableDeclaration_Initializer_Should_BeLiteralIntegerExpr() throws Exception
    {
        String declaration = "int var1 = 1";
        VariableDeclarationExpr node = astNodeFor(declaration, parser -> parser.localVariableDeclaration());

        assertObjectInstanceOf(node.getVariables().get(0).getInitializer().get(), IntegerLiteralExpr.class);
    }

    @Test
    public void testParsedLocalVariableDeclaration_Should_HaveCorrectNumberOfAnnotations() throws Exception
    {
        String declaration = "@Ann1 @Ann2 @Ann3 int var1 = 1";
        VariableDeclarationExpr node = astNodeFor(declaration, parser -> parser.localVariableDeclaration());

        assertEquals(3, node.getAnnotations().size());
    }

    @Test
    public void testParsedLocalVariableDeclaration_Should_HaveCorrectNamesForAnnotations() throws Exception
    {
        String declaration = "@Ann1 @Ann2 @Ann3 int var1 = 1";
        VariableDeclarationExpr node = astNodeFor(declaration, parser -> parser.localVariableDeclaration());

        assertEquals("Ann1", node.getAnnotations().get(0).getNameAsString());
        assertEquals("Ann2", node.getAnnotations().get(1).getNameAsString());
        assertEquals("Ann3", node.getAnnotations().get(2).getNameAsString());
    }
}
