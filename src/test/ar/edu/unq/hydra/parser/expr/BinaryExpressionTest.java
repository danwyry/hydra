package ar.edu.unq.hydra.parser.expr;

import ar.edu.unq.hydra.ast.expr.*;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectsClassExtends;
import static org.junit.Assert.assertEquals;

/**
 * Created by Daniel Wyrytowski on 1/6/17.
 */
public class BinaryExpressionTest extends HydraAstBuilderTest
{

    @Test
    public void testMultExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode =  astNodeFor("i*2", parser -> parser.multiplicativeExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), Expression.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.MULTIPLY);
    }

    @Test
    public void testDivExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i/2", parser -> parser.multiplicativeExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), Expression.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.DIVIDE);
    }

    @Test
    public void testModExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i%2", parser -> parser.multiplicativeExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), Expression.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.REMAINDER);
    }

    @Test
    public void testAddExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i+i", parser -> parser.additiveExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), AmbiguousNameExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.PLUS);
    }

    @Test
    public void testSubExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i-i", parser -> parser.additiveExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), AmbiguousNameExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.MINUS);
    }

    @Test
    public void testLeftShiftExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i << 2", parser -> parser.shiftExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), IntegerLiteralExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.LEFT_SHIFT);
    }

    @Test
    public void testSignedRightShiftExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i>>2", parser -> parser.shiftExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), IntegerLiteralExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.SIGNED_RIGHT_SHIFT);
    }

    @Test
    public void testUnsignedRightShiftExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i >>> 2", parser -> parser.shiftExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), IntegerLiteralExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.UNSIGNED_RIGHT_SHIFT);
    }

    //-------------------------
    //-- RELATIONAL EXPRESSIONs

    @Test
    public void testLtRelationalExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i<2", parser -> parser.relationalExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), IntegerLiteralExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.LESS);
    }

    @Test
    public void testLeqRelationalExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i<=2", parser -> parser.relationalExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), IntegerLiteralExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.LESS_EQUALS);
    }

    @Test
    public void testGtRelationalExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i>2", parser -> parser.relationalExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), IntegerLiteralExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.GREATER);
    }

    @Test
    public void testGeqRelationalExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i>=2", parser -> parser.relationalExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), IntegerLiteralExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.GREATER_EQUALS);
    }

    @Test
    public void testEqRelationalExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i==2", parser -> parser.equalityExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), IntegerLiteralExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.EQUALS);
    }

    @Test
    public void testNeqRelationalExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i != 2", parser -> parser.equalityExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), IntegerLiteralExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.NOT_EQUALS);
    }

    //-------------------
    // BITWISE OPERATIONS

    @Test
    public void testAndExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i & 2", parser -> parser.andExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), IntegerLiteralExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.BINARY_AND);
    }

    @Test
    public void testInclusiveOrExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i | 2", parser -> parser.inclusiveOrExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), IntegerLiteralExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.BINARY_OR);
    }

    @Test
    public void testExclusiveOrExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i ^ 2", parser -> parser.exclusiveOrExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), IntegerLiteralExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.XOR);
    }

    //--------------------------
    //-- CONDITIONAL EXPRESSIONs

    @Test
    public void testAndConditionalExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i && 2", parser -> parser.conditionalAndExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), IntegerLiteralExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.AND);
    }

    @Test
    public void testConditionalOrExpression_Should_HoldCorrectValues() throws Exception
    {
        BinaryExpr exprNode = astNodeFor("i || 2", parser -> parser.conditionalOrExpression());

        assertObjectsClassExtends(exprNode.getLeft(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getRight(), IntegerLiteralExpr.class);
        assertEquals(exprNode.getOperator(), BinaryExpr.Operator.OR);
    }

    @Test
    public void testConditionalExpression_Should_HoldCorrectValues() throws Exception
    {
        ConditionalExpr exprNode = astNodeFor("true ? i : 2", parser -> parser.conditionalExpression());

        assertObjectsClassExtends(exprNode.getCondition(), BooleanLiteralExpr.class);
        assertObjectsClassExtends(exprNode.getThenExpr(), AmbiguousNameExpr.class);
        assertObjectsClassExtends(exprNode.getElseExpr(), IntegerLiteralExpr.class);
    }



}
