package ar.edu.unq.hydra.parser.expr;

import ar.edu.unq.hydra.ast.expr.*;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectsClassExtends;
import static org.junit.Assert.assertEquals;

/**
 * Created by Daniel Wyrytowski on 1/10/17.
 */
public class StatementExpressionTest extends HydraAstBuilderTest
{
    @Test
    public void testAssignExpression_Should_HoldCorrectValues() throws Exception
    {
        AssignExpr exprNode = astNodeFor("a = 1", parser -> parser.assignment());

        assertObjectsClassExtends(exprNode.getTarget(), AmbiguousNameExpr.class);
        assertEquals(exprNode.getOperator(), AssignExpr.Operator.ASSIGN);
        assertObjectsClassExtends(exprNode.getValue(), IntegerLiteralExpr.class);
    }

    @Test
    public void testAssignUnaryExpression_Should_HoldCorrectValues() throws Exception
    {
        AssignExpr exprNode = astNodeFor("a = ++i", parser -> parser.assignment());

        assertObjectsClassExtends(exprNode.getTarget(), AmbiguousNameExpr.class);
        assertEquals(exprNode.getOperator(), AssignExpr.Operator.ASSIGN);
        assertObjectsClassExtends(exprNode.getValue(), UnaryExpr.class);
    }

    @Test
    public void testAssignBinaryExpression_Should_HoldCorrectValues() throws Exception
    {
        AssignExpr exprNode = astNodeFor("a = 1+i", parser -> parser.assignment());

        assertObjectsClassExtends(exprNode.getTarget(), AmbiguousNameExpr.class);
        assertEquals(exprNode.getOperator(), AssignExpr.Operator.ASSIGN);
        assertObjectsClassExtends(exprNode.getValue(), BinaryExpr.class);
    }
}
