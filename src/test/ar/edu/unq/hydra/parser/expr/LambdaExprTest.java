package ar.edu.unq.hydra.parser.expr;

import ar.edu.unq.hydra.ast.NodeList;
import ar.edu.unq.hydra.ast.expr.Expression;
import ar.edu.unq.hydra.ast.expr.IntegerLiteralExpr;
import ar.edu.unq.hydra.ast.expr.LambdaExpr;
import ar.edu.unq.hydra.ast.stmt.BlockStmt;
import ar.edu.unq.hydra.ast.stmt.ExpressionStmt;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class LambdaExprTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedLambdaExpression_Should_BeALambdaExpr()
    {
        String sourceCodeBit = "i -> 1";
        Expression expr = astNodeFor(sourceCodeBit, parser -> parser.lambdaExpression());

        assertObjectInstanceOf(expr, LambdaExpr.class);
    }

    @Test
    public void testParsedLambdaExpression_LeftSide_Should_BeA1ElementNodeListAndNotBeEnclosingExpression()
    {
        String sourceCodeBit = "i -> 1";
        LambdaExpr expr = astNodeFor(sourceCodeBit, parser -> parser.lambdaExpression());

        assertObjectInstanceOf(expr.getParameters(), NodeList.class);
        assertEquals(1, expr.getParameters().size());

        assertFalse(expr.isEnclosingParameters());
    }

    @Test
    public void testParsedLambdaExpression_RightSide_Should_BeLiteralIntegerExpr1InsideExpressionStmt()
    {
        String sourceCodeBit = "i -> 1";
        LambdaExpr expr = astNodeFor(sourceCodeBit, parser -> parser.lambdaExpression());

        assertObjectInstanceOf(expr.getBody(), ExpressionStmt.class);
        Expression expression = ((ExpressionStmt) expr.getBody()).getExpression();
        assertObjectInstanceOf(expression, IntegerLiteralExpr.class);
        assertEquals(1, ((IntegerLiteralExpr)expression).getIntValue());
    }

    @Test
    public void testParsedLambdaExpression_RightSide_Should_BeABlockStmt()
    {
        String sourceCodeBit = "i -> { return 1; }";
        LambdaExpr expr = astNodeFor(sourceCodeBit, parser -> parser.lambdaExpression());
        assertObjectInstanceOf(expr.getBody(), BlockStmt.class);
    }


}
