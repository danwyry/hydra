package ar.edu.unq.hydra.parser.expr;

import ar.edu.unq.hydra.ast.NodeList;
import ar.edu.unq.hydra.ast.expr.*;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class MethodCallExprTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedFunctionInvocation_Should_BeAMethodCallExpr() throws Exception
    {
        String declaration = "fun()";
        Visitable node = astNodeFor(declaration, parser -> parser.functionInvocation());
        assertObjectInstanceOf(node, MethodCallExpr.class);
        // Parser checks for function declaration to determine if this is a function or a method call
    }

    @Test
    public void testParsedFunctionInvocation_Should_HavecCorrectName() throws Exception
    {
        String declaration = "fun()";
        MethodCallExpr node = astNodeFor(declaration, parser -> parser.functionInvocation());

        assertEquals("fun", node.getNameAsString());
    }

    @Test
    public void testParsedFunctionInvocation_Should_HavecCorrectNumberOfArguments() throws Exception
    {
        String declaration = "fun(arg1 , arg2, arg3)";
        MethodCallExpr node = astNodeFor(declaration, parser -> parser.functionInvocation());

        assertEquals(3,node.getArguments().size());
    }


    @Test
    public void testParsedArgumentList_Should_HavecCorrectArgumentTypes() throws Exception
    {
        String declaration = "1, \"string\", object";
        NodeList<Expression> node = astNodeFor(declaration, parser -> parser.argumentList());

        Expression arg1 = node.get(0);
        Expression arg2 = node.get(1);
        Expression arg3 = node.get(2);

        assertObjectInstanceOf(arg1, IntegerLiteralExpr.class);
        assertObjectInstanceOf(arg2, StringLiteralExpr.class);
        assertObjectInstanceOf(arg3, AmbiguousNameExpr.class);
    }

}
