package ar.edu.unq.hydra.parser.expr;

import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.ast.expr.*;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static org.junit.Assert.*;


public class LiteralTest extends HydraAstBuilderTest
{
    @Test
    public void testLiteralIntegerResult_Should_BeOfTypeIntegerLiteralExpr() throws Exception
    {
        Visitable ast = astNodeFor("1", parser -> parser.literal());

        assertTrue(ast instanceof IntegerLiteralExpr);
    }

    @Test
    public void testLiteralInteger_Should_HoldCorrectValue() throws Exception
    {
        IntegerLiteralExpr integerNode = astNodeFor("1", parser -> parser.literal());
        assertEquals("1", integerNode.getValue());

        LongLiteralExpr longNode = astNodeFor("1L", parser -> parser.literal());
        assertEquals("1", longNode.getValue());

    }

    @Test
    public void testLiteralIntegerResult_Should_BeOfTypeLongLiteralExpr() throws Exception
    {
        Visitable ast = astNodeFor("1L", parser -> parser.literal());
        assertTrue(ast instanceof LongLiteralExpr);

        ast = astNodeFor("1l", parser -> parser.literal());

        assertTrue(ast instanceof LongLiteralExpr);
    }

    @Test
    public void testLiteralCharacterResult_Should_BeOfTypeCharLiteralExpr() throws Exception
    {
        Visitable ast = astNodeFor("'C'", parser -> parser.literal());

        assertTrue(ast instanceof CharLiteralExpr);
    }

    @Test
    public void testLiteralCharacter_Should_HoldCorrectValue() throws Exception
    {
        CharLiteralExpr charNode = astNodeFor("'C'", parser -> parser.literal());

        assertEquals("C", charNode.getValue());
    }

    @Test
    public void testLiteralStringResult_Should_BeOfTypeStringLiteralExpr() throws Exception
    {
        Visitable ast = astNodeFor("\"String literal\"", parser -> parser.literal());

        assertTrue(ast instanceof StringLiteralExpr);
    }

    @Test
    public void testLiteralString_Should_HoldCorrectValue() throws Exception
    {
        StringLiteralExpr charNode = astNodeFor("\"String literal\"", parser -> parser.literal());

        assertEquals("String literal", charNode.getValue());
    }

    @Test
    public void testLiteralFloatingPointResult_Should_BeOfTypeDoubleLiteralExpr() throws Exception
    {
        Visitable ast = astNodeFor("1.1d", parser -> parser.literal());

        assertTrue(ast instanceof DoubleLiteralExpr);
    }

    @Test
    public void testLiteralFloatingPoint_Should_HoldCorrectValue() throws Exception
    {
        DoubleLiteralExpr doubleNode = astNodeFor("1.1d", parser -> parser.literal());

        assertEquals("1.1", doubleNode.getValue());
        assertEquals(1.1d, doubleNode.getDoubleValue(),0);
    }

    @Test
    public void testLiteralBooleanResult_Should_BeOfTypeBooleanLiteralExpr() throws Exception
    {

        Visitable ast = astNodeFor("true", parser -> parser.literal());
        assertTrue(ast instanceof BooleanLiteralExpr);

        ast = astNodeFor("false", parser -> parser.literal());
        assertTrue(ast instanceof BooleanLiteralExpr);
    }

    @Test
    public void testLiteralBoolean_Should_HoldCorrectValue() throws Exception
    {
        BooleanLiteralExpr boolNode = astNodeFor("true", parser -> parser.literal());
        assertEquals(true, boolNode.getValue());

        boolNode = astNodeFor("false", parser -> parser.literal());
        assertEquals(false, boolNode.getValue());
    }

    @Test
    public void testLiteralNullResult_Should_BeOfTypeNullLiteralExpr() throws Exception
    {
        Visitable ast = astNodeFor("null", parser -> parser.literal());

        assertTrue(ast instanceof NullLiteralExpr);
    }


}
