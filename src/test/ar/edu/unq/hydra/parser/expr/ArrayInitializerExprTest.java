package ar.edu.unq.hydra.parser.expr;

import ar.edu.unq.hydra.ast.expr.ArrayInitializerExpr;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class ArrayInitializerExprTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedArrayInitializer_Should_BeAnArrayInitializerExpr() throws Exception
    {
        String declaration = "{ 1, 2, 3 , 4 }";
        ArrayInitializerExpr node = astNodeFor(declaration, parser -> parser.arrayInitializer());

        assertObjectInstanceOf(node, ArrayInitializerExpr.class);
    }

    @Test
    public void testParsedArrayInitializer_Should_HoldCorrenNumberOfValues() throws Exception
    {
        String declaration = "{ 1, 2, 3 , 4 }";
        ArrayInitializerExpr node = astNodeFor(declaration, parser -> parser.arrayInitializer());

        assertEquals(4, node.getValues().size());
    }
}
