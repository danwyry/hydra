package ar.edu.unq.hydra.parser.expr;

import ar.edu.unq.hydra.ast.expr.AmbiguousNameExpr;
import ar.edu.unq.hydra.ast.expr.ArrayAccessExpr;
import ar.edu.unq.hydra.ast.expr.ArrayInitializerExpr;
import ar.edu.unq.hydra.ast.expr.NameExpr;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static org.junit.Assert.assertEquals;


public class ArrayAccessExprTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedArrayAccess_Should_BeAnArrayAccessExpr() throws Exception
    {
        String declaration = "a[0]";
        ArrayAccessExpr node = astNodeFor(declaration, parser -> parser.arrayAccess());

        assertObjectInstanceOf(node, ArrayAccessExpr.class);
    }

    @Test
    public void testParsedArrayAccess_Scope_Should_BeNameExpr() throws Exception
    {
        String declaration = "a[0]";
        ArrayAccessExpr node = astNodeFor(declaration, parser -> parser.arrayAccess());

        assertObjectInstanceOf(node.getName(), AmbiguousNameExpr.class);
    }

}
