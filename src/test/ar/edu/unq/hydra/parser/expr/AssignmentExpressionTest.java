package ar.edu.unq.hydra.parser.expr;

import ar.edu.unq.hydra.ast.expr.*;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.misc.Assert;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static junit.framework.Assert.assertEquals;


public class AssignmentExpressionTest extends HydraAstBuilderTest
{
    // Test RESULTING NODE TYPES
    @Test
    public void testSimpleAssignmentExpressionResult_Should_BeOfTypeAssignExpr() throws Exception
    {
        Expression node = astNodeFor("i=1", parser -> parser.assignmentExpression());

        assertObjectInstanceOf(node, AssignExpr.class);
    }


    @Test
    public void testSimpleAssignmentExpression_Should_HoldCorrectValues() throws Exception
    {
        AssignExpr exprNode = astNodeFor("i=1", parser -> parser.assignmentExpression());

        Assert.assertObjectInstanceOf(exprNode.getTarget(), AmbiguousNameExpr.class);
        assertEquals(AssignExpr.Operator.ASSIGN, exprNode.getOperator());
        Assert.assertObjectInstanceOf(exprNode.getValue(), IntegerLiteralExpr.class);
    }

    @Test
    public void testLeftHandSide_Should_BeOfTypeAmbiguousNameExpr() throws Exception
    {
        AmbiguousNameExpr expr = astNodeFor("name", parser -> parser.leftHandSide());
        Assert.assertObjectInstanceOf(expr, AmbiguousNameExpr.class);

        expr = astNodeFor("clazz.name;", parser -> parser.leftHandSide());
        Assert.assertObjectInstanceOf(expr, AmbiguousNameExpr.class);
    }

    @Test
    public void testLeftHandSide_Should_BNameExpr() throws Exception
    {
        AmbiguousNameExpr expr = astNodeFor("name", parser -> parser.leftHandSide());
        Assert.assertObjectInstanceOf(expr, AmbiguousNameExpr.class);

        expr = astNodeFor("clazz.name;", parser -> parser.leftHandSide());
        Assert.assertObjectInstanceOf(expr, AmbiguousNameExpr.class);
    }


//    @Test
//    public void testBlah3() throws Exception
//    {
//        Node target = astNodeFor("clazz.name().a", parser -> parser.leftHandSide());
//        Assert.assertObjectInstanceOf(target, FieldAccessExpr.class);
//    }



}
