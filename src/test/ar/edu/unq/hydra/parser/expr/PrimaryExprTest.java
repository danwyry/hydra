package ar.edu.unq.hydra.parser.expr;

import ar.edu.unq.hydra.ast.expr.*;
import ar.edu.unq.hydra.ast.type.ArrayType;
import ar.edu.unq.hydra.ast.type.ClassOrInterfaceType;
import ar.edu.unq.hydra.ast.type.PrimitiveType;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static org.junit.Assert.assertEquals;


public class PrimaryExprTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedArrayInitializer_Should_BeAnArrayInitializerExpr() throws Exception
    {
        String declaration = "(i.new Input()).i;";
        Expression node = astNodeFor(declaration, parser -> parser.primary());

        assertObjectInstanceOf(node, FieldAccessExpr.class);
    }

    @Test
    public void testParsedArrayInitializer_Should_BeAnArrayInitializerExpr1111111() throws Exception
    {
        String declaration = "(i.new Input()).i();";
        Expression node = astNodeFor(declaration, parser -> parser.primary());

        assertObjectInstanceOf(node, MethodCallExpr.class);
    }

    @Test
    public void testParsedPrimaryNoNewArrayLfnoArrayAccessTypeNameClass_Should_BeAClassExpr() throws Exception
    {
        String declaration = "Int.class";
        ClassExpr node = astNodeFor(declaration, parser -> parser.primaryNoNewArray_lfno_arrayAccess());
        assertObjectInstanceOf(node, ClassExpr.class);
    }

    @Test
    public void testParsedPrimaryNoNewArrayLfnoArrayAccessTypeNameClass_Should_BeAClassExprContainingAnArrayType() throws Exception
    {
        String declaration = "Int[][].class";
        ClassExpr node = astNodeFor(declaration, parser -> parser.primaryNoNewArray_lfno_arrayAccess());
        assertObjectInstanceOf(node, ClassExpr.class);
        assertObjectInstanceOf(node.getType(), ArrayType.class);
        assertEquals(2,node.getType().getArrayLevel());
        assertObjectInstanceOf(node.getType().getElementType(), ClassOrInterfaceType.class);
        assertEquals("Int", node.getType().getElementType().asString());
    }


    @Test
    public void testParsedPrimaryNoNewArrayLfNoPrimaryTypeNameClass_Should_BeAClassExpr() throws Exception
    {
        String declaration = "Int.class";
        ClassExpr node = astNodeFor(declaration, parser -> parser.primaryNoNewArray_lfno_primary());
        assertObjectInstanceOf(node, ClassExpr.class);
    }

    @Test
    public void testParsedPrimaryNoNewArrayLfNoPrimaryTypeNameClass_Should_BeAClassExprContainingAnArrayType() throws Exception
    {
        String declaration = "Int[][].class";
        ClassExpr node = astNodeFor(declaration, parser -> parser.primaryNoNewArray_lfno_primary());
        assertObjectInstanceOf(node, ClassExpr.class);
        assertObjectInstanceOf(node.getType(), ArrayType.class);
        assertEquals(2,node.getType().getArrayLevel());
        assertObjectInstanceOf(node.getType().getElementType(), ClassOrInterfaceType.class);
        assertEquals("Int", node.getType().getElementType().asString());
    }

    @Test
    public void testParsedPrimaryNoNewArrayLfNoPrimaryUnnanPrimitiveType_Should_BeAClassExpr() throws Exception
    {
        String declaration = "int.class";
        ClassExpr node = astNodeFor(declaration, parser -> parser.primaryNoNewArray_lfno_primary());
        assertObjectInstanceOf(node, ClassExpr.class);
    }

    @Test
    public void testParsedPrimaryNoNewArrayLfNoPrimaryUnnanPrimitiveType_Should_BeAClassExprContainingAnArrayType() throws Exception
    {
        String declaration = "int[][].class";
        ClassExpr node = astNodeFor(declaration, parser -> parser.primaryNoNewArray_lfno_primary());
        assertObjectInstanceOf(node, ClassExpr.class);
        assertObjectInstanceOf(node.getType(), ArrayType.class);
        assertEquals(2,node.getType().getArrayLevel());
        assertObjectInstanceOf(node.getType().getElementType(), PrimitiveType.class);
        assertEquals("int", (node.getType().getElementType()).asString());
    }




}
