package ar.edu.unq.hydra.parser;

import ar.edu.unq.hydra.ast.visitor.Visitable;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.After;
import org.junit.Before;

import java.util.function.Function;

/** 
* HydraAstBuilder Tester.
*/
public class HydraAstBuilderTest
{
    @Before
    public void before() throws Exception
    {
    }

    @After
    public void after() throws Exception
    {
    }

    public <T extends Visitable> T astNodeFor(String sourceCodeBit, Function<HydraParser,ParseTree> parserVisitFunction)
    {
        return (T) AstBuilderUtils.parseToAstNode(sourceCodeBit,parserVisitFunction);
    }

} 
