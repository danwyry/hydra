package ar.edu.unq.hydra.parser.body;

import ar.edu.unq.hydra.ast.stmt.BlockStmt;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class BlockStmtTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedBlockStmt_Should_BeABlockStmt() throws Exception
    {
        String declaration = "{ }";
        Visitable node = astNodeFor(declaration, parser -> parser.block());
        assertObjectInstanceOf(node, BlockStmt.class);
    }

    @Test
    public void testParsedBlockStmt_Should_NotHoldStatements() throws Exception
    {
        String declaration = "{ }";
        BlockStmt node = astNodeFor(declaration, parser -> parser.block());

        assertTrue(node.getStatements().isEmpty());
    }

    @Test
    public void testParsedBlockStmt_Should_HoldCorrectNumberOfStatements() throws Exception
    {
        String declaration = "{ i = 2; i++;  }";
        BlockStmt node = astNodeFor(declaration, parser -> parser.block());

        assertEquals(2,node.getStatements().size());
    }

}
