package ar.edu.unq.hydra.parser.body;

import ar.edu.unq.hydra.ast.body.FunctionDeclaration;
import ar.edu.unq.hydra.ast.stmt.ScriptBlockStmt;
import ar.edu.unq.hydra.ast.type.ClassOrInterfaceType;
import ar.edu.unq.hydra.ast.type.PrimitiveType;
import ar.edu.unq.hydra.ast.type.UnknownType;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class FunctionDeclarationTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedFunctionDeclaration_Should_ResultInFunctionDeclarationNode() throws Exception
    {
        String declaration = "testFun() { }";
        FunctionDeclaration node = astNodeFor(declaration, parser -> parser.functionDeclaration());
        assertObjectInstanceOf(node, FunctionDeclaration.class);
    }

    @Test
    public void testParsedFunctionDeclaration_Should_HaveCorrectName() throws Exception
    {
        String declaration = "testFun() { }";
        FunctionDeclaration node = astNodeFor(declaration, parser -> parser.functionDeclaration());
        assertEquals("testFun", node.getNameAsString());
    }

    @Test
    public void testParsedFunctionDeclaration_Should_HaveNoReturnType() throws Exception
    {
        String declaration = "testFun() { }";
        FunctionDeclaration node = astNodeFor(declaration, parser -> parser.functionDeclaration());
        assertObjectInstanceOf(node.getType(), UnknownType.class);
    }

    @Test
    public void testParsedFunctionDeclaration_Should_ReturnAPrimitiveIntType() throws Exception
    {
        String declaration = "int testFun() { }";
        FunctionDeclaration node = astNodeFor(declaration, parser -> parser.functionDeclaration());
        assertObjectInstanceOf(node.getType(), PrimitiveType.intType().getClass());
    }

    @Test
    public void testParsedFunctionDeclaration_Should_ReturnAClassOrInterfaceType() throws Exception
    {
        String declaration = "Tester testFun() { }";
        FunctionDeclaration node = astNodeFor(declaration, parser -> parser.functionDeclaration());
        assertObjectInstanceOf(node.getType(), ClassOrInterfaceType.class);
    }

    @Test
    public void testParsedFunctionDeclaration_Should_HaveReturnTypeTester() throws Exception
    {
        String declaration = "Tester testFun() { }";
        FunctionDeclaration node = astNodeFor(declaration, parser -> parser.functionDeclaration());
        ClassOrInterfaceType type = (ClassOrInterfaceType) node.getType();
        assertEquals("Tester", type.getNameAsString());
    }

    @Test
    public void testParsedFunctionDeclaration_Should_HaveThrownExceptions() throws Exception
    {
        String declaration = "Tester testFun() throws Error { }";
        FunctionDeclaration node = astNodeFor(declaration, parser -> parser.functionDeclaration());
        assertTrue(node.getThrownExceptions().isNonEmpty());
    }

    @Test
    public void testParsedFunctionDeclaration_Should_ThrowErrorException() throws Exception
    {
        String declaration = "Tester testFun() throws Error { }";
        FunctionDeclaration node = astNodeFor(declaration, parser -> parser.functionDeclaration());
        ClassOrInterfaceType exceptionType = (ClassOrInterfaceType) node.getThrownExceptions().get(0);
        assertEquals("Error", exceptionType.getNameAsString());
    }

    @Test
    public void testParsedFunctionDeclaration_Should_HaveTypeParameters() throws Exception
    {
        String declaration = "<T> Tester testFun() { }";
        FunctionDeclaration node = astNodeFor(declaration, parser -> parser.functionDeclaration());

        assertTrue(node.getTypeParameters().isNonEmpty());
    }

    @Test
    public void testParsedFunctionDeclaration_Should_HaveCorrectNumberOfTypeParameters() throws Exception
    {
        String declaration = "<T,R,S> Tester testFun() { }";
        FunctionDeclaration node = astNodeFor(declaration, parser -> parser.functionDeclaration());

        assertEquals(3, node.getTypeParameters().size());
    }

    @Test
    public void testParsedFunctionDeclaration_Should_HaveAnnotations() throws Exception
    {
        String declaration = "<T> @Annotation Tester testFun() { }";
        FunctionDeclaration node = astNodeFor(declaration, parser -> parser.functionDeclaration());

        assertTrue(node.getAnnotations().isNonEmpty());
    }

    @Test
    public void testParsedFunctionDeclaration_Should_HaveCorrectNumberOfAnnotations() throws Exception
    {
        String declaration = "<T> @Ann1 @Ann2 @Ann3 Tester testFun() { }";
        FunctionDeclaration node = astNodeFor(declaration, parser -> parser.functionDeclaration());

        assertEquals(3, node.getAnnotations().size());
    }

    @Test
    public void testParsedFunctionDeclaration_Body_Should_BeAScriptBlockStatement() throws Exception
    {
        String declaration = "Tester testFun() { }";
        FunctionDeclaration node = astNodeFor(declaration, parser -> parser.functionDeclaration());

        assertObjectInstanceOf(node.getBody().get(), ScriptBlockStmt.class);
    }


//    functionDeclaration :	functionHeader functionBody

//    functionHeader      :	result? functionDeclarator throws_?
//                        |	typeParameters annotation* result? functionDeclarator throws_?

//    functionDeclarator  :	functionName '(' formalParameterList? ')' dims?

//    functionName        : Identifier

//    functionBody        :	scriptBlock

}
