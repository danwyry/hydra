package ar.edu.unq.hydra.parser.body;

import ar.edu.unq.hydra.ast.NodeList;
import ar.edu.unq.hydra.ast.body.Parameter;
import ar.edu.unq.hydra.ast.expr.AnnotationExpr;
import ar.edu.unq.hydra.ast.type.*;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class ParameterTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedFormalParameter_Should_BeParameter() throws Exception
    {
        String declaration = "parameter";
        Visitable node = astNodeFor(declaration, parser -> parser.formalParameter());

        assertObjectInstanceOf(node, Parameter.class);
    }

    @Test
    public void testParsedFormalParameter_Should_HaveCorrectName() throws Exception
    {
        String declaration = "parameter";
        Parameter node = astNodeFor(declaration, parser -> parser.formalParameter());

        assertEquals("parameter", node.getNameAsString());
    }

    @Test
    public void testParsedFormalParameter_Should_HaveUnknownType() throws Exception
    {
        String declaration = "parameter";
        Parameter node = astNodeFor(declaration, parser -> parser.formalParameter());

        assertObjectInstanceOf(node.getType(), UnknownType.class);
    }

    @Test
    public void testParsedFormalParameter_Should_HaveClassOrInterfaceType() throws Exception
    {
        String declaration = "Atype parameter";
        Parameter node = astNodeFor(declaration, parser -> parser.formalParameter());

        assertObjectInstanceOf(node.getType(), ClassOrInterfaceType.class);
    }

    @Test
    public void testParsedFormalParameter_Should_HaveArrayType() throws Exception
    {
        String declaration = "Atype parameter[]";
        Parameter node = astNodeFor(declaration, parser -> parser.formalParameter());

        assertObjectInstanceOf(node.getType(), ArrayType.class);
    }

    @Test
    public void testParsedFormalParameter_Should_HaveArrayTypeWith2Dims() throws Exception
    {
        String declaration = "Atype[] parameter[]";
        Parameter node = astNodeFor(declaration, parser -> parser.formalParameter());

        ArrayType type = (ArrayType) node.getType();
        assertEquals(2,type.getArrayLevel());
    }

    @Test
    public void testParsedFormalParameter_Should_HaveAnnotations() throws Exception
    {
        //        	:	variableModifier* unannType? variableDeclaratorId // Hydra2 gradual typing

        String declaration = "@Annotation parameter";
        Parameter node = astNodeFor(declaration, parser -> parser.formalParameter());

        assertTrue(node.getAnnotations().isNonEmpty());
        AnnotationExpr annotationExpr = node.getAnnotations().get(0);
        assertEquals("Annotation", annotationExpr.getNameAsString());
    }

    // LAST FORMAL PARAMETER


    @Test
    public void testParsedLastFormalParameter_Should_BeParameter() throws Exception
    {
        String declaration = "... parameter";
        Visitable node = astNodeFor(declaration, parser -> parser.lastFormalParameter());

        assertObjectInstanceOf(node, Parameter.class);
    }

    @Test
    public void testParsedLastFormalParameter_Should_HaveCorrectName() throws Exception
    {
        String declaration = "... parameter";
        Parameter node = astNodeFor(declaration, parser -> parser.lastFormalParameter());

        assertEquals("parameter", node.getNameAsString());
    }

    @Test
    public void testParsedLastFormalParameter_Should_HaveUnknownType() throws Exception
    {
        String declaration = "... parameter";
        Parameter node = astNodeFor(declaration, parser -> parser.lastFormalParameter());

        assertObjectInstanceOf(node.getType(), UnknownType.class);
    }

    @Test
    public void testParsedLastFormalParameter_Should_HaveClassOrInterfaceType() throws Exception
    {
        String declaration = "Atype ... parameter";
        Parameter node = astNodeFor(declaration, parser -> parser.lastFormalParameter());

        assertObjectInstanceOf(node.getType(), ClassOrInterfaceType.class);
    }

    @Test
    public void testParsedLastFormalParameter_Should_HaveArrayType() throws Exception
    {
        String declaration = "Atype ... parameter[]";
        Parameter node = astNodeFor(declaration, parser -> parser.lastFormalParameter());

        assertObjectInstanceOf(node.getType(), ArrayType.class);
    }

    @Test
    public void testParsedLastFormalParameter_Should_HaveArrayTypeWith2Dims() throws Exception
    {
        String declaration = "Atype[] ... parameter[]";
        Parameter node = astNodeFor(declaration, parser -> parser.lastFormalParameter());

        ArrayType type = (ArrayType) node.getType();
        assertEquals(2,type.getArrayLevel());
    }

    @Test
    public void testParsedLastFormalParameter_Should_HaveAnnotations() throws Exception
    {

        String declaration = "@Annotation ...  parameter";
        Parameter node = astNodeFor(declaration, parser -> parser.lastFormalParameter());

        assertTrue(node.getAnnotations().isNonEmpty());
        AnnotationExpr annotationExpr = node.getAnnotations().get(0);
        assertEquals("Annotation", annotationExpr.getNameAsString());
    }

    @Test
    public void testParsedLastFormalParameter_Should_HaveVarArgsAnnotations() throws Exception
    {

        String declaration = "T @Annotation ... parameter";
        Parameter node = astNodeFor(declaration, parser -> parser.lastFormalParameter());

        assertTrue(node.getVarArgsAnnotations().isNonEmpty());
        AnnotationExpr annotationExpr = node.getVarArgsAnnotations().get(0);
        assertEquals("Annotation", annotationExpr.getNameAsString());
    }




    @Test
    public void testParsedFormalParameterList_Should_BeANodeList()
    {
        String declaration = "param1";
        Visitable parameters = astNodeFor(declaration, parser -> parser.formalParameterList());

        assertObjectInstanceOf(parameters,NodeList.class);
    }

    @Test
    public void testParsedFormalParameterList_Should_ContainAParameter()
    {
        String declaration = "param1";
        NodeList<Parameter> parameters = astNodeFor(declaration, parser -> parser.formalParameterList());

        assertEquals(1 , parameters.size());
    }

    @Test
    public void testParsedFormalParameterList_Should_Contain3Parameters()
    {
        String declaration = "param1,param2, param3)";
        NodeList<Parameter> parameters = astNodeFor(declaration, parser -> parser.formalParameterList());

        assertEquals(3 , parameters.size());
    }

}
