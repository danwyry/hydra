package ar.edu.unq.hydra.parser.body;

import ar.edu.unq.hydra.ast.stmt.ScriptBlockStmt;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class ScriptBlockStmtTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedScriptBlockStmt_Should_BeAScriptBlockStmt() throws Exception
    {
        String declaration = "{ }";
        Visitable node = astNodeFor(declaration, parser -> parser.scriptBlock());
        assertObjectInstanceOf(node, ScriptBlockStmt.class);
    }

    @Test
    public void testParsedScriptBlockStmt_Should_NotHoldFunctionDeclarations() throws Exception
    {
        String declaration = "{ }";
        ScriptBlockStmt node = astNodeFor(declaration, parser -> parser.scriptBlock());

        assertTrue(node.getFunctions().isEmpty());
    }

    @Test
    public void testParsedScriptBlockStmt_Should_HoldCorrectNumberOfFunctionDeclarations() throws Exception
    {
        String declaration = "{ T fun1() {} R fun2() {}  }";
        ScriptBlockStmt node = astNodeFor(declaration, parser -> parser.scriptBlock());

        assertEquals(2, node.getFunctions().size());
    }

    @Test
    public void testParsedScriptBlockStmt_Should_NotHoldStatements() throws Exception
    {
        String declaration = "{ }";
        ScriptBlockStmt node = astNodeFor(declaration, parser -> parser.scriptBlock());

        assertTrue(node.getStatements().isEmpty());
    }

    @Test
    public void testParsedScriptBlockStmt_Should_HoldCorrectNumberOfStatements() throws Exception
    {
        String declaration = "{ T fun1() {} i = 2; i++;  }";
        ScriptBlockStmt node = astNodeFor(declaration, parser -> parser.scriptBlock());

        assertEquals(2,node.getStatements().size());
    }

}


// scriptBlock : '{' scriptBlockStatements? '}'
//
// scriptBlockStatements :	scriptBlockStatement scriptBlockStatement*
//
// scriptBlockStatement    :	localVariableDeclarationStatement
//                        |   scriptBlock
//                        |	classDeclaration
//                        |	statement
//                        |   functionDeclaration
