package ar.edu.unq.hydra.parser.type;

import ar.edu.unq.hydra.ast.expr.AnnotationExpr;
import ar.edu.unq.hydra.ast.type.*;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectsClassExtends;
import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;


public class WildcardTypeTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedWildcard_Should_BeWildcardType() throws Exception
    {
        String declaration = "?";
        Visitable node = astNodeFor(declaration, parser -> parser.wildcard());

        assertObjectInstanceOf(node, WildcardType.class);
    }

    @Test
    public void testParsedWildcard_Should_HaveAnnotations() throws Exception
    {
        String declaration = "@Annotation ?";
        WildcardType node = astNodeFor(declaration, parser -> parser.wildcard());

        assertTrue(node.getAnnotations().isNonEmpty());
        AnnotationExpr annotationExpr = node.getAnnotations().get(0);
        assertEquals("Annotation", annotationExpr.getNameAsString());
    }

    @Test
    public void testParsedWildcard_Should_HaveExtendedBoundAndIsReferenceType() throws Exception
    {
        String declaration = "@Annotation ? extends T";
        WildcardType node = astNodeFor(declaration, parser -> parser.wildcard());

        assertTrue(node.getExtendedType().isPresent());
        assertObjectsClassExtends(node.getExtendedType().get(), ReferenceType.class);
    }

    @Test
    public void testParsedWildcard_Should_HaveSuperBoundAndIsReferenceType() throws Exception
    {
        String declaration = "@Annotation ? super T";
        WildcardType node = astNodeFor(declaration, parser -> parser.wildcard());

        assertTrue(node.getSuperType().isPresent());
        assertObjectsClassExtends(node.getSuperType().get(), ReferenceType.class);
    }

}
