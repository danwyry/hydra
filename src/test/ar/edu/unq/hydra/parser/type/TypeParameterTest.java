package ar.edu.unq.hydra.parser.type;

import ar.edu.unq.hydra.ast.expr.AnnotationExpr;
import ar.edu.unq.hydra.ast.type.ClassOrInterfaceType;
import ar.edu.unq.hydra.ast.type.TypeParameter;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class TypeParameterTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedTypeParameter_Should_BeTypeParameter() throws Exception
    {
        String declaration = "T";
        Visitable node = astNodeFor(declaration, parser -> parser.typeParameter());

        assertObjectInstanceOf(node, TypeParameter.class);
    }

    @Test
    public void testParsedTypeParameter_Should_HaveCorrectName() throws Exception
    {
        String declaration = "T";
        TypeParameter node = astNodeFor(declaration, parser -> parser.typeParameter());

        assertEquals("T", node.getNameAsString());
    }

    @Test
    public void testParsedTypeParameter_Should_HaveAnnotations() throws Exception
    {
        String declaration = "@Annotation T";
        TypeParameter node = astNodeFor(declaration, parser -> parser.typeParameter());

        assertTrue(node.getAnnotations().isNonEmpty());
        AnnotationExpr annotationExpr = node.getAnnotations().get(0);
        assertEquals("Annotation", annotationExpr.getNameAsString());
    }

    @Test
    public void testParsedTypeParameter_Should_HaveOneTypeBound() throws Exception
    {
        String declaration = "T extends R";
        TypeParameter node = astNodeFor(declaration, parser -> parser.typeParameter());

        Integer expected = 1;
        assertEquals(expected, (Integer) node.getTypeBound().size());
    }

    @Test
    public void testParsedTypeParameter_TypeBound_Should_BeAClassOrInterfaceType() throws Exception
    {
        String declaration = "T extends R";
        TypeParameter node = astNodeFor(declaration, parser -> parser.typeParameter());

        assertObjectInstanceOf(node.getTypeBound().get(0),ClassOrInterfaceType.class);
    }

    @Test
    public void testParsedTypeParameter_Should_HaveAdditionalBounds() throws Exception
    {
        String declaration = "T extends R & S & U";
        TypeParameter node = astNodeFor(declaration, parser -> parser.typeParameter());

        Integer expected = 3;
        assertEquals(expected, (Integer) node.getTypeBound().size());
    }

    @Test
    public void testParsedTypeParameter_TypeBound_Should_AllBeClassOrInterfaceType() throws Exception
    {
        String declaration = "T extends R & S & U";
        TypeParameter node = astNodeFor(declaration, parser -> parser.typeParameter());

        for ( ClassOrInterfaceType bound : node.getTypeBound())
            assertObjectInstanceOf(bound, ClassOrInterfaceType.class);
    }


}
