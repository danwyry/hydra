package ar.edu.unq.hydra.parser.type;

import ar.edu.unq.hydra.ast.type.ArrayType;
import ar.edu.unq.hydra.ast.type.ClassOrInterfaceType;
import ar.edu.unq.hydra.ast.type.PrimitiveType;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;


public class UnannotatedReferenceTypeTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedUnannType_Should_BeClassOrInterfaceType() throws Exception
    {
        String declaration = "Tested";
        Visitable node = astNodeFor(declaration, parser -> parser.unannType());

        assertObjectInstanceOf(node, ClassOrInterfaceType.class);
    }

    @Test
    public void testParsedUnannType_Should_BeArrayType() throws Exception
    {
        String declaration = "Tested[]";
        Visitable node = astNodeFor(declaration, parser -> parser.unannType());

        assertObjectInstanceOf(node, ArrayType.class);
    }

    @Test
    public void testParsedUnannReferenceType_Should_BeClassOrInterfaceType() throws Exception
    {
        String declaration = "Tested";
        Visitable node = astNodeFor(declaration, parser -> parser.unannReferenceType());

        assertObjectInstanceOf(node, ClassOrInterfaceType.class);
    }

    @Test
    public void testParsedUnannClassOrInterface_Should_BeClassOrInterfaceType() throws Exception
    {
        String declaration = "Tested";

        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.unannClassOrInterfaceType());

        assertObjectInstanceOf(node, ClassOrInterfaceType.class);
    }

    @Test
    public void testParsedUnannClassOrInterface_Should_HaveNoScope() throws Exception
    {
        String declaration = "Tested";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.unannClassOrInterfaceType());
        assertFalse(node.getScope().isPresent());
    }

    @Test
    public void testParsedUnannClassOrInterface_Should_BeHaveCorrectName() throws Exception
    {
        String declaration = "Tested";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.unannClassOrInterfaceType());

        assertEquals("Tested", node.getNameAsString());
    }

    @Test
    public void testParsedUnannClassOrInterface_Should_HaveTestedAsItsScopeAndWithScopeAsItsName() throws Exception
    {
        String declaration = "Tested.WithScope;";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.unannClassOrInterfaceType());

        assertTrue(node.getScope().isPresent());
        assertEquals("Tested", node.getScope().get().getNameAsString());

        assertEquals("WithScope", node.getNameAsString());
    }

    @Test
    public void testParsedUnannClassOrInterface_Should_HaveTypeArguments() throws Exception
    {
        String declaration = "Tested <T1, T2>";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.unannClassOrInterfaceType());

        assertTrue(node.getTypeArguments().isPresent());
        assertEquals(2,node.getTypeArguments().get().size());
    }

    @Test
    public void testParsedUnannClassOrInterface_Should_HaveCorrectTypeArgumentNames() throws Exception
    {
        String declaration = "Tested <T1, T2>";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.unannClassOrInterfaceType());

        ClassOrInterfaceType first = (ClassOrInterfaceType) node.getTypeArguments().get().get(0);
        ClassOrInterfaceType second = (ClassOrInterfaceType) node.getTypeArguments().get().get(1);

        assertEquals("T1", first.getNameAsString());
        assertEquals("T2", second.getNameAsString());

    }

    @Test
    public void testParsedUnannTypeVariable_Should_BeClassOrInterfaceType() throws Exception
    {
        String declaration = "Tested";
        Visitable node = astNodeFor(declaration, parser -> parser.unannTypeVariable());

        assertObjectInstanceOf(node, ClassOrInterfaceType.class);
    }


    @Test
    public void testParsedUnannTypeVariable_Should_HaveCorrectName() throws Exception
    {
        String declaration = "Tested";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.unannTypeVariable());

        assertEquals("Tested", node.getNameAsString());
    }

    @Test
    public void testParsedArrayType_Should_BeArrayType() throws Exception
    {
        String declaration = "Tested[]";
        Visitable node = astNodeFor(declaration, parser -> parser.unannArrayType());

        assertObjectInstanceOf(node, ArrayType.class);
    }

    @Test
    public void testParsedArrayType_Should_WrapAPrimitiveType() throws Exception
    {
        String declaration = "int[]";
        ArrayType node = astNodeFor(declaration, parser -> parser.unannArrayType());

        PrimitiveType elemType = (PrimitiveType) node.getComponentType();
        assertEquals("int", elemType.asString());
    }

    @Test
    public void testParsedArrayType_Should_WrapAClassOrInterfaceType() throws Exception
    {
        String declaration = "Tested[]";
        ArrayType node = astNodeFor(declaration, parser -> parser.unannArrayType());

        ClassOrInterfaceType elemType = (ClassOrInterfaceType) node.getComponentType();
        assertEquals("Tested", elemType.getNameAsString());
    }

    @Test
    public void testParsedArrayTypesGetElementType_Should_MatchGetComponentType() throws Exception
    {
        String declaration = "Tested[]";
        ArrayType node = astNodeFor(declaration, parser -> parser.unannArrayType());

        assertSame(node.getComponentType(), node.getElementType());
    }

    @Test
    public void testParsedArrayTypeGetArrayLevel_Should_EqualItsDims() throws Exception
    {
        String declaration = "Tested[][][]";
        ArrayType node = astNodeFor(declaration, parser -> parser.unannArrayType());

        Integer expected = 3;
        assertEquals(expected, (Integer) node.getArrayLevel());
    }

    // -- ClassType

    @Test
    public void testParsedUnannClassType_Should_BeClassOrInterfaceType() throws Exception
    {
        String declaration = "Tested";
        Visitable node = astNodeFor(declaration, parser -> parser.unannClassType());

        assertObjectInstanceOf(node, ClassOrInterfaceType.class);
    }

    @Test
    public void testParsedUnannClassType_Should_HaveNoScope() throws Exception
    {
        String declaration = "Tested";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.unannClassType());
        assertFalse(node.getScope().isPresent());
    }

    @Test
    public void testParsedUnannClassType_Should_BeHaveCorrectName() throws Exception
    {
        String declaration = "Tested";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.unannClassType());

        assertEquals("Tested", node.getNameAsString());
    }

    @Test
    public void testParsedUnannClassType_Should_HaveTestedAsItsScopeAndWithScopeAsItsName() throws Exception
    {
        String declaration = "Tested.WithScope";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.unannClassType());

        assertTrue(node.getScope().isPresent());
        assertEquals("Tested", node.getScope().get().getNameAsString());

        assertEquals("WithScope", node.getNameAsString());
    }

    @Test
    public void testParsedUnannClassType_Should_HaveTypeArguments() throws Exception
    {
        String declaration = "Tested <T1, T2>";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.unannClassType());

        assertTrue(node.getTypeArguments().isPresent());
        assertEquals(2,node.getTypeArguments().get().size());
    }

    @Test
    public void testParsedUnannClassType_Should_HaveCorrectTypeArgumentNames() throws Exception
    {
        String declaration = "Tested <T1, T2>";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.unannClassType());

        ClassOrInterfaceType first = (ClassOrInterfaceType) node.getTypeArguments().get().get(0);
        ClassOrInterfaceType second = (ClassOrInterfaceType) node.getTypeArguments().get().get(1);

        assertEquals("T1", first.getNameAsString());
        assertEquals("T2", second.getNameAsString());

    }



}
