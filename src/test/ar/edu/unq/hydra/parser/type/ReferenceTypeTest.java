package ar.edu.unq.hydra.parser.type;

import ar.edu.unq.hydra.ast.expr.AnnotationExpr;
import ar.edu.unq.hydra.ast.type.ArrayType;
import ar.edu.unq.hydra.ast.type.ClassOrInterfaceType;
import ar.edu.unq.hydra.ast.type.PrimitiveType;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;


public class ReferenceTypeTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedType_Should_BeClassOrInterfaceType() throws Exception
    {
        String declaration = "Tested";
        Visitable node = astNodeFor(declaration, parser -> parser.type());

        assertObjectInstanceOf(node, ClassOrInterfaceType.class);
    }

    @Test
    public void testParsedType_Should_BeArrayType() throws Exception
    {
        String declaration = "Tested[]";
        Visitable node = astNodeFor(declaration, parser -> parser.type());

        assertObjectInstanceOf(node, ArrayType.class);
    }

    @Test
    public void testParsedReferenceType_Should_BeClassOrInterfaceType() throws Exception
    {
        String declaration = "Tested";
        Visitable node = astNodeFor(declaration, parser -> parser.referenceType());

        assertObjectInstanceOf(node, ClassOrInterfaceType.class);
    }

    @Test
    public void testParsedClassOrInterface_Should_BeClassOrInterfaceType() throws Exception
    {
        String declaration = "Tested";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.classOrInterfaceType());

        assertObjectInstanceOf(node, ClassOrInterfaceType.class);
    }

    @Test
    public void testParsedClassOrInterface_Should_HaveNoScope() throws Exception
    {
        String declaration = "Tested";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.classOrInterfaceType());
        assertFalse(node.getScope().isPresent());
    }

    @Test
    public void testParsedClassOrInterface_Should_BeHaveCorrectName() throws Exception
    {
        String declaration = "Tested";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.classOrInterfaceType());

        assertEquals("Tested", node.getNameAsString());
    }

    @Test
    public void testParsedClassOrInterface_Should_HaveTestedAsItsScopeAndWithScopeAsItsName() throws Exception
    {
        String declaration = "Tested.WithScope";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.classOrInterfaceType());

        assertTrue(node.getScope().isPresent());
        assertEquals("Tested", node.getScope().get().getNameAsString());

        assertEquals("WithScope", node.getNameAsString());
    }

    @Test
    public void testParsedClassOrInterface_Should_HaveAnnotations() throws Exception
    {
        String declaration = "@Annotation Tested";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.classOrInterfaceType());

        assertTrue(node.getAnnotations().isNonEmpty());
        AnnotationExpr annotationExpr = node.getAnnotations().get(0);
        assertEquals("Annotation", annotationExpr.getNameAsString());
    }

    @Test
    public void testParsedClassOrInterface_Should_HaveTypeArguments() throws Exception
    {
        String declaration = "@Annotation Tested <T1, T2>";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.classOrInterfaceType());

        assertTrue(node.getTypeArguments().isPresent());
        assertEquals(2,node.getTypeArguments().get().size());
    }

    @Test
    public void testParsedClassOrInterface_Should_HaveCorrectTypeArgumentNames() throws Exception
    {
        String declaration = "@Annotation Tested <T1, T2>";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.classOrInterfaceType());

        ClassOrInterfaceType first = (ClassOrInterfaceType) node.getTypeArguments().get().get(0);
        ClassOrInterfaceType second = (ClassOrInterfaceType) node.getTypeArguments().get().get(1);

        assertEquals("T1", first.getNameAsString());
        assertEquals("T2", second.getNameAsString());

    }

    @Test
    public void testParsedTypeVariable_Should_BeClassOrInterfaceType() throws Exception
    {
        String declaration = "Tested";
        Visitable node = astNodeFor(declaration, parser -> parser.typeVariable());

        assertObjectInstanceOf(node, ClassOrInterfaceType.class);
    }

    @Test
    public void testParsedTypeVariable_Should_HaveAnnotations() throws Exception
    {
        String declaration = "@Annotation Tested";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.typeVariable());

        assertTrue(node.getAnnotations().isNonEmpty());

        AnnotationExpr annotationExpr = node.getAnnotations().get(0);
        assertEquals("Annotation", annotationExpr.getNameAsString());
    }

    @Test
    public void testParsedTypeVariable_Should_HaveCorrectName() throws Exception
    {
        String declaration = "@Annotation Tested";
        ClassOrInterfaceType node = astNodeFor(declaration, parser -> parser.typeVariable());

        assertEquals("Tested", node.getNameAsString());
    }

    @Test
    public void testParsedArrayType_Should_BeArrayType() throws Exception
    {
        String declaration = "Tested[]";
        Visitable node = astNodeFor(declaration, parser -> parser.arrayType());

        assertObjectInstanceOf(node, ArrayType.class);
    }

    @Test
    public void testParsedArrayType_Should_WrapAPrimitiveType() throws Exception
    {
        String declaration = "int[]";
        ArrayType node = astNodeFor(declaration, parser -> parser.arrayType());

        PrimitiveType elemType = (PrimitiveType) node.getComponentType();
        assertEquals("int", elemType.asString());
    }

    @Test
    public void testParsedArrayType_Should_WrapAClassOrInterfaceType() throws Exception
    {
        String declaration = "Tested[]";
        ArrayType node = astNodeFor(declaration, parser -> parser.arrayType());

        ClassOrInterfaceType elemType = (ClassOrInterfaceType) node.getComponentType();
        assertEquals("Tested", elemType.getNameAsString());
    }

    @Test
    public void testParsedArrayTypesGetElementType_Should_MatchGetComponentType() throws Exception
    {
        String declaration = "Tested[]";
        ArrayType node = astNodeFor(declaration, parser -> parser.arrayType());

        assertSame(node.getComponentType(), node.getElementType());
    }

    @Test
    public void testParsedArrayTypeGetArrayLevel_Should_EqualItsDims() throws Exception
    {
        String declaration = "Tested[][][]";
        ArrayType node = astNodeFor(declaration, parser -> parser.arrayType());

        Integer expected = 3;
        assertEquals(expected, (Integer) node.getArrayLevel());
    }

    //

}
