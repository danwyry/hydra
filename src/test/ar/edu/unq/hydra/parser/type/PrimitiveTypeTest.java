package ar.edu.unq.hydra.parser.type;

import ar.edu.unq.hydra.ast.type.PrimitiveType;
import ar.edu.unq.hydra.ast.visitor.Visitable;
import ar.edu.unq.hydra.parser.HydraAstBuilderTest;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static org.junit.Assert.assertEquals;


public class PrimitiveTypeTest extends HydraAstBuilderTest
{
    @Test
    public void testParsedType_Should_BePrimitiveType() throws Exception
    {
        String declaration = "int";
        Visitable node =  astNodeFor(declaration, parser -> parser.type());
        assertObjectInstanceOf(node, PrimitiveType.class);
    }

    @Test
    public void testParsedPrimitiveType_Should_BePrimitiveType() throws Exception
    {
        String declaration = "int";
        Visitable node =  astNodeFor(declaration, parser -> parser.primitiveType());
        assertObjectInstanceOf(node, PrimitiveType.class);
    }

    @Test
    public void testParsedPrimitiveType_Should_BePrimitiveTypeINT() throws Exception
    {
        String declaration = "int";
        PrimitiveType node =  astNodeFor(declaration, parser -> parser.primitiveType());
        assertEquals(PrimitiveType.Primitive.INT, node.getType());
    }

    @Test
    public void testParsedPrimitiveType_Should_BePrimitiveTypeFLOAT() throws Exception
    {
        String declaration = "float";
        PrimitiveType node =  astNodeFor(declaration, parser -> parser.primitiveType());
        assertEquals(PrimitiveType.Primitive.FLOAT, node.getType());
    }

    @Test
    public void testParsedPrimitiveType_Should_BePrimitiveTypeDOUBLE() throws Exception
    {
        String declaration = "double";
        PrimitiveType node =  astNodeFor(declaration, parser -> parser.primitiveType());
        assertEquals(PrimitiveType.Primitive.DOUBLE, node.getType());
    }

    @Test
    public void testParsedPrimitiveType_Should_BePrimitiveTypeLONG() throws Exception
    {
        String declaration = "long";
        PrimitiveType node =  astNodeFor(declaration, parser -> parser.primitiveType());
        assertEquals(PrimitiveType.Primitive.LONG, node.getType());
    }

    @Test
    public void testParsedPrimitiveType_Should_BePrimitiveTypeSHORT() throws Exception
    {
        String declaration = "short";
        PrimitiveType node =  astNodeFor(declaration, parser -> parser.primitiveType());
        assertEquals(PrimitiveType.Primitive.SHORT, node.getType());
    }

    @Test
    public void testParsedPrimitiveType_Should_BePrimitiveTypeBYTE() throws Exception
    {
        String declaration = "byte";
        PrimitiveType node =  astNodeFor(declaration, parser -> parser.primitiveType());
        assertEquals(PrimitiveType.Primitive.BYTE, node.getType());
    }

    @Test
    public void testParsedPrimitiveType_Should_BePrimitiveTypeCHAR() throws Exception
    {
        String declaration = "char";
        PrimitiveType node =  astNodeFor(declaration, parser -> parser.primitiveType());
        assertEquals(PrimitiveType.Primitive.CHAR, node.getType());
    }

    @Test
    public void testParsedPrimitiveType_Should_BePrimitiveTypeLBOOLEAN() throws Exception
    {
        String declaration = "boolean";
        PrimitiveType node =  astNodeFor(declaration, parser -> parser.primitiveType());
        assertEquals(PrimitiveType.Primitive.BOOLEAN, node.getType());
    }


}
