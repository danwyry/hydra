package ar.edu.unq.hydra.interpreter.context;

import ar.edu.unq.hydra.interpreter.context.frames.Frame;
import ar.edu.unq.hydra.interpreter.context.frames.StoreStack;
import ar.edu.unq.hydra.interpreter.values.HInt;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static ar.edu.unq.hydra.interpreter.values.HObject.NULL_OBJECT;
import static org.junit.Assert.*;

public class StoreStackTest
{
    private StoreStack storeStack;

    @Before
    public void before() throws Exception
    {
        storeStack = new StoreStack();
    }

    @After
    public void after() throws Exception
    {
    }

    @Test
    public void testIsStoredVariable_Should_ReturnFalse()
    {
        //storeStack.storeVariable("name" , HObject.NULL_OBJECT);
        assertFalse(storeStack.isStoredVariable("name"));
    }

    @Test(expected = RuntimeException.class)
    public void testStoreVariable_Should_FailOnEmptyStack()
    {
        storeStack.storeVariable("name" , NULL_OBJECT);
    }

    @Test
    public void testPush_Should_ReturnAStackWithTheGivenScopeAsItsTopScope()
    {
        Frame scope = new Frame();
        StoreStack newStack = storeStack.push(scope);
        assertEquals(scope, newStack.topScope());
    }

    @Test
    public void testPop_Should_ReturnTheOriginalStack()
    {
        Frame scope = new Frame();
        StoreStack newStack = storeStack.push(scope);
        StoreStack originalStack = newStack.pop();
        assertEquals(storeStack, originalStack);
    }

    @Test
    public void testStoreVariable_Should_StoreAVariableOfTheGivenNameInTheCurrentScope()
    {
        Frame scope = new Frame();
        StoreStack newStack = storeStack.push(scope);

        newStack.storeVariable("name", NULL_OBJECT);

        assertTrue(scope.isStoredVariable("name"));
        assertEquals(NULL_OBJECT, scope.getStoredVariable("name"));
    }

    @Test
    public void testStoreVariable_Should_StoreTheNewValueInTheScopeWhereItWasStored()
    {
        Frame firstScope = new Frame();
        StoreStack newStack = storeStack.push(firstScope);
        newStack.storeVariable("name", NULL_OBJECT);
        Frame secondScope = new Frame();
        newStack = newStack.push(secondScope);

        HInt newValue = new HInt(1);
        newStack.storeVariable("name", newValue);

        assertTrue(firstScope.isStoredVariable("name"));
        assertFalse(secondScope.isStoredVariable("name"));
        assertEquals(newValue, firstScope.getStoredVariable("name"));
    }

    @Test
    public void testStoreVariable_When_forceNewTrue_Should_StoreTheNewValueInTheTopScope()
    {
        Frame firstScope = new Frame();
        StoreStack newStack = storeStack.push(firstScope);
        newStack.storeVariable("name", NULL_OBJECT);
        Frame secondScope = new Frame();
        newStack = newStack.push(secondScope);

        HInt newValue = new HInt(1);
        newStack.storeVariable("name", newValue, true);

        assertTrue(firstScope.isStoredVariable("name"));
        assertTrue(secondScope.isStoredVariable("name"));
        assertEquals(NULL_OBJECT, firstScope.getStoredVariable("name"));
        assertEquals(newValue, secondScope.getStoredVariable("name"));
    }


}

