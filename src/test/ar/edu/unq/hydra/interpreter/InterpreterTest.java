package ar.edu.unq.hydra.interpreter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;


public class InterpreterTest
{
    PrintStream originalOut ;
    PrintStream originalErr ;

    PrintStream printErrStream;
    ByteArrayOutputStream errorStream;

    PrintStream printOutStream;
    ByteArrayOutputStream outputStream;

    Interpreter interpreter ;
    @Before
    public void before() throws Exception
    {
        outputStream = new ByteArrayOutputStream();
        printOutStream = new PrintStream(outputStream);
        errorStream = new ByteArrayOutputStream();
        printErrStream = new PrintStream(errorStream);
        System.setOut(printOutStream);
        System.setErr(printErrStream);

        interpreter = new Interpreter(null, printOutStream, printErrStream);
    }

    @After
    public void after() throws Exception
    {
        if (System.out != originalOut);
            outputStream.reset();
    }

    @Test
    public void testMethodInvocationTypeName()
    {
        String sourceCodeBit = "java.lang.System.out.print(\"Hello world!\");";
        interpreter.interpret(sourceCodeBit);
        assertEquals("Hello world!",outputStream.toString());

        outputStream.reset();

        sourceCodeBit = "java.lang.System.out.print(java.util.Objects.isNull(null));";
        interpreter.interpret(sourceCodeBit);
        assertEquals("true",outputStream.toString());

    }

    @Test
    public void testConditionals()
    {
        String sourceCodeBit = "if (true) java.lang.System.out.print(true);";
        interpreter.interpret(sourceCodeBit);
        assertEquals("true",outputStream.toString());

        outputStream.reset();

        sourceCodeBit = "if (false) ; else java.lang.System.out.print(true);";
        interpreter.interpret(sourceCodeBit);
        assertEquals("true",outputStream.toString());

        outputStream.reset();

        sourceCodeBit = "if (false) { ; } else { java.lang.System.out.print(true); }";
        interpreter.interpret(sourceCodeBit);
        assertEquals("true",outputStream.toString());

        outputStream.reset();

        sourceCodeBit = "java.lang.System.out.print(true ? 1 : 2); ";
        interpreter.interpret(sourceCodeBit);
        assertEquals("1",outputStream.toString());

        outputStream.reset();

    }

    @Test
    public void testImports()
    {
        String sourceCodeBit = "import static java.lang.System.out; out.print(\"single static import is ok\");";
        interpreter.interpret(sourceCodeBit);
        assertEquals("single static import is ok",outputStream.toString());

        outputStream.reset();

        sourceCodeBit = "import java.lang.System; System.out.print(\"single type is ok\");";
        interpreter.interpret(sourceCodeBit);
        assertEquals("single type is ok",outputStream.toString());

        outputStream.reset();

        sourceCodeBit = "import java.lang.*; System.out.print(\"on demand type is ok\");";
        interpreter.interpret(sourceCodeBit);
        assertEquals("on demand type is ok",outputStream.toString());

        outputStream.reset();

        sourceCodeBit = "import static java.lang.System.*; out.print(\"on demand static field is ok\");";
        interpreter.interpret(sourceCodeBit);
        assertEquals("on demand static field is ok",outputStream.toString());

        outputStream.reset();
    }
    @Test
    public void testBasicForLoop()
    {
        String sourceCodeBit = "for (int i = 1 ; i <= 5 ; ++i ) java.lang.System.out.print(i);";
        interpreter.interpret(sourceCodeBit);
        assertEquals("12345", outputStream.toString());

        outputStream.reset();

        sourceCodeBit = "for (int i = 1, j=1 ; i <= 5 ; ++i, j=(j+2) ) { java.lang.System.out.print(i); java.lang.System.out.print(j);  }";
        interpreter.interpret(sourceCodeBit);
        assertEquals("1123354759", outputStream.toString());

        outputStream.reset();
    }

    @Test
    public void testEnhacedForLoop()
    {
        String sourceCodeBit = "import java.util.ArrayList;" +
                "l = new ArrayList() ; " +
                "l.add(1);" +
                "l.add(2);" +
                "for ( i : l ) java.lang.System.out.print(i);";
        interpreter.interpret(sourceCodeBit);
        assertEquals("12", outputStream.toString());
    }

    @Test
    public void testWhileLoop()
    {
        String sourceCodeBit = "i = 0 ; while (i < 2) { java.lang.System.out.print(i); i++; }";
        interpreter.interpret(sourceCodeBit);
        assertEquals("01", outputStream.toString());

        outputStream.reset();
    }

    @Test
    public void testWhileWithBreakAndContinue()
    {
        String sourceCodeBit = "i = 0 ; while (i < 5) { if (i == 3) { i++ ; continue ; } java.lang.System.out.print(i); i++; }";
        interpreter.interpret(sourceCodeBit);
        assertEquals("0124", outputStream.toString());

        outputStream.reset();

        sourceCodeBit = "i = 0 ; while (i < 5) { if (i == 4) { break ; } java.lang.System.out.print(i); i++; }";
        interpreter.interpret(sourceCodeBit);
        assertEquals("0123", outputStream.toString());

        outputStream.reset();
    }

    @Test
    public void testRepeatLoop()
    {
        String sourceCodeBit = "repeat(2) { java.lang.System.out.print(1); }";
        interpreter.interpret(sourceCodeBit);
        assertEquals("11", outputStream.toString());

        outputStream.reset();
    }



    @Test
    public void testFunctionInvocation()
    {
        String sourceCodeBit = "i = 0 ; " +
                "printI() { java.lang.System.out.print(i); } " +
                "while (i < 2) { printI(); i++; }";
        interpreter.interpret(sourceCodeBit);
        assertEquals("01", outputStream.toString());

        outputStream.reset();

        sourceCodeBit = "i = 0 ; " +
                "printIAndSucc() { java.lang.System.out.print(i);  return i+1; } " +
                "while (i < 2) { i = printIAndSucc();  }";
        interpreter.interpret(sourceCodeBit);
        assertEquals("01", outputStream.toString());

        outputStream.reset();

        sourceCodeBit = "import java.lang.*; " +
                "int i = 0 ; " +
                "printIAndSucc() { java.lang.System.out.print(i);  return i + 1; } " +
                "while (i < 2) { i = printIAndSucc();  }";
        interpreter.interpret(sourceCodeBit);
        assertEquals("01", outputStream.toString());

        outputStream.reset();
    }

    @Test
    public void testFunctionInvocationWrongReturnTypes()
    {
        String sourceCodeBit =  "import java.lang.*; " +
                "int i = 0 ; " +
                "void printIAndSucc() { java.lang.System.out.print(i);  return i + 1; } " +
                "while (i < 2) { i = printIAndSucc();  }";
        Interpreter i = new Interpreter(null, new PrintStream(outputStream), new PrintStream(errorStream));
        i.interpret(sourceCodeBit);
        assertThat(errorStream.toString(), containsString("ClassCastException"));
        assertEquals("0", outputStream.toString());

        outputStream.reset();
        errorStream.reset();

    }

    @Test
    public void testBasicThread()
    {
        String sourceCodeBit = "int i = 0 ; " +
                                "thread i=1;" +
                " for (int u = 0 ; u < 200 ; u++ ) java.lang.System.out.println(i) ;" ;
        interpreter.interpret(sourceCodeBit);
    }

    @Test
    public void testThreadThatDependsOnParentProcessContextDoesntCrash()
    {
        String sourceCodeBit = "int i = 5000 ; " +
                                "thread { " +
                                    "for(int j = 0 ; j < 5000 ; j++) " +
                                        " java.lang.System.out.println(i);" +
                                "}" +
                " while (i > 0) i=i-1;" ;

        interpreter.interpret(sourceCodeBit);
//        assertEquals("\n0", outputStream.toString().substring(outputStream.size()-2));
    }

    @Test //(expected = Exception.class)
    public void testThrowException()
    {
        String sourceCodeBit = "throw new java.lang.Exception(\"Probando\");" ;
        interpreter.interpret(sourceCodeBit);
        assertThat(errorStream.toString(), containsString("Exception"));
        errorStream.reset();
    }

    @Test
    public void testTryCatchException()
    {
        String sourceCodeBit = "try { " +
                    "throw new java.lang.RuntimeException(\"Probando\");" +
                "}  catch (java.lang.Exception e) {" +
                    "java.lang.System.out.println(1111111);" +
                "} catch (java.lang.RuntimeException e) {" +
                    "java.lang.System.out.println(2222222);" +
                "} " ;

        interpreter.interpret(sourceCodeBit);
        assertThat(outputStream.toString(), containsString("2222222"));
    }


    @Test
    public void testParsedArrayCreationExpr_Should_()
    {
        String sourceCodeBit = "is = new int[][] {{1},{2}};" +
                "is[0][0] = 2;" +
                "java.lang.System.out.print(is[0][0]);";
        interpreter.interpret(sourceCodeBit);
        assertThat(outputStream.toString(), containsString("2"));
    }


    @Test
    public void testLambdaExprWithNoParams_Should_ReturnInCorrectValuesWhenApplied()
    {
        String sourceCodeBit = "i = () -> 1;" +
                "java.lang.System.out.print(i.get());";
        interpreter.interpret(sourceCodeBit);
        assertEquals("1", outputStream.toString());
    }

    @Test
    public void testLambdaExprWith1Parameter_Should_ReturnCorrectValuesWhenApplied()
    {
        String sourceCodeBit = "i = b -> b;" +
                "java.lang.System.out.print(i.apply(1));";
        interpreter.interpret(sourceCodeBit);
        assertEquals("1", outputStream.toString());
    }

    @Test
    public void testLambdaExprWithParameters_Should_ReturnCorrectValuesWhenApplied()
    {
        String sourceCodeBit = "i = (a,b) -> a-b;" +
                "java.lang.System.out.print(i.apply(3,1));";
        interpreter.interpret(sourceCodeBit);
        assertEquals("2", outputStream.toString());
    }

    @Test(expected = Throwable.class)
    public void testLambdaExprWithParameters_Should_NotConfuseArrayWithVariableArgumentsWhenApplyIsCalled()
    {
        String sourceCodeBit = "i = (a,b) -> a-b;" +
                "print(i.apply(new int[] { 3,1 }));";
        interpreter.interpret(sourceCodeBit);
        assertEquals("2", outputStream.toString());
    }

    @Test
    public void testLambdaExprWithParameters_Should_AcceptArraysAsParameters()
    {
        String sourceCodeBit = "i = (a,b) -> a[0]-b[0];" +
                "print(i.apply(new int[] { 3,1 }, new int[] { 1 }));";
        interpreter.interpret(sourceCodeBit);
        assertEquals("2", outputStream.toString());
    }

    @Test
    public void testLambdaExprAppliedToListMap_Should_MapToCorrectValues()
    {
        String sourceCodeBit = "" +
                "java.util.List l = edu.emory.mathcs.backport.java.util.Arrays.asList(new java.lang.Integer[] {1,2,3});" +
                "l2 = l.stream().map(a -> a*2).collect(java.util.stream.Collectors.toList());" +
                "java.lang.System.out.print(l2);";
        interpreter.interpret(sourceCodeBit);
        assertEquals("[2, 4, 6]", outputStream.toString());

    }

    @Test
    public void testDefaultImportsWork()
    {
        String sourceCodeBit = "i = new Integer(1);" +
                "print(i);";
        interpreter.interpret(sourceCodeBit);
        assertEquals("1", outputStream.toString());

    }


}

