package ar.edu.unq.hydra.interpreter;

import ar.edu.unq.hydra.ui.ConsoleUI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.FileSystems;

import static java.lang.Thread.sleep;

/**
 * Just a Test class to invoke the interpreter with example files as a arguments.
 */
public class ConsoleUITest
{
    @Before
    public void before() throws Exception
    {

    }

    @After
    public void after() throws Exception
    {
    }

    @Test
    public void test000_functionParameterTyping()
    {
        evalTestScriptFile("000_functionParameterTyping");
    }

    @Test
    public void test001_nonDeterminism()
    {
        evalTestScriptFile("001_nonDeterminism");
    }

    @Test
    public void test002_nonDeterminism()
    {
        evalTestScriptFile("002_nonDeterminism");
    }

    @Test
    public void test003_parameteredThreadStatement()
    {
        evalTestScriptFile("003_parameteredThreadStatement");
    }

    @Test
    public void test004_parameteredProcessStatement()
    {
        evalTestScriptFile("004_parameteredProcessStatement");
    }

    @Test
    public void test005_nullAsAUniqueInstance()
    {
        evalTestScriptFile("005_nullAsAUniqueInstance");
    }

    @Test
    public void test006_switchStatement()
    {
        evalTestScriptFile("006_switchStatement");
    }

    @Test
    public void test007_scopedFunctions()
    {
        evalTestScriptFile("007_scopedFunctions");
    }

    @Test
    public void test008_atomicOperations()
    {
        evalTestScriptFile("008_atomicOperations");
    }

    @Test
    public void test009_arrayAccess()
    {
        evalTestScriptFile("009_arrayAccess");
    }

    @Test
    public void test009_arrayAcce1ss()
    {
        evalTestScriptFile("010_typeChange");
    }


    private String getPathForTestScript(String file)
    {
        return FileSystems.getDefault().getPath("src/test/ar/edu/unq/hydra/interpreter/scripts/" + file + ".hyd").toAbsolutePath().toString();
    }

    private void evalTestScriptFile(String file)
    {
        String pathForTestScript = getPathForTestScript(file);

        long startTime = System.nanoTime();

        ConsoleUI.main(pathForTestScript);

        long estimatedTime = System.nanoTime() - startTime;

        System.out.println( "\n//-----------------------------------");
        System.out.println( "//-- Estimated time: " + estimatedTime);

        try {
            sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

