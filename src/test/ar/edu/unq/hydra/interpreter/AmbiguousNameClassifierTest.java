package ar.edu.unq.hydra.interpreter;

import ar.edu.unq.hydra.ast.ImportDeclaration;
import ar.edu.unq.hydra.ast.expr.*;
import ar.edu.unq.hydra.ast.type.ClassOrInterfaceType;
import ar.edu.unq.hydra.interpreter.context.Context;
import ar.edu.unq.hydra.interpreter.import_manager.ImportManager;
import ar.edu.unq.hydra.interpreter.values.HJObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static org.junit.Assert.assertEquals;


public class AmbiguousNameClassifierTest
{

    private Context emptyContext;
    private ImportManager importManager;

    @Before
    public void before() throws Exception
    {
        importManager = new ImportManager();
        emptyContext = new Context();
    }

    @After
    public void after() throws Exception
    {
    }

    @Test
    public void testNameResolver_Name_Should_BeAClassExpr()
    {
        AmbiguousNameExpr name = new AmbiguousNameExpr(Name.parse("java.lang.System"));
        ClassExpr target = (ClassExpr) AmbiguousNameClassifier.reClassifyExpression(name, importManager, emptyContext);
        assertObjectInstanceOf(target, ClassExpr.class);
        assertObjectInstanceOf(target.getType(), ClassOrInterfaceType.class);
        assertEquals("java.lang.System", target.getType().asString());

        name = new AmbiguousNameExpr(Name.parse("java.lang"));
        target = (ClassExpr) AmbiguousNameClassifier.reClassifyExpression(name, importManager, emptyContext);
        assertObjectInstanceOf(target, ClassExpr.class);
        assertObjectInstanceOf(target.getType(), ClassOrInterfaceType.class);
        assertEquals("java.lang", target.getType().asString());
   }

    @Test
    public void testNameResolver_Name_Should_BeAFieldAccessToAClassExpr()
    {
        AmbiguousNameExpr name = new AmbiguousNameExpr(Name.parse("java.lang.System.out"));

        FieldAccessExpr fieldAccessExpr = (FieldAccessExpr) AmbiguousNameClassifier.reClassifyExpression(name, importManager, emptyContext);
        assertObjectInstanceOf(fieldAccessExpr, FieldAccessExpr.class);

        ClassExpr scopeExpr = (ClassExpr) fieldAccessExpr.getScope().get();
        assertEquals("java.lang.System", scopeExpr.getType().asString());
    }


    @Test
    public void testNameResolver_Name_Should_BeAFieldAccessToANameExpr()
    {
        emptyContext.openScope();

        emptyContext.storeVariable("java", new HJObject(null));

        AmbiguousNameExpr name = new AmbiguousNameExpr(Name.parse("java.lang.System.out"));

        FieldAccessExpr fieldAccessExpr = (FieldAccessExpr) AmbiguousNameClassifier.reClassifyExpression(name, importManager, emptyContext);
        assertEquals("out", fieldAccessExpr.getNameAsString());

        fieldAccessExpr = (FieldAccessExpr) fieldAccessExpr.getScope().get();
        assertEquals("System", fieldAccessExpr.getNameAsString());

        fieldAccessExpr = (FieldAccessExpr) fieldAccessExpr.getScope().get();
        assertEquals("lang", fieldAccessExpr.getNameAsString());

        NameExpr scopeExpr = (NameExpr) fieldAccessExpr.getScope().get();
        assertEquals("java", scopeExpr.getNameAsString());

        emptyContext.closeScope();
    }

    @Test
    public void testNameResolver_Name_Should_BeAFieldAccessToANameExprThatPointsToImportedField()
    {
        importManager.addImport(new ImportDeclaration(Name.parse("java.lang.System.out"), true, false));

        AmbiguousNameExpr name = new AmbiguousNameExpr(Name.parse("out.aField.aSecondField"));

        FieldAccessExpr fieldAccessExpr = (FieldAccessExpr) AmbiguousNameClassifier.reClassifyExpression(name, importManager, emptyContext);
        assertEquals("aSecondField", fieldAccessExpr.getNameAsString());

        fieldAccessExpr = (FieldAccessExpr) fieldAccessExpr.getScope().get();
        assertEquals("aField", fieldAccessExpr.getNameAsString());

        NameExpr scopeExpr = (NameExpr) fieldAccessExpr.getScope().get();
        assertEquals("out", scopeExpr.getNameAsString());
    }
}
