package ar.edu.unq.hydra.interpreter;

import ar.edu.unq.hydra.ast.ImportDeclaration;
import ar.edu.unq.hydra.ast.body.FunctionDeclaration;
import ar.edu.unq.hydra.ast.expr.*;
import ar.edu.unq.hydra.interpreter.context.Context;
import ar.edu.unq.hydra.interpreter.exceptions.HydraNotSuchVariableException;
import ar.edu.unq.hydra.interpreter.import_manager.ImportManager;
import ar.edu.unq.hydra.interpreter.values.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Array;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static ar.edu.unq.hydra.misc.Assert.assertObjectsClassExtends;
import static ar.edu.unq.hydra.parser.AstBuilderUtils.parseToAstNode;
import static org.junit.Assert.*;

public class ExpressionEvaluatorTest
{
    private ExpressionEvaluator expressionEvaluator;
    private Context emptyContext;
    private ImportManager importManager;
    @Before
    public void before() throws Exception
    {
        importManager = new ImportManager();
        this.expressionEvaluator = new ExpressionEvaluator(new StatementEvaluator(), importManager);
        this.emptyContext = new Context();
    }

    @After
    public void after() throws Exception
    {
    }

    // -- LITERAL EXPRESSIONS

    @Test
    public void testBooleanLiteralExpression_Should_BeAHBoolean() throws Exception
    {
        BooleanLiteralExpr expr = parseToAstNode("true", parser -> parser.literal());
        HObject result  = expressionEvaluator.eval(expr, emptyContext);

        assertObjectsClassExtends(result, HBoolean.class);
    }

    @Test
    public void testBooleanLiteralExpression_ResultValue_Should_BeCorrect() throws Exception
    {
        BooleanLiteralExpr expr = parseToAstNode("true", parser -> parser.literal());
        HObject result  = expressionEvaluator.eval(expr, emptyContext);
        assertEquals(true, result.getBooleanValue());
    }

    @Test
    public void testNullLiteralExpression() throws Exception
    {
        NullLiteralExpr expr = parseToAstNode("null", parser -> parser.literal());
        HObject result  = expressionEvaluator.eval(expr, emptyContext);

        assertNull(result.getValueAsObject());
    }

    @Test(expected = HydraNotSuchVariableException.class)
    public void testNameExpression_Name_Should_NotBeFound() throws Exception
    {
        NameExpr nameExpr = new NameExpr("variable");
        HObject result  = expressionEvaluator.eval(nameExpr, emptyContext);
    }

    @Test
    public void testNameExpression_Name_Should_BeFound() throws Exception
    {
        emptyContext.openScope();
        emptyContext.storeVariable("variable", new HJObject(null));
        NameExpr nameExpr = new NameExpr("variable");
        HObject result  = expressionEvaluator.eval(nameExpr, emptyContext);

        assertNotNull(result);
    }

    @Test(expected = ClassCastException.class)
    public void testVariableDeclarationExpr_WhenTypesMismatch_Should_ThrowClassCastExceptions() throws Exception
    {
        emptyContext.openScope();

        VariableDeclarationExpr expr = parseToAstNode("java.lang.Integer i = \"hola\"", parser -> parser.localVariableDeclaration());
        HObject result  = expressionEvaluator.eval(expr, emptyContext);
    }

    @Test
    public void testVariableDeclarationExpr_Should_CreateVariableInContext() throws Exception
    {
        emptyContext.openScope();

        VariableDeclarationExpr expr = parseToAstNode("java.lang.Integer i = 0", parser -> parser.localVariableDeclaration());
        HObject result  = expressionEvaluator.eval(expr, emptyContext);

        assertTrue(emptyContext.isStoredVariable("i"));
        assertEquals(0, result.getIntValue());
    }

    @Test
    public void testVariableDeclarationExpr_Should_CreateVariableInContext1() throws Exception
    {
        emptyContext.openScope();

        VariableDeclarationExpr expr = parseToAstNode("java.lang.Integer i", parser -> parser.localVariableDeclaration());
        HObject result  = expressionEvaluator.eval(expr, emptyContext);

        assertTrue(emptyContext.isStoredVariable("i"));
        assertEquals(result, emptyContext.getStoredVariable("i"));
        assertNull(result.getValueAsObject());
    }

    @Test
    public void testBinaryExpressionTypePromotion() throws Exception
    {
        BinaryExpr expr = parseToAstNode("1d+2", parser -> parser.expression());
        HObject result  = expressionEvaluator.eval(expr, emptyContext);

        assertObjectsClassExtends(result, HDouble.class);
    }


    @Test
    public void testAdditionBinaryExpression() throws Exception
    {
        BinaryExpr expr = (BinaryExpr) parseToAstNode("1+2", parser -> parser.expression());
        HObject result  = expressionEvaluator.eval(expr, emptyContext);

        assertObjectsClassExtends(result, HInt.class);
        assertEquals(result.getIntValue(),3);
    }

    @Test
    public void testProductBinaryExpression() throws Exception
    {
        BinaryExpr expr = (BinaryExpr) parseToAstNode("1*2", parser -> parser.expression());
        HObject result  = expressionEvaluator.eval(expr, emptyContext);

        assertObjectsClassExtends(result, HInt.class);
        assertEquals(result.getIntValue(),2);
    }

    @Test
    public void testSubstractBinaryExpression() throws Exception
    {
        BinaryExpr expr = (BinaryExpr) parseToAstNode("1-2", parser -> parser.expression());
        HObject result  = expressionEvaluator.eval(expr, emptyContext);

        assertObjectsClassExtends(result, HInt.class);
        assertEquals(result.getIntValue(),-1);
    }

    @Test
    public void testDivideBinaryExpression() throws Exception
    {
        BinaryExpr expr = (BinaryExpr) parseToAstNode("2/2", parser -> parser.expression());
        HObject result  = expressionEvaluator.eval(expr, emptyContext);

        assertObjectsClassExtends(result, HInt.class);
        assertEquals(result.getIntValue(),1);
    }

    @Test
    public void testSimpleAssignExpression_Should_StoreVariableInContext() throws Exception
    {
        AssignExpr expr = (AssignExpr) parseToAstNode("name=1", parser -> parser.expression());

        emptyContext.openScope();
        expressionEvaluator.eval(expr, emptyContext);

        assertTrue(emptyContext.isStoredVariable("name"));
    }

    @Test
    public void testSimpleAssignExpression_Should_StoreCorrectEvaluatedValueInContext() throws Exception
    {
        emptyContext.openScope();
        AssignExpr expr = (AssignExpr) parseToAstNode("name=1+3", parser -> parser.expression());
        expressionEvaluator.eval(expr, emptyContext);
        HObject result = emptyContext.getStoredVariable("name");

        assertEquals(result.getIntValue(), 4);
    }

    @Test
    public void testCompoundPlusAssignExpression_Should_() throws Exception
    {
        emptyContext.openScope();
        emptyContext.storeVariable("name", new HInt(0));
        AssignExpr expr = parseToAstNode("name-=3", parser -> parser.expression());
        expressionEvaluator.eval(expr, emptyContext);
        HObject result = emptyContext.getStoredVariable("name");

        assertEquals(-3, result.getIntValue());
    }

    @Test
    public void tesClassExpr_Should_NewHJObjectContainingAnIntegerClass()
    {
        ClassExpr oc = parseToAstNode("java.lang.Integer.class", parser -> parser.expression());

        HObject clazz =  expressionEvaluator.eval(oc, emptyContext);
        assertObjectInstanceOf(clazz, HJObject.class);
        assertObjectInstanceOf(clazz.getValueAsObject(), Class.class);
        assertEquals(Integer.class, clazz.getValueAsObject());
    }

    @Test
    public void testObjectCreationExpr_When_FullyQualifiedNameGiven_Should_ReturnNewHJObject()
    {
        ObjectCreationExpr oc = parseToAstNode("new java.lang.Integer(1)", parser -> parser.unqualifiedClassInstanceCreationExpression());

        HObject one =  expressionEvaluator.eval(oc, emptyContext);
        assertObjectInstanceOf(one, HJObject.class);
    }

    @Test
    public void testObjectCreationExpr_When_FullyQualifiedNameGiven_Result_Should_HoldCorrectValue()
    {
        ObjectCreationExpr oc = parseToAstNode("new java.lang.Integer(1)", parser -> parser.unqualifiedClassInstanceCreationExpression());

        HObject one =  expressionEvaluator.eval(oc, emptyContext);

        assertObjectInstanceOf(one.getValueAsObject(), Integer.class);
        assertEquals(1,one.getValueAsObject());
    }

    @Test
    public void testObjectCreationExpr_When_UnQualifiedNameGiven_And_NameInImports_Result_Should_ReturnNewHJObject()
    {

        importManager.addImport(new ImportDeclaration(Name.parse("java.lang.Integer"), false, false));

        ObjectCreationExpr oc = parseToAstNode("new Integer(1)", parser -> parser.unqualifiedClassInstanceCreationExpression());
        HObject one =  expressionEvaluator.eval(oc, emptyContext);

        assertObjectInstanceOf(one, HJObject.class);
    }

    @Test
    public void testObjectCreationExpr_When_UnQualifiedNameGiven_And_NameInImports_Result_Should_HoldCorrectValue()
    {
        importManager.addImport(new ImportDeclaration(Name.parse("java.lang.Integer"), false, false));

        ObjectCreationExpr oc = parseToAstNode("new Integer(1)", parser -> parser.unqualifiedClassInstanceCreationExpression());

        HObject one =  expressionEvaluator.eval(oc, emptyContext);
        assertObjectInstanceOf(one.getValueAsObject(), Integer.class);
        assertEquals(1,one.getValueAsObject());
    }

    @Test
    public void testFunctionCallExpr()
    {
        emptyContext.openScope();

        String sourceCodeBit = "succ(n) { return n+1; }";
        FunctionDeclaration fd = parseToAstNode(sourceCodeBit, parser -> parser.functionDeclaration());
        emptyContext.addFunctionDeclaration(fd);

        sourceCodeBit = "r = 5" ;
        Expression expr = parseToAstNode(sourceCodeBit, parser -> parser.expression());
        expressionEvaluator.eval(expr, emptyContext);

        sourceCodeBit = "r = succ(succ(succ(r)))";
        expr = parseToAstNode(sourceCodeBit, parser -> parser.expression());
        expressionEvaluator.eval(expr, emptyContext);

        HObject r = emptyContext.getStoredVariable("r");
        assertEquals(8, r.getValueAsObject());
    }

    @Test(expected = ClassCastException.class)
    public void testCastExpr()
    {
        String sourceCodeBit = "(long)1";
        Expression mc = parseToAstNode(sourceCodeBit, parser -> parser.expression());
        HObject r = expressionEvaluator.eval(mc, emptyContext);

        assertEquals(r.getClass(), HLong.class);

        sourceCodeBit = "(java.lang.Long)1";
        mc = parseToAstNode(sourceCodeBit, parser -> parser.expression());
        expressionEvaluator.eval(mc, emptyContext);
    }


    @Test
    public void testMethodCallExpr()
    {
        emptyContext.openScope();

        importManager.addImport(new ImportDeclaration(Name.parse("java.lang.System.out"), true, false));

        String sourceCodeBit = "out.println(\"Hello world!\")";
        Expression mc = parseToAstNode(sourceCodeBit, parser -> parser.expression());

        expressionEvaluator.eval(mc, emptyContext);

    }

    @Test
    public void testMethodCallExpr_CalledWithFullyQualifiedName()
    {
        String sourceCodeBit = "java.lang.System.out.println(\"Hello world!\")";
        Expression mc = parseToAstNode(sourceCodeBit, parser -> parser.expression());

        expressionEvaluator.eval(mc, emptyContext);

        sourceCodeBit = "java.lang.System.out.println(java.util.Objects.isNull(new java.lang.Integer(1+1)))";
        mc = parseToAstNode(sourceCodeBit, parser -> parser.expression());

        expressionEvaluator.eval(mc, emptyContext);
    }

    @Test
    public void testParsedArrayCreationExpr_Should_BeAnEmpty1Dimension2ElementsArray()
    {
        String sourceCodeBit = "new int[2]";
        Expression mc = parseToAstNode(sourceCodeBit, parser -> parser.arrayCreationExpression());
        HObject o = expressionEvaluator.eval(mc, emptyContext);
        assertObjectInstanceOf(o.getValueAsObject(), int[].class);
        assertEquals(2,((int[])o.getValueAsObject()).length);
    }

    @Test
    public void testParsedArrayCreationExpr_Should_BeAnEmpty2Dimension2ElementsArray()
    {
        String sourceCodeBit = "new int[3][2]";
        Expression mc = parseToAstNode(sourceCodeBit, parser -> parser.arrayCreationExpression());
        HObject o = expressionEvaluator.eval(mc, emptyContext);

        assertObjectInstanceOf(o.getValueAsObject(), int[][].class);
        assertEquals(3,((int[][])o.getValueAsObject()).length);

        Object secondDim = Array.get(o.getValueAsObject(), 0);
        assertEquals(2,((int[])secondDim).length);
    }

    @Test
    public void testParsedArrayCreationExpr_Should_BeAnInitialized1Dimension2ElementsArray_And_BeCorrectlyInitialized()
    {
        String sourceCodeBit = "new int[] {1,2}";
        Expression mc = parseToAstNode(sourceCodeBit, parser -> parser.arrayCreationExpression());
        HObject o = expressionEvaluator.eval(mc, emptyContext);

        int value = (int) Array.get(o.getValueAsObject(),0);
        assertEquals(1,value);

        value = (int) Array.get(o.getValueAsObject(),1);
        assertEquals(2,value);
    }

    @Test
    public void testParsedArrayCreationExpr_Should_BeAMultidimensionArray_And_BeCorrectlyInitialized()
    {
        String sourceCodeBit = "new int[][] {{1},{2}}";
        Expression mc = parseToAstNode(sourceCodeBit, parser -> parser.arrayCreationExpression());
        HObject o = expressionEvaluator.eval(mc, emptyContext);

        Object value = Array.get(o.getValueAsObject(),0);
        assertEquals(1,Array.get(value,0));

        value = Array.get(o.getValueAsObject(),1);
        assertEquals(2,Array.get(value,0));

        //-----------------------------------------------------------------------------------------

        sourceCodeBit = "new int[][][] {{{1}},{{2}}}";
        mc = parseToAstNode(sourceCodeBit, parser -> parser.arrayCreationExpression());
        o = expressionEvaluator.eval(mc, emptyContext);

        value = Array.get(o.getValueAsObject(),0);
        Object actual = Array.get(value, 0);
        assertEquals(1, Array.get(actual,0));

        value = Array.get(o.getValueAsObject(),1);
        actual = Array.get(value,0);
        assertEquals(2,Array.get(actual,0));
    }


}

