package ar.edu.unq.hydra.interpreter;

import ar.edu.unq.hydra.ast.ImportDeclaration;
import ar.edu.unq.hydra.ast.expr.Name;
import ar.edu.unq.hydra.interpreter.import_manager.ImportManager;
import ar.edu.unq.hydra.interpreter.import_manager.StaticMethod;
import ar.edu.unq.hydra.misc.temp.Estatica;
import ar.edu.unq.hydra.interpreter.import_manager.StaticField;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static ar.edu.unq.hydra.misc.Assert.assertObjectInstanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ImportManagerTest
{
    private ImportManager importManager ;

    @Before
    public void before() throws Exception
    {
        importManager = new ImportManager();
    }

    @After
    public void after() throws Exception
    {
    }

    @Test
    public void testImportManager_Should_ImportASingleClass()
    {
        Name name = Name.parse("ar.edu.unq.hydra.misc.temp.Estatica");
        ImportDeclaration id = new ImportDeclaration(name, false, false);
        importManager.addImport(id);

        assertTrue(importManager.isImport("Estatica"));
        assertEquals(Estatica.class, importManager.getImportedType("Estatica"));
    }

    @Test
    public void testImportManager_Should_StaticallyImportAMemberClass()
    {
        Name name = Name.parse("ar.edu.unq.hydra.misc.temp.Estatica.SubEstatica");
        ImportDeclaration id = new ImportDeclaration(name, true, false);
        importManager.addImport(id);

        assertTrue(importManager.isImport("SubEstatica"));
        assertEquals(Estatica.SubEstatica.class, importManager.getImportedType("SubEstatica"));
    }

    @Test
    public void testImportManager_Should_ImportAStaticField()
    {
        Name name = Name.parse("ar.edu.unq.hydra.misc.temp.Estatica.i");
        ImportDeclaration id = new ImportDeclaration(name, true, false);
        importManager.addImport(id);

        assertTrue(importManager.isImport("i"));
        StaticField field = importManager.getImportedField("i");
        assertEquals(0, field.get());
    }

    @Test
    public void testImportManager_Should_ImportAStaticMethod()
    {
        Name name = Name.parse("ar.edu.unq.hydra.misc.temp.Estatica.metodo");
        ImportDeclaration id = new ImportDeclaration(name, true, false);
        importManager.addImport(id);

        assertTrue(importManager.isImport("metodo"));
    }

    @Test
    public void testImportManager_Should_ImportAllStaticMembers()
    {
        Name name = Name.parse("ar.edu.unq.hydra.misc.temp.Estatica");
        ImportDeclaration id = new ImportDeclaration(name, true, true);
        importManager.addImport(id);

        assertTrue(importManager.isImport("SubEstatica"));
        assertEquals(Estatica.SubEstatica.class, importManager.getImportedType("SubEstatica"));

        assertTrue(importManager.isImport("i"));
        assertObjectInstanceOf(importManager.getImportedField("i"), StaticField.class);

        assertTrue(importManager.isImport("metodo"));
        assertObjectInstanceOf(importManager.getImportedMethod("metodo"), StaticMethod.class);

        assertFalse(importManager.isImport("SubNoEstatica"));
        assertFalse(importManager.isImport("e"));
    }

    @Test
    public void testImportManager_Should_ImportAllNonStaticTypes()
    {
        Name name = Name.parse("ar.edu.unq.hydra.misc.temp.Estatica");
        ImportDeclaration id = new ImportDeclaration(name, false, true);
        importManager.addImport(id);

        assertTrue(importManager.isImport("SubNoEstatica"));
        assertEquals(Estatica.SubNoEstatica.class, importManager.getImportedType("SubNoEstatica"));
    }
}
