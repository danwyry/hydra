package ar.edu.unq.hydra.interpreter;

import ar.edu.unq.hydra.ast.ImportDeclaration;
import ar.edu.unq.hydra.ast.NodeList;
import ar.edu.unq.hydra.ast.expr.Name;
import ar.edu.unq.hydra.ast.type.ArrayType;
import ar.edu.unq.hydra.ast.type.ClassOrInterfaceType;
import ar.edu.unq.hydra.ast.type.PrimitiveType;
import ar.edu.unq.hydra.interpreter.import_manager.ImportManager;
import ar.edu.unq.hydra.utils.exceptions.ReflectionUtilsException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static ar.edu.unq.hydra.parser.AstBuilderUtils.parseToAstNode;
import static org.junit.Assert.assertEquals;

public class TypeMapperTest
{
    private TypeMapper typeMapper;
    private NodeList<ImportDeclaration> importDeclarations;
    private ImportManager importManager;

    @Before
    public void before() throws Exception
    {
        typeMapper = TypeMapper.getInstance();
        importDeclarations = new NodeList<>();
        importManager = new ImportManager();
    }

    @After
    public void after() throws Exception
    {
    }

    @Test
    public void testPrimitiveByteType()
    {
        PrimitiveType type = parseToAstNode("byte", parser -> parser.primitiveType());

        Class c = typeMapper.map(type, importManager);
        assertEquals(c, byte.class);
    }

    @Test
    public void testPrimitiveCharType()
    {
        PrimitiveType type = parseToAstNode("char", parser -> parser.primitiveType());

        Class c = typeMapper.map(type, importManager);
        assertEquals(c, char.class);
    }

    @Test
    public void testPrimitiveShortType()
    {
        PrimitiveType type = parseToAstNode("short", parser -> parser.primitiveType());

        Class c = typeMapper.map(type, importManager);
        assertEquals(c, short.class);
    }

    @Test
    public void testPrimitiveIntType()
    {
        PrimitiveType type = parseToAstNode("int", parser -> parser.primitiveType());

        Class c = typeMapper.map(type, importManager);
        assertEquals(c, int.class);
    }

    @Test
    public void testPrimitiveLongType()
    {
        PrimitiveType type = parseToAstNode("long", parser -> parser.primitiveType());

        Class c = typeMapper.map(type, importManager);
        assertEquals(c, long.class);
    }

    @Test
    public void testPrimitiveDoubleType()
    {
        PrimitiveType type = parseToAstNode("double", parser -> parser.primitiveType());

        Class c = typeMapper.map(type, importManager);
        assertEquals(c, double.class);
    }

    @Test
    public void testClassOrInterfaceTypeWithFullyQualifiedName()
    {
        ClassOrInterfaceType type = parseToAstNode("java.lang.Integer", parser -> parser.classOrInterfaceType());

        Class c = typeMapper.map(type, importManager);
        assertEquals(c, Integer.class);
    }

    @Test(expected = ReflectionUtilsException.class)
    public void testClassOrInterfaceTypeWithSimpleNameShouldFailWithoutImportDeclaration()
    {
        ClassOrInterfaceType type = parseToAstNode("Integer", parser -> parser.classOrInterfaceType());

        Class c = typeMapper.map(type, importManager);
        assertEquals(c, Integer.class);
    }

    @Test
    public void testClassOrInterfaceTypeWithSimpleName()
    {
        Name fullyQualifiedName = Name.parse("java.lang.Integer");
        ImportDeclaration importDeclaration = new ImportDeclaration(fullyQualifiedName, false, false);
        importManager.addImport(importDeclaration);

        ClassOrInterfaceType type = parseToAstNode("Integer", parser -> parser.classOrInterfaceType());

        Class c = typeMapper.map(type, importManager);
        assertEquals(c, Integer.class);
    }

    @Test
    public void testArrayType()
    {
        ArrayType type = parseToAstNode("int[][]", parser -> parser.arrayType());

        Class c = typeMapper.map(type, importManager);
        assertEquals(c, int[][].class);
    }


}
