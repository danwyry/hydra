package ar.edu.unq.hydra.misc;

import ar.edu.unq.hydra.ast.Node;
import ar.edu.unq.hydra.misc.temp.A;
import ar.edu.unq.hydra.misc.temp.D;
import ar.edu.unq.hydra.misc.temp.E;
import edu.emory.mathcs.backport.java.util.Arrays;
import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.*;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.stream.Stream;

import static ar.edu.unq.hydra.interpreter.StaticCommands.*;
import static java.lang.System.out;
import static java.util.Objects.isNull;
import static org.junit.Assert.*;
/**
* HydraAstBuilderVisitor Tester.
* 
* @author <Authors name> 
* @since <pre>Sep 16, 2016</pre> 
* @version 1.0 
*/ 
public class MiscTest
{
    class AAA {
        void accept(final Prueba p) {
            p.p(this);
        }
    }
    class BBB extends AAA {
        public class CCC {}
        void accept(final Prueba p) {
            p.p(this);
        }

    }

    class Prueba
    {
        public void p(AAA a)
        {
            System.out.println("Entro por AAA" + a.getClass());
        }
        public void p(BBB a)
        {
            System.out.println("Entro por BBB " + a.getClass());
        }

        public void vis(AAA a)
        {
            a.accept(this);
        }

    }
    @Before
    public void before() throws Exception
    {
    }

    @After
    public void after() throws Exception
    {
    }

    @Test
    public void testAssignments()
    {
        class B { public Integer num ; } ;
        class A { protected B b = new B() ; public B getB() { return b; } }
        A a = new A();
        a.getB().num = 1;



    }

    @Test
    public void testIteradorDeque()
    {
        Deque<String> q = new ArrayDeque();
        q.addFirst("1");
        q.addFirst("2");
        Iterator<String> iterator = q.descendingIterator();
        while (iterator.hasNext())
            java.lang.System.out.println(iterator.next());
//        for (String s : q.)
//        {
//
//            java.lang.System.out.println(s);
//
//        }

    }

    @Test
    public void testInicializacionObjetosNumericos()
    {
        Integer i  = null ;
        assertTrue(isNull(i));
    }

    @Test
    public void testPolimorfismo()
    {
        Prueba p = new Prueba();
        AAA a = new AAA();
        AAA b = (BBB)new BBB();
        p.vis(a);
        p.vis(b);
    }

    @Test
    public void testExpresionesNumericasEnCondicional()
    {
        float x = (0x000F | 2);

    }

    @Test
    public void testBinOpPrimNoPrimReturnsPrim()
    {
        int a = 1 ;
        Integer b = 2 ;

        out.println(a+b);
    }

    @Test
    public void testQueseyo()
    {
        Integer val = Integer.decode("1000000000");
        assertEquals((Integer)1000000000,val);
    }



    @Test
    public void testNumberAsVariableType()
    {
        double  a = 1d ;
        double b = 1d;
        //b = b + 1d;
        int c = (int) (a+b);
        out.println(   );

    }


    @Test
    public void testArraysPrimitivos()
    {
        Class c = double.class;
        Object o = Array.newInstance(c, 0, 0, 0);
        Class intArrayClass = o.getClass();
        double[][][] err = { };

        java.lang.System.out.println(c.getMethods().length);

        for (Method m : c.getMethods())
            java.lang.System.out.println(m.getName());

        java.lang.System.out.println(err.getClass().toString());
        java.lang.System.out.println(intArrayClass.toString());
    }

    @Test
    public void reflectConstructors()
    {
        Constructor<?>[] constructors = null;
        constructors = AAA.class.getDeclaredConstructors();

        for (Constructor<?> c : constructors)
        {
            Parameter[] parameters = c.getParameters();
            if (parameters.length > 0 )
            {
                Parameter parameter = parameters[0];
                if ( parameter.getType().isAssignableFrom(Node.class) )
                    java.lang.System.out.println(parameter.getType().getName() + " " + parameter.getName() );
            }
        }
        java.lang.System.out.println("constructores  : " ) ;
    }

    @Test
    public void testGetConstructorResolution2()
    {
        Class[] paramTypes = { D.class , E.class } ;
        Object[] arguments = { new D(), new E() };

        try {

            A a = ConstructorUtils.invokeConstructor(A.class , arguments);
            assertNotNull(a);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testArrays()
    {
        int i = 0 ;
        int[] a = new int[] { i++ , i++ };
        assertTrue(true);
    }

    @Test
    public void testLambdas()
    {
        Stream stream = Arrays.asList(new Integer[]{1, 2, 3}).stream().map(i -> (Integer) i * 2);
        System.out.print(stream.collect(java.util.stream.Collectors.toList()));
    }

    @Test
    public void testL1()
    {
        Integer i = 0 ;

        switch (i )
        {
            case 0:
                println("0");
            case 1:
                println("1");
            case 2:
                println("2");
                break ;
            default:
        }
    }



}

