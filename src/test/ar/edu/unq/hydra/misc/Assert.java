package ar.edu.unq.hydra.misc;
import static org.junit.Assert.fail;
/**
 * Created by Daniel Wyrytowski on 1/7/17.
 */
public class Assert
{
    public static void assertObjectInstanceOf(Object target, Class type)
    {
        if ( target.getClass() != type )
            fail("Expected " + target.getClass().toString() + " to be of type " + type.toString() );
    }

    public static void assertObjectsClassExtends(Object target, Class predecesor)
    {
        if (! predecesor.isAssignableFrom(target.getClass()))
            fail("Expected " + target.getClass().toString() + " to inherit from " + predecesor.toString() );
    }

}
