ME QUEDÉ EN: parseo de ArrayAccessLfnoPrimaryLfno


Not supported java features:

    - class declarations
    - annonymous classes



CASOS JODDIOS PARA TESTEAR:
*  (p)+x --> dependiendo de qué sea p es el tipo de operación: podría ser un cast o podría ser una operación aritmética. (lo mismo para la resta)
            para solucionar esto habría que hacer análisis sintáctico del ast para ver si p es un tipo, una variable, etc.


PROBLEMAS/CUESTIONES A MEJORAR:

    La función siguiente no debería buscar parentNode tipo CompilationUnit sino HydraScript:

        public void tryAddImportToParentCompilationUnit(Class<?> clazz) {
            CompilationUnit parentNode = getParentNodeOfType(CompilationUnit.class);
            if (parentNode != null) {
                parentNode.addImport(clazz);
            }
        }

    Debería ser :

        public void tryAddImportToParentHydraScript(Class<?> clazz) {
             CompilationUnit parentNode = getParentNodeOfType(HydraScript.class);
             if (parentNode != null) {
                 parentNode.addImport(clazz);
             }
        }


SOBRE LAS DECISIONES DE DISEÑO:

- en el intérprete los "types" se crearon para solucionar el problema de representar valores en el stack que no son objetos
 (léase primitivos y void)

- Dado que hydra es un lenguaje de tipado gradual algunas producciones gramaticales pueden resultar ambiguas.
  + Los typeName, packageOrTypeName , packageName y expressionName no siempre pueden determinarse en el momento del parseo.
  + Las invocaciones a static methods importados y las invocaciones a funciones también resultan ambiguas
  + Las declaraciones de variables y asignaciones también lo son cuando no se declara su tipo explícitamente

  Algunas de estas ambiguedades se resuelven en tiempo de parseo y otras en tiempo de ejecución . (extender)


- refactors constantes sobre construccion de nodos que se crean a partir de diferentes producciones de la gramática.
  a veces la creación del mismo nodo desde contextos difernetes terminaba generando inconsistencias que se reflejaban en
  la ejecución dependiendo del código ejecutado.

